% Shear test of Periodic Boundary Conditions
% Domain: 4x4 rectangle block
% Loading: Prescribed shear displacement.
%
% Last revision: 12/07/2018 TJT

clear
% clc

nen = 4;
nel = 4;

numc = 4;2;1;
xl = [1 0 0
      2 numc 0
      4 0 numc
      3 numc numc];
type = 'cart';
rinc = numc;
sinc = 5;
node1 = 1;
elmt1 = 1;
mat = 1;
rskip = 0;
btype = 0;
[Coordinates,NodesOnElement,RegionOnElement,numnp,numel] = block2d(type,rinc,sinc,node1,elmt1,mat,rskip,btype,xl,nen);
Coordinates = Coordinates';
NodesOnElement = NodesOnElement';

nummat = 1;
MatTypeTable = [1
                1];
MateT = [100 0.25 1];

% Generate MPCList for Lagrange multiplier pairs on block boundaries
[MPCList,numMPC,NodeBC,numBC] = block2dMPC(rinc,sinc);

CornerXYZ = [numc 0.000000
             0.000000 numc];
[Coordinates,NodesOnElement,RegionOnElement,MatTypeTable,MateT,...
    numnp,numel,nummat,nen,NodeTypeNum] = AddPBNodes(MPCList,CornerXYZ,Coordinates,...
    NodesOnElement,RegionOnElement,MatTypeTable,MateT,numnp,numel,nummat,nen);

% Applied displacements instead of forces
NodeLoad2 = [NodeTypeNum(2) 1 0.01
            NodeTypeNum(2)+1 1 0
            NodeTypeNum(2)+1 2 0];
NodeBC = [NodeBC; NodeLoad2];
numBC = length(NodeBC);
% % Nodal forces allow rotation of cube; use RIGHT node to prevent
% NodeLoad = [NodeTypeNum(2) 1 1
%             NodeTypeNum(2) 2 0
%             NodeTypeNum(2)+1 1 0
%             NodeTypeNum(2)+1 2 1];
% numNodalF = length(NodeLoad);


% Output quantity flags
DHist = 1;
FHist = 1;
SHist = 1;
SEHist = 1;


ProbType = [numnp numel nummat 2 2 nen];