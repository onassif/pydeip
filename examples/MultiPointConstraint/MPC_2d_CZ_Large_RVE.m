% Shear test of MP Periodic Boundary Conditions with CZ couplers
% Domain: 2X2 square block with hexagonal grains with 12 regions 
% Loading: Prescribed shear displacement.
%
% Last revision: 09/18/2018 Sunday Aduloju


clear
% clc
meshref=4; %permits 8 levels of mesh refinement

nen = 3;
nel = 3;
L=1;
H=2;
numc = 12;2;1;
m1=numc;
n1=numc*2;
xl = [1 0 0
      2 L 0
      4 0 H
      3 L H];
type = 'cart';
rinc = m1*meshref;
sinc = n1*meshref;
node1 = 1;
elmt1 = 1;
mat = 1;
rskip = 0;

if meshref==1
    btype = 6;
else
    btype = 5;
end

[Coordinates,NodesOnElement,~,numnp,numel] = block2d(type,rinc,sinc,node1,elmt1,mat,rskip,btype,xl,nen);
Coordinates = Coordinates';
NodesOnElement = NodesOnElement';
numnp1=numnp;numel1=numel; Coordinates1 =Coordinates;NodesOnElement1=NodesOnElement;  

% Modify regions
%   RegionOnElement = 1* ones(numel,1);
%% Define Hexagonal pattern of regions
StepRight1=5;
StepRight2=StepRight1+2; %7
numelemY=sinc*2; %num  of triang. element along y dir.
numelemX=rinc*2; %num  of triang. element along x dir.

Tableref1 =[ 0 0 
             0 0];
Tableref2 =[ 1  -1 -1  1 
             -1  1  1 -1];
Tableref4=[  3  1  -1 -3 -3 -1  1  3
            -3 -1  1  3  3  1 -1 -3];
Tableref8=[ 7  5  3  1 -1 -3 -5 -7 -7 -5 -3 -1  1  3  5  7
           -7 -5 -3 -1  1  3  5  7  7  5  3  1 -1 -3 -5 -7];
 if meshref==1
     Tableref=Tableref1;
 elseif meshref==2
      Tableref=Tableref2;
 elseif meshref==4
      Tableref=Tableref4;
  elseif meshref==8
      Tableref=Tableref8;
 else
 end
 
%% just make hexagons with regions consisting of 2 regions only
counterStep=1;
TransY= round(numelemY/(4*meshref));
for i= 1: TransY
       
    for k= 1:2*meshref 
       Row_r=(i-1)*(meshref*2)+k;
    if mod(counterStep,2)==1 % odd
            ElementstoRight=StepRight1 * meshref + Tableref(1,k);
            for j= 1: ElementstoRight
                RegionMat(Row_r,j)=1;
            end 
            for j= ElementstoRight+1 : (numelemX-ElementstoRight)
                RegionMat(Row_r,j)=2;
            end 
            for j=(numelemX-ElementstoRight)+1 : numelemX
             RegionMat(Row_r,j)=1;         
            end            
    else 
            ElementstoRight = StepRight2 * meshref + Tableref(2,k);
             for j= 1: ElementstoRight
                RegionMat(Row_r,j)=1;
            end 
            for j= ElementstoRight+1 : (numelemX-ElementstoRight)
                RegionMat(Row_r,j)=2;
            end 
            for j=(numelemX-ElementstoRight)+1 : numelemX
             RegionMat(Row_r,j)=1;         
            end                     
    end           
    end 
    counterStep=counterStep+1;
end

% RegionMat= RegionMat';
% RegionOnElement=reshape(RegionMat,[],1);

%% Extend the domain to each hegaxon has a region 
%middle
stepmat2=3*meshref+1;
kountmat=3;
controws=0;
for p= stepmat2:numelemY/2
    for j=1: numelemX
      if RegionMat(p,j)==2
           RegionMat(p,j)=kountmat;
      end
    end
       controws=controws+1;
      if controws==4*meshref;
          kountmat=kountmat+1;
          controws=0;
      end
end 
% Left
stepmat2=1*meshref+1; 
 kountmat=kountmat+1; % continue from the  kountmat
controws=0;
for p= stepmat2:numelemY/2
    for j=1: numelemX/2
      if RegionMat(p,j)==1
           RegionMat(p,j)=kountmat;
      end
    end
       controws=controws+1;
      if controws==4*meshref
          kountmat=kountmat+1;
          controws=0;
      end
end
kountmatLeft=kountmat;
%Right
stepmat2=1*meshref+1; 
 kountmat=kountmat+1; % continue from the  kountmat
controws=0;
for p= stepmat2:numelemY/2
    for j=(numelemX/2)+1: numelemX
      if RegionMat(p,j)==1
           RegionMat(p,j)=kountmat;
      end
    end
       controws=controws+1;
      if controws==4*meshref
          kountmat=kountmat+1;
          controws=0;
      end
end
%Left lower edge
stepmat2=1;
kountmat=kountmat+1; % continue from the  kountmat
controws=0;
for p= stepmat2:meshref*1
    for j=(numelemX/2)+1: numelemX;
      if RegionMat(p,j)==1
           RegionMat(p,j)=kountmat;
      end
    end
end
% RegionMat= RegionMat';
% RegionOnElement=reshape(RegionMat,[],1);

%% make second copy to extend the rectangular domain to a square of twice
%  rectangle's size
 xl = [1 L 0
      2 2*L 0
      4 L H
      3 2*L H];
type = 'cart';
rinc = m1*meshref;
sinc = n1*meshref;
node1 = numnp+1;
elmt1 = numel+1;
mat = 1;
rskip = 0;

if meshref==1
    btype = 6;
else
    btype = 5;
end
 x= Coordinates;
 ix= NodesOnElement;
 RonE= ones(numel,1);

if meshref==1
    btype = 6;
else
    btype = 5;
end

[Coordinates,NodesOnElement,~,numnp,numel] = block2d(type,rinc,sinc,node1,elmt1,mat,rskip,btype,xl,nen);
Coordinates = Coordinates';
NodesOnElement = NodesOnElement';
numnp2=numnp;numel2=numel; Coordinates2 =Coordinates;NodesOnElement2=NodesOnElement; 
Coordinates(1:numnp1,:) = Coordinates1(1:numnp1,:);
NodesOnElement(1:numel1,:) = NodesOnElement1(1:numel1,:) ;


%% Final arrangement of regionOnElement
RegionMat= RegionMat';
% RegionMat2 = fliplr(RegionMat);
RegionMat2 = flipud(RegionMat);
RegionMat= [RegionMat RegionMat2];

for i= 1: numelemX
    for  j= numelemY/2+1: numelemY 
        
        if  RegionMat(i,j) <= kountmatLeft
        RegionMat(i,j)= RegionMat(i,j)+ kountmat;
        end
        
        
    end
end
RegionOnElement=reshape(RegionMat,[],1);

nodeRemo = find(abs(Coordinates1(:,1)-L)<1e-9);
nodeRepl = find(abs(Coordinates2(:,1)-L)<1e-9);
for k= 1: size(nodeRemo,1)
for i=1:  size(NodesOnElement,2)
    for j= 1:size(NodesOnElement,1)    
        NodesOnElement(NodesOnElement==nodeRemo (k)) = nodeRepl(k);
     end
end
end

%%  material properties  
MatTypeTable(1,:) = 1:35;   
MatTypeTable(2,:) = ones(1,35);
     
nummat=35;
MateT=zeros(nummat,3);
% The same materials 
 for i=1:1:nummat
      MateT(i,1:3) = [100 0.25 1 ];     
end
%%     
     
%% Different materials
% 
% E=100;
% for i=1:1:nummat
%     MateT(i,1) = E+(i-3)^2*10
%     MateT(i,2:3) = [0.25 1 ];     
% end
% %% 
 
%% Different materials
% E=100;
% v=0.25
% for i=1:1:nummat
%     MateT(i,1) = E+(i-1)^2*10
%     MateT(i,2) = 0.37-(i)*0.016
%     MateT(i,3) = 1;     
% end
% %% %             
% %%           
%             
            
           

% Boundary conditions
NodeBC = [1 1 0
          1 2 0];
numBC = size(NodeBC,1);

% Create periodic boundary conditions
Left= find(abs(Coordinates(:,1))<1e-9);
Right= find(abs(Coordinates(:,1)-2*L)<1e-9);
Bottom= find(abs(Coordinates(:,2))<1e-9);
Top= find(abs(Coordinates(:,2)-H)<1e-9);
Top(end)=[]; Bottom(end)=[];
PBCListx=[ Right  Left];
PBCListy=[Top Bottom];
MPCListx = [ PBCListx ones(size(PBCListx,1),1)*[1,0]];
MPCListy = [ PBCListy ones(size(PBCListy,1),1)*[0,1]];
MPCList = [MPCListx; MPCListy];

% Output quantity flags
DHist = 1;
FHist = 1;
SHist = 1;
SEHist = 1;


% Generate CZM interface: pairs of elements and faces, duplicate the nodes,
% update the connectivities
numnpCG = numnp;
usePBC = 2; % flag to turn on keeping PBC
numMPC = length(MPCList);

InterTypes= zeros(35);
InterTypes(2,1)=1; InterTypes(3,2)=1; InterTypes(4,3)=1; InterTypes(5,4)=1;
InterTypes(6,5)=1; InterTypes(7,6)=1; InterTypes(8,7)=1; InterTypes(9,1)=1;
InterTypes(9,2)=1; InterTypes(9,3)=1; InterTypes(10,3)=1; InterTypes(10,4)=1;
InterTypes(10,9)=1;InterTypes(11,4)=1; InterTypes(11,5)=1; InterTypes(11,10)=1;
InterTypes(12,5)=1; InterTypes(12,6)=1; InterTypes(12,11)=1; InterTypes(13,6)=1;
InterTypes(13,7)=1; InterTypes(13,12)=1; InterTypes(14,7)=1; InterTypes(14,8)=1;
InterTypes(14,13)=1; InterTypes(15,2)=1; InterTypes(15,3)=1; InterTypes(16,3)=1;
InterTypes(16,4)=1; InterTypes(16,15)=1; InterTypes(17,4)=1; InterTypes(17,5)=1;
InterTypes(17,16)=1;InterTypes(18,5)=1; InterTypes(18,6)=1; InterTypes(18,17)=1;
InterTypes(19,6)=1; InterTypes(19,7)=1; InterTypes(19,18)=1; InterTypes(20,7)=1;
InterTypes(20,8)=1; InterTypes(20,19)=1; InterTypes(21,2)=1; InterTypes(21,15)=1;
InterTypes(23,15)=1; InterTypes(23,21)=1; InterTypes(23,22)=1; InterTypes(24,15)=1;
InterTypes(24,16)=1; InterTypes(24,23)=1; InterTypes(25,16)=1; InterTypes(25,17)=1;
InterTypes(25,24)=1; InterTypes(26,17)=1; InterTypes(26,18)=1; InterTypes(26,25)=1;
InterTypes(27,18)=1; InterTypes(27,19)=1; InterTypes(27,26)=1; InterTypes(28,19)=1;
InterTypes(28,20)=1; InterTypes(28,27)=1; InterTypes(29,20)=1; InterTypes(29,28)=1;
InterTypes(30,22)=1; InterTypes(30,23)=1; InterTypes(30,24)=1; InterTypes(31,24)=1; InterTypes(31,25)=1;
InterTypes(31,30)=1; InterTypes(32,25)=1; InterTypes(32,26)=1; InterTypes(32,31)=1;
InterTypes(33,26)=1; InterTypes(33,27)=1; InterTypes(33,32)=1; InterTypes(34,27)=1;
InterTypes(34,28)=1; InterTypes(34,33)=1; InterTypes(35,28)=1; InterTypes(35,29)=1;
InterTypes(35,34)=1;

ndm = 2;
[NodesOnElement,RegionOnElement,Coordinates,numnp,Output_data,MPCList,numMPC] = DEIPFunction(InterTypes,NodesOnElement,RegionOnElement,Coordinates,numnp,numel,nummat,nen,ndm,usePBC,numMPC,MPCList);


% Insert couplers; use extended DG mesh with ghost elements so that
% couplers appear properly on the domain boundary
% CZ element stiffness
CZprop =500;5*10^10;5000;50;50000;1;
% MPC spring element properties
pencoeff = 1e9;
CornerXYZ = [0.000000 0.000000
             1.000000 0.000000];

[NodesOnElement,RegionOnElement,Coordinates,numnp,nen,numel,nummat,RegionsOnInterface,MateT,MatTypeTable,NodeTypeNum,numMPC,MPCList...
] =InterFunction(4,InterTypes,NodesOnElement,RegionOnElement,Coordinates,numnp,numel,nummat,nen,ndm,Output_data,7,CZprop,MateT,MatTypeTable,usePBC,numMPC,MPCList,...
pencoeff,CornerXYZ);


% Applied displacements instead of forces
NodeLoad2 = [NodeTypeNum(2)+ndm 1 0
            NodeTypeNum(2)+ndm 2 0.01
            NodeTypeNum(2)+ndm+1 1 0.01
            NodeTypeNum(2)+ndm+1 2 0];
NodeBC = [NodeBC; NodeLoad2];
numBC = length(NodeBC);
% % Nodal forces allow rotation of cube
% NodeLoad = [NodeTypeNum(2)+ndm 1 1
%             NodeTypeNum(2)+ndm 2 0
%             NodeTypeNum(2)+ndm+1 1 0
%             NodeTypeNum(2)+ndm+1 2 1];
% numNodalF = length(NodeLoad);

ProbType = [numnp numel nummat ndm ndm nen];
