% Shear test of MP Periodic Boundary Conditions with CZ couplers
% Domain: 1X1 square block domain with hexagon grains with 12 regions 
% Loading: Prescribed shear displacement.
%
% Created: 09/27/2018 % Sunday Aduloju
% Last revision: 11/05/2018 TJT

clear
% clc
meshref=1; %permits 8 levels of mesh refinement

nen = 3;
nel = 3;
L=1;
H=1;
m1=12;
n1=12;
numc = 12;2;1;
xl = [1 0 0
      2 L 0
      4 0 H
      3 L H];
type = 'cart';
rinc = m1*meshref;
sinc = n1*meshref;
node1 = 1;
elmt1 = 1;
mat = 1;
rskip = 0;

if meshref==1
    btype = 6;
else
    btype = 5;
end

[Coordinates,NodesOnElement,~,numnp,numel] = block2d(type,rinc,sinc,node1,elmt1,mat,rskip,btype,xl,nen);
Coordinates = Coordinates';
NodesOnElement = NodesOnElement';

% Modify regions
%   RegionOnElement = 1* ones(numel,1);

StepRight1=5;
StepRight2=StepRight1+2; %7
numelemY=sinc*2; %num  of triang. element along y dir.
numelemX=rinc*2; %num  of triang. element along x dir.

Tableref1 =[ 0 0 
             0 0];
Tableref2 =[ 1  -1 -1  1 
             -1  1  1 -1];
Tableref4=[  3  1  -1 -3 -3 -1  1  3
            -3 -1  1  3  3  1 -1 -3];
Tableref8=[ 7  5  3  1 -1 -3 -5 -7 -7 -5 -3 -1  1  3  5  7
           -7 -5 -3 -1  1  3  5  7  7  5  3  1 -1 -3 -5 -7];
 if meshref==1
     Tableref=Tableref1;
 elseif meshref==2
     Tableref=Tableref2;
 elseif meshref==4
     Tableref=Tableref4;
 elseif meshref==8
     Tableref=Tableref8;
 end
 
%% Construct hexagons from only 2 regions
counterStep = 1;
TransY = round(numelemY/(4*meshref));
RegionMat = zeros(numelemY/2,numelemY);
for i= 1:TransY
       
    for k= 1:2*meshref 
        Row_r = (i-1)*(meshref*2)+k;
        if mod(counterStep,2)==1 % odd
            ElementstoRight = StepRight1 * meshref + Tableref(1,k);
            for j = 1:ElementstoRight
                RegionMat(Row_r,j)=1;
            end 
            for j = ElementstoRight+1:(numelemX-ElementstoRight)
                RegionMat(Row_r,j)=2;
            end 
            for j =(numelemX-ElementstoRight)+1:numelemX
                RegionMat(Row_r,j)=1;         
            end            
        else 
            ElementstoRight = StepRight2 * meshref + Tableref(2,k);
            for j = 1:ElementstoRight
                RegionMat(Row_r,j)=1;
            end 
            for j = ElementstoRight+1:(numelemX-ElementstoRight)
                RegionMat(Row_r,j)=2;
            end 
            for j = (numelemX-ElementstoRight)+1:numelemX
                RegionMat(Row_r,j)=1;         
            end                     
        end           
    end
    
    counterStep=counterStep+1;
    
end
        
%% Extend the domain to each hegaxon has a region 
%middle
stepmat2=3*meshref+1;
kountmat=3;
controws=0;
for p = stepmat2:numelemY/2
    for j = 1:numelemY
        if RegionMat(p,j)==2
            RegionMat(p,j)=kountmat;
        end
    end
    controws=controws+1;
    if controws==4*meshref
        kountmat=kountmat+1;
        controws=0;
    end
end 
% Left
stepmat2=1*meshref+1; 
 kountmat=kountmat+1; % continue from the  kountmat
controws=0;
for p = stepmat2:numelemY/2
    for j= 1:numelemY/2
        if RegionMat(p,j)==1
            RegionMat(p,j)=kountmat;
        end
    end
       controws=controws+1;
      if controws==4*meshref
          kountmat=kountmat+1;
          controws=0;
      end
end
%Right
stepmat2=1*meshref+1; 
 kountmat=kountmat+1; % continue from the  kountmat
controws=0;
for p = stepmat2:numelemY/2
    for j = (numelemY/2)+1:numelemY
      if RegionMat(p,j)==1
          RegionMat(p,j)=kountmat;
      end
    end
    controws=controws+1;
    if controws==4*meshref
        kountmat=kountmat+1;
        controws=0;
    end
end
%Left lower edge
stepmat2=1;
kountmat=kountmat+1; % continue from the  kountmat
controws=0;
for p= stepmat2:meshref*1
    for j=(numelemY/2)+1: numelemY
      if RegionMat(p,j)==1
           RegionMat(p,j)=kountmat;
      end
    end
end
RegionMat= RegionMat';
RegionOnElement=reshape(RegionMat,[],1);


MatTypeTable = [1 2 3 4 5 6 7 8 9 10 11 12
                1 1 1 1 1 1 1 1 1  1  1  1];     
nummat=12;

%% The same materials 
MateT = ones(nummat,1)*[100 0.25 1 ];
  
%% Different materials
% MateT=zeros(nummat,3);
% E=100;
% for i=1:1:nummat
%     MateT(i,1) = E+(i-3)^2*10;
%     MateT(i,2:3) = [0.25 1 ];     
% end
% % Different materials
% MateT = zeros(nummat,3);
% E=100;
% v=0.25
% for i=1:1:nummat
%     MateT(i,1) = E+(i-1)^2*10;
%     MateT(i,2) = 0.37-(i)*0.016;
%     MateT(i,3) = 1;     
% end

%%            
% Boundary conditions
FIX = 1;
RIGHT = (rinc+1); %x
TOP = (rinc+1)*(sinc)+1; %y
TOPRIGHT = TOP + RIGHT - 1; %xy

NodeBC = [1 1 0
          1 2 0];

% Create periodic boundary conditions
dx = 1;
dy = rinc+1;
%dxy = TOP - 1;
PBCListx = [(RIGHT:dy:TOPRIGHT)' (FIX:dy:TOP)' RIGHT*ones(sinc+1,1) FIX*ones(sinc+1,1)]; %y
% remove repeats
RIGHTface = 2:sinc+1;
PBCListx = PBCListx(RIGHTface,:);
PBCListy = [(TOP:dx:TOPRIGHT)' (FIX:dx:RIGHT)' TOP*ones(rinc+1,1) FIX*ones(rinc+1,1)]; %x
% remove repeats
TOPface = 1:rinc+1;
TOPface = setdiff(TOPface,dy);
TOPface = setdiff(TOPface,1);
PBCListy = PBCListy(TOPface,:);
PBCList = [PBCListx; PBCListy];
MPCListx = [PBCListx(:,1:2) ones(size(PBCListx,1),1)*[1 0]
            PBCListx(1,3:4) [1 0]];
MPCListy = [PBCListy(:,1:2) ones(size(PBCListy,1),1)*[0 1]
            PBCListy(1,3:4) [0 1]];
MPCList = [MPCListx; MPCListy];

% Output quantity flags
DHist = 1;
FHist = 1;
SHist = 1;
SEHist = 1;


% Generate CZM interface: pairs of elements and faces, duplicate the nodes,
% update the connectivities
numnpCG = numnp;
usePBC = 2; % flag to turn on keeping PBC
numMPC = length(MPCList);
InterTypes = [0 0 0 0 0 0 0 0 0 0 0 0 
              1 0 0 0 0 0 0 0 0 0 0 0 
              0 1 0 0 0 0 0 0 0 0 0 0
              0 0 1 0 0 0 0 0 0 0 0 0
              0 0 0 1 0 0 0 0 0 0 0 0
              1 1 1 0 0 0 0 0 0 0 0 0
              0 0 1 1 0 1 0 0 0 0 0 0
              0 0 0 1 1 0 1 0 0 0 0 0
              0 1 1 0 0 0 0 0 0 0 0 0
              0 0 1 1 0 0 0 0 1 0 0 0
              0 0 0 1 1 0 0 0 0 1 0 0
              0 1 0 0 0 0 0 0 1 0 0 0
              ];
ndm = 2;
[NodesOnElement,RegionOnElement,Coordinates,numnp,Output_data,MPCList,numMPC]...
= DEIPFunction(InterTypes,NodesOnElement,RegionOnElement,Coordinates,numnp,...
               numel,nummat,nen,ndm,usePBC,numMPC,MPCList);
           

%% Insert couplers
CZprop = 500e10;500;
% MPC spring element properties
pencoeff = 1e9;
CornerXYZ = [0.000000 0.000000
             1.000000 0.000000];

[NodesOnElement,RegionOnElement,Coordinates,numnp,nen,numel,nummat,...
    RegionsOnInterface,MateT,MatTypeTable,NodeTypeNum,numMPC,MPCList...
] = InterFunction(4,InterTypes,NodesOnElement,RegionOnElement,Coordinates,...
numnp,numel,nummat,nen,ndm,Output_data,7,CZprop,MateT,MatTypeTable,usePBC,...
numMPC,MPCList,pencoeff,CornerXYZ);


% Applied displacements instead of forces
NodeLoad2 = [NodeTypeNum(2)+ndm 1 0
            NodeTypeNum(2)+ndm 2 0.01
            NodeTypeNum(2)+ndm+1 1 0.01
            NodeTypeNum(2)+ndm+1 2 0];
NodeBC = [NodeBC; NodeLoad2];
numBC = length(NodeBC);
% % Nodal forces allow rotation of cube
% NodeLoad = [NodeTypeNum(2)+ndm 1 1
%             NodeTypeNum(2)+ndm 2 0
%             NodeTypeNum(2)+ndm+1 1 0
%             NodeTypeNum(2)+ndm+1 2 1];
% numNodalF = length(NodeLoad);

ProbType = [numnp numel nummat ndm ndm nen];
