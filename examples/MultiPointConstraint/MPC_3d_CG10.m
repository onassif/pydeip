% Shear test of Periodic Boundary Conditions, quadratic tetrahedral element
% Domain: 4x4x4 rectangle block
% Loading: Prescribed shear displacement.
%
% Last revision: 12/07/2018 TJT

clear
% clc

nen = 27;
nel = 27;

numc = 8;2;1;
rinc = numc;
sinc = numc;
tinc = numc;
xl = [1 0 0 0
      2 numc 0 0
      4 0 numc 0
      3 numc numc 0
      5 0 0 numc
      6 numc 0 numc
      8 0 numc numc
      7 numc numc numc];
[Coordinates,NodesOnElement,RegionOnElement,numnp,numel] = block3d('cart',numc,numc,numc,1,1,1,13,xl,nen);
Coordinates = Coordinates';
NodesOnElement = NodesOnElement';

nummat = 1;
MatTypeTable = [1
                1];
MateT = [100 0.25 1];

% Generate MPCList for Lagrange multiplier pairs on block boundaries
[MPCList,numMPC,NodeBC,numBC] = block3dMPC(rinc,sinc,tinc);

CornerXYZ = [numc 0.000000 0.000000
             0.000000 numc 0.000000
             0.000000 0.000000 numc];
[Coordinates,NodesOnElement,RegionOnElement,MatTypeTable,MateT,...
    numnp,numel,nummat,nen,NodeTypeNum] = AddPBNodes(MPCList,CornerXYZ,Coordinates,...
    NodesOnElement,RegionOnElement,MatTypeTable,MateT,numnp,numel,nummat,nen);

% Applied displacements instead of forces
NodeLoad2 = [NodeTypeNum(2) 1 0
            NodeTypeNum(2)+1 1 0.01
            NodeTypeNum(2)+1 2 0
            NodeTypeNum(2)+2 1 0
            NodeTypeNum(2)+2 2 0
            NodeTypeNum(2)+2 3 0];
NodeBC = [NodeBC; NodeLoad2];
numBC = length(NodeBC);
% % Nodal forces allow rotation of cube
% NodeLoad = [NodeTypeNum(2) 1 1
%             NodeTypeNum(2) 2 0
%             NodeTypeNum(2) 3 0
%             NodeTypeNum(2)+1 1 0
%             NodeTypeNum(2)+1 2 1
%             NodeTypeNum(2)+1 3 0
%             NodeTypeNum(2)+2 1 0
%             NodeTypeNum(2)+2 2 0
%             NodeTypeNum(2)+2 3 1];
% numNodalF = length(NodeLoad);


% Output quantity flags
DHist = 1;
FHist = 1;
SHist = 1;
SEHist = 1;

ProbType = [numnp numel nummat 3 3 nen];