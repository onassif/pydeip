% Axial test of MP Periodic Boundary Conditions with CZ couplers, tetrahedral
% elements, 8 grains, quadratic
% Domain: 4x4x4 rectangle block
% Loading: Prescribed axial displacement.
%
% Last revision: 12/07/2018 TJT

clear
% clc

nen = 10;
nel = 10;

numc = 8;2;1;
rinc = numc;
sinc = numc;
tinc = numc;
xl = [1 0 0 0
      2 numc 0 0
      4 0 numc 0
      3 numc numc 0
      5 0 0 numc
      6 numc 0 numc
      8 0 numc numc
      7 numc numc numc];
[Coordinates,NodesOnElement,~,numnp,numel] = block3d('cart',numc,numc,numc,1,1,1,13,xl,nen);
Coordinates = Coordinates';
NodesOnElement = NodesOnElement';

% Modify regions
RegionOnElement = [1*ones(1,6) 1*ones(1,6) 2*ones(1,6) 2*ones(1,6) 1*ones(1,6) 1*ones(1,6) 2*ones(1,6) 2*ones(1,6) 3*ones(1,6) 3*ones(1,6) 4*ones(1,6) 4*ones(1,6) 3*ones(1,6) 3*ones(1,6) 4*ones(1,6) 4*ones(1,6) ...
                   1*ones(1,6) 1*ones(1,6) 2*ones(1,6) 2*ones(1,6) 1*ones(1,6) 1*ones(1,6) 2*ones(1,6) 2*ones(1,6) 3*ones(1,6) 3*ones(1,6) 4*ones(1,6) 4*ones(1,6) 3*ones(1,6) 3*ones(1,6) 4*ones(1,6) 4*ones(1,6) ...
                   5*ones(1,6) 5*ones(1,6) 6*ones(1,6) 6*ones(1,6) 5*ones(1,6) 5*ones(1,6) 6*ones(1,6) 6*ones(1,6) 7*ones(1,6) 7*ones(1,6) 8*ones(1,6) 8*ones(1,6) 7*ones(1,6) 7*ones(1,6) 8*ones(1,6) 8*ones(1,6) ...
                   5*ones(1,6) 5*ones(1,6) 6*ones(1,6) 6*ones(1,6) 5*ones(1,6) 5*ones(1,6) 6*ones(1,6) 6*ones(1,6) 7*ones(1,6) 7*ones(1,6) 8*ones(1,6) 8*ones(1,6) 7*ones(1,6) 7*ones(1,6) 8*ones(1,6) 8*ones(1,6) ]';
% RegionOnElement = [7*ones(1,6) 8*ones(1,6) 8*ones(1,6) 7*ones(1,6) 5*ones(1,6) 6*ones(1,6) 6*ones(1,6) 5*ones(1,6) 5*ones(1,6) 6*ones(1,6) 6*ones(1,6) 5*ones(1,6) 7*ones(1,6) 8*ones(1,6) 8*ones(1,6) 7*ones(1,6) ...
%                    3*ones(1,6) 4*ones(1,6) 4*ones(1,6) 3*ones(1,6) 1*ones(1,6) 2*ones(1,6) 2*ones(1,6) 1*ones(1,6) 1*ones(1,6) 2*ones(1,6) 2*ones(1,6) 1*ones(1,6) 3*ones(1,6) 4*ones(1,6) 4*ones(1,6) 3*ones(1,6) ...
%                    3*ones(1,6) 4*ones(1,6) 4*ones(1,6) 3*ones(1,6) 1*ones(1,6) 2*ones(1,6) 2*ones(1,6) 1*ones(1,6) 1*ones(1,6) 2*ones(1,6) 2*ones(1,6) 1*ones(1,6) 3*ones(1,6) 4*ones(1,6) 4*ones(1,6) 3*ones(1,6) ...
%                    7*ones(1,6) 8*ones(1,6) 8*ones(1,6) 7*ones(1,6) 5*ones(1,6) 6*ones(1,6) 6*ones(1,6) 5*ones(1,6) 5*ones(1,6) 6*ones(1,6) 6*ones(1,6) 5*ones(1,6) 7*ones(1,6) 8*ones(1,6) 8*ones(1,6) 7*ones(1,6) ...
%                    ]';
ix = randperm(numel);
NodesOnElement = NodesOnElement(ix,:);
RegionOnElement = RegionOnElement(ix);
nummat = 8;
MatTypeTable = [1 2 3 4 5 6 7 8
                1 1 1 1 1 1 1 1];
MateT = [100 0.25 1
         100 0.25 1
         100 0.25 1
         100 0.25 1
         100 0.25 1
         100 0.25 1
         100 0.25 1
         100 0.25 1];

% Generate MPCList for Lagrange multiplier pairs on block boundaries
[MPCList,numMPC,NodeBC,numBC] = block3dMPC(rinc,sinc,tinc);


% Generate CZM interface: pairs of elements and faces, duplicate the nodes,
% update the connectivities
numnpCG = numnp;
usePBC = 2; % flag to turn on keeping PBC
InterTypes = [0 0 0 0 0 0 0 0
              1 0 0 0 0 0 0 0
              1 1 0 0 0 0 0 0
              1 1 1 0 0 0 0 0
              1 1 1 1 0 0 0 0
              1 1 1 1 1 0 0 0
              1 1 1 1 1 1 0 0
              1 1 1 1 1 1 1 0]; % only put CZM between the element edges between materials 1-2
ndm = 3;
[NodesOnElement,RegionOnElement,Coordinates,numnp,Output_data,MPCList,numMPC] = DEIPFunction(InterTypes,NodesOnElement,RegionOnElement,Coordinates,numnp,numel,nummat,nen,ndm,usePBC,numMPC,MPCList);


% Insert couplers; use extended DG mesh with ghost elements so that
% couplers appear properly on the domain boundary
% CZ element stiffness
CZprop = 50;1;
% MPC spring element properties
pencoeff = 1e9;
CornerXYZ = [0.000000 0.000000 0.000000
             1.000000 0.000000 0.000000
             0.000000 1.000000 0.000000];

[NodesOnElement,RegionOnElement,Coordinates,numnp,nen,numel,nummat,RegionsOnInterface,MateT,MatTypeTable,NodeTypeNum,numMPC,MPCList...
] =InterFunction(3,InterTypes,NodesOnElement,RegionOnElement,Coordinates,numnp,numel,nummat,nen,ndm,Output_data,7,CZprop,MateT,MatTypeTable,usePBC,numMPC,MPCList,...
pencoeff,CornerXYZ);


% Applied displacements instead of forces
NodeLoad2 = [NodeTypeNum(2)+ndm 1 .01
            NodeTypeNum(2)+ndm 2 0
            NodeTypeNum(2)+ndm 3 0
            NodeTypeNum(2)+ndm+1 1 0
            NodeTypeNum(2)+ndm+1 2 .01
            NodeTypeNum(2)+ndm+1 3 0
            NodeTypeNum(2)+ndm+2 1 0
            NodeTypeNum(2)+ndm+2 2 0
            NodeTypeNum(2)+ndm+2 3 .01];
NodeBC = [NodeBC; NodeLoad2];
numBC = length(NodeBC);
% % Nodal forces allow rotation of cube
% NodeLoad = [NodeTypeNum(2)+ndm 1 1
%             NodeTypeNum(2)+ndm 2 0
%             NodeTypeNum(2)+ndm 3 0
%             NodeTypeNum(2)+ndm+1 1 0
%             NodeTypeNum(2)+ndm+1 2 1
%             NodeTypeNum(2)+ndm+1 3 0
%             NodeTypeNum(2)+ndm+2 1 0
%             NodeTypeNum(2)+ndm+2 2 0
%             NodeTypeNum(2)+ndm+2 3 1];
% numNodalF = length(NodeLoad);

ProbType = [numnp numel nummat 3 3 nen];

% plotNodeCont3(Coordinates+Node_U_V*100, Node_U_V(:,1), NodesOnElement, 2,1:64*6)