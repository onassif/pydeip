% 06/20/2017 TJT
% A self periodic micrstructure containing 10 grains
% 3D Domain: 1.355528516514000 X 1.271597984617000 X 1.289175242983000 
% Loading: Prescribed macro strain of 0.01 
% Last revision: 11/13/2019 Sunday Aduloju


clear
% clc

load('10grains.mat')
nen = 4;
% read periodic file
Perfile = 'n10-id1.per';
fid = fopen(Perfile);
FormatString=repmat('%f',1,5);  % Create format string based on parameter
InputText=textscan(fid,FormatString); % Read data block
NeperPBC=cell2mat(InputText); % Convert to numerical array from cell
fclose(fid);
nummat=10;

% %% Harvest RegionOnElement data
% nummatS=nummat;
% RegionOnElementS= RegionOnElement;
% nummat=1;
% RegionOnElement= ones(numel,1);
% %%


% SEHist = 1;
% SHist = 1;

MateT = ones(nummat,1)*[100 .25 1];
MatTypeTable = [(1:nummat); ones(1,nummat)];
     
MPCList = NeperPBC;
% [~,inds] = unique(MPCList(:,2));
% MPCList = MPCList(inds,:);
numMPC = length(MPCList);

logic_x = abs(NeperPBC(:,3))==1;
logic_y = abs(NeperPBC(:,4))==1;
logic_z = abs(NeperPBC(:,5))==1;
logic_xy = (abs(NeperPBC(:,3))==1)&(abs(NeperPBC(:,4))==1);
logic_yz = (abs(NeperPBC(:,4))==1)&(abs(NeperPBC(:,5))==1);
logic_zx = (abs(NeperPBC(:,5))==1)&(abs(NeperPBC(:,3))==1);
logic_xyz = (abs(NeperPBC(:,3))==1)&(abs(NeperPBC(:,4))==1)&(abs(NeperPBC(:,5))==1);
% cut out the multi-direction links
logic_x = logic_x&~logic_xy&~logic_zx&~logic_xyz;
logic_y = logic_y&~logic_yz&~logic_xy&~logic_xyz;
logic_z = logic_z&~logic_yz&~logic_zx&~logic_xyz;
PBCListx = NeperPBC(logic_x,:);
PBCListy = NeperPBC(logic_y,:);
PBCListz = NeperPBC(logic_z,:);

% Get rigid body modes
% or even just look for a node with 2 links
FIX = intersect(PBCListx(:,2),intersect(PBCListy(:,2),PBCListz(:,2)));
FIX = FIX(1);
% indx = find(PBCListx(:,2)==FIX);
% RIGHT = PBCListx(indx,1);
% indy = find(PBCListy(:,2)==FIX);
% TOP = PBCListy(indy,1);
% indz = find(PBCListz(:,2)==FIX);
% FRONT = PBCListz(indz,1);


NodeBC = [FIX 1 0
          FIX 2 0
          FIX 3 0
%           FRONT 1 0
%           FRONT 2 0
%           FRONT 3 0
%           RIGHT 2 0
%           RIGHT 3 0
%           TOP 1 0
          ];
% numBC = size(NodeBC,1);





% Output quantity flags
DHist = 1;
FHist = 1;
SHist = 1;
SEHist = 1;


% Generate DG interface: pairs of elements and faces, duplicate the nodes,
% update the connectivities
numnpCG = numnp;
usePBC = 2; % flag to turn on keeping PBC
ndm = 3;
InterTypes = zeros(nummat,nummat); % generate DG elements on surface of domain
[NodesOnElement,RegionOnElement,Coordinates,numnp,Output_data,MPCList,numMPC] = ...
    DEIPFunction(InterTypes,NodesOnElement,RegionOnElement,Coordinates,numnp,numel,nummat,nen,ndm,usePBC,numMPC,MPCList);

% Insert couplers; use extended DG mesh with ghost elements so that
% couplers appear properly on the domain boundary
pencoeff = 1e9;
CornerXYZ = [0.000000 0.000000 0.000000
             4.000000 0.000000 0.000000
             0.000000 4.000000 0.000000];

[NodesOnElement,RegionOnElement,Coordinates,numnp,nen,numel,nummat, ...
    RegionsOnInterface,MateT,MatTypeTable,NodeTypeNum,numMPC,MPCList] ...
  = InterFunction(5,InterTypes,NodesOnElement,RegionOnElement,Coordinates,...
  numnp,numel,nummat,nen,ndm,Output_data,8,0,MateT,MatTypeTable,usePBC,numMPC,MPCList,...
  pencoeff,CornerXYZ);
     
% %% % apply macrostress
% NodeLoad = [NodeTypeNum(2)   1 0
%             NodeTypeNum(2)   2 10
%             NodeTypeNum(2)+1 1 10
%             NodeTypeNum(2)+1 2 0];
% numNodalF = length(NodeLoad);
% NodeBC = [NodeBC; [rinc+1 2 0]];
% numBC = numBC+1;     

%% apply macrostrain
PbcStra=0.01;

NodeLoad2 = [NodeTypeNum(2)   1 PbcStra
             NodeTypeNum(2)   2 PbcStra
             NodeTypeNum(2)   3 0
             NodeTypeNum(2)+1 1 0
             NodeTypeNum(2)+1 2 0
             NodeTypeNum(2)+1 3 0
             NodeTypeNum(2)+2 1 0
             NodeTypeNum(2)+2 2 0
             NodeTypeNum(2)+2 3 0];
NodeBC = [NodeBC; NodeLoad2];
numBC = length(NodeBC);


% % Generate CZM interface: pairs of elements and faces, duplicate the nodes,
% % update the connectivities
% numnpCG = numnp;
% usePBC = 2; % flag to turn on keeping PBC
% % InterTypes = [0];% only put CZM between the element edges between materials 1-2
% InterTypes=zeros(nummat);
% 
% DEIProgram3
% % [FacetsOnPBCNum] = DEIProgram2function(Coordinates,NodesOnElement,RegionOnElement,InterTypes,nen,numnp,numel,nummat)
% 
% % Insert couplers; use extended DG mesh with ghost elements so that
% % couplers appear properly on the domain boundary
% ndm = 3;
% 
% nummatCG = nummat;
% numSI = numCL;
% numelCG = numel;
% nen_bulk = nen;
% SurfacesI = zeros(0,8);
% numSI = 0;
% % Arrays for new MPC links being formed
% MPCListNew = zeros(0,2+ndm);
% numMPCnew = 0;
% CouplerNodes = zeros(0,1); % extra nodes for MPC-CZ
% NodesOnLinkNew = zeros(4,numnp);
% NodesOnLinknumNew = zeros(numnp,1);
% % Interface region information: [materialID in MateT; mat1; mat2; regI; first coupler number; last coupler number]
% RegionsOnInterface = zeros(nummat*(nummat+1)/2,6);




% for mat2 = 1:nummat
%      for mat1 = 1:mat2
%      
%         matI = GetRegionsInt(mat1,mat2);
%         numSIi = numEonPBC(matI);
%             locF = FacetsOnPBCNum(matI):(FacetsOnPBCNum(matI+1)-1);
%             facs = FacetsOnPBC(locF);
%             SurfacesIi = ElementsOnFacet(facs,:);
%             SurfacesIi = ReverseFacets(SurfacesIi,NodesOnElement,Coordinates,numSIi,ndm);
%             ElementsOnFacet(facs,:) = SurfacesIi;
%             SurfacesI(numSI+1:numSI+numSIi,5:8) = SurfacesIi;
%             numel_old = numel;
%         [NodesOnElement,RegionOnElement,nen,numel,nummat,MatTypeTable,MateT] = ...
%          FormDG(SurfacesIi,NodesOnElement,RegionOnElement,Coordinates,numSIi,nen_bulk,ndm,numel,nummat,6, ...
%                 9,0,[0],MatTypeTable,MateT);
%         if numel > numel_old
%         RegionsOnInterface(nummat-nummatCG,:) = [nummat mat1 mat2 matI numel_old+1 numel];
%         end
%     end
% end
% 
% nen = 11;

%%

% MPC_BCx=numnp+1;
% MPC_BCy=numnp + 2;
% MPC_BCz=numnp + 3;
% numnp = numnp + 3;
% Coordinates = [Coordinates; CornerXYZ];
% NodesOnElement = [NodesOnElement [zeros(numelCG,3); [MPC_BCx*ones(numel-numelCG,1) MPC_BCy*ones(numel-numelCG,1) MPC_BCz*ones(numel-numelCG,1)]]];
% 
% PbcStra=0.01;
% 
% NodeLoad2 = [MPC_BCx 1 PbcStra
%             MPC_BCx 2 PbcStra
%             MPC_BCx 3 0
%             MPC_BCy 1 0
%             MPC_BCy 2 0
%             MPC_BCy 3 0
%             MPC_BCz 1 0
%             MPC_BCz 2 0
%             MPC_BCz 3 0];
% % numNodalF = length(NodeLoad);
% NodeBC = [NodeBC; NodeLoad2];
% numBC = size(NodeBC,1);

ProbType = [numnp numel nummat ndm ndm nen];

% numSI= numSIi;

 AlgoType = [0; 1; 0];

% SEHist = 1;

% hrstore = 1;
% itermax = 30;30;
% Residratio = 10^-11;
% reststep = 10; % dump data at steps equalting multiples of this #



%% Plots for after FEA_Program is called

% % Plot the u_y displacement, takes a long time
% plotNodeCont3(Coordinates+Node_U_V*20, Node_U_V(:,2), NodesOnElement,100, 1:numel,1:numnp, [1 0 0])
% % Extract the macroscale strain and stress*volume
% [RVE_E,RVE_F] = GetMacroSS(DispList,ForcList,NodeTypeNum,ndm)
