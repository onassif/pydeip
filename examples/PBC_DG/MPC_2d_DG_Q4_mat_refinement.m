% 11/13/2019 %Sunday Aduloju
% Shear test of Periodic Boundary Conditions with DG couplers, stress load
% Domain: 4x4 rectangle block with inclusion
% Loading: Prescribed shear macro strain of 0.01.
%                  OR shear macro stress of 10/4^2
% Last revision: 12/10/2019 Tim Truster

clear
% clc

nen = 4;
nel = 4;
div=4;%%1,2,4,8,16,32 % no of divisions

L_x=4.000000;
L_y=L_x;
xl = [1 0   0
      2 L_x 0
      4 0   L_y
      3 L_x L_y];
  
numc = 4;2;1;
type = 'cart';
rinc = numc*div;
sinc = numc*div;
node1 = 1;
elmt1 = 1;
mat = 1;
rskip = 0;
btype = 0;
[Coordinates,NodesOnElement,~,numnp,numel] = block2d(type,rinc,sinc,node1,elmt1,mat,rskip,btype,xl,nen);
Coordinates = Coordinates';
NodesOnElement = NodesOnElement';

rsinc=rinc*sinc;
refine=[1 2 4 8 16 32 64]';
m=size(refine,1);
 
 for i=1:m
     if div == refine(i)
        pow_r=i;
     end
 end
 startRegion=numc^pow_r+div;
 
 % stuck the material at the bottom
 RegionOnElement(1:startRegion,1)=ones(1,startRegion);
 
 % stuck in the second material in the middle
 i=startRegion+1;
 while i < rsinc-startRegion+1
     
  RegionOnElement(i:i+div*2-1,1)=2*ones(1,div*2);
  RegionOnElement(i+div*2:i+div*4-1,1)= ones(1,div*2);
    i=i+div*4;   
 end
 
 % the first material at the top
RegionOnElement(rsinc-startRegion+1:rsinc)=ones(1,startRegion); 

%  RegionOnElement = ones(numel,1)

nummat = 2;
MatTypeTable = [1 2
                1 1];
% MateT = [100 0.25 1
%          100 0.25 1];
MateT = [100 0.25 1
         500 0.25 1];

% Create periodic boundary conditions
[MPCList,numMPC,NodeBC] = block2dMPC(rinc,sinc);
NodeBC = NodeBC(1:2,:);
numBC = size(NodeBC,1);

% Output quantity flags
DHist = 1;
FHist = 1;
SHist = 1;
SEHist = 1;


% Generate DG interface: pairs of elements and faces, duplicate the nodes,
% update the connectivities
numnpCG = numnp;
usePBC = 2; % flag to turn on keeping PBC
ndm = 2;
InterTypes = [0 0
              0 0]; % only put CZM between the element edges between materials 1-2
[NodesOnElement,RegionOnElement,Coordinates,numnp,Output_data,MPCList,numMPC] = ...
    DEIPFunction(InterTypes,NodesOnElement,RegionOnElement,Coordinates,numnp,numel,nummat,nen,ndm,usePBC,numMPC,MPCList);


% Insert couplers; use extended DG mesh with ghost elements so that
% couplers appear properly on the domain boundary
ndm = 2;
% MPC spring element properties
pencoeff = 1e9;
CornerXYZ = [4.000000 0.000000
             0.000000 4.000000];

[NodesOnElement,RegionOnElement,Coordinates,numnp,nen,numel,nummat, ...
    RegionsOnInterface,MateT,MatTypeTable,NodeTypeNum,numMPC,MPCList] ...
  = InterFunction(5,InterTypes,NodesOnElement,RegionOnElement,Coordinates,...
  numnp,numel,nummat,nen,ndm,Output_data,8,0,MateT,MatTypeTable,usePBC,numMPC,MPCList,...
  pencoeff,CornerXYZ);
     
%% % apply macrostress
NodeLoad = [NodeTypeNum(2)   1 0
            NodeTypeNum(2)   2 10
            NodeTypeNum(2)+1 1 10
            NodeTypeNum(2)+1 2 0];
numNodalF = length(NodeLoad);
NodeBC = [NodeBC; [rinc+1 2 0]];
numBC = numBC+1;     

%% % apply macrostrain
% NodeLoad2 = [NodeTypeNum(2)   1 0
%              NodeTypeNum(2)   2 0.01
%              NodeTypeNum(2)+1 1 0.01
%              NodeTypeNum(2)+1 2 0];
% NodeBC = [NodeBC; NodeLoad2];
% numBC = length(NodeBC);

 ProbType = [numnp numel nummat ndm ndm nen];

%% Plots for after FEA_Program is called

% % See the regions
% plotElemCont2(Coordinates,RegionOnElement,NodesOnElement,1,1:(rsinc),[1 1 1]) 
% % Plot the u_x displacement
% plotNodeCont2(Coordinates+Node_U_V*10, Node_U_V(:,1), NodesOnElement, 2, 1:(rsinc), [1 1 1], 0,[3 4 6 9 0]) 
% % Extract the macroscale strain and stress*volume
% [RVE_E,RVE_F] = GetMacroSS(DispList,ForcList,NodeTypeNum,ndm)
