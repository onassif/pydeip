from numpy import zeros, unique, isin, set_printoptions, setdiff1d, ones, outer, nonzero, tile
from scipy.io import loadmat
from scipy.sparse import csc_matrix as sparse
from pathlib import Path

# For indexing purposes:
o = 1
# For printing purposes:
set_printoptions(linewidth=165)
# %% Read from matlab workspace (Remove afterwards):
ws = loadmat(Path('../examples/MultiPointConstraint/MPC_2d_CZ'))  

coor, intrTyps, MPCLst, usePBC, ndesOnElm, rgnsOnElm, ndm, nen, numMPC, numEl, numMat, numnp  =       \
   ws['Coordinates'], ws['InterTypes'].astype(int), ws['MPCList'].astype(int), int(ws['usePBC']),     \
   ws['NodesOnElement'].astype(int), ws['RegionOnElement'].astype(int), int(ws['ndm']),               \
   int(ws['nen']), int(ws['numMPC']), int(ws['numel']), int(ws['nummat']), int(ws['numnp']) 

# %% Setting Variables
maxel = 12                                                                                            #58
ndm = 2                                                                                               #59
numz = 0                                                                                              #92

ElmsOnNdeNum = zeros(numnp, dtype=int, order='F')                                                     #62
   
if usePBC:                                                                                
   ElmsOnNdePBCNum = zeros(numnp, dtype=int, order='F')                                               #69

Coor3  = zeros((numEl*nen, ndm), dtype=float, order='F')                                              #74
NdesOnElmCG = ndesOnElm                                                                               #75

if usePBC: # Add space in connectivity for ghost elements                                 
   # Connectivity that is zipped, having PBC nodes condensed together:
   NdesOnElmPBC = NdesOnElmCG                                                                         #86
else:
   NdesOnElmPBC = 0                                                                                   #89
   

# %% Set up PBC data structures
PBCLst = MPCLst                                                                                       #97
if usePBC:                                                                                            #83
   # Lists of node pairs in convenient table format
   ## nodes that are paired up
   ndesOnPBC = zeros((4,numnp), dtype=int, order='F')                                                 #100
   
   ## number of nodes with common pairing
   ndesOnPBCnum = zeros((numnp,), dtype=int, order='F')                                               #101
   
   ## IDs of PBC referring to node
   ndesOnLnk = zeros((4,numnp), dtype=int, order='F')                                                 #102
   
   ## number of PBC referring to node
   ndesOnLnknum = zeros((numnp,), dtype=int, order='F')                                               #103 
   
   # Handle the corners first
   numPBC = PBCLst.shape[0]                                                               
   if usePBC != 2:                                                                                    #105, but usePBC is always 2
      TieNodes, IA = unique(PBCLst[:,2:4], return_index=True, axis=0)                                 #111
      if TieNodes.shape[0] != ndm:
         raise Exception('Wrong number of master pairs')
      else:
         for iPBC in  range(ndm):
            ndesAB = TieNodes[iPBC, :]                                                                #116
            numA = 0
            ndesOnPBC[numA, ndesAB[0]] = ndesAB[1]
            ndesOnPBCnum[ndesAB[0]] = numA
            numB = iPBC
            ndesOnPBC[numB, ndesAB[1]] = ndesAB[0]
            ndesOnPBCnum[ndesAB[1]] = numB
            numA = 0
            ndesOnLnk[numA, ndesAB[0]] = -IA[iPBC]
            ndesOnLnknum[ndesAB[0]] = numA
            numB = iPBC
            ndesOnLnk[numB, ndesAB[1]] = -IA[iPBC]
            ndesOnLnknum[ndesAB[1]] = numB
            
   # Now do the rest of the nodes
   for iPBC in range(numPBC):
      ndesAB = PBCLst[iPBC, 0:2]                                                                      #133
      
      # make sure pair isn't already in the list for nodeA
      if not isin(ndesAB[1], ndesOnPBC[:ndesOnPBCnum[ndesAB[0]-o], ndesAB[0]-o]):
        # add the pair to nodeA, expand by one
        ndesOnPBC[ndesOnPBCnum[ndesAB[0]-o]+1-o, ndesAB[0]-o] = ndesAB[1]
        ndesOnPBCnum[ndesAB[0]-o] += 1
        
        # Record which PBC ID refers to these nodes
        ndesOnLnk[ndesOnLnknum[ndesAB[0]-o], ndesAB[0]-o] = iPBC + o
        ndesOnLnknum[ndesAB[0]-o] += 1                                                                #151
      
      # make sure pair isn't already in the list for nodeB
      if not isin(ndesAB[0], ndesOnPBC[0:ndesOnPBCnum[ndesAB[1]-o], ndesAB[1]-o]):
         # add the pair to nodeB, expand by one
         ndesOnPBC[ndesOnPBCnum[ndesAB[1]-o]+1-o, ndesAB[1]-o] = ndesAB[0]
         ndesOnPBCnum[ndesAB[1]-o] += 1
         
         # Record which PBC ID refers to these nodes
         ndesOnLnk[ndesOnLnknum[ndesAB[1]-o], ndesAB[1]-o] = iPBC + o
         ndesOnLnknum[ndesAB[1]-o] += 1                                                               #169          
   
   # Find additional connections for nodes with pairs of pairs; this WILL find the extra pairs that 
   # are usually deleted to remove linear dependence in the stiffness matrix,  e.g. the repeated edges 
   # in 2d or 3d.
   MorePBC = ndesOnPBCnum>1
   ndes = zeros(20, dtype=int, order='F')                                                             #176
   for i in range(sum(MorePBC)):
      ndes[0] = MorePBC[i]
      
      # Skip pairs already condensed:
      if ndesOnPBCnum[ndes[0]] <=0: continue
      
      # use a tree of connectivity to get to all node pairs involving MorePBC(i)
      notDone, lenn = True, 1
      while notDone:
         i1 = lenn
         for nde in ndes[:lenn]-o:
            ndes[i1:ndesOnPBCnum[nde]+i1] = ndesOnPBC[:ndesOnPBCnum[nde], nde]
            i1 += ndesOnPBCnum[nde]
         ndesU = unique(ndes[:i1-o])
         lennU = len(ndesU)
         ndes[:lennU] = ndesU
         if lennU == lenn:
            notDone = False
         lenn = lennU
      ndes[lenn:] = 0                                                                                 #204
      
      # record the maximum connected pairs into the NodesOnPBC for all the connected nodes in the set;
      # also combine together the NodesOnLink at the same time
      links = zeros(20,dtype=int,order='F')
      i1, i2 = 0, 1    
      for nde in ndes[:lenn]-o:
         ndesOnPBCnum[nde] = -(lenn - 1); # flag this node as already condensed
         ndesOnPBC[:lenn-1, nde] = ndes[(ndes != nde+o) *(ndes != 0)]
         links[i1:i1+ndesOnLnknum[nde]] = ndesOnLnk[:ndesOnLnknum[nde], nde]
         i1 += ndesOnLnknum[nde];

      links2 = unique(links[:i1-o])
      lenl = len(links2)
      ndesOnLnk[:len(links2), ndes[:lenn]-o] = tile(links2, (4,1)).transpose() 
      ndesOnLnknum[ndes[:lenn]-o] = lenl
   
   ndesOnPBCnum = abs(ndesOnPBCnum)
else:
   ndesOnPBC = zeros((4,0), dtype=int, order='F') # nodes that are paired up
   ndesOnPBCnum = zeros((0,1), dtype=int, order='F') # number of nodes with common pairing
   ndesOnLnk = zeros((4,0), dtype=int, order='F') # IDs of PBC referring to node
   ndesOnLnknum = zeros((0,1), dtype=int, order='F') # number of PBC referring to node                #232
s = 'ss'