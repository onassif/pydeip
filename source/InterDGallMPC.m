% Program: InterDGallMPC
% Tim Truster
% 12/10/2019
%
% Coupler insertion, standard script
% Coupler type: DG couplers on all interfaces and intrafaces defined in
% InterTypes array
% Domain surfaces are assigned DG PBC elements
%
% Input: ndm = spatial dimension
%        CornerXYZ = [coordinates of first master node
%                     2nd node
%                     third node];
%        remainder of arrays from DEIProgram
%
% Output: all necessary arrays for FEA_Program
%         Extra arrays:
%        -Interface region information: [materialID in MateT; mat1; mat2; regI; first coupler number; last coupler number]
%         RegionsOnInterface = zeros(nummat*(nummat+1)/2,6);
%        -Node number for each group of nodes: solid, masterPBC
%         NodeTypeNum = [1 numnp+1 numnp+ndm+1]';

if iscell(CZprop)
    CZpropall = CZprop;
    usemany = 1;
else
    usemany = 0;
end
nummatCG = nummat;
numSI = numCL;
numelCG = numel;
nen_bulk = nen;
SurfacesI = zeros(0,8);
numSI = 0;
% Arrays for new MPC links being formed
MPCListNew = zeros(0,2+ndm);
numMPCnew = 0;
CouplerNodes = zeros(0,1); % extra nodes for MPC-CZ
NodesOnLinkNew = zeros(4,numnp);
NodesOnLinknumNew = zeros(numnp,1);
% Interface region information: [materialID in MateT; mat1; mat2; regI; first coupler number; last coupler number]
RegionsOnInterface = zeros(nummat*(nummat+1)/2,6);

% Add master PBC nodes to all DG elements
Coordinates = [Coordinates; CornerXYZ];
MPC_BC = (numnp+1:numnp+ndm);

% Node number for each group of nodes: solid, masterPBC, and Lagrange
% multipliers
NodeTypeNum = [1 numnp+1 numnp+ndm+1]';
numnp = numnp + ndm;

for mat2 = 1:nummat
    for mat1 = 1:mat2
        
        matI = GetRegionsInt(mat1,mat2);
        numSIi = numEonPBC(matI);
        if numSIi > 0 % Create PBC pairs first
            
            locF = FacetsOnPBCNum(matI):(FacetsOnPBCNum(matI+1)-1);
            facs = FacetsOnPBC(locF);
            SurfacesIi = ElementsOnFacet(facs,:);
            SurfacesIi = ReverseFacets(SurfacesIi,NodesOnElement,Coordinates,numSIi,ndm);
            ElementsOnFacet(facs,:) = SurfacesIi;
            SurfacesI(numSI+1:numSI+numSIi,5:8) = SurfacesIi;
            numel_old = numel;
            if usemany
                CZprop = CZpropall{matI};
            end
            [NodesOnElement,RegionOnElement,nen,numel,nummat,MatTypeTable,MateT] = ...
             FormDGPBC(SurfacesIi,NodesOnElement,RegionOnElement,Coordinates,numSIi,nen_bulk,ndm,numel,nummat,6, ...
                    MPC_BC,9,0,CZprop,MatTypeTable,MateT);
            if numel > numel_old
            RegionsOnInterface(nummat-nummatCG,:) = [nummat mat1 mat2 matI numel_old+1 numel];
            end
            
        elseif InterTypes(mat2,mat1) > 0 % just make regular DG couplers
            
            numSIi = numEonF(matI);
            locF = FacetsOnInterfaceNum(matI):(FacetsOnInterfaceNum(matI+1)-1);
            facs = FacetsOnInterface(locF);
            SurfacesIi = ElementsOnFacet(facs,:);
            SurfacesI(numSI+1:numSI+numSIi,5:8) = SurfacesIi;
            numSI = numSI + numSIi;
            numel_old = numel;
            if usemany
                CZprop = CZpropall{matI};
            end
            [NodesOnElement,RegionOnElement,nen,numel,nummat,MatTypeTable,MateT] = ...
             FormDG(SurfacesIi,NodesOnElement,RegionOnElement,Coordinates,numSIi,nen_bulk,ndm,numel,nummat,6, ...
                    iel,nonlin,CZprop,MatTypeTable,MateT);
            if numel > numel_old
            RegionsOnInterface(nummat-nummatCG,:) = [nummat mat1 mat2 matI numel_old+1 numel];
            end
            
        end
    end
end
RegionsOnInterface = RegionsOnInterface(1:nummat-nummatCG,:);
