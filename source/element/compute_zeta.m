% Computes the N-matrix and F[x],for periodic boundaries 
% only for 2D domains, 08/22/2018 
% Extended to 3D domains, 01/23/2019 % Sunday Aduloju

function [zetaX,zeta,NmatM] = compute_zeta(xlR,xlL,ulresM,xlresM,ndm)

if ndm == 3
%     nodenos= (size(xlR,2))/2 
   if size(xlR,2)== 8   % Q8
       nodenos = 4; 
   else  %T4
       nodenos = 3;
   end

    AllxR=xlR(1,1:nodenos);
    AllyR=xlR(2,1:nodenos);
    AllzR=xlR(3,1:nodenos);    
    xR_centroid = mean(AllxR);
    yR_centroid = mean(AllyR);
    zR_centroid = mean(AllzR);

    AllxL=xlL(1,1:nodenos);
    AllyL=xlL(2,1:nodenos);
    AllzL=xlL(3,1:nodenos);    
    xL_centroid = mean(AllxL);
    yL_centroid = mean(AllyL);
    zL_centroid = mean(AllzL);
  
    dx= xR_centroid - xL_centroid;
    dy= yR_centroid - yL_centroid;
    dz= zR_centroid - zL_centroid;
    
%     if dx ~= xlresM(1)
%             dx=0;
%      end
%      if dy ~= xlresM(5) 
%             dy=0;
%      end 
%      if dz ~= xlresM(9) 
%             dz=0;
%      end      
  
    zetaX = [dx
             dy
             dz]; 
        
    etazeta = reshape(ulresM,[3,3]);
    zeta= etazeta*zetaX;
%     NmatM=[dx  0 0  dy  0 0  dz 0 0
%            0   0 dx  0  0 dy 0  0 dz];
%    
    NmatM=[dx  0  0  dy  0   0   dz  0   0
           0  dx  0   0  dy  0    0  dz  0
           0   0  dx  0  0   dy   0  0   dz];   
       
else % For 2 dimensions
    dx =  xlR(1,2)- xlL(1,1);
    dy =  xlR(2,2) - xlL(2,1);

%      if dx ~= xlresM(1)
%             dx=0;
%      elseif dy ~= xlresM(4) 
%             dy=0;
%      else
%        fprintf('There is error in the selection of dx or dy\n');  
%     end  

     if dx ~= xlresM(1)
            dx=0;
     end
     if dy ~= xlresM(4) 
            dy=0;
     end
     
    zetaX = [dx
             dy];
    etazeta = reshape(ulresM,[2,2]);
    zeta= etazeta*zetaX;
    
  % compute N-matrix  
    NmatM=[dx  0  dy  0
           0  dx  0  dy];
end
     