function [MPListN,MPListC,numMP,maxMPC] = PBCrref(PBCList,numMPC,numnp)

% Form MPC array in CPR form
MPCarray = spalloc(numMPC+1,numnp,numMPC*3);
for j = 1:numMPC
    MPCarray(j,PBCList(j,1:3)) = [1 -1 -1]; %#ok<SPRIX>
end
% MPCarray(numMPC+1,1) = 1;
% [R,jb] = rref(MPCarray); %Reduced row echelon form (Gauss-Jordan elimination)
[R,jb] = frref(MPCarray); %Reduced row echelon form (Gauss-Jordan elimination), sparse version, https://www.mathworks.com/matlabcentral/fileexchange/21583-fast-reduced-row-echelon-form
R = round(R); % round the result to remove small numbers near zero
[row,col,v] = find(R);

% Convert R to CSR, which is actually the MPCList that we want
numMP = length(jb);
MPListN = zeros(numMP,6); % nodes involved in the constraint; assume no more than 6 per equation
MPListC = zeros(numMP,6); % coefficients associated with the equation, typically +-1
numMP2 = zeros(numMP,1); % number of nodes in each constraint equation
for j = 1:nnz(R) % loop over entries in R, random order
    r = row(j); c = col(j); s = v(j); % row is the equation number, column is the global node number, entry in the matrix is the coefficient
    n = numMP2(r) + 1;
    numMP2(r) = n; % expand the count for the equation
    MPListN(r,n) = c;
    MPListC(r,n) = s;
end
% Shrink the arrays to the maximum number of nodes per equation
maxMPC = max(numMP2);
MPListN = MPListN(:,1:maxMPC);
MPListC = MPListC(:,1:maxMPC);