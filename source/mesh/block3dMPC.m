function [MPCList,numMPC,NodeBC,numBC] = block3dMPC(rinc,sinc,tinc)
     
% Boundary conditions
numnp = (rinc+1)*(sinc+1)*(tinc+1);
FIX = 1;
FRONT = (numnp-(rinc+1)*(sinc+1)+1); %z
RIGHT = (rinc+1); %x
TOP = (rinc+1)*(sinc)+1; %y
TOPRIGHT = TOP + RIGHT - 1; %xy
FRONTRIGHT = FRONT + RIGHT - 1; %xz
TOPFRONT = FRONT + TOP - 1; %yz
TOPFRONTRIGHT = numnp; %yz
NodeBC = [FIX 1 0
          FIX 2 0
          FIX 3 0
          RIGHT 2 0
          RIGHT 3 0
          TOP 3 0
          ];
numBC = size(NodeBC,1);

% Create periodic boundary conditions
dx = 1;
dy = rinc+1;
dr = rinc+1;
ds = sinc+1;
dt = tinc+1;
dz = ds*dt;
dxy = TOP - 1;
PBCListx = [(RIGHT:dy:TOPFRONTRIGHT)' (FIX:dy:TOPFRONT)' RIGHT*ones(dz,1) FIX*ones(dz,1)]; %yz
% remove repeats
RIGHTface = 2:dz;
PBCListx = PBCListx(RIGHTface,:);
PBCListy = zeros(dr*dt,4); %xz
ind = 0;
TOP2 = TOP - 1;
FIX2 = 0;
for j = 1:dt
    for i = 1:dr
        TOP2 = TOP2 + 1;
        FIX2 = FIX2 + 1;
        ind = ind + 1;
        PBCListy(ind,:) = [TOP2 FIX2 TOP FIX];
    end
    TOP2 = TOP2 + TOP - 1;
    FIX2 = FIX2 + TOP - 1;
end
% remove repeats
TOPface = 1:dr*dt;
TOPface = setdiff(TOPface,(dr:dr:dr*dt));
TOPface = setdiff(TOPface,1);
PBCListy = PBCListy(TOPface,:);
PBCListz = [(FRONT:dx:TOPFRONTRIGHT)' (FIX:dx:TOPRIGHT)' FRONT*ones(dr*ds,1) FIX*ones(dr*ds,1)]; %xy
PBCListz = PBCListz(1:dr*ds-dr,:);
% remove repeats
FRONTface = 1:dr*ds;
FRONTface = setdiff(FRONTface,(dr*ds-dr:dr*ds));
FRONTface = setdiff(FRONTface,(dr:dr:dr*ds-dr));
FRONTface = setdiff(FRONTface,1);
PBCListz = PBCListz(FRONTface,:);
PBCList = [PBCListx; PBCListy; PBCListz];
MPCListx = [PBCListx(:,1:2) ones(length(PBCListx),1)*[1 0 0]
            PBCListx(1,3:4) [1 0 0]];
MPCListy = [PBCListy(:,1:2) ones(length(PBCListy),1)*[0 1 0]
            PBCListy(1,3:4) [0 1 0]];
MPCListz = [PBCListz(:,1:2) ones(length(PBCListz),1)*[0 0 1]
            PBCListz(1,3:4) [0 0 1]];
MPCList = [MPCListx; MPCListy; MPCListz];
numMPC = length(MPCList);