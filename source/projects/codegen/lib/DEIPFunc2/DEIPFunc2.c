/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * DEIPFunc2.c
 *
 * Code generation for function 'DEIPFunc2'
 *
 */

/* Include files */
#include <math.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "DEIPFunc2.h"
#include "DEIPFunc2_emxutil.h"
#include "sparse1.h"
#include "norm.h"
#include "diag1.h"
#include "parenAssign2D.h"
#include "ifWhileCond.h"
#include "any.h"
#include "sort1.h"
#include "not.h"
#include "eml_setop.h"
#include "sum.h"
#include "sparse.h"
#include "ismember.h"
#include "issorted.h"
#include "sign.h"
#include "abs.h"
#include "sortLE.h"
#include <stdio.h>

/* Function Declarations */
static double rt_roundd_snf(double u);

/* Function Definitions */
static double rt_roundd_snf(double u)
{
  double y;
  if (fabs(u) < 4.503599627370496E+15) {
    if (u >= 0.5) {
      y = floor(u + 0.5);
    } else if (u > -0.5) {
      y = u * 0.0;
    } else {
      y = ceil(u - 0.5);
    }
  } else {
    y = u;
  }

  return y;
}

void DEIPFunc2(emxArray_real_T *InterTypes, emxArray_real_T *NodesOnElement,
               const emxArray_real_T *RegionOnElement, emxArray_real_T
               *Coordinates, double *numnp, double numel, double nummat, double
               nen, double ndm, double usePBC, const double *numMPC, const
               emxArray_real_T *MPCList, double *numEonB, emxArray_real_T
               *numEonF, emxArray_real_T *ElementsOnBoundary, double *numSI,
               emxArray_real_T *ElementsOnFacet, coder_internal_sparse
               *ElementsOnNode, coder_internal_sparse *ElementsOnNodeDup,
               emxArray_real_T *ElementsOnNodeNum, double *numfac,
               emxArray_real_T *ElementsOnNodeNum2, double *numinttype,
               emxArray_real_T *FacetsOnElement, emxArray_real_T
               *FacetsOnElementInt, emxArray_real_T *FacetsOnInterface,
               emxArray_real_T *FacetsOnInterfaceNum, coder_internal_sparse
               *FacetsOnNode, coder_internal_sparse *FacetsOnNodeCut,
               coder_internal_sparse *FacetsOnNodeInt, emxArray_real_T
               *FacetsOnNodeNum, emxArray_real_T *NodeCGDG,
               coder_internal_sparse *NodeReg, emxArray_real_T *NodesOnElementCG,
               emxArray_real_T *NodesOnElementDG, emxArray_real_T
               *NodesOnInterface, double *NodesOnInterfaceNum, double *numCL,
               emxArray_real_T *NodesOnPBC, emxArray_real_T *NodesOnPBCnum,
               emxArray_real_T *NodesOnLink, emxArray_real_T *NodesOnLinknum,
               emxArray_real_T *numEonPBC, emxArray_real_T *FacetsOnPBC,
               emxArray_real_T *FacetsOnPBCNum, emxArray_real_T
               *FacetsOnIntMinusPBC, emxArray_real_T *FacetsOnIntMinusPBCNum)
{
  int i21;
  int loop_ub;
  emxArray_real_T *ElementsOnNodePBCNum;
  emxArray_real_T *Coordinates3;
  int i22;
  int b_loop_ub;
  emxArray_real_T *NodesOnElementPBC;
  emxArray_real_T *setnPBC;
  emxArray_boolean_T *elems;
  emxArray_real_T *a;
  emxArray_int32_T *idx;
  emxArray_int32_T *iwork;
  emxArray_real_T *ycol;
  emxArray_real_T *b_NodesOnPBCnum;
  int nb;
  int k0;
  int b_idx;
  int notdone;
  boolean_T exitg2;
  int numPBC;
  double i1;
  long long q0;
  double numA;
  long long qY;
  long long mat;
  emxArray_real_T *b;
  emxArray_int64_T *inter;
  boolean_T old;
  emxArray_real_T *setAll;
  int k;
  emxArray_int32_T *ii;
  int kEnd;
  double reg;
  long long b_qY;
  double absxk;
  emxArray_real_T *ElementsOnNodeRow;
  emxArray_real_T *ElementsOnNodePBC_d;
  emxArray_int32_T *ElementsOnNodePBC_rowidx;
  int n;
  emxArray_int32_T *internodes_rowidx;
  coder_internal_sparse_2 NodeRegSum;
  emxArray_int32_T *j;
  int i23;
  emxArray_int32_T *t0_colidx;
  emxArray_int32_T *t0_rowidx;
  emxArray_real_T *b_setAll;
  emxArray_real_T *NodeRegInd;
  emxArray_real_T *b_NodeRegInd;
  emxArray_real_T *c_NodeRegInd;
  emxArray_real_T *d_NodeRegInd;
  emxArray_real_T *e_NodeRegInd;
  double nodes[20];
  int i;
  int nume;
  emxArray_real_T *ElementsOnNodePBCRow;
  int i2;
  int b_j;
  int pEnd;
  int p;
  int q;
  unsigned int nri;
  int exponent;
  double links[20];
  double nodeloc_data[9];
  int exitg1;
  int nodes_size[1];
  double nodes_data[20];
  double links2_data[20];
  int links2_size[1];
  int ia_data[20];
  int ia_size[1];
  int ib_size[1];
  int b_exponent;
  coder_internal_sparse expl_temp;
  coder_internal_sparse b_ElementsOnNodePBCRow;
  double numFPBC;
  emxArray_int32_T *r0;
  emxArray_real_T *FacetsOnNodeInd;
  signed char nloop3[6];
  signed char nloop4[8];
  emxArray_real_T *ElementsOnNodeA;
  emxArray_real_T *ElementsOnNodeB;
  emxArray_boolean_T *r1;
  emxArray_int32_T *r2;
  emxArray_int32_T *r3;
  emxArray_int32_T *b_ycol;
  emxArray_int32_T *c_ycol;
  emxArray_real_T *b_ElementsOnFacet;
  double nodeA;
  double nodeB;
  int i24;
  boolean_T guard1 = false;
  boolean_T guard2 = false;
  emxArray_real_T b_ElementsOnNodeB;
  int c_ElementsOnNodeB[1];
  int c_exponent;
  double nodeD;
  double regA;
  double regB;
  double regI;
  double nodeD2;
  emxArray_boolean_T *internodes_d;
  emxArray_boolean_T *t2_d;
  coder_internal_sparse_1 b_expl_temp;
  emxArray_real_T *setPBC;
  boolean_T guard3 = false;
  int d_ElementsOnNodeB[1];
  int d_exponent;
  int e_exponent;
  emxArray_real_T *interreg2;
  coder_internal_sparse_4 c_expl_temp;
  int c_loop_ub;
  int nx;
  boolean_T x_data[9];
  signed char ii_data[9];
  signed char node_dup_tmp_data[9];
  int b_nx;
  unsigned int iSec2;
  signed char b_nodeloc_data[4];
  double ycol_data[4];
  int c_nx;
  int d_nx;
  (void)ndm;
  (void)numMPC;

  /*  Program: DEIProgram2 */
  /*  Tim Truster */
  /*  08/15/2014 */
  /*  */
  /*  Script to convert CG mesh (e.g. from block program output) into DG mesh */
  /*  Addition of modules to insert interfaces selectively between materials */
  /*   */
  /*  Input: matrix InterTypes(nummat,nummat) for listing the interface */
  /*  material types to assign; only lower triangle is accessed; value 0 means */
  /*  interface is not inserted between those materials (including diagonal). */
  /*  */
  /*  Numbering of interface types (matI) is like: */
  /*  1 */
  /*  2 3 */
  /*  4 5 6 ... */
  /*  */
  /*  Output: */
  /*  Actual list of used interfaces is SurfacesI and numCL, new BC arrays and */
  /*  load arrays */
  /*  Total list of interfaces is in numIfac and Interfac, although there is */
  /*  no designation on whether the interfaces are active or not. These are */
  /*  useful for assigning different DG element types to different interfaces. */
  /*  Revisions: */
  /*  02/10/2017: adding periodic boundary conditions (PBC); uses both zipped mesh */
  /*  (collapsing linked PBC nodes into a single node and updating */
  /*  connectivity), and ghost elements (leaving original mesh, added elements */
  /*  along PBC edges that connect across the domain); both result in a 'torus' */
  /*  mesh. */
  /*  Revisions: */
  /*  04/04/2017: adding periodic BC in the form of multi-point constraints, */
  /*  which do not require the domain to be a cube. Still uses the zipped mesh, */
  /*  but does not add ghost elements. Instead, node duplication is performed */
  /*  directly on the zipped mesh, and MPC elements are applied outside of this */
  /*  subroutine. */
  /*  % flags for clearing out intermediate variables in this script; turned on */
  /*  % by default */
  /*  currvariables = who(); */
  /*  clearinter = 1; */
  *numSI = 0.0;

  /*  % Test for Octave vs Matlab compatibility */
  /*  isOctave = exist('OCTAVE_VERSION', 'builtin') ~= 0; */
  /*  ElementsOnNode = zeros(maxel,numnp); % Array of elements connected to nodes */
  /*  ElementsOnNodeDup = zeros(maxel,numnp); % actual node ID given to element for an original node; general counterpart of NodeMat */
  i21 = ElementsOnNodeNum->size[0];
  loop_ub = (int)*numnp;
  ElementsOnNodeNum->size[0] = loop_ub;
  emxEnsureCapacity_real_T(ElementsOnNodeNum, i21);
  for (i21 = 0; i21 < loop_ub; i21++) {
    ElementsOnNodeNum->data[i21] = 0.0;
  }

  /*  if ~exist('usePBC','var') */
  /*      usePBC = 0; */
  /*  end */
  emxInit_real_T(&ElementsOnNodePBCNum, 1);
  if (usePBC != 0.0) {
    /*  create extra arrays for zipped mesh */
    /*  ElementsOnNodePBC = zeros(maxel,numnp); % Array of elements connected to nodes */
    /*  ElementsOnNodePBCDup = zeros(maxel,numnp); % actual node ID given to element for an original node; general counterpart of NodeMat */
    i21 = ElementsOnNodePBCNum->size[0];
    ElementsOnNodePBCNum->size[0] = loop_ub;
    emxEnsureCapacity_real_T(ElementsOnNodePBCNum, i21);
    for (i21 = 0; i21 < loop_ub; i21++) {
      ElementsOnNodePBCNum->data[i21] = 0.0;
    }
  } else {
    i21 = ElementsOnNodePBCNum->size[0];
    ElementsOnNodePBCNum->size[0] = 1;
    emxEnsureCapacity_real_T(ElementsOnNodePBCNum, i21);
    ElementsOnNodePBCNum->data[0] = 0.0;
  }

  emxInit_real_T(&Coordinates3, 2);

  /*  NodeReg = zeros(numnp,nummat); */
  i21 = Coordinates3->size[0] * Coordinates3->size[1];
  i22 = (int)(numel * nen);
  Coordinates3->size[0] = i22;
  Coordinates3->size[1] = 2;
  emxEnsureCapacity_real_T(Coordinates3, i21);
  b_loop_ub = i22 << 1;
  for (i21 = 0; i21 < b_loop_ub; i21++) {
    Coordinates3->data[i21] = 0.0;
  }

  i21 = NodesOnElementCG->size[0] * NodesOnElementCG->size[1];
  NodesOnElementCG->size[0] = NodesOnElement->size[0];
  NodesOnElementCG->size[1] = NodesOnElement->size[1];
  emxEnsureCapacity_real_T(NodesOnElementCG, i21);
  b_loop_ub = NodesOnElement->size[0] * NodesOnElement->size[1];
  for (i21 = 0; i21 < b_loop_ub; i21++) {
    NodesOnElementCG->data[i21] = NodesOnElement->data[i21];
  }

  emxInit_real_T(&NodesOnElementPBC, 2);
  if (usePBC != 0.0) {
    /*  Add space in connectivity for ghost elements */
    /*      if nen == 3 */
    /*          NodesOnElementCG = [NodesOnElementCG zeros(numel,4-nen) */
    /*                              zeros(numPBC*2,4)]; */
    /*          nen = 4; */
    /*      elseif nen == 6 */
    /*          NodesOnElementCG = [NodesOnElementCG zeros(numel,9-nen) */
    /*                              zeros(numPBC*2,9)]; */
    /*          nen = 9; */
    /*      end */
    i21 = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1];
    NodesOnElementPBC->size[0] = NodesOnElement->size[0];
    NodesOnElementPBC->size[1] = NodesOnElement->size[1];
    emxEnsureCapacity_real_T(NodesOnElementPBC, i21);
    b_loop_ub = NodesOnElement->size[0] * NodesOnElement->size[1];
    for (i21 = 0; i21 < b_loop_ub; i21++) {
      NodesOnElementPBC->data[i21] = NodesOnElement->data[i21];
    }

    /*  Connectivity that is zipped, having PBC nodes condensed together */
    /*      RegionOnElement = [RegionOnElement; zeros(numPBC*2,1)]; */
  } else {
    i21 = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1];
    NodesOnElementPBC->size[0] = 1;
    NodesOnElementPBC->size[1] = 1;
    emxEnsureCapacity_real_T(NodesOnElementPBC, i21);
    NodesOnElementPBC->data[0] = 0.0;

    /*  RegionOnElement = RegionOnElement; */
  }

  /*  Set up PBC data structures */
  emxInit_real_T(&setnPBC, 1);
  emxInit_boolean_T(&elems, 1);
  emxInit_real_T(&a, 2);
  emxInit_int32_T(&idx, 1);
  emxInit_int32_T(&iwork, 1);
  emxInit_real_T(&ycol, 1);
  emxInit_real_T(&b_NodesOnPBCnum, 1);
  if (usePBC != 0.0) {
    /*  Lists of node pairs in convenient table format */
    i21 = NodesOnPBC->size[0] * NodesOnPBC->size[1];
    NodesOnPBC->size[0] = 4;
    NodesOnPBC->size[1] = loop_ub;
    emxEnsureCapacity_real_T(NodesOnPBC, i21);
    nb = loop_ub << 2;
    for (i21 = 0; i21 < nb; i21++) {
      NodesOnPBC->data[i21] = 0.0;
    }

    /*  nodes that are paired up */
    i21 = NodesOnPBCnum->size[0];
    NodesOnPBCnum->size[0] = loop_ub;
    emxEnsureCapacity_real_T(NodesOnPBCnum, i21);
    for (i21 = 0; i21 < loop_ub; i21++) {
      NodesOnPBCnum->data[i21] = 0.0;
    }

    /*  number of nodes with common pairing */
    i21 = NodesOnLink->size[0] * NodesOnLink->size[1];
    NodesOnLink->size[0] = 4;
    NodesOnLink->size[1] = loop_ub;
    emxEnsureCapacity_real_T(NodesOnLink, i21);
    for (i21 = 0; i21 < nb; i21++) {
      NodesOnLink->data[i21] = 0.0;
    }

    /*  IDs of PBC referring to node */
    i21 = NodesOnLinknum->size[0];
    NodesOnLinknum->size[0] = loop_ub;
    emxEnsureCapacity_real_T(NodesOnLinknum, i21);
    for (i21 = 0; i21 < loop_ub; i21++) {
      NodesOnLinknum->data[i21] = 0.0;
    }

    /*  number of PBC referring to node */
    /*  Handle the corners first */
    if (usePBC == 2.0) {
      /*      PBCList = MPCList; */
      numPBC = MPCList->size[0];
    } else {
      /*      PBCList = MPCList; */
      numPBC = MPCList->size[0];
      b_loop_ub = MPCList->size[0];
      i21 = a->size[0] * a->size[1];
      a->size[0] = b_loop_ub;
      a->size[1] = 2;
      emxEnsureCapacity_real_T(a, i21);
      for (i21 = 0; i21 < b_loop_ub; i21++) {
        a->data[i21] = MPCList->data[i21 + MPCList->size[0] * 2];
      }

      for (i21 = 0; i21 < b_loop_ub; i21++) {
        a->data[i21 + a->size[0]] = MPCList->data[i21 + MPCList->size[0] * 3];
      }

      i21 = MPCList->size[0];
      emxInit_real_T(&b, 2);
      if (i21 == 0) {
        b_loop_ub = MPCList->size[0];
        i21 = b->size[0] * b->size[1];
        b->size[0] = b_loop_ub;
        b->size[1] = 2;
        emxEnsureCapacity_real_T(b, i21);
        for (i21 = 0; i21 < b_loop_ub; i21++) {
          b->data[i21] = MPCList->data[i21 + MPCList->size[0] * 2];
        }

        for (i21 = 0; i21 < b_loop_ub; i21++) {
          b->data[i21 + b->size[0]] = MPCList->data[i21 + MPCList->size[0] * 3];
        }

        iwork->size[0] = 0;
      } else {
        b_loop_ub = MPCList->size[0];
        i21 = b->size[0] * b->size[1];
        b->size[0] = b_loop_ub;
        b->size[1] = 2;
        emxEnsureCapacity_real_T(b, i21);
        for (i21 = 0; i21 < b_loop_ub; i21++) {
          b->data[i21] = MPCList->data[i21 + MPCList->size[0] * 2];
        }

        for (i21 = 0; i21 < b_loop_ub; i21++) {
          b->data[i21 + b->size[0]] = MPCList->data[i21 + MPCList->size[0] * 3];
        }

        n = MPCList->size[0] + 1;
        b_loop_ub = MPCList->size[0];
        i21 = idx->size[0];
        idx->size[0] = b_loop_ub;
        emxEnsureCapacity_int32_T(idx, i21);
        for (i21 = 0; i21 < b_loop_ub; i21++) {
          idx->data[i21] = 0;
        }

        i21 = MPCList->size[0];
        i22 = iwork->size[0];
        iwork->size[0] = i21;
        emxEnsureCapacity_int32_T(iwork, i22);
        i21 = MPCList->size[0];
        for (k = 1; k <= i21 - 1; k += 2) {
          if (sortLE(a, k, k + 1)) {
            idx->data[k - 1] = k;
            idx->data[k] = k + 1;
          } else {
            idx->data[k - 1] = k + 1;
            idx->data[k] = k;
          }
        }

        i21 = MPCList->size[0];
        if ((i21 & 1) != 0) {
          i21 = MPCList->size[0] - 1;
          i22 = MPCList->size[0];
          idx->data[i21] = i22;
        }

        i = 2;
        while (i < n - 1) {
          i2 = i << 1;
          b_j = 1;
          for (pEnd = 1 + i; pEnd < n; pEnd = nb + i) {
            p = b_j;
            q = pEnd;
            nb = b_j + i2;
            if (nb > n) {
              nb = n;
            }

            k = 0;
            kEnd = nb - b_j;
            while (k + 1 <= kEnd) {
              if (sortLE(a, idx->data[p - 1], idx->data[q - 1])) {
                iwork->data[k] = idx->data[p - 1];
                p++;
                if (p == pEnd) {
                  while (q < nb) {
                    k++;
                    iwork->data[k] = idx->data[q - 1];
                    q++;
                  }
                }
              } else {
                iwork->data[k] = idx->data[q - 1];
                q++;
                if (q == nb) {
                  while (p < pEnd) {
                    k++;
                    iwork->data[k] = idx->data[p - 1];
                    p++;
                  }
                }
              }

              k++;
            }

            for (k = 0; k < kEnd; k++) {
              idx->data[(b_j + k) - 1] = iwork->data[k];
            }

            b_j = nb;
          }

          i = i2;
        }

        i21 = MPCList->size[0] - 1;
        i22 = MPCList->size[0];
        i23 = ycol->size[0];
        ycol->size[0] = i22;
        emxEnsureCapacity_real_T(ycol, i23);
        for (i = 0; i <= i21; i++) {
          ycol->data[i] = b->data[idx->data[i] - 1];
        }

        for (i = 0; i <= i21; i++) {
          b->data[i] = ycol->data[i];
        }

        for (i = 0; i <= i21; i++) {
          ycol->data[i] = b->data[(idx->data[i] + b->size[0]) - 1];
        }

        for (i = 0; i <= i21; i++) {
          b->data[i + b->size[0]] = ycol->data[i];
        }

        i21 = ycol->size[0];
        ycol->size[0] = idx->size[0];
        emxEnsureCapacity_real_T(ycol, i21);
        b_loop_ub = idx->size[0];
        for (i21 = 0; i21 < b_loop_ub; i21++) {
          ycol->data[i21] = idx->data[i21];
        }

        nb = -1;
        i21 = MPCList->size[0];
        k = 0;
        while (k + 1 <= i21) {
          k0 = k;
          do {
            exitg1 = 0;
            k++;
            if (k + 1 > i21) {
              exitg1 = 1;
            } else {
              old = false;
              b_j = 0;
              exitg2 = false;
              while ((!exitg2) && (b_j < 2)) {
                i1 = b->data[k0 + b->size[0] * b_j];
                reg = b->data[k + b->size[0] * b_j];
                absxk = fabs(reg / 2.0);
                if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
                  if (absxk <= 2.2250738585072014E-308) {
                    absxk = 4.94065645841247E-324;
                  } else {
                    frexp(absxk, &notdone);
                    absxk = ldexp(1.0, notdone - 53);
                  }
                } else {
                  absxk = rtNaN;
                }

                if ((fabs(reg - i1) < absxk) || (rtIsInf(i1) && rtIsInf(reg) &&
                     ((i1 > 0.0) == (reg > 0.0)))) {
                  b_j++;
                } else {
                  old = true;
                  exitg2 = true;
                }
              }

              if (old) {
                exitg1 = 1;
              }
            }
          } while (exitg1 == 0);

          nb++;
          b->data[nb] = b->data[k0];
          b->data[nb + b->size[0]] = b->data[k0 + b->size[0]];
          ycol->data[nb] = ycol->data[k0];
        }

        if (1 > nb + 1) {
          b_loop_ub = -1;
        } else {
          b_loop_ub = nb;
        }

        i21 = a->size[0] * a->size[1];
        a->size[0] = b_loop_ub + 1;
        a->size[1] = 2;
        emxEnsureCapacity_real_T(a, i21);
        for (i21 = 0; i21 <= b_loop_ub; i21++) {
          a->data[i21] = b->data[i21];
        }

        for (i21 = 0; i21 <= b_loop_ub; i21++) {
          a->data[i21 + a->size[0]] = b->data[i21 + b->size[0]];
        }

        i21 = b->size[0] * b->size[1];
        b->size[0] = a->size[0];
        b->size[1] = 2;
        emxEnsureCapacity_real_T(b, i21);
        b_loop_ub = a->size[0] * a->size[1];
        for (i21 = 0; i21 < b_loop_ub; i21++) {
          b->data[i21] = a->data[i21];
        }

        i21 = iwork->size[0];
        iwork->size[0] = nb + 1;
        emxEnsureCapacity_int32_T(iwork, i21);
        for (k = 0; k <= nb; k++) {
          iwork->data[k] = (int)ycol->data[k];
        }
      }

      i21 = ycol->size[0];
      ycol->size[0] = iwork->size[0];
      emxEnsureCapacity_real_T(ycol, i21);
      b_loop_ub = iwork->size[0];
      for (i21 = 0; i21 < b_loop_ub; i21++) {
        ycol->data[i21] = iwork->data[i21];
      }

      if (b->size[0] == 2) {
        NodesOnPBC->data[((int)b->data[0] - 1) << 2] = b->data[b->size[0]];
        NodesOnPBCnum->data[(int)b->data[0] - 1] = 1.0;
        NodesOnPBC->data[((int)b->data[b->size[0]] - 1) << 2] = b->data[0];
        NodesOnPBCnum->data[(int)b->data[b->size[0]] - 1] = 1.0;
        NodesOnLink->data[((int)b->data[0] - 1) << 2] = -ycol->data[0];
        NodesOnLinknum->data[(int)b->data[0] - 1] = 1.0;
        NodesOnLink->data[((int)b->data[b->size[0]] - 1) << 2] = -ycol->data[0];
        NodesOnLinknum->data[(int)b->data[b->size[0]] - 1] = 1.0;
        NodesOnPBC->data[((int)b->data[1] - 1) << 2] = b->data[1 + b->size[0]];
        NodesOnPBCnum->data[(int)b->data[1] - 1] = 1.0;
        NodesOnPBC->data[1 + (((int)b->data[1 + b->size[0]] - 1) << 2)] =
          b->data[1];
        NodesOnPBCnum->data[(int)b->data[1 + b->size[0]] - 1] = 2.0;
        NodesOnLink->data[((int)b->data[1] - 1) << 2] = -ycol->data[1];
        NodesOnLinknum->data[(int)b->data[1] - 1] = 1.0;
        NodesOnLink->data[1 + (((int)b->data[1 + b->size[0]] - 1) << 2)] =
          -ycol->data[1];
        NodesOnLinknum->data[(int)b->data[1 + b->size[0]] - 1] = 2.0;
      }

      emxFree_real_T(&b);
    }

    /*  Now do the rest of the nodes */
    for (k0 = 0; k0 < numPBC; k0++) {
      numA = NodesOnPBCnum->data[(int)MPCList->data[k0] - 1];
      if (1.0 > NodesOnPBCnum->data[(int)MPCList->data[k0] - 1]) {
        i21 = 0;
      } else {
        i21 = (int)NodesOnPBCnum->data[(int)MPCList->data[k0] - 1];
      }

      /*  make sure pair isn't already in the list for nodeA */
      /*      if isOctave */
      i1 = MPCList->data[k0 + MPCList->size[0]];
      old = false;
      k = 0;
      exitg2 = false;
      while ((!exitg2) && (k <= i21 - 1)) {
        kEnd = (int)MPCList->data[k0];
        reg = NodesOnPBC->data[k + ((kEnd - 1) << 2)];
        absxk = fabs(reg / 2.0);
        if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
          if (absxk <= 2.2250738585072014E-308) {
            absxk = 4.94065645841247E-324;
          } else {
            frexp(absxk, &b_idx);
            absxk = ldexp(1.0, b_idx - 53);
          }
        } else {
          absxk = rtNaN;
        }

        kEnd = (int)MPCList->data[k0];
        if (fabs(NodesOnPBC->data[k + ((kEnd - 1) << 2)] - i1) < absxk) {
          old = true;
          exitg2 = true;
        } else if (rtIsInf(i1) && rtIsInf(reg)) {
          kEnd = (int)MPCList->data[k0];
          if ((i1 > 0.0) == (NodesOnPBC->data[k + ((kEnd - 1) << 2)] > 0.0)) {
            old = true;
            exitg2 = true;
          } else {
            k++;
          }
        } else {
          k++;
        }
      }

      /*      else */
      /*      old = ismembc(nodesAB(2),currPBC); */
      /*      end */
      if (!old) {
        /*  add the pair to nodeA, expand by one */
        NodesOnPBC->data[((int)(NodesOnPBCnum->data[(int)MPCList->data[k0] - 1]
          + 1.0) + (((int)MPCList->data[k0] - 1) << 2)) - 1] = MPCList->data[k0
          + MPCList->size[0]];
        NodesOnPBCnum->data[(int)MPCList->data[k0] - 1]++;

        /*  Record which PBC ID refers to these nodes */
        numA = NodesOnLinknum->data[(int)MPCList->data[k0] - 1] + 1.0;
        NodesOnLink->data[((int)(NodesOnLinknum->data[(int)MPCList->data[k0] - 1]
          + 1.0) + (((int)MPCList->data[k0] - 1) << 2)) - 1] = 1.0 + (double)k0;
        NodesOnLinknum->data[(int)MPCList->data[k0] - 1]++;
      }

      if (1.0 > NodesOnPBCnum->data[(int)MPCList->data[k0 + MPCList->size[0]] -
          1]) {
        i21 = 0;
      } else {
        i21 = (int)NodesOnPBCnum->data[(int)MPCList->data[k0 + MPCList->size[0]]
          - 1];
      }

      /*  make sure pair isn't already in the list for nodeB */
      /*      if isOctave */
      i1 = MPCList->data[k0];
      old = false;
      k = 0;
      exitg2 = false;
      while ((!exitg2) && (k <= i21 - 1)) {
        kEnd = (int)MPCList->data[k0 + MPCList->size[0]];
        reg = NodesOnPBC->data[k + ((kEnd - 1) << 2)];
        absxk = fabs(reg / 2.0);
        if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
          if (absxk <= 2.2250738585072014E-308) {
            absxk = 4.94065645841247E-324;
          } else {
            frexp(absxk, &nume);
            absxk = ldexp(1.0, nume - 53);
          }
        } else {
          absxk = rtNaN;
        }

        kEnd = (int)MPCList->data[k0 + MPCList->size[0]];
        if (fabs(NodesOnPBC->data[k + ((kEnd - 1) << 2)] - i1) < absxk) {
          old = true;
          exitg2 = true;
        } else if (rtIsInf(i1) && rtIsInf(reg)) {
          kEnd = (int)MPCList->data[k0 + MPCList->size[0]];
          if ((i1 > 0.0) == (NodesOnPBC->data[k + ((kEnd - 1) << 2)] > 0.0)) {
            old = true;
            exitg2 = true;
          } else {
            k++;
          }
        } else {
          k++;
        }
      }

      /*      else */
      /*      old = ismembc(nodesAB(1),currPBC); */
      /*      end */
      if (!old) {
        /*  add the pair to nodeB, expand by one */
        NodesOnPBC->data[((int)(NodesOnPBCnum->data[(int)MPCList->data[k0 +
          MPCList->size[0]] - 1] + 1.0) + (((int)MPCList->data[k0 +
          MPCList->size[0]] - 1) << 2)) - 1] = MPCList->data[k0];
        NodesOnPBCnum->data[(int)MPCList->data[k0 + MPCList->size[0]] - 1]++;

        /*  Record which PBC ID refers to these nodes */
        NodesOnLinknum->data[(int)MPCList->data[k0 + MPCList->size[0]] - 1]++;
        NodesOnLink->data[((int)numA + (((int)MPCList->data[k0 + MPCList->size[0]]
          - 1) << 2)) - 1] = 1.0 + (double)k0;
      }
    }

    /*  Find additional connections for nodes with pairs of pairs; this WILL find */
    /*  the extra pairs that are usually deleted to remove linear dependence in */
    /*  the stiffness matrix, e.g. the repeated edges in 2d or 3d. */
    i21 = elems->size[0];
    elems->size[0] = NodesOnPBCnum->size[0];
    emxEnsureCapacity_boolean_T(elems, i21);
    b_loop_ub = NodesOnPBCnum->size[0];
    for (i21 = 0; i21 < b_loop_ub; i21++) {
      elems->data[i21] = (NodesOnPBCnum->data[i21] > 1.0);
    }

    k0 = elems->size[0];
    b_idx = 0;
    i21 = iwork->size[0];
    iwork->size[0] = elems->size[0];
    emxEnsureCapacity_int32_T(iwork, i21);
    notdone = 0;
    exitg2 = false;
    while ((!exitg2) && (notdone <= k0 - 1)) {
      if (elems->data[notdone]) {
        b_idx++;
        iwork->data[b_idx - 1] = notdone + 1;
        if (b_idx >= k0) {
          exitg2 = true;
        } else {
          notdone++;
        }
      } else {
        notdone++;
      }
    }

    if (elems->size[0] == 1) {
      if (b_idx == 0) {
        iwork->size[0] = 0;
      }
    } else if (1 > b_idx) {
      iwork->size[0] = 0;
    } else {
      i21 = iwork->size[0];
      iwork->size[0] = b_idx;
      emxEnsureCapacity_int32_T(iwork, i21);
    }

    i21 = ycol->size[0];
    ycol->size[0] = iwork->size[0];
    emxEnsureCapacity_real_T(ycol, i21);
    b_loop_ub = iwork->size[0];
    for (i21 = 0; i21 < b_loop_ub; i21++) {
      ycol->data[i21] = iwork->data[i21];
    }

    memset(&nodes[0], 0, 20U * sizeof(double));
    i21 = ycol->size[0];
    for (i = 0; i < i21; i++) {
      nodes[0] = ycol->data[i];
      if (NodesOnPBCnum->data[(int)ycol->data[i] - 1] > 0.0) {
        /*  pairs not already condensed */
        notdone = 1;
        numPBC = 1;

        /*  use a tree of connectivity to get to all node pairs involving MorePBC(i) */
        while (notdone != 0) {
          /*              nodesS = nodes(1:lenn); */
          i1 = (double)numPBC + 1.0;
          numA = (double)numPBC + 1.0;
          for (b_j = 0; b_j < numPBC; b_j++) {
            if (1.0 > NodesOnPBCnum->data[(int)nodes[b_j] - 1]) {
              b_loop_ub = 0;
            } else {
              b_loop_ub = (int)NodesOnPBCnum->data[(int)nodes[b_j] - 1];
            }

            numA = (i1 + (double)b_loop_ub) - 1.0;
            if (i1 > numA) {
              i22 = 1;
            } else {
              i22 = (int)i1;
            }

            i1 = nodes[b_j];
            for (i23 = 0; i23 < b_loop_ub; i23++) {
              nodes[(i22 + i23) - 1] = NodesOnPBC->data[i23 + (((int)i1 - 1) <<
                2)];
            }

            i1 = numA + 1.0;
          }

          if (1.0 > numA) {
            b_loop_ub = 0;
          } else {
            b_loop_ub = (int)numA;
          }

          i22 = idx->size[0];
          idx->size[0] = b_loop_ub;
          emxEnsureCapacity_int32_T(idx, i22);
          for (i22 = 0; i22 < b_loop_ub; i22++) {
            idx->data[i22] = 0;
          }

          if (b_loop_ub != 0) {
            i22 = iwork->size[0];
            iwork->size[0] = b_loop_ub;
            emxEnsureCapacity_int32_T(iwork, i22);
            i22 = b_loop_ub - 1;
            for (k = 1; k <= i22; k += 2) {
              if ((nodes[k - 1] <= nodes[k]) || rtIsNaN(nodes[k])) {
                idx->data[k - 1] = k;
                idx->data[k] = k + 1;
              } else {
                idx->data[k - 1] = k + 1;
                idx->data[k] = k;
              }
            }

            if ((b_loop_ub & 1) != 0) {
              idx->data[b_loop_ub - 1] = b_loop_ub;
            }

            k0 = 2;
            while (k0 < b_loop_ub) {
              i2 = k0 << 1;
              b_j = 1;
              for (pEnd = 1 + k0; pEnd < b_loop_ub + 1; pEnd = nb + k0) {
                p = b_j;
                q = pEnd - 1;
                nb = b_j + i2;
                if (nb > b_loop_ub + 1) {
                  nb = b_loop_ub + 1;
                }

                k = 0;
                kEnd = nb - b_j;
                while (k + 1 <= kEnd) {
                  if ((nodes[idx->data[p - 1] - 1] <= nodes[idx->data[q] - 1]) ||
                      rtIsNaN(nodes[idx->data[q] - 1])) {
                    iwork->data[k] = idx->data[p - 1];
                    p++;
                    if (p == pEnd) {
                      while (q + 1 < nb) {
                        k++;
                        iwork->data[k] = idx->data[q];
                        q++;
                      }
                    }
                  } else {
                    iwork->data[k] = idx->data[q];
                    q++;
                    if (q + 1 == nb) {
                      while (p < pEnd) {
                        k++;
                        iwork->data[k] = idx->data[p - 1];
                        p++;
                      }
                    }
                  }

                  k++;
                }

                for (k = 0; k < kEnd; k++) {
                  idx->data[(b_j + k) - 1] = iwork->data[k];
                }

                b_j = nb;
              }

              k0 = i2;
            }
          }

          i22 = setnPBC->size[0];
          setnPBC->size[0] = (signed char)b_loop_ub;
          emxEnsureCapacity_real_T(setnPBC, i22);
          for (k = 0; k < b_loop_ub; k++) {
            setnPBC->data[k] = nodes[idx->data[k] - 1];
          }

          k = 0;
          while ((k + 1 <= b_loop_ub) && rtIsInf(setnPBC->data[k]) &&
                 (setnPBC->data[k] < 0.0)) {
            k++;
          }

          kEnd = k;
          k = b_loop_ub - 1;
          while ((k + 1 >= 1) && rtIsNaN(setnPBC->data[k])) {
            k--;
          }

          k0 = b_loop_ub - k;
          while ((k + 1 >= 1) && rtIsInf(setnPBC->data[k]) && (setnPBC->data[k] >
                  0.0)) {
            k--;
          }

          i2 = (b_loop_ub - k) - k0;
          nb = -1;
          if (kEnd > 0) {
            nb = 0;
          }

          while (kEnd + 1 <= k + 1) {
            i1 = setnPBC->data[kEnd];
            do {
              exitg1 = 0;
              kEnd++;
              if (kEnd + 1 > k + 1) {
                exitg1 = 1;
              } else {
                absxk = fabs(i1 / 2.0);
                if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
                  if (absxk <= 2.2250738585072014E-308) {
                    absxk = 4.94065645841247E-324;
                  } else {
                    frexp(absxk, &exponent);
                    absxk = ldexp(1.0, exponent - 53);
                  }
                } else {
                  absxk = rtNaN;
                }

                if ((fabs(i1 - setnPBC->data[kEnd]) < absxk) || (rtIsInf
                     (setnPBC->data[kEnd]) && rtIsInf(i1) && ((setnPBC->
                       data[kEnd] > 0.0) == (i1 > 0.0)))) {
                } else {
                  exitg1 = 1;
                }
              }
            } while (exitg1 == 0);

            nb++;
            setnPBC->data[nb] = i1;
          }

          if (i2 > 0) {
            nb++;
            setnPBC->data[nb] = setnPBC->data[k + 1];
          }

          kEnd = (k + i2) + 1;
          for (b_j = 0; b_j <= k0 - 2; b_j++) {
            nb++;
            setnPBC->data[nb] = setnPBC->data[kEnd + b_j];
          }

          if (1 > nb + 1) {
            setnPBC->size[0] = 0;
          } else {
            i22 = setnPBC->size[0];
            setnPBC->size[0] = nb + 1;
            emxEnsureCapacity_real_T(setnPBC, i22);
          }

          if (setnPBC->size[0] == numPBC) {
            /*  starting list of node pairs matches the unique short list */
            b_loop_ub = setnPBC->size[0];
            for (i22 = 0; i22 < b_loop_ub; i22++) {
              nodes[i22] = setnPBC->data[i22];
            }

            notdone = 0;
          } else {
            /*  found some new pairs, try searching again */
            b_loop_ub = setnPBC->size[0];
            for (i22 = 0; i22 < b_loop_ub; i22++) {
              nodes[i22] = setnPBC->data[i22];
            }
          }

          numPBC = setnPBC->size[0];
        }

        if (numPBC + 1U > 20U) {
          i22 = 0;
          i23 = -1;
        } else {
          i22 = numPBC;
          i23 = 19;
        }

        b_loop_ub = (i23 - i22) + 1;
        if (0 <= b_loop_ub - 1) {
          memset(&nodes[i22], 0, (unsigned int)(b_loop_ub * (int)sizeof(double)));
        }

        /* clean it out */
        /*  record the maximum connected pairs into the NodesOnPBC for all */
        /*  the connected nodes in the set; also combine together the */
        /*  NodesOnLink at the same time */
        /*          lenn = length(nodes); */
        memset(&links[0], 0, 20U * sizeof(double));
        i1 = 1.0;
        numA = 1.0;
        for (b_j = 0; b_j < numPBC; b_j++) {
          i22 = (int)nodes[b_j] - 1;
          NodesOnPBCnum->data[i22] = -((double)numPBC - 1.0);

          /*  flag this node as already condensed */
          if (1 > numPBC) {
            b_loop_ub = 0;
          } else {
            b_loop_ub = numPBC;
          }

          nodes_size[0] = b_loop_ub;
          if (0 <= b_loop_ub - 1) {
            memcpy(&nodes_data[0], &nodes[0], (unsigned int)(b_loop_ub * (int)
                    sizeof(double)));
          }

          do_vectors(nodes_data, nodes_size, nodes[b_j], links2_data,
                     links2_size, ia_data, ia_size, ib_size);
          b_loop_ub = links2_size[0];
          for (i23 = 0; i23 < b_loop_ub; i23++) {
            NodesOnPBC->data[i23 + (i22 << 2)] = links2_data[i23];
          }

          numA = (i1 + NodesOnLinknum->data[i22]) - 1.0;
          if (1.0 > NodesOnLinknum->data[(int)nodes[b_j] - 1]) {
            b_loop_ub = 0;
          } else {
            b_loop_ub = (int)NodesOnLinknum->data[(int)nodes[b_j] - 1];
          }

          if (i1 > numA) {
            i22 = 1;
          } else {
            i22 = (int)i1;
          }

          for (i23 = 0; i23 < b_loop_ub; i23++) {
            links[(i22 + i23) - 1] = NodesOnLink->data[i23 + (((int)nodes[b_j] -
              1) << 2)];
          }

          i1 = numA + 1.0;
        }

        if (1.0 > numA) {
          b_loop_ub = 0;
        } else {
          b_loop_ub = (int)numA;
        }

        i22 = idx->size[0];
        idx->size[0] = b_loop_ub;
        emxEnsureCapacity_int32_T(idx, i22);
        for (i22 = 0; i22 < b_loop_ub; i22++) {
          idx->data[i22] = 0;
        }

        if (b_loop_ub != 0) {
          i22 = iwork->size[0];
          iwork->size[0] = b_loop_ub;
          emxEnsureCapacity_int32_T(iwork, i22);
          i22 = b_loop_ub - 1;
          for (k = 1; k <= i22; k += 2) {
            if ((links[k - 1] <= links[k]) || rtIsNaN(links[k])) {
              idx->data[k - 1] = k;
              idx->data[k] = k + 1;
            } else {
              idx->data[k - 1] = k + 1;
              idx->data[k] = k;
            }
          }

          if ((b_loop_ub & 1) != 0) {
            idx->data[b_loop_ub - 1] = b_loop_ub;
          }

          k0 = 2;
          while (k0 < b_loop_ub) {
            i2 = k0 << 1;
            b_j = 1;
            for (pEnd = 1 + k0; pEnd < b_loop_ub + 1; pEnd = nb + k0) {
              p = b_j;
              q = pEnd - 1;
              nb = b_j + i2;
              if (nb > b_loop_ub + 1) {
                nb = b_loop_ub + 1;
              }

              k = 0;
              kEnd = nb - b_j;
              while (k + 1 <= kEnd) {
                if ((links[idx->data[p - 1] - 1] <= links[idx->data[q] - 1]) ||
                    rtIsNaN(links[idx->data[q] - 1])) {
                  iwork->data[k] = idx->data[p - 1];
                  p++;
                  if (p == pEnd) {
                    while (q + 1 < nb) {
                      k++;
                      iwork->data[k] = idx->data[q];
                      q++;
                    }
                  }
                } else {
                  iwork->data[k] = idx->data[q];
                  q++;
                  if (q + 1 == nb) {
                    while (p < pEnd) {
                      k++;
                      iwork->data[k] = idx->data[p - 1];
                      p++;
                    }
                  }
                }

                k++;
              }

              for (k = 0; k < kEnd; k++) {
                idx->data[(b_j + k) - 1] = iwork->data[k];
              }

              b_j = nb;
            }

            k0 = i2;
          }
        }

        i22 = setnPBC->size[0];
        setnPBC->size[0] = (signed char)b_loop_ub;
        emxEnsureCapacity_real_T(setnPBC, i22);
        for (k = 0; k < b_loop_ub; k++) {
          setnPBC->data[k] = links[idx->data[k] - 1];
        }

        k = 0;
        while ((k + 1 <= b_loop_ub) && rtIsInf(setnPBC->data[k]) &&
               (setnPBC->data[k] < 0.0)) {
          k++;
        }

        kEnd = k;
        k = b_loop_ub - 1;
        while ((k + 1 >= 1) && rtIsNaN(setnPBC->data[k])) {
          k--;
        }

        k0 = b_loop_ub - k;
        while ((k + 1 >= 1) && rtIsInf(setnPBC->data[k]) && (setnPBC->data[k] >
                0.0)) {
          k--;
        }

        i2 = (b_loop_ub - k) - k0;
        nb = -1;
        if (kEnd > 0) {
          nb = 0;
        }

        while (kEnd + 1 <= k + 1) {
          i1 = setnPBC->data[kEnd];
          do {
            exitg1 = 0;
            kEnd++;
            if (kEnd + 1 > k + 1) {
              exitg1 = 1;
            } else {
              absxk = fabs(i1 / 2.0);
              if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
                if (absxk <= 2.2250738585072014E-308) {
                  absxk = 4.94065645841247E-324;
                } else {
                  frexp(absxk, &b_exponent);
                  absxk = ldexp(1.0, b_exponent - 53);
                }
              } else {
                absxk = rtNaN;
              }

              if ((fabs(i1 - setnPBC->data[kEnd]) < absxk) || (rtIsInf
                   (setnPBC->data[kEnd]) && rtIsInf(i1) && ((setnPBC->data[kEnd]
                     > 0.0) == (i1 > 0.0)))) {
              } else {
                exitg1 = 1;
              }
            }
          } while (exitg1 == 0);

          nb++;
          setnPBC->data[nb] = i1;
        }

        if (i2 > 0) {
          nb++;
          setnPBC->data[nb] = setnPBC->data[k + 1];
        }

        kEnd = (k + i2) + 1;
        for (b_j = 0; b_j <= k0 - 2; b_j++) {
          nb++;
          setnPBC->data[nb] = setnPBC->data[kEnd + b_j];
        }

        if (1 > nb + 1) {
          setnPBC->size[0] = 0;
        } else {
          i22 = setnPBC->size[0];
          setnPBC->size[0] = nb + 1;
          emxEnsureCapacity_real_T(setnPBC, i22);
        }

        b_loop_ub = setnPBC->size[0];
        for (i22 = 0; i22 < b_loop_ub; i22++) {
          links2_data[i22] = setnPBC->data[i22];
        }

        i2 = setnPBC->size[0] - 1;
        kEnd = 0;
        for (k0 = 0; k0 <= i2; k0++) {
          if (setnPBC->data[k0] != 0.0) {
            kEnd++;
          }
        }

        pEnd = 0;
        for (k0 = 0; k0 <= i2; k0++) {
          if (links2_data[k0] != 0.0) {
            links2_data[pEnd] = links2_data[k0];
            pEnd++;
          }
        }

        for (i22 = 0; i22 < kEnd; i22++) {
          for (i23 = 0; i23 < numPBC; i23++) {
            NodesOnLink->data[i22 + (((int)nodes[i23] - 1) << 2)] =
              links2_data[i22];
          }
        }

        if (1 > numPBC) {
          b_loop_ub = 0;
        } else {
          b_loop_ub = numPBC;
        }

        for (i22 = 0; i22 < b_loop_ub; i22++) {
          ia_data[i22] = (int)nodes[i22];
        }

        for (i22 = 0; i22 < b_loop_ub; i22++) {
          NodesOnLinknum->data[ia_data[i22] - 1] = (signed char)kEnd;
        }
      }
    }

    i21 = b_NodesOnPBCnum->size[0];
    b_NodesOnPBCnum->size[0] = NodesOnPBCnum->size[0];
    emxEnsureCapacity_real_T(b_NodesOnPBCnum, i21);
    b_loop_ub = NodesOnPBCnum->size[0];
    for (i21 = 0; i21 < b_loop_ub; i21++) {
      b_NodesOnPBCnum->data[i21] = NodesOnPBCnum->data[i21];
    }

    b_abs(b_NodesOnPBCnum, NodesOnPBCnum);
  } else {
    NodesOnPBC->size[0] = 4;
    NodesOnPBC->size[1] = 0;

    /*  nodes that are paired up */
    NodesOnPBCnum->size[0] = 0;

    /*  number of nodes with common pairing */
    NodesOnLink->size[0] = 4;
    NodesOnLink->size[1] = 0;

    /*  IDs of PBC referring to node */
    NodesOnLinknum->size[0] = 0;

    /*  number of PBC referring to node */
  }

  /*  Mesh error checks */
  /*  Error checks for mesh arrays */
  /*  Nodes */
  /*  Elements */
  /*  Check that interfaces are assigned also when intraface couplers are requested */
  k0 = InterTypes->size[0] * InterTypes->size[1];
  b_idx = 0;
  i21 = iwork->size[0];
  iwork->size[0] = k0;
  emxEnsureCapacity_int32_T(iwork, i21);
  notdone = 0;
  exitg2 = false;
  while ((!exitg2) && (notdone <= k0 - 1)) {
    if (InterTypes->data[notdone] != 0.0) {
      b_idx++;
      iwork->data[b_idx - 1] = notdone + 1;
      if (b_idx >= k0) {
        exitg2 = true;
      } else {
        notdone++;
      }
    } else {
      notdone++;
    }
  }

  if (k0 == 1) {
    if (b_idx == 0) {
      iwork->size[0] = 0;
    }
  } else if (1 > b_idx) {
    iwork->size[0] = 0;
  } else {
    i21 = iwork->size[0];
    iwork->size[0] = b_idx;
    emxEnsureCapacity_int32_T(iwork, i21);
  }

  /*  reset to logical */
  b_loop_ub = iwork->size[0] - 1;
  for (i21 = 0; i21 <= b_loop_ub; i21++) {
    InterTypes->data[iwork->data[i21] - 1] = 1.0;
  }

  i1 = rt_roundd_snf(nummat);
  if (i1 < 9.2233720368547758E+18) {
    if (i1 >= -9.2233720368547758E+18) {
      q0 = (long long)i1;
    } else {
      q0 = MIN_int64_T;
    }
  } else if (i1 >= 9.2233720368547758E+18) {
    q0 = MAX_int64_T;
  } else {
    q0 = 0LL;
  }

  if (q0 < -9223372036854775807LL) {
    qY = MIN_int64_T;
  } else {
    qY = q0 - 1LL;
  }

  mat = 1LL;
  emxInit_int64_T(&inter, 1);
  emxInit_real_T(&setAll, 2);
  emxInit_int32_T(&ii, 2);
  while (mat <= qY) {
    if (InterTypes->data[((int)mat + InterTypes->size[0] * ((int)mat - 1)) - 1]
        != 0.0) {
      if (mat > 9223372036854775806LL) {
        b_qY = MAX_int64_T;
      } else {
        b_qY = mat + 1LL;
      }

      if (b_qY > q0) {
        i21 = 0;
        i22 = 0;
      } else {
        i21 = (int)b_qY - 1;
        i22 = (int)q0;
      }

      i23 = ycol->size[0];
      b_loop_ub = i22 - i21;
      ycol->size[0] = b_loop_ub;
      emxEnsureCapacity_real_T(ycol, i23);
      for (i22 = 0; i22 < b_loop_ub; i22++) {
        ycol->data[i22] = InterTypes->data[(i21 + i22) + InterTypes->size[0] *
          ((int)mat - 1)] - 1.0;
      }

      k0 = ycol->size[0];
      b_idx = 0;
      i21 = iwork->size[0];
      iwork->size[0] = ycol->size[0];
      emxEnsureCapacity_int32_T(iwork, i21);
      notdone = 0;
      exitg2 = false;
      while ((!exitg2) && (notdone <= k0 - 1)) {
        if (ycol->data[notdone] != 0.0) {
          b_idx++;
          iwork->data[b_idx - 1] = notdone + 1;
          if (b_idx >= k0) {
            exitg2 = true;
          } else {
            notdone++;
          }
        } else {
          notdone++;
        }
      }

      if (ycol->size[0] == 1) {
        if (b_idx == 0) {
          iwork->size[0] = 0;
        }
      } else if (1 > b_idx) {
        iwork->size[0] = 0;
      } else {
        i21 = iwork->size[0];
        iwork->size[0] = b_idx;
        emxEnsureCapacity_int32_T(iwork, i21);
      }

      i21 = inter->size[0];
      inter->size[0] = iwork->size[0];
      emxEnsureCapacity_int64_T(inter, i21);
      b_loop_ub = iwork->size[0];
      for (i21 = 0; i21 < b_loop_ub; i21++) {
        inter->data[i21] = iwork->data[i21];
      }

      if (inter->size[0] != 0) {
        printf("intraface: %lli; additional interface flags have been added\n",
               mat);
        fflush(stdout);
        i21 = idx->size[0];
        idx->size[0] = inter->size[0];
        emxEnsureCapacity_int32_T(idx, i21);
        b_loop_ub = inter->size[0];
        for (i21 = 0; i21 < b_loop_ub; i21++) {
          b_qY = inter->data[i21];
          if ((b_qY < 0LL) && (mat < MIN_int64_T - b_qY)) {
            b_qY = MIN_int64_T;
          } else if ((b_qY > 0LL) && (mat > MAX_int64_T - b_qY)) {
            b_qY = MAX_int64_T;
          } else {
            b_qY += mat;
          }

          idx->data[i21] = (int)b_qY;
        }

        i2 = idx->size[0];
        for (i21 = 0; i21 < i2; i21++) {
          InterTypes->data[(idx->data[i21] + InterTypes->size[0] * ((int)mat - 1))
            - 1] = 1.0;
        }
      }

      if (mat < -9223372036854775807LL) {
        b_qY = MIN_int64_T;
      } else {
        b_qY = mat - 1LL;
      }

      if (1LL > b_qY) {
        b_loop_ub = 0;
      } else {
        b_loop_ub = (int)b_qY;
      }

      i21 = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      setAll->size[1] = b_loop_ub;
      emxEnsureCapacity_real_T(setAll, i21);
      for (i21 = 0; i21 < b_loop_ub; i21++) {
        setAll->data[i21] = InterTypes->data[((int)mat + InterTypes->size[0] *
          i21) - 1] - 1.0;
      }

      k0 = setAll->size[1];
      b_idx = 0;
      i21 = ii->size[0] * ii->size[1];
      ii->size[0] = 1;
      ii->size[1] = setAll->size[1];
      emxEnsureCapacity_int32_T(ii, i21);
      notdone = 0;
      exitg2 = false;
      while ((!exitg2) && (notdone <= k0 - 1)) {
        if (setAll->data[notdone] != 0.0) {
          b_idx++;
          ii->data[b_idx - 1] = notdone + 1;
          if (b_idx >= k0) {
            exitg2 = true;
          } else {
            notdone++;
          }
        } else {
          notdone++;
        }
      }

      if (setAll->size[1] == 1) {
        if (b_idx == 0) {
          ii->size[0] = 1;
          ii->size[1] = 0;
        }
      } else if (1 > b_idx) {
        ii->size[1] = 0;
      } else {
        i21 = ii->size[0] * ii->size[1];
        ii->size[1] = b_idx;
        emxEnsureCapacity_int32_T(ii, i21);
      }

      i21 = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      setAll->size[1] = ii->size[1];
      emxEnsureCapacity_real_T(setAll, i21);
      b_loop_ub = ii->size[0] * ii->size[1];
      for (i21 = 0; i21 < b_loop_ub; i21++) {
        setAll->data[i21] = ii->data[i21];
      }

      if (setAll->size[1] != 0) {
        printf("intraface: %lli; additional interface flags have been added\n",
               mat);
        fflush(stdout);
        i21 = idx->size[0];
        idx->size[0] = setAll->size[1];
        emxEnsureCapacity_int32_T(idx, i21);
        b_loop_ub = setAll->size[1];
        for (i21 = 0; i21 < b_loop_ub; i21++) {
          idx->data[i21] = (int)setAll->data[i21];
        }

        k0 = idx->size[0];
        for (i21 = 0; i21 < k0; i21++) {
          InterTypes->data[((int)mat + InterTypes->size[0] * (idx->data[i21] - 1))
            - 1] = 1.0;
        }
      }
    }

    if (mat < -9223372036854775807LL) {
      b_qY = MIN_int64_T;
    } else {
      b_qY = mat - 1LL;
    }

    if (1LL > b_qY) {
      i21 = 1;
    } else {
      i21 = (int)b_qY + 1;
    }

    k0 = i21 - 2;
    b_idx = 0;
    i22 = iwork->size[0];
    iwork->size[0] = i21 - 1;
    emxEnsureCapacity_int32_T(iwork, i22);
    notdone = 0;
    exitg2 = false;
    while ((!exitg2) && (notdone <= k0)) {
      if (InterTypes->data[notdone + InterTypes->size[0] * ((int)mat - 1)] !=
          0.0) {
        b_idx++;
        iwork->data[b_idx - 1] = notdone + 1;
        if (b_idx >= k0 + 1) {
          exitg2 = true;
        } else {
          notdone++;
        }
      } else {
        notdone++;
      }
    }

    if (i21 - 1 == 1) {
      if (b_idx == 0) {
        iwork->size[0] = 0;
      }
    } else if (1 > b_idx) {
      iwork->size[0] = 0;
    } else {
      i21 = iwork->size[0];
      iwork->size[0] = b_idx;
      emxEnsureCapacity_int32_T(iwork, i21);
    }

    if (iwork->size[0] != 0) {
      printf("Warning: some entries found in the upper triangle of Intertypes(:,%lli)\n",
             mat);
      fflush(stdout);
    }

    mat++;
  }

  emxFree_int32_T(&ii);
  emxFree_int64_T(&inter);

  /*  Form ElementsOnNode, ElementsOnNodeNum, DG nodes and connectivity */
  emxInit_real_T(&ElementsOnNodeRow, 2);
  emxInit_real_T(&ElementsOnNodePBC_d, 1);
  emxInit_int32_T(&ElementsOnNodePBC_rowidx, 1);
  emxInit_int32_T(&internodes_rowidx, 1);
  d_emxInitStruct_coder_internal_(&NodeRegSum);
  emxInit_int32_T(&j, 1);
  emxInit_int32_T(&t0_colidx, 1);
  emxInit_int32_T(&t0_rowidx, 1);
  emxInit_real_T(&b_setAll, 2);
  emxInit_real_T(&NodeRegInd, 2);
  emxInit_real_T(&b_NodeRegInd, 2);
  emxInit_real_T(&c_NodeRegInd, 2);
  if (usePBC != 0.0) {
    emxInit_real_T(&e_NodeRegInd, 2);

    /*  also form the zipped mesh at the same time */
    i21 = e_NodeRegInd->size[0] * e_NodeRegInd->size[1];
    e_NodeRegInd->size[0] = 3;
    e_NodeRegInd->size[1] = (int)(numel * 8.0);
    emxEnsureCapacity_real_T(e_NodeRegInd, i21);
    b_loop_ub = 3 * (int)(numel * 8.0);
    for (i21 = 0; i21 < b_loop_ub; i21++) {
      e_NodeRegInd->data[i21] = 0.0;
    }

    emxInit_real_T(&ElementsOnNodePBCRow, 2);

    /*  node, region attached, regular=1 or PBC=-1 */
    i21 = ElementsOnNodePBCRow->size[0] * ElementsOnNodePBCRow->size[1];
    ElementsOnNodePBCRow->size[0] = (int)numel;
    ElementsOnNodePBCRow->size[1] = (int)nen;
    emxEnsureCapacity_real_T(ElementsOnNodePBCRow, i21);
    b_loop_ub = (int)numel * (int)nen;
    for (i21 = 0; i21 < b_loop_ub; i21++) {
      ElementsOnNodePBCRow->data[i21] = 0.0;
    }

    i21 = ElementsOnNodeRow->size[0] * ElementsOnNodeRow->size[1];
    ElementsOnNodeRow->size[0] = (int)numel;
    ElementsOnNodeRow->size[1] = (int)nen;
    emxEnsureCapacity_real_T(ElementsOnNodeRow, i21);
    b_loop_ub = (int)numel * (int)nen;
    for (i21 = 0; i21 < b_loop_ub; i21++) {
      ElementsOnNodeRow->data[i21] = 0.0;
    }

    nri = 0U;
    numA = 0.0;
    i21 = (int)numel;
    for (exponent = 0; exponent < i21; exponent++) {
      if (1.0 > nen) {
        b_loop_ub = 0;
      } else {
        b_loop_ub = (int)nen;
      }

      for (i22 = 0; i22 < b_loop_ub; i22++) {
        nodeloc_data[i22] = NodesOnElementPBC->data[exponent +
          NodesOnElementPBC->size[0] * i22];
      }

      n = 0;
      i22 = b_loop_ub - 1;
      for (k = 0; k <= i22; k++) {
        if (nodeloc_data[k] != 0.0) {
          n++;
        }
      }

      reg = RegionOnElement->data[exponent];
      for (nume = 0; nume < n; nume++) {
        /*  Loop over local Nodes */
        absxk = NodesOnElementPBC->data[exponent + NodesOnElementPBC->size[0] *
          nume];

        /*    Add element to star list, increment number of elem in star */
        i22 = (int)absxk - 1;
        ElementsOnNodePBCRow->data[exponent + ElementsOnNodePBCRow->size[0] *
          nume] = ElementsOnNodePBCNum->data[i22] + 1.0;
        ElementsOnNodePBCNum->data[i22]++;

        /*              ElementsOnNodePBC(locE,node) = elem; */
        /*              ElementsOnNodePBCDup(locE,node) = node; */
        if (NodesOnPBCnum->data[i22] > 0.0) {
          i1 = NodesOnPBC->data[i22 << 2];
          if (absxk > i1) {
          } else {
            i1 = absxk;
          }

          /*  use smallest node number as the zipped node */
          nri++;
          e_NodeRegInd->data[3 * ((int)nri - 1)] = i1;
          e_NodeRegInd->data[1 + 3 * ((int)nri - 1)] = reg;
          e_NodeRegInd->data[2 + 3 * ((int)nri - 1)] = 1.0;

          /*  flag that node is in current material set */
          if (absxk != i1) {
            nri++;

            /*  also add for original edge */
            i22 = 3 * ((int)nri - 1);
            e_NodeRegInd->data[i22] = absxk;
            e_NodeRegInd->data[1 + i22] = reg;
            e_NodeRegInd->data[2 + i22] = -1.0;

            /*  flag that node is in current material set */
          }

          NodesOnElementCG->data[exponent + NodesOnElementCG->size[0] * nume] =
            i1;

          /*    Add element to star list, increment number of elem in star */
          i22 = (int)i1 - 1;
          ElementsOnNodeRow->data[exponent + ElementsOnNodeRow->size[0] * nume] =
            ElementsOnNodeNum->data[i22] + 1.0;
          ElementsOnNodeNum->data[i22]++;

          /*                  ElementsOnNode(locE,nodePBC) = elem; */
          /*                  ElementsOnNodeDup(locE,nodePBC) = nodePBC; */
        } else {
          nri++;
          i23 = 3 * ((int)nri - 1);
          e_NodeRegInd->data[i23] = absxk;
          e_NodeRegInd->data[1 + i23] = reg;
          e_NodeRegInd->data[2 + i23] = 1.0;

          /*  flag that node is in current material set */
          /*    Add element to star list, increment number of elem in star */
          ElementsOnNodeRow->data[exponent + ElementsOnNodeRow->size[0] * nume] =
            ElementsOnNodeNum->data[i22] + 1.0;
          ElementsOnNodeNum->data[i22]++;

          /*                  ElementsOnNode(locE,nodePBC) = elem; */
          /*                  ElementsOnNodeDup(locE,nodePBC) = nodePBC; */
        }
      }

      i22 = (int)(nen + (1.0 - ((double)n + 1.0)));
      for (nume = 0; nume < i22; nume++) {
        numA++;
        i23 = (int)(((double)n + 1.0) + (double)nume) - 1;
        ElementsOnNodeRow->data[exponent + ElementsOnNodeRow->size[0] * i23] =
          numA;
        ElementsOnNodePBCRow->data[exponent + ElementsOnNodePBCRow->size[0] *
          i23] = numA;
      }
    }

    i2 = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1] - 1;
    kEnd = 0;
    for (i = 0; i <= i2; i++) {
      if (NodesOnElementPBC->data[i] == 0.0) {
        kEnd++;
      }
    }

    i21 = j->size[0];
    j->size[0] = kEnd;
    emxEnsureCapacity_int32_T(j, i21);
    pEnd = 0;
    for (i = 0; i <= i2; i++) {
      if (NodesOnElementPBC->data[i] == 0.0) {
        j->data[pEnd] = i + 1;
        pEnd++;
      }
    }

    b_loop_ub = j->size[0] - 1;
    for (i21 = 0; i21 <= b_loop_ub; i21++) {
      NodesOnElementPBC->data[j->data[i21] - 1] = *numnp + 1.0;
    }

    /* move empty numbers to end of array */
    i2 = NodesOnElementCG->size[0] * NodesOnElementCG->size[1] - 1;
    kEnd = 0;
    for (i = 0; i <= i2; i++) {
      if (NodesOnElementCG->data[i] == 0.0) {
        kEnd++;
      }
    }

    i21 = ElementsOnNodePBC_rowidx->size[0];
    ElementsOnNodePBC_rowidx->size[0] = kEnd;
    emxEnsureCapacity_int32_T(ElementsOnNodePBC_rowidx, i21);
    pEnd = 0;
    for (i = 0; i <= i2; i++) {
      if (NodesOnElementCG->data[i] == 0.0) {
        ElementsOnNodePBC_rowidx->data[pEnd] = i + 1;
        pEnd++;
      }
    }

    b_loop_ub = ElementsOnNodePBC_rowidx->size[0] - 1;
    for (i21 = 0; i21 <= b_loop_ub; i21++) {
      NodesOnElementCG->data[ElementsOnNodePBC_rowidx->data[i21] - 1] = *numnp +
        1.0;
    }

    /* move empty numbers to end of array */
    if (numel < 1.0) {
      setAll->size[0] = 1;
      setAll->size[1] = 0;
    } else {
      i21 = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      setAll->size[1] = (int)floor(numel - 1.0) + 1;
      emxEnsureCapacity_real_T(setAll, i21);
      b_loop_ub = (int)floor(numel - 1.0);
      for (i21 = 0; i21 <= b_loop_ub; i21++) {
        setAll->data[i21] = 1.0 + (double)i21;
      }
    }

    i21 = b_setAll->size[0] * b_setAll->size[1];
    b_setAll->size[0] = setAll->size[1];
    b_setAll->size[1] = (int)nen;
    emxEnsureCapacity_real_T(b_setAll, i21);
    b_loop_ub = setAll->size[1];
    for (i21 = 0; i21 < b_loop_ub; i21++) {
      i2 = (int)nen;
      for (i22 = 0; i22 < i2; i22++) {
        i1 = setAll->data[i21];
        b_setAll->data[i21 + b_setAll->size[0] * i22] = i1;
      }
    }

    sparse(ElementsOnNodeRow, NodesOnElementCG, b_setAll, ElementsOnNode);

    /* zeros(maxel,numnp); % Array of elements connected to nodes */
    sparse(ElementsOnNodeRow, NodesOnElementCG, NodesOnElementCG,
           ElementsOnNodeDup);

    /*  actual node ID given to element for an original node; general counterpart of NodeMat */
    if (numel < 1.0) {
      setAll->size[0] = 1;
      setAll->size[1] = 0;
    } else {
      i21 = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      setAll->size[1] = (int)floor(numel - 1.0) + 1;
      emxEnsureCapacity_real_T(setAll, i21);
      b_loop_ub = (int)floor(numel - 1.0);
      for (i21 = 0; i21 <= b_loop_ub; i21++) {
        setAll->data[i21] = 1.0 + (double)i21;
      }
    }

    i21 = b_setAll->size[0] * b_setAll->size[1];
    b_setAll->size[0] = setAll->size[1];
    b_setAll->size[1] = (int)nen;
    emxEnsureCapacity_real_T(b_setAll, i21);
    b_loop_ub = setAll->size[1];
    for (i21 = 0; i21 < b_loop_ub; i21++) {
      i2 = (int)nen;
      for (i22 = 0; i22 < i2; i22++) {
        i1 = setAll->data[i21];
        b_setAll->data[i21 + b_setAll->size[0] * i22] = i1;
      }
    }

    c_emxInitStruct_coder_internal_(&expl_temp);
    sparse(ElementsOnNodePBCRow, NodesOnElementPBC, b_setAll, &expl_temp);
    i21 = ElementsOnNodePBC_d->size[0];
    ElementsOnNodePBC_d->size[0] = expl_temp.d->size[0];
    emxEnsureCapacity_real_T(ElementsOnNodePBC_d, i21);
    b_loop_ub = expl_temp.d->size[0];
    for (i21 = 0; i21 < b_loop_ub; i21++) {
      ElementsOnNodePBC_d->data[i21] = expl_temp.d->data[i21];
    }

    i21 = iwork->size[0];
    iwork->size[0] = expl_temp.colidx->size[0];
    emxEnsureCapacity_int32_T(iwork, i21);
    b_loop_ub = expl_temp.colidx->size[0];
    for (i21 = 0; i21 < b_loop_ub; i21++) {
      iwork->data[i21] = expl_temp.colidx->data[i21];
    }

    i21 = ElementsOnNodePBC_rowidx->size[0];
    ElementsOnNodePBC_rowidx->size[0] = expl_temp.rowidx->size[0];
    emxEnsureCapacity_int32_T(ElementsOnNodePBC_rowidx, i21);
    b_loop_ub = expl_temp.rowidx->size[0];
    for (i21 = 0; i21 < b_loop_ub; i21++) {
      ElementsOnNodePBC_rowidx->data[i21] = expl_temp.rowidx->data[i21];
    }

    c_emxFreeStruct_coder_internal_(&expl_temp);
    c_emxInitStruct_coder_internal_(&b_ElementsOnNodePBCRow);

    /* zeros(maxel,numnp); % Array of elements connected to nodes */
    sparse(ElementsOnNodePBCRow, NodesOnElementPBC, NodesOnElementPBC,
           &b_ElementsOnNodePBCRow);

    /*  actual node ID given to element for an original node; general counterpart of NodeMat */
    c_emxFreeStruct_coder_internal_(&b_ElementsOnNodePBCRow);
    emxFree_real_T(&ElementsOnNodePBCRow);
    if (1 > (int)nri) {
      b_loop_ub = 0;
      i2 = 0;
      kEnd = 0;
    } else {
      b_loop_ub = (int)nri;
      i2 = (int)nri;
      kEnd = (int)nri;
    }

    i21 = NodeRegInd->size[0] * NodeRegInd->size[1];
    NodeRegInd->size[0] = 1;
    NodeRegInd->size[1] = b_loop_ub;
    emxEnsureCapacity_real_T(NodeRegInd, i21);
    for (i21 = 0; i21 < b_loop_ub; i21++) {
      NodeRegInd->data[i21] = e_NodeRegInd->data[3 * i21];
    }

    i21 = b_NodeRegInd->size[0] * b_NodeRegInd->size[1];
    b_NodeRegInd->size[0] = 1;
    b_NodeRegInd->size[1] = i2;
    emxEnsureCapacity_real_T(b_NodeRegInd, i21);
    for (i21 = 0; i21 < i2; i21++) {
      b_NodeRegInd->data[i21] = e_NodeRegInd->data[1 + 3 * i21];
    }

    i21 = c_NodeRegInd->size[0] * c_NodeRegInd->size[1];
    c_NodeRegInd->size[0] = 1;
    c_NodeRegInd->size[1] = kEnd;
    emxEnsureCapacity_real_T(c_NodeRegInd, i21);
    for (i21 = 0; i21 < kEnd; i21++) {
      c_NodeRegInd->data[i21] = e_NodeRegInd->data[2 + 3 * i21];
    }

    emxFree_real_T(&e_NodeRegInd);
    b_sparse(NodeRegInd, b_NodeRegInd, c_NodeRegInd, NodeReg);

    /*  allow allocation of PBC nodes too, but with negatives so as not to be in facet counts */
    k0 = NodeReg->colidx->data[NodeReg->colidx->size[0] - 1] - 1;
    if (NodeReg->colidx->data[NodeReg->colidx->size[0] - 1] - 1 == 0) {
      idx->size[0] = 0;
      j->size[0] = 0;
    } else {
      i21 = idx->size[0];
      idx->size[0] = NodeReg->colidx->data[NodeReg->colidx->size[0] - 1] - 1;
      emxEnsureCapacity_int32_T(idx, i21);
      i21 = j->size[0];
      j->size[0] = NodeReg->colidx->data[NodeReg->colidx->size[0] - 1] - 1;
      emxEnsureCapacity_int32_T(j, i21);
      for (b_idx = 0; b_idx < k0; b_idx++) {
        idx->data[b_idx] = NodeReg->rowidx->data[b_idx];
      }

      b_idx = 0;
      kEnd = 1;
      while (b_idx < k0) {
        if (b_idx == NodeReg->colidx->data[kEnd] - 1) {
          kEnd++;
        } else {
          b_idx++;
          j->data[b_idx - 1] = kEnd;
        }
      }

      if (NodeReg->colidx->data[NodeReg->colidx->size[0] - 1] - 1 == 1) {
        if (b_idx == 0) {
          idx->size[0] = 0;
          j->size[0] = 0;
        }
      } else if (1 > b_idx) {
        idx->size[0] = 0;
        j->size[0] = 0;
      } else {
        i21 = idx->size[0];
        idx->size[0] = b_idx;
        emxEnsureCapacity_int32_T(idx, i21);
        i21 = j->size[0];
        j->size[0] = b_idx;
        emxEnsureCapacity_int32_T(j, i21);
      }
    }

    i21 = setnPBC->size[0];
    setnPBC->size[0] = idx->size[0];
    emxEnsureCapacity_real_T(setnPBC, i21);
    b_loop_ub = idx->size[0];
    for (i21 = 0; i21 < b_loop_ub; i21++) {
      setnPBC->data[i21] = idx->data[i21];
    }

    i21 = ycol->size[0];
    ycol->size[0] = j->size[0];
    emxEnsureCapacity_real_T(ycol, i21);
    b_loop_ub = j->size[0];
    for (i21 = 0; i21 < b_loop_ub; i21++) {
      ycol->data[i21] = j->data[i21];
    }

    /*  sparse adds together entries when duplicated, so reduce back to original magnitude */
    n = setnPBC->size[0];
    if (setnPBC->size[0] <= 2) {
      if (setnPBC->size[0] == 1) {
        kEnd = (int)setnPBC->data[0];
      } else if (setnPBC->data[0] < setnPBC->data[1]) {
        kEnd = (int)setnPBC->data[1];
      } else {
        kEnd = (int)setnPBC->data[0];
      }
    } else {
      kEnd = (int)setnPBC->data[0];
      for (k = 2; k <= n; k++) {
        if (kEnd < (int)setnPBC->data[k - 1]) {
          kEnd = (int)setnPBC->data[k - 1];
        }
      }
    }

    i21 = ycol->size[0];
    ycol->size[0] = setnPBC->size[0];
    emxEnsureCapacity_real_T(ycol, i21);
    b_loop_ub = setnPBC->size[0];
    for (i21 = 0; i21 < b_loop_ub; i21++) {
      ycol->data[i21] = (int)setnPBC->data[i21] + kEnd * ((int)ycol->data[i21] -
        1);
    }

    sparse_parenReference(NodeReg->d, NodeReg->colidx, NodeReg->rowidx,
                          NodeReg->m, ycol, NodeRegSum.d, NodeRegSum.colidx,
                          NodeRegSum.rowidx, &NodeRegSum.m);
    b_sign(&NodeRegSum);
    sparse_times(setnPBC, NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                 NodeRegSum.m, b_NodesOnPBCnum, t0_colidx, t0_rowidx, &k0);
    sparse_parenAssign(NodeReg, b_NodesOnPBCnum, t0_colidx, t0_rowidx, k0, ycol);

    /* keep the negative sign for PBC nodes to ignore below */
    i2 = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1] - 1;
    kEnd = 0;
    for (i = 0; i <= i2; i++) {
      if (NodesOnElementPBC->data[i] == *numnp + 1.0) {
        kEnd++;
      }
    }

    i21 = internodes_rowidx->size[0];
    internodes_rowidx->size[0] = kEnd;
    emxEnsureCapacity_int32_T(internodes_rowidx, i21);
    pEnd = 0;
    for (i = 0; i <= i2; i++) {
      if (NodesOnElementPBC->data[i] == *numnp + 1.0) {
        internodes_rowidx->data[pEnd] = i + 1;
        pEnd++;
      }
    }

    b_loop_ub = internodes_rowidx->size[0] - 1;
    for (i21 = 0; i21 <= b_loop_ub; i21++) {
      NodesOnElementPBC->data[internodes_rowidx->data[i21] - 1] = 0.0;
    }

    /* move empty numbers to end of array */
    i2 = NodesOnElementCG->size[0] * NodesOnElementCG->size[1] - 1;
    kEnd = 0;
    for (i = 0; i <= i2; i++) {
      if (NodesOnElementCG->data[i] == *numnp + 1.0) {
        kEnd++;
      }
    }

    emxInit_int32_T(&r0, 1);
    i21 = r0->size[0];
    r0->size[0] = kEnd;
    emxEnsureCapacity_int32_T(r0, i21);
    pEnd = 0;
    for (i = 0; i <= i2; i++) {
      if (NodesOnElementCG->data[i] == *numnp + 1.0) {
        r0->data[pEnd] = i + 1;
        pEnd++;
      }
    }

    b_loop_ub = r0->size[0] - 1;
    for (i21 = 0; i21 <= b_loop_ub; i21++) {
      NodesOnElementCG->data[r0->data[i21] - 1] = 0.0;
    }

    emxFree_int32_T(&r0);

    /* move empty numbers to end of array */
    /*      % get memory back */
    /*      clear NodeRegInd */
    /*      clear ElementsOnNodeRow */
    /*      clear ElementsOnNodePBCRow */
  } else {
    emxInit_real_T(&d_NodeRegInd, 2);
    i21 = d_NodeRegInd->size[0] * d_NodeRegInd->size[1];
    d_NodeRegInd->size[0] = 2;
    i22 = (int)(numel * 8.0);
    d_NodeRegInd->size[1] = i22;
    emxEnsureCapacity_real_T(d_NodeRegInd, i21);
    b_loop_ub = i22 << 1;
    for (i21 = 0; i21 < b_loop_ub; i21++) {
      d_NodeRegInd->data[i21] = 0.0;
    }

    i21 = ElementsOnNodeRow->size[0] * ElementsOnNodeRow->size[1];
    i22 = (int)numel;
    ElementsOnNodeRow->size[0] = i22;
    b_loop_ub = (int)nen;
    ElementsOnNodeRow->size[1] = b_loop_ub;
    emxEnsureCapacity_real_T(ElementsOnNodeRow, i21);
    nb = i22 * b_loop_ub;
    for (i21 = 0; i21 < nb; i21++) {
      ElementsOnNodeRow->data[i21] = 0.0;
    }

    /*      ElementsOnNodeCol = zeros(nen,numel); */
    nri = 0U;
    numA = 0.0;
    for (exponent = 0; exponent < i22; exponent++) {
      if (1.0 > nen) {
        i2 = 0;
      } else {
        i2 = (int)nen;
      }

      for (i21 = 0; i21 < i2; i21++) {
        nodeloc_data[i21] = NodesOnElement->data[exponent + NodesOnElement->
          size[0] * i21];
      }

      n = 0;
      i21 = i2 - 1;
      for (k = 0; k <= i21; k++) {
        if (nodeloc_data[k] != 0.0) {
          n++;
        }
      }

      for (nume = 0; nume < n; nume++) {
        /*  Loop over local Nodes */
        absxk = NodesOnElement->data[exponent + NodesOnElement->size[0] * nume];
        nri++;
        i1 = RegionOnElement->data[exponent];
        i21 = ((int)nri - 1) << 1;
        d_NodeRegInd->data[i21] = absxk;
        d_NodeRegInd->data[1 + i21] = i1;

        /*              NodeReg(node,reg) = node; % flag that node is in current material set */
        /*    Add element to star list, increment number of elem in star */
        i21 = (int)absxk - 1;
        ElementsOnNodeRow->data[exponent + ElementsOnNodeRow->size[0] * nume] =
          ElementsOnNodeNum->data[i21] + 1.0;

        /*              ElementsOnNode(locE,node) = elem; */
        ElementsOnNodeNum->data[i21]++;

        /*              ElementsOnNodeCol(locN,elem) = node; */
        /*              ElementsOnNodeDup(locE,node) = node; */
      }

      i21 = (int)(nen + (1.0 - ((double)n + 1.0)));
      for (nume = 0; nume < i21; nume++) {
        numA++;
        ElementsOnNodeRow->data[exponent + ElementsOnNodeRow->size[0] * ((int)
          (((double)n + 1.0) + (double)nume) - 1)] = numA;
      }
    }

    i2 = NodesOnElement->size[0] * NodesOnElement->size[1] - 1;
    kEnd = 0;
    for (i = 0; i <= i2; i++) {
      if (NodesOnElement->data[i] == 0.0) {
        kEnd++;
      }
    }

    i21 = j->size[0];
    j->size[0] = kEnd;
    emxEnsureCapacity_int32_T(j, i21);
    pEnd = 0;
    for (i = 0; i <= i2; i++) {
      if (NodesOnElement->data[i] == 0.0) {
        j->data[pEnd] = i + 1;
        pEnd++;
      }
    }

    i2 = j->size[0];
    for (i21 = 0; i21 < i2; i21++) {
      NodesOnElement->data[j->data[i21] - 1] = *numnp + 1.0;
    }

    /* move empty numbers to end of array */
    if (numel < 1.0) {
      setAll->size[0] = 1;
      setAll->size[1] = 0;
    } else {
      i21 = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      i2 = (int)floor(numel - 1.0);
      setAll->size[1] = i2 + 1;
      emxEnsureCapacity_real_T(setAll, i21);
      for (i21 = 0; i21 <= i2; i21++) {
        setAll->data[i21] = 1.0 + (double)i21;
      }
    }

    i21 = b_setAll->size[0] * b_setAll->size[1];
    b_setAll->size[0] = setAll->size[1];
    b_setAll->size[1] = b_loop_ub;
    emxEnsureCapacity_real_T(b_setAll, i21);
    i2 = setAll->size[1];
    for (i21 = 0; i21 < i2; i21++) {
      for (i23 = 0; i23 < b_loop_ub; i23++) {
        i1 = setAll->data[i21];
        b_setAll->data[i21 + b_setAll->size[0] * i23] = i1;
      }
    }

    sparse(ElementsOnNodeRow, NodesOnElement, b_setAll, ElementsOnNode);

    /* zeros(maxel,numnp); % Array of elements connected to nodes */
    sparse(ElementsOnNodeRow, NodesOnElement, NodesOnElement, ElementsOnNodeDup);

    /*  actual node ID given to element for an original node; general counterpart of NodeMat */
    if (1 > (int)nri) {
      i2 = 0;
      kEnd = 0;
      k0 = 0;
    } else {
      i2 = (int)nri;
      kEnd = (int)nri;
      k0 = (int)nri;
    }

    i21 = NodeRegInd->size[0] * NodeRegInd->size[1];
    NodeRegInd->size[0] = 1;
    NodeRegInd->size[1] = i2;
    emxEnsureCapacity_real_T(NodeRegInd, i21);
    for (i21 = 0; i21 < i2; i21++) {
      NodeRegInd->data[i21] = d_NodeRegInd->data[i21 << 1];
    }

    i21 = b_NodeRegInd->size[0] * b_NodeRegInd->size[1];
    b_NodeRegInd->size[0] = 1;
    b_NodeRegInd->size[1] = kEnd;
    emxEnsureCapacity_real_T(b_NodeRegInd, i21);
    for (i21 = 0; i21 < kEnd; i21++) {
      b_NodeRegInd->data[i21] = d_NodeRegInd->data[1 + (i21 << 1)];
    }

    i21 = c_NodeRegInd->size[0] * c_NodeRegInd->size[1];
    c_NodeRegInd->size[0] = 1;
    c_NodeRegInd->size[1] = k0;
    emxEnsureCapacity_real_T(c_NodeRegInd, i21);
    for (i21 = 0; i21 < k0; i21++) {
      c_NodeRegInd->data[i21] = d_NodeRegInd->data[i21 << 1];
    }

    emxFree_real_T(&d_NodeRegInd);
    b_sparse(NodeRegInd, b_NodeRegInd, c_NodeRegInd, NodeReg);
    k0 = NodeReg->colidx->data[NodeReg->colidx->size[0] - 1] - 1;
    if (NodeReg->colidx->data[NodeReg->colidx->size[0] - 1] - 1 == 0) {
      idx->size[0] = 0;
      j->size[0] = 0;
    } else {
      i21 = idx->size[0];
      idx->size[0] = NodeReg->colidx->data[NodeReg->colidx->size[0] - 1] - 1;
      emxEnsureCapacity_int32_T(idx, i21);
      i21 = j->size[0];
      j->size[0] = NodeReg->colidx->data[NodeReg->colidx->size[0] - 1] - 1;
      emxEnsureCapacity_int32_T(j, i21);
      for (b_idx = 0; b_idx < k0; b_idx++) {
        idx->data[b_idx] = NodeReg->rowidx->data[b_idx];
      }

      b_idx = 0;
      kEnd = 1;
      while (b_idx < k0) {
        if (b_idx == NodeReg->colidx->data[kEnd] - 1) {
          kEnd++;
        } else {
          b_idx++;
          j->data[b_idx - 1] = kEnd;
        }
      }

      if (NodeReg->colidx->data[NodeReg->colidx->size[0] - 1] - 1 == 1) {
        if (b_idx == 0) {
          idx->size[0] = 0;
          j->size[0] = 0;
        }
      } else if (1 > b_idx) {
        idx->size[0] = 0;
        j->size[0] = 0;
      } else {
        i21 = idx->size[0];
        idx->size[0] = b_idx;
        emxEnsureCapacity_int32_T(idx, i21);
        i21 = j->size[0];
        j->size[0] = b_idx;
        emxEnsureCapacity_int32_T(j, i21);
      }
    }

    i21 = setnPBC->size[0];
    setnPBC->size[0] = idx->size[0];
    emxEnsureCapacity_real_T(setnPBC, i21);
    i2 = idx->size[0];
    for (i21 = 0; i21 < i2; i21++) {
      setnPBC->data[i21] = idx->data[i21];
    }

    i21 = ycol->size[0];
    ycol->size[0] = j->size[0];
    emxEnsureCapacity_real_T(ycol, i21);
    i2 = j->size[0];
    for (i21 = 0; i21 < i2; i21++) {
      ycol->data[i21] = j->data[i21];
    }

    i21 = ycol->size[0];
    ycol->size[0] = setnPBC->size[0];
    emxEnsureCapacity_real_T(ycol, i21);
    i2 = setnPBC->size[0];
    for (i21 = 0; i21 < i2; i21++) {
      ycol->data[i21] = (int)setnPBC->data[i21] + loop_ub * ((int)ycol->data[i21]
        - 1);
    }

    b_sparse_parenAssign(NodeReg, setnPBC, ycol);
    i2 = NodesOnElement->size[0] * NodesOnElement->size[1] - 1;
    kEnd = 0;
    for (i = 0; i <= i2; i++) {
      if (NodesOnElement->data[i] == *numnp + 1.0) {
        kEnd++;
      }
    }

    i21 = ElementsOnNodePBC_rowidx->size[0];
    ElementsOnNodePBC_rowidx->size[0] = kEnd;
    emxEnsureCapacity_int32_T(ElementsOnNodePBC_rowidx, i21);
    pEnd = 0;
    for (i = 0; i <= i2; i++) {
      if (NodesOnElement->data[i] == *numnp + 1.0) {
        ElementsOnNodePBC_rowidx->data[pEnd] = i + 1;
        pEnd++;
      }
    }

    i2 = ElementsOnNodePBC_rowidx->size[0];
    for (i21 = 0; i21 < i2; i21++) {
      NodesOnElement->data[ElementsOnNodePBC_rowidx->data[i21] - 1] = 0.0;
    }

    /* move empty numbers to end of array */
    i21 = ElementsOnNodeRow->size[0] * ElementsOnNodeRow->size[1];
    ElementsOnNodeRow->size[0] = i22;
    ElementsOnNodeRow->size[1] = b_loop_ub;
    emxEnsureCapacity_real_T(ElementsOnNodeRow, i21);
    for (i21 = 0; i21 < nb; i21++) {
      ElementsOnNodeRow->data[i21] = 0.0;
    }

    c_sparse(ElementsOnNodeRow, ElementsOnNodePBC_d, iwork,
             ElementsOnNodePBC_rowidx, &i2, &k0, &kEnd);

    /*      % get memory back */
    /*      clear NodeRegInd */
    /*      clear ElementsOnNodeRow */
  }

  emxFree_real_T(&b_setAll);
  emxFree_real_T(&ElementsOnNodeRow);
  i21 = NodesOnElementDG->size[0] * NodesOnElementDG->size[1];
  NodesOnElementDG->size[0] = NodesOnElementCG->size[0];
  NodesOnElementDG->size[1] = NodesOnElementCG->size[1];
  emxEnsureCapacity_real_T(NodesOnElementDG, i21);
  b_loop_ub = NodesOnElementCG->size[0] * NodesOnElementCG->size[1];
  for (i21 = 0; i21 < b_loop_ub; i21++) {
    NodesOnElementDG->data[i21] = NodesOnElementCG->data[i21];
  }

  if (1.0 > *numnp) {
    b_loop_ub = 0;
  } else {
    b_loop_ub = (int)*numnp;
  }

  for (i21 = 0; i21 < b_loop_ub; i21++) {
    Coordinates3->data[i21] = Coordinates->data[i21];
  }

  for (i21 = 0; i21 < b_loop_ub; i21++) {
    Coordinates3->data[i21 + Coordinates3->size[0]] = Coordinates->data[i21 +
      Coordinates->size[0]];
  }

  *numinttype = nummat * (nummat + 1.0) / 2.0;
  i21 = numEonF->size[0];
  b_loop_ub = (int)*numinttype;
  numEonF->size[0] = b_loop_ub;
  emxEnsureCapacity_real_T(numEonF, i21);
  for (i21 = 0; i21 < b_loop_ub; i21++) {
    numEonF->data[i21] = 0.0;
  }

  *numEonB = 0.0;
  i21 = ElementsOnFacet->size[0] * ElementsOnFacet->size[1];
  i2 = (int)(4.0 * numel);
  ElementsOnFacet->size[0] = i2;
  ElementsOnFacet->size[1] = 4;
  emxEnsureCapacity_real_T(ElementsOnFacet, i21);
  kEnd = i2 << 2;
  for (i21 = 0; i21 < kEnd; i21++) {
    ElementsOnFacet->data[i21] = 0.0;
  }

  i21 = FacetsOnInterface->size[0];
  FacetsOnInterface->size[0] = i2;
  emxEnsureCapacity_real_T(FacetsOnInterface, i21);
  for (i21 = 0; i21 < i2; i21++) {
    FacetsOnInterface->data[i21] = 0.0;
  }

  i21 = ElementsOnBoundary->size[0] * ElementsOnBoundary->size[1];
  ElementsOnBoundary->size[0] = i2;
  ElementsOnBoundary->size[1] = 2;
  emxEnsureCapacity_real_T(ElementsOnBoundary, i21);
  kEnd = i2 << 1;
  for (i21 = 0; i21 < kEnd; i21++) {
    ElementsOnBoundary->data[i21] = 0.0;
  }

  /*  All exposed faces */
  if (usePBC != 0.0) {
    i21 = FacetsOnPBC->size[0];
    FacetsOnPBC->size[0] = i2;
    emxEnsureCapacity_real_T(FacetsOnPBC, i21);
    for (i21 = 0; i21 < i2; i21++) {
      FacetsOnPBC->data[i21] = 0.0;
    }

    /*  separate list for PBC facets that are found; grouped by interface type */
    i21 = numEonPBC->size[0];
    numEonPBC->size[0] = b_loop_ub;
    emxEnsureCapacity_real_T(numEonPBC, i21);
    for (i21 = 0; i21 < b_loop_ub; i21++) {
      numEonPBC->data[i21] = 0.0;
    }

    numFPBC = 0.0;

    /*  record how many PBC facets are found */
  } else {
    i21 = FacetsOnPBC->size[0];
    FacetsOnPBC->size[0] = 4;
    emxEnsureCapacity_real_T(FacetsOnPBC, i21);
    FacetsOnPBC->data[0] = 0.0;
    FacetsOnPBC->data[1] = 0.0;
    FacetsOnPBC->data[2] = 0.0;
    FacetsOnPBC->data[3] = 0.0;

    /*  separate list for PBC facets that are found; grouped by interface type */
    i21 = numEonPBC->size[0];
    numEonPBC->size[0] = 2;
    emxEnsureCapacity_real_T(numEonPBC, i21);
    numEonPBC->data[0] = 0.0;
    numEonPBC->data[1] = 0.0;
    numFPBC = 0.0;

    /*  record how many PBC facets are found */
  }

  i21 = FacetsOnElement->size[0] * FacetsOnElement->size[1];
  FacetsOnElement->size[0] = (int)numel;
  FacetsOnElement->size[1] = 4;
  emxEnsureCapacity_real_T(FacetsOnElement, i21);
  nb = (int)numel << 2;
  for (i21 = 0; i21 < nb; i21++) {
    FacetsOnElement->data[i21] = 0.0;
  }

  /*  each fac in mesh, gets assigned an fac ID based on its material (1:numSI for that material) */
  i21 = FacetsOnElementInt->size[0] * FacetsOnElementInt->size[1];
  FacetsOnElementInt->size[0] = (int)numel;
  FacetsOnElementInt->size[1] = 4;
  emxEnsureCapacity_real_T(FacetsOnElementInt, i21);
  for (i21 = 0; i21 < nb; i21++) {
    FacetsOnElementInt->data[i21] = 0.0;
  }

  /*  materialI ID for each fac in mesh, according to element */
  i21 = FacetsOnNodeNum->size[0];
  FacetsOnNodeNum->size[0] = loop_ub;
  emxEnsureCapacity_real_T(FacetsOnNodeNum, i21);
  for (i21 = 0; i21 < loop_ub; i21++) {
    FacetsOnNodeNum->data[i21] = 0.0;
  }

  emxInit_real_T(&FacetsOnNodeInd, 2);

  /*  number of facs attached to a node. */
  i21 = FacetsOnNodeInd->size[0] * FacetsOnNodeInd->size[1];
  FacetsOnNodeInd->size[0] = 5;
  i22 = (int)(20.0 * numel);
  FacetsOnNodeInd->size[1] = i22;
  emxEnsureCapacity_real_T(FacetsOnNodeInd, i21);
  loop_ub = 5 * i22;
  for (i21 = 0; i21 < loop_ub; i21++) {
    FacetsOnNodeInd->data[i21] = 0.0;
  }

  nri = 0U;
  nloop3[0] = 1;
  nloop3[3] = 2;
  nloop3[1] = 2;
  nloop3[4] = 3;
  nloop3[2] = 3;
  nloop3[5] = 1;
  nloop4[0] = 1;
  nloop4[4] = 2;
  nloop4[1] = 2;
  nloop4[5] = 3;
  nloop4[2] = 3;
  nloop4[6] = 4;
  nloop4[3] = 4;
  nloop4[7] = 1;
  *numfac = 0.0;

  /*  Find all facets in mesh */
  i21 = (int)numel;
  emxInit_real_T(&ElementsOnNodeA, 2);
  emxInit_real_T(&ElementsOnNodeB, 2);
  emxInit_boolean_T(&r1, 2);
  emxInit_int32_T(&r2, 1);
  emxInit_int32_T(&r3, 1);
  emxInit_int32_T(&b_ycol, 1);
  emxInit_int32_T(&c_ycol, 1);
  for (exponent = 0; exponent < i21; exponent++) {
    if (1.0 > nen) {
      loop_ub = 0;
    } else {
      loop_ub = (int)nen;
    }

    for (i22 = 0; i22 < loop_ub; i22++) {
      nodeloc_data[i22] = NodesOnElementCG->data[exponent +
        NodesOnElementCG->size[0] * i22];
    }

    n = 0;
    i22 = loop_ub - 1;
    for (k = 0; k <= i22; k++) {
      if (nodeloc_data[k] != 0.0) {
        n++;
      }
    }

    if ((n == 3) || (n == 6)) {
      nume = 2;
    } else {
      nume = 3;
    }

    /*      Loop over facs of element */
    for (b_exponent = 0; b_exponent <= nume; b_exponent++) {
      if (!(FacetsOnElement->data[exponent + FacetsOnElement->size[0] *
            b_exponent] != 0.0)) {
        /*  reg1 is overwritten below, so it must be re-evaluated */
        if ((n == 3) || (n == 6)) {
          nodeA = NodesOnElementCG->data[exponent + NodesOnElementCG->size[0] *
            (nloop3[b_exponent] - 1)];
          nodeB = NodesOnElementCG->data[exponent + NodesOnElementCG->size[0] *
            (nloop3[3 + b_exponent] - 1)];
        } else {
          nodeA = NodesOnElementCG->data[exponent + NodesOnElementCG->size[0] *
            (nloop4[b_exponent] - 1)];
          nodeB = NodesOnElementCG->data[exponent + NodesOnElementCG->size[0] *
            (nloop4[4 + b_exponent] - 1)];
        }

        i22 = (int)nodeA - 1;
        if (ElementsOnNodeNum->data[i22] < 1.0) {
          setAll->size[0] = 1;
          setAll->size[1] = 0;
        } else if (rtIsInf(ElementsOnNodeNum->data[i22]) && (1.0 ==
                    ElementsOnNodeNum->data[i22])) {
          i23 = setAll->size[0] * setAll->size[1];
          setAll->size[0] = 1;
          setAll->size[1] = 1;
          emxEnsureCapacity_real_T(setAll, i23);
          setAll->data[0] = rtNaN;
        } else {
          i23 = setAll->size[0] * setAll->size[1];
          setAll->size[0] = 1;
          setAll->size[1] = (int)floor(ElementsOnNodeNum->data[i22] - 1.0) + 1;
          emxEnsureCapacity_real_T(setAll, i23);
          loop_ub = (int)floor(ElementsOnNodeNum->data[i22] - 1.0);
          for (i23 = 0; i23 <= loop_ub; i23++) {
            setAll->data[i23] = 1.0 + (double)i23;
          }
        }

        b_sparse_parenReference(ElementsOnNode->d, ElementsOnNode->colidx,
          ElementsOnNode->rowidx, setAll, nodeA, NodeRegSum.d, NodeRegSum.colidx,
          NodeRegSum.rowidx, &NodeRegSum.m);
        b_sparse_full(NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                      NodeRegSum.m, ElementsOnNodeA);
        i23 = (int)nodeB - 1;
        if (ElementsOnNodeNum->data[i23] < 1.0) {
          setAll->size[0] = 1;
          setAll->size[1] = 0;
        } else if (rtIsInf(ElementsOnNodeNum->data[i23]) && (1.0 ==
                    ElementsOnNodeNum->data[i23])) {
          i24 = setAll->size[0] * setAll->size[1];
          setAll->size[0] = 1;
          setAll->size[1] = 1;
          emxEnsureCapacity_real_T(setAll, i24);
          setAll->data[0] = rtNaN;
        } else {
          i24 = setAll->size[0] * setAll->size[1];
          setAll->size[0] = 1;
          setAll->size[1] = (int)floor(ElementsOnNodeNum->data[i23] - 1.0) + 1;
          emxEnsureCapacity_real_T(setAll, i24);
          loop_ub = (int)floor(ElementsOnNodeNum->data[i23] - 1.0);
          for (i24 = 0; i24 <= loop_ub; i24++) {
            setAll->data[i24] = 1.0 + (double)i24;
          }
        }

        b_sparse_parenReference(ElementsOnNode->d, ElementsOnNode->colidx,
          ElementsOnNode->rowidx, setAll, nodeB, NodeRegSum.d, NodeRegSum.colidx,
          NodeRegSum.rowidx, &NodeRegSum.m);
        b_sparse_full(NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                      NodeRegSum.m, ElementsOnNodeB);

        /*  Determine if fac is on domain interior or boundary */
        guard1 = false;
        if ((ElementsOnNodeNum->data[i22] > 1.0) && (ElementsOnNodeNum->data[i23]
             > 1.0)) {
          /*  Clean and fast way to intersect the 3 sets of elements, using */
          /*  built-in Matlab functions; change ismembc to ismember if the */
          /*  function is not in the standard package */
          /*              if isOctave */
          notdone = ElementsOnNodeA->size[0] - 1;
          pEnd = ElementsOnNodeB->size[0];
          numA = ElementsOnNodeA->size[0];
          i24 = r1->size[0] * r1->size[1];
          r1->size[0] = (int)numA;
          r1->size[1] = 1;
          emxEnsureCapacity_boolean_T(r1, i24);
          loop_ub = (int)numA;
          for (i24 = 0; i24 < loop_ub; i24++) {
            r1->data[i24] = false;
          }

          guard2 = false;
          if (ElementsOnNodeB->size[0] <= 4) {
            guard2 = true;
          } else {
            i2 = 31;
            kEnd = 0;
            exitg2 = false;
            while ((!exitg2) && (i2 - kEnd > 1)) {
              p = (kEnd + i2) >> 1;
              k0 = 1 << p;
              if (k0 == pEnd) {
                i2 = p;
                exitg2 = true;
              } else if (k0 > pEnd) {
                i2 = p;
              } else {
                kEnd = p;
              }
            }

            if (ElementsOnNodeA->size[0] <= 4 + i2) {
              guard2 = true;
            } else {
              i2 = ElementsOnNodeB->size[0];
              b_ElementsOnNodeB = *ElementsOnNodeB;
              c_ElementsOnNodeB[0] = i2;
              b_ElementsOnNodeB.size = &c_ElementsOnNodeB[0];
              b_ElementsOnNodeB.numDimensions = 1;
              if (!issorted(&b_ElementsOnNodeB)) {
                i24 = ycol->size[0];
                ycol->size[0] = ElementsOnNodeB->size[0];
                emxEnsureCapacity_real_T(ycol, i24);
                loop_ub = ElementsOnNodeB->size[0];
                for (i24 = 0; i24 < loop_ub; i24++) {
                  ycol->data[i24] = ElementsOnNodeB->data[i24];
                }

                sort(ycol, b_ycol);
                for (k = 0; k <= notdone; k++) {
                  if (bsearchni(k + 1, ElementsOnNodeA, ycol) > 0) {
                    r1->data[k] = true;
                  }
                }
              } else {
                for (k = 0; k <= notdone; k++) {
                  if (bsearchni(k + 1, ElementsOnNodeA, ElementsOnNodeB) > 0) {
                    r1->data[k] = true;
                  }
                }
              }
            }
          }

          if (guard2) {
            for (b_j = 0; b_j <= notdone; b_j++) {
              k = 0;
              exitg2 = false;
              while ((!exitg2) && (k <= pEnd - 1)) {
                reg = ElementsOnNodeB->data[k];
                absxk = fabs(reg / 2.0);
                if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
                  if (absxk <= 2.2250738585072014E-308) {
                    absxk = 4.94065645841247E-324;
                  } else {
                    frexp(absxk, &c_exponent);
                    absxk = ldexp(1.0, c_exponent - 53);
                  }
                } else {
                  absxk = rtNaN;
                }

                if ((fabs(ElementsOnNodeB->data[k] - ElementsOnNodeA->data[b_j])
                     < absxk) || (rtIsInf(ElementsOnNodeA->data[b_j]) && rtIsInf
                                  (reg) && ((ElementsOnNodeA->data[b_j] > 0.0) ==
                      (ElementsOnNodeB->data[k] > 0.0)))) {
                  r1->data[b_j] = true;
                  exitg2 = true;
                } else {
                  k++;
                }
              }
            }
          }

          i2 = r1->size[0] - 1;
          kEnd = 0;
          for (i = 0; i <= i2; i++) {
            if (r1->data[i]) {
              kEnd++;
            }
          }

          i24 = r2->size[0];
          r2->size[0] = kEnd;
          emxEnsureCapacity_int32_T(r2, i24);
          pEnd = 0;
          for (i = 0; i <= i2; i++) {
            if (r1->data[i]) {
              r2->data[pEnd] = i + 1;
              pEnd++;
            }
          }

          /*              else */
          /*              twoelem = ElementsOnNodeA(ismembc(ElementsOnNodeA,ElementsOnNodeB)); */
          /*              end */
          if (r2->size[0] == 2) {
            /*  element interface */
            if (ElementsOnNodeA->data[r2->data[0] - 1] == 1.0 + (double)exponent)
            {
              i1 = ElementsOnNodeA->data[r2->data[1] - 1];
            } else {
              i1 = ElementsOnNodeA->data[r2->data[0] - 1];
            }

            /* element interface, add to SurfacesI */
            /*  Find which slots nodeA and nodeB occupy on elemA */
            i = 1;
            kEnd = (int)i1;
            k0 = kEnd - 1;
            absxk = NodesOnElementCG->data[k0];
            while ((i < 5) && (absxk != nodeA)) {
              i++;
              absxk = NodesOnElementCG->data[(kEnd + NodesOnElementCG->size[0] *
                (i - 1)) - 1];
            }

            absxk = i;
            i = 1;
            nodeD = NodesOnElementCG->data[k0];
            while ((i < 5) && (nodeD != nodeB)) {
              i++;
              nodeD = NodesOnElementCG->data[(kEnd + NodesOnElementCG->size[0] *
                (i - 1)) - 1];
            }

            i22 = (int)absxk * i;
            if (i22 == 2) {
              notdone = 0;
            } else if (i22 == 6) {
              notdone = 1;
            } else if (i22 == 12) {
              notdone = 2;
            } else if (i22 == 3) {
              notdone = 2;
            } else {
              /*  nodeC*nodeD==4 */
              notdone = 3;
            }

            if (FacetsOnElement->data[(kEnd + FacetsOnElement->size[0] * notdone)
                - 1] == 0.0) {
              /*  New fac, add to list */
              if (1.0 > nen) {
                loop_ub = 0;
              } else {
                loop_ub = (int)nen;
              }

              for (i22 = 0; i22 < loop_ub; i22++) {
                nodeloc_data[i22] = NodesOnElementCG->data[(kEnd +
                  NodesOnElementCG->size[0] * i22) - 1];
              }

              pEnd = 0;
              i22 = loop_ub - 1;
              for (k = 0; k <= i22; k++) {
                if (nodeloc_data[k] != 0.0) {
                  pEnd++;
                }
              }

              kEnd = n;
              nb = exponent;
              q = b_exponent;
              if (RegionOnElement->data[k0] > RegionOnElement->data[exponent]) {
                /* swap the order of L and R so that L is always larger material ID */
                regA = RegionOnElement->data[exponent];
                regB = RegionOnElement->data[k0];
                nb = k0;
                i1 = 1.0 + (double)exponent;
                q = notdone;
                notdone = b_exponent;
                kEnd = pEnd;
                pEnd = n;
              } else {
                regA = RegionOnElement->data[k0];
                regB = RegionOnElement->data[exponent];
              }

              regI = regB * (regB - 1.0) / 2.0 + regA;

              /*  ID for material pair (row=mat2, col=mat1) */
              numPBC = (int)regI - 1;
              *numSI = numEonF->data[numPBC] + 1.0;
              (*numfac)++;
              numEonF->data[numPBC]++;
              FacetsOnElement->data[nb + FacetsOnElement->size[0] * q] = *numfac;
              i22 = (int)i1;
              FacetsOnElement->data[(i22 + FacetsOnElement->size[0] * notdone) -
                1] = *numfac;
              FacetsOnElementInt->data[nb + FacetsOnElementInt->size[0] * q] =
                regI;
              FacetsOnElementInt->data[(i22 + FacetsOnElementInt->size[0] *
                notdone) - 1] = regI;
              if ((pEnd == 3) || (pEnd == 6)) {
                nodeA = NodesOnElementCG->data[(i22 + NodesOnElementCG->size[0] *
                  (nloop3[notdone] - 1)) - 1];
                nodeB = NodesOnElementCG->data[(i22 + NodesOnElementCG->size[0] *
                  (nloop3[3 + notdone] - 1)) - 1];
              } else {
                nodeA = NodesOnElementCG->data[(i22 + NodesOnElementCG->size[0] *
                  (nloop4[notdone] - 1)) - 1];
                nodeB = NodesOnElementCG->data[(i22 + NodesOnElementCG->size[0] *
                  (nloop4[4 + notdone] - 1)) - 1];
              }

              i23 = (int)*numfac;
              i24 = i23 - 1;
              ElementsOnFacet->data[i24] = nb + 1;

              /* elemL */
              ElementsOnFacet->data[(i23 + ElementsOnFacet->size[0]) - 1] = i1;

              /* elemR */
              ElementsOnFacet->data[(i23 + (ElementsOnFacet->size[0] << 1)) - 1]
                = (double)q + 1.0;

              /* facL */
              ElementsOnFacet->data[(i23 + ElementsOnFacet->size[0] * 3) - 1] =
                (double)notdone + 1.0;

              /* facR */
              FacetsOnInterface->data[i24] = regI;

              /*  Assign nodal fac pairs */
              i2 = (int)nodeA - 1;
              i1 = FacetsOnNodeNum->data[i2] + 1.0;
              FacetsOnNodeNum->data[i2]++;
              nri += 2U;
              i23 = 5 * ((int)nri - 2);
              FacetsOnNodeInd->data[i23] = i1;
              FacetsOnNodeInd->data[1 + i23] = nodeA;
              FacetsOnNodeInd->data[2 + i23] = *numfac;
              FacetsOnNodeInd->data[3 + i23] = regI;

              /*                  FacetsOnNode(facnumA,nodeA) = numfac; */
              /*                  FacetsOnNodeInt(facnumA,nodeA) = regI; */
              i2 = (int)nodeB - 1;
              i1 = FacetsOnNodeNum->data[i2] + 1.0;
              FacetsOnNodeNum->data[i2]++;
              i24 = 5 * ((int)nri - 1);
              FacetsOnNodeInd->data[i24] = i1;
              FacetsOnNodeInd->data[1 + i24] = nodeB;
              FacetsOnNodeInd->data[2 + i24] = *numfac;
              FacetsOnNodeInd->data[3 + i24] = regI;

              /*                  FacetsOnNode(facnumB,nodeB) = numfac; */
              /*                  FacetsOnNodeInt(facnumB,nodeB) = regI; */
              if (usePBC != 0.0) {
                if ((pEnd == 3) || (pEnd == 6)) {
                  i1 = NodesOnElementPBC->data[(i22 + NodesOnElementPBC->size[0]
                    * (nloop3[notdone] - 1)) - 1];
                  numA = NodesOnElementPBC->data[(i22 + NodesOnElementPBC->size
                    [0] * (nloop3[3 + notdone] - 1)) - 1];
                } else {
                  i1 = NodesOnElementPBC->data[(i22 + NodesOnElementPBC->size[0]
                    * (nloop4[notdone] - 1)) - 1];
                  numA = NodesOnElementPBC->data[(i22 + NodesOnElementPBC->size
                    [0] * (nloop4[4 + notdone] - 1)) - 1];
                }

                if ((kEnd == 3) || (kEnd == 6)) {
                  kEnd = nloop3[q] - 1;
                  absxk = NodesOnElementCG->data[nb + NodesOnElementCG->size[0] *
                    kEnd];
                  i2 = nloop3[3 + q] - 1;
                  nodeD = NodesOnElementCG->data[nb + NodesOnElementCG->size[0] *
                    i2];
                  reg = NodesOnElementPBC->data[nb + NodesOnElementPBC->size[0] *
                    kEnd];
                  nodeD2 = NodesOnElementPBC->data[nb + NodesOnElementPBC->size
                    [0] * i2];
                } else {
                  kEnd = nloop4[q] - 1;
                  absxk = NodesOnElementCG->data[nb + NodesOnElementCG->size[0] *
                    kEnd];
                  i2 = nloop4[4 + q] - 1;
                  nodeD = NodesOnElementCG->data[nb + NodesOnElementCG->size[0] *
                    i2];
                  reg = NodesOnElementPBC->data[nb + NodesOnElementPBC->size[0] *
                    kEnd];
                  nodeD2 = NodesOnElementPBC->data[nb + NodesOnElementPBC->size
                    [0] * i2];
                }

                if (((nodeA != i1) && (nodeB != numA)) || ((absxk != reg) &&
                     (nodeD != nodeD2))) {
                  /*  Make sure facet is NOT an interface in the */
                  /*  unzipped mesh, because for triangles both nodes */
                  /*  might be in PBCList but they are on different */
                  /*  boundary surfaces */
                  i22 = (int)i1 - 1;
                  if (ElementsOnNodePBCNum->data[i22] < 1.0) {
                    setAll->size[0] = 1;
                    setAll->size[1] = 0;
                  } else if (rtIsInf(ElementsOnNodePBCNum->data[i22]) && (1.0 ==
                              ElementsOnNodePBCNum->data[i22])) {
                    notdone = setAll->size[0] * setAll->size[1];
                    setAll->size[0] = 1;
                    setAll->size[1] = 1;
                    emxEnsureCapacity_real_T(setAll, notdone);
                    setAll->data[0] = rtNaN;
                  } else {
                    notdone = setAll->size[0] * setAll->size[1];
                    setAll->size[0] = 1;
                    setAll->size[1] = (int)floor(ElementsOnNodePBCNum->data[i22]
                      - 1.0) + 1;
                    emxEnsureCapacity_real_T(setAll, notdone);
                    loop_ub = (int)floor(ElementsOnNodePBCNum->data[i22] - 1.0);
                    for (notdone = 0; notdone <= loop_ub; notdone++) {
                      setAll->data[notdone] = 1.0 + (double)notdone;
                    }
                  }

                  b_sparse_parenReference(ElementsOnNodePBC_d, iwork,
                    ElementsOnNodePBC_rowidx, setAll, i1, NodeRegSum.d,
                    NodeRegSum.colidx, NodeRegSum.rowidx, &NodeRegSum.m);
                  b_sparse_full(NodeRegSum.d, NodeRegSum.colidx,
                                NodeRegSum.rowidx, NodeRegSum.m, ElementsOnNodeA);
                  notdone = (int)numA - 1;
                  if (ElementsOnNodePBCNum->data[notdone] < 1.0) {
                    setAll->size[0] = 1;
                    setAll->size[1] = 0;
                  } else if (rtIsInf(ElementsOnNodePBCNum->data[notdone]) &&
                             (1.0 == ElementsOnNodePBCNum->data[notdone])) {
                    k0 = setAll->size[0] * setAll->size[1];
                    setAll->size[0] = 1;
                    setAll->size[1] = 1;
                    emxEnsureCapacity_real_T(setAll, k0);
                    setAll->data[0] = rtNaN;
                  } else {
                    k0 = setAll->size[0] * setAll->size[1];
                    setAll->size[0] = 1;
                    setAll->size[1] = (int)floor(ElementsOnNodePBCNum->
                      data[notdone] - 1.0) + 1;
                    emxEnsureCapacity_real_T(setAll, k0);
                    loop_ub = (int)floor(ElementsOnNodePBCNum->data[notdone] -
                                         1.0);
                    for (k0 = 0; k0 <= loop_ub; k0++) {
                      setAll->data[k0] = 1.0 + (double)k0;
                    }
                  }

                  b_sparse_parenReference(ElementsOnNodePBC_d, iwork,
                    ElementsOnNodePBC_rowidx, setAll, numA, NodeRegSum.d,
                    NodeRegSum.colidx, NodeRegSum.rowidx, &NodeRegSum.m);
                  b_sparse_full(NodeRegSum.d, NodeRegSum.colidx,
                                NodeRegSum.rowidx, NodeRegSum.m, ElementsOnNodeB);

                  /*  Determine if fac is on domain interior or boundary */
                  guard2 = false;
                  if ((ElementsOnNodePBCNum->data[i22] > 1.0) &&
                      (ElementsOnNodePBCNum->data[notdone] > 1.0)) {
                    /*  Clean and fast way to intersect the 3 sets of elements, using */
                    /*  built-in Matlab functions; change ismembc to ismember if the */
                    /*  function is not in the standard package */
                    /*                            if isOctave */
                    notdone = ElementsOnNodeA->size[0] - 1;
                    pEnd = ElementsOnNodeB->size[0];
                    numA = ElementsOnNodeA->size[0];
                    i22 = r1->size[0] * r1->size[1];
                    r1->size[0] = (int)numA;
                    r1->size[1] = 1;
                    emxEnsureCapacity_boolean_T(r1, i22);
                    loop_ub = (int)numA;
                    for (i22 = 0; i22 < loop_ub; i22++) {
                      r1->data[i22] = false;
                    }

                    guard3 = false;
                    if (ElementsOnNodeB->size[0] <= 4) {
                      guard3 = true;
                    } else {
                      i2 = 31;
                      kEnd = 0;
                      exitg2 = false;
                      while ((!exitg2) && (i2 - kEnd > 1)) {
                        p = (kEnd + i2) >> 1;
                        k0 = 1 << p;
                        if (k0 == pEnd) {
                          i2 = p;
                          exitg2 = true;
                        } else if (k0 > pEnd) {
                          i2 = p;
                        } else {
                          kEnd = p;
                        }
                      }

                      if (ElementsOnNodeA->size[0] <= 4 + i2) {
                        guard3 = true;
                      } else {
                        i2 = ElementsOnNodeB->size[0];
                        b_ElementsOnNodeB = *ElementsOnNodeB;
                        d_ElementsOnNodeB[0] = i2;
                        b_ElementsOnNodeB.size = &d_ElementsOnNodeB[0];
                        b_ElementsOnNodeB.numDimensions = 1;
                        if (!issorted(&b_ElementsOnNodeB)) {
                          i22 = ycol->size[0];
                          ycol->size[0] = ElementsOnNodeB->size[0];
                          emxEnsureCapacity_real_T(ycol, i22);
                          loop_ub = ElementsOnNodeB->size[0];
                          for (i22 = 0; i22 < loop_ub; i22++) {
                            ycol->data[i22] = ElementsOnNodeB->data[i22];
                          }

                          sort(ycol, c_ycol);
                          for (k = 0; k <= notdone; k++) {
                            if (bsearchni(k + 1, ElementsOnNodeA, ycol) > 0) {
                              r1->data[k] = true;
                            }
                          }
                        } else {
                          for (k = 0; k <= notdone; k++) {
                            if (bsearchni(k + 1, ElementsOnNodeA,
                                          ElementsOnNodeB) > 0) {
                              r1->data[k] = true;
                            }
                          }
                        }
                      }
                    }

                    if (guard3) {
                      for (b_j = 0; b_j <= notdone; b_j++) {
                        k = 0;
                        exitg2 = false;
                        while ((!exitg2) && (k <= pEnd - 1)) {
                          reg = ElementsOnNodeB->data[k];
                          absxk = fabs(reg / 2.0);
                          if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
                            if (absxk <= 2.2250738585072014E-308) {
                              absxk = 4.94065645841247E-324;
                            } else {
                              frexp(absxk, &d_exponent);
                              absxk = ldexp(1.0, d_exponent - 53);
                            }
                          } else {
                            absxk = rtNaN;
                          }

                          if ((fabs(ElementsOnNodeB->data[k] -
                                    ElementsOnNodeA->data[b_j]) < absxk) ||
                              (rtIsInf(ElementsOnNodeA->data[b_j]) && rtIsInf
                               (reg) && ((ElementsOnNodeA->data[b_j] > 0.0) ==
                                         (ElementsOnNodeB->data[k] > 0.0)))) {
                            r1->data[b_j] = true;
                            exitg2 = true;
                          } else {
                            k++;
                          }
                        }
                      }
                    }

                    i2 = r1->size[0] - 1;
                    kEnd = 0;
                    for (i = 0; i <= i2; i++) {
                      if (r1->data[i]) {
                        kEnd++;
                      }
                    }

                    i22 = r3->size[0];
                    r3->size[0] = kEnd;
                    emxEnsureCapacity_int32_T(r3, i22);
                    pEnd = 0;
                    for (i = 0; i <= i2; i++) {
                      if (r1->data[i]) {
                        r3->data[pEnd] = i + 1;
                        pEnd++;
                      }
                    }

                    /*                            else */
                    /*                            twoelem = ElementsOnNodeA(ismembc(ElementsOnNodeA,ElementsOnNodeB)); */
                    /*                            end */
                    if (r3->size[0] == 2) {
                      /*  element interface */
                      k0 = 0;
                    } else {
                      /*  domain boundary */
                      guard2 = true;
                    }
                  } else {
                    /*  domain boundary */
                    guard2 = true;
                  }

                  if (guard2) {
                    k0 = 1;
                    *numSI = numEonPBC->data[numPBC] + 1.0;
                    numEonPBC->data[numPBC]++;
                    numFPBC++;
                    FacetsOnPBC->data[(int)*numfac - 1] = regI;
                  }
                } else {
                  k0 = 0;
                }
              } else {
                k0 = 0;
              }

              if ((InterTypes->data[((int)regB + InterTypes->size[0] * ((int)
                     regA - 1)) - 1] > 0.0) || (k0 != 0)) {
                /*  Mark facs as being cut */
                FacetsOnNodeInd->data[4 + i23] = 1.0;
                FacetsOnNodeInd->data[4 + i24] = 1.0;

                /*                      FacetsOnNodeCut(facnumA,nodeA) = 1; */
                /*                      FacetsOnNodeCut(facnumB,nodeB) = 1; */
              }
            }
          } else {
            /*  domain boundary */
            guard1 = true;
          }
        } else {
          /*  domain boundary */
          guard1 = true;
        }

        if (guard1) {
          /* domain boundary, add to SurfaceF */
          *numSI = *numEonB + 1.0;
          (*numEonB)++;
          FacetsOnElement->data[exponent + FacetsOnElement->size[0] * b_exponent]
            = -*numSI;
          i24 = (int)*numSI;
          ElementsOnBoundary->data[i24 - 1] = 1.0 + (double)exponent;
          ElementsOnBoundary->data[(i24 + ElementsOnBoundary->size[0]) - 1] =
            1.0 + (double)b_exponent;

          /*  Assign nodal fac pairs */
          i1 = FacetsOnNodeNum->data[i22] + 1.0;
          FacetsOnNodeNum->data[i22]++;
          nri += 2U;
          i22 = 5 * ((int)nri - 2);
          FacetsOnNodeInd->data[i22] = i1;
          FacetsOnNodeInd->data[1 + i22] = nodeA;
          FacetsOnNodeInd->data[2 + i22] = *numSI;
          FacetsOnNodeInd->data[3 + i22] = -1.0;

          /*              FacetsOnNode(facnum,nodeA) = numSI; */
          /*              FacetsOnNodeInt(facnum,nodeA) = -1; */
          i1 = FacetsOnNodeNum->data[i23] + 1.0;
          FacetsOnNodeNum->data[i23]++;
          i22 = 5 * ((int)nri - 1);
          FacetsOnNodeInd->data[i22] = i1;
          FacetsOnNodeInd->data[1 + i22] = nodeB;
          FacetsOnNodeInd->data[2 + i22] = *numSI;
          FacetsOnNodeInd->data[3 + i22] = -1.0;

          /*              FacetsOnNode(facnum,nodeB) = numSI; */
          /*              FacetsOnNodeInt(facnum,nodeB) = -1; */
        }
      }

      /* facet not identified */
    }
  }

  emxFree_int32_T(&c_ycol);
  emxFree_int32_T(&b_ycol);
  emxFree_int32_T(&r3);
  emxFree_int32_T(&r2);
  emxFree_boolean_T(&r1);
  emxFree_real_T(&ElementsOnNodeB);
  if (1.0 > *numfac) {
    loop_ub = 0;
  } else {
    loop_ub = (int)*numfac;
  }

  emxInit_real_T(&b_ElementsOnFacet, 2);
  i21 = b_ElementsOnFacet->size[0] * b_ElementsOnFacet->size[1];
  b_ElementsOnFacet->size[0] = loop_ub;
  b_ElementsOnFacet->size[1] = 4;
  emxEnsureCapacity_real_T(b_ElementsOnFacet, i21);
  for (i21 = 0; i21 < loop_ub; i21++) {
    b_ElementsOnFacet->data[i21] = ElementsOnFacet->data[i21];
  }

  for (i21 = 0; i21 < loop_ub; i21++) {
    b_ElementsOnFacet->data[i21 + b_ElementsOnFacet->size[0]] =
      ElementsOnFacet->data[i21 + ElementsOnFacet->size[0]];
  }

  for (i21 = 0; i21 < loop_ub; i21++) {
    b_ElementsOnFacet->data[i21 + (b_ElementsOnFacet->size[0] << 1)] =
      ElementsOnFacet->data[i21 + (ElementsOnFacet->size[0] << 1)];
  }

  for (i21 = 0; i21 < loop_ub; i21++) {
    b_ElementsOnFacet->data[i21 + b_ElementsOnFacet->size[0] * 3] =
      ElementsOnFacet->data[i21 + ElementsOnFacet->size[0] * 3];
  }

  i21 = ElementsOnFacet->size[0] * ElementsOnFacet->size[1];
  ElementsOnFacet->size[0] = b_ElementsOnFacet->size[0];
  ElementsOnFacet->size[1] = 4;
  emxEnsureCapacity_real_T(ElementsOnFacet, i21);
  loop_ub = b_ElementsOnFacet->size[0] * b_ElementsOnFacet->size[1];
  for (i21 = 0; i21 < loop_ub; i21++) {
    ElementsOnFacet->data[i21] = b_ElementsOnFacet->data[i21];
  }

  emxFree_real_T(&b_ElementsOnFacet);
  if (1.0 > *numEonB) {
    loop_ub = 0;
  } else {
    loop_ub = (int)*numEonB;
  }

  i21 = a->size[0] * a->size[1];
  a->size[0] = loop_ub;
  a->size[1] = 2;
  emxEnsureCapacity_real_T(a, i21);
  for (i21 = 0; i21 < loop_ub; i21++) {
    a->data[i21] = ElementsOnBoundary->data[i21];
  }

  for (i21 = 0; i21 < loop_ub; i21++) {
    a->data[i21 + a->size[0]] = ElementsOnBoundary->data[i21 +
      ElementsOnBoundary->size[0]];
  }

  i21 = ElementsOnBoundary->size[0] * ElementsOnBoundary->size[1];
  ElementsOnBoundary->size[0] = a->size[0];
  ElementsOnBoundary->size[1] = 2;
  emxEnsureCapacity_real_T(ElementsOnBoundary, i21);
  loop_ub = a->size[0] * a->size[1];
  for (i21 = 0; i21 < loop_ub; i21++) {
    ElementsOnBoundary->data[i21] = a->data[i21];
  }

  emxFree_real_T(&a);
  if (1 > (int)nri) {
    loop_ub = 0;
    i2 = 0;
    kEnd = 0;
  } else {
    loop_ub = (int)nri;
    i2 = (int)nri;
    kEnd = (int)nri;
  }

  i21 = NodeRegInd->size[0] * NodeRegInd->size[1];
  NodeRegInd->size[0] = 1;
  NodeRegInd->size[1] = loop_ub;
  emxEnsureCapacity_real_T(NodeRegInd, i21);
  for (i21 = 0; i21 < loop_ub; i21++) {
    NodeRegInd->data[i21] = FacetsOnNodeInd->data[5 * i21];
  }

  i21 = b_NodeRegInd->size[0] * b_NodeRegInd->size[1];
  b_NodeRegInd->size[0] = 1;
  b_NodeRegInd->size[1] = i2;
  emxEnsureCapacity_real_T(b_NodeRegInd, i21);
  for (i21 = 0; i21 < i2; i21++) {
    b_NodeRegInd->data[i21] = FacetsOnNodeInd->data[1 + 5 * i21];
  }

  i21 = c_NodeRegInd->size[0] * c_NodeRegInd->size[1];
  c_NodeRegInd->size[0] = 1;
  c_NodeRegInd->size[1] = kEnd;
  emxEnsureCapacity_real_T(c_NodeRegInd, i21);
  for (i21 = 0; i21 < kEnd; i21++) {
    c_NodeRegInd->data[i21] = FacetsOnNodeInd->data[2 + 5 * i21];
  }

  b_sparse(NodeRegInd, b_NodeRegInd, c_NodeRegInd, FacetsOnNode);

  /*  for each node, which other node is connected to it by an edge; value is the nodal ID of the connecting node */
  if (1 > (int)nri) {
    loop_ub = 0;
    i2 = 0;
    kEnd = 0;
  } else {
    loop_ub = (int)nri;
    i2 = (int)nri;
    kEnd = (int)nri;
  }

  i21 = NodeRegInd->size[0] * NodeRegInd->size[1];
  NodeRegInd->size[0] = 1;
  NodeRegInd->size[1] = loop_ub;
  emxEnsureCapacity_real_T(NodeRegInd, i21);
  for (i21 = 0; i21 < loop_ub; i21++) {
    NodeRegInd->data[i21] = FacetsOnNodeInd->data[5 * i21];
  }

  i21 = b_NodeRegInd->size[0] * b_NodeRegInd->size[1];
  b_NodeRegInd->size[0] = 1;
  b_NodeRegInd->size[1] = i2;
  emxEnsureCapacity_real_T(b_NodeRegInd, i21);
  for (i21 = 0; i21 < i2; i21++) {
    b_NodeRegInd->data[i21] = FacetsOnNodeInd->data[1 + 5 * i21];
  }

  i21 = c_NodeRegInd->size[0] * c_NodeRegInd->size[1];
  c_NodeRegInd->size[0] = 1;
  c_NodeRegInd->size[1] = kEnd;
  emxEnsureCapacity_real_T(c_NodeRegInd, i21);
  for (i21 = 0; i21 < kEnd; i21++) {
    c_NodeRegInd->data[i21] = FacetsOnNodeInd->data[4 + 5 * i21];
  }

  b_sparse(NodeRegInd, b_NodeRegInd, c_NodeRegInd, FacetsOnNodeCut);

  /*  flag for whether that edge is being cut between two nodes; 1 for cut, 0 for retain. */
  if (1 > (int)nri) {
    loop_ub = 0;
    i2 = 0;
    kEnd = 0;
  } else {
    loop_ub = (int)nri;
    i2 = (int)nri;
    kEnd = (int)nri;
  }

  i21 = NodeRegInd->size[0] * NodeRegInd->size[1];
  NodeRegInd->size[0] = 1;
  NodeRegInd->size[1] = loop_ub;
  emxEnsureCapacity_real_T(NodeRegInd, i21);
  for (i21 = 0; i21 < loop_ub; i21++) {
    NodeRegInd->data[i21] = FacetsOnNodeInd->data[5 * i21];
  }

  i21 = b_NodeRegInd->size[0] * b_NodeRegInd->size[1];
  b_NodeRegInd->size[0] = 1;
  b_NodeRegInd->size[1] = i2;
  emxEnsureCapacity_real_T(b_NodeRegInd, i21);
  for (i21 = 0; i21 < i2; i21++) {
    b_NodeRegInd->data[i21] = FacetsOnNodeInd->data[1 + 5 * i21];
  }

  i21 = c_NodeRegInd->size[0] * c_NodeRegInd->size[1];
  c_NodeRegInd->size[0] = 1;
  c_NodeRegInd->size[1] = kEnd;
  emxEnsureCapacity_real_T(c_NodeRegInd, i21);
  for (i21 = 0; i21 < kEnd; i21++) {
    c_NodeRegInd->data[i21] = FacetsOnNodeInd->data[3 + 5 * i21];
  }

  emxFree_real_T(&FacetsOnNodeInd);
  b_sparse(NodeRegInd, b_NodeRegInd, c_NodeRegInd, FacetsOnNodeInt);

  /*  flag for materialI ID for that edge; -1 means domain edge. */
  /*  clear FacetsOnNodeInd */
  /*  Group facets according to interface type */
  emxFree_real_T(&c_NodeRegInd);
  emxFree_real_T(&b_NodeRegInd);
  emxFree_real_T(&NodeRegInd);
  if (1.0 > *numfac) {
    loop_ub = 0;
  } else {
    loop_ub = (int)*numfac;
  }

  i21 = ycol->size[0];
  ycol->size[0] = loop_ub;
  emxEnsureCapacity_real_T(ycol, i21);
  for (i21 = 0; i21 < loop_ub; i21++) {
    ycol->data[i21] = FacetsOnInterface->data[i21];
  }

  sort(ycol, iwork);
  i21 = FacetsOnInterface->size[0];
  FacetsOnInterface->size[0] = iwork->size[0];
  emxEnsureCapacity_real_T(FacetsOnInterface, i21);
  loop_ub = iwork->size[0];
  for (i21 = 0; i21 < loop_ub; i21++) {
    FacetsOnInterface->data[i21] = iwork->data[i21];
  }

  i21 = FacetsOnIntMinusPBCNum->size[0];
  FacetsOnIntMinusPBCNum->size[0] = 1 + numEonF->size[0];
  emxEnsureCapacity_real_T(FacetsOnIntMinusPBCNum, i21);
  FacetsOnIntMinusPBCNum->data[0] = 1.0;
  loop_ub = numEonF->size[0];
  for (i21 = 0; i21 < loop_ub; i21++) {
    FacetsOnIntMinusPBCNum->data[i21 + 1] = numEonF->data[i21];
  }

  i21 = FacetsOnInterfaceNum->size[0];
  FacetsOnInterfaceNum->size[0] = FacetsOnIntMinusPBCNum->size[0];
  emxEnsureCapacity_real_T(FacetsOnInterfaceNum, i21);
  loop_ub = FacetsOnIntMinusPBCNum->size[0];
  for (i21 = 0; i21 < loop_ub; i21++) {
    FacetsOnInterfaceNum->data[i21] = FacetsOnIntMinusPBCNum->data[i21];
  }

  for (kEnd = 0; kEnd < b_loop_ub; kEnd++) {
    FacetsOnInterfaceNum->data[kEnd + 1] += FacetsOnInterfaceNum->data[kEnd];
  }

  /*  The actual facet identifiers for interface type regI are then: */
  /*  locF = FacetsOnInterfaceNum(regI):(FacetsOnInterfaceNum(regI+1)-1); */
  /*  facs = FacetsOnInterface(locF); */
  /*  true = all(ElementsOnFacet2(facs,1:4) == ElementsOnFacet(1:numEonF(regI),1:4,regI)); */
  if (usePBC != 0.0) {
    /*  sort to find the list of PBC facets grouped by interface type */
    if (1.0 > *numfac) {
      loop_ub = 0;
    } else {
      loop_ub = (int)*numfac;
    }

    i21 = ycol->size[0];
    ycol->size[0] = loop_ub;
    emxEnsureCapacity_real_T(ycol, i21);
    for (i21 = 0; i21 < loop_ub; i21++) {
      ycol->data[i21] = FacetsOnPBC->data[i21];
    }

    sort(ycol, iwork);
    i21 = ycol->size[0];
    ycol->size[0] = iwork->size[0];
    emxEnsureCapacity_real_T(ycol, i21);
    loop_ub = iwork->size[0];
    for (i21 = 0; i21 < loop_ub; i21++) {
      ycol->data[i21] = iwork->data[i21];
    }

    i1 = *numfac - numFPBC;
    if (i1 + 1.0 > *numfac) {
      i21 = 1;
      i22 = 0;
    } else {
      i21 = (int)(i1 + 1.0);
      i22 = (int)*numfac;
    }

    i23 = FacetsOnPBC->size[0];
    loop_ub = i22 - i21;
    FacetsOnPBC->size[0] = loop_ub + 1;
    emxEnsureCapacity_real_T(FacetsOnPBC, i23);
    for (i22 = 0; i22 <= loop_ub; i22++) {
      FacetsOnPBC->data[i22] = ycol->data[(i21 + i22) - 1];
    }

    /*  delete the zeros */
    i22 = FacetsOnPBCNum->size[0];
    FacetsOnPBCNum->size[0] = 1 + numEonPBC->size[0];
    emxEnsureCapacity_real_T(FacetsOnPBCNum, i22);
    FacetsOnPBCNum->data[0] = 1.0;
    loop_ub = numEonPBC->size[0];
    for (i22 = 0; i22 < loop_ub; i22++) {
      FacetsOnPBCNum->data[i22 + 1] = numEonPBC->data[i22];
    }

    for (kEnd = 0; kEnd < b_loop_ub; kEnd++) {
      FacetsOnPBCNum->data[kEnd + 1] += FacetsOnPBCNum->data[kEnd];
    }

    i22 = FacetsOnIntMinusPBC->size[0];
    loop_ub = (int)i1;
    FacetsOnIntMinusPBC->size[0] = loop_ub;
    emxEnsureCapacity_real_T(FacetsOnIntMinusPBC, i22);
    for (i22 = 0; i22 < loop_ub; i22++) {
      FacetsOnIntMinusPBC->data[i22] = 0.0;
    }

    emxInit_real_T(&setPBC, 2);
    for (kEnd = 0; kEnd < b_loop_ub; kEnd++) {
      if (FacetsOnInterfaceNum->data[1 + kEnd] - 1.0 <
          FacetsOnInterfaceNum->data[kEnd]) {
        setAll->size[0] = 1;
        setAll->size[1] = 0;
      } else if ((rtIsInf(FacetsOnInterfaceNum->data[kEnd]) || rtIsInf
                  (FacetsOnInterfaceNum->data[1 + kEnd] - 1.0)) &&
                 (FacetsOnInterfaceNum->data[kEnd] == FacetsOnInterfaceNum->
                  data[1 + kEnd] - 1.0)) {
        i22 = setAll->size[0] * setAll->size[1];
        setAll->size[0] = 1;
        setAll->size[1] = 1;
        emxEnsureCapacity_real_T(setAll, i22);
        setAll->data[0] = rtNaN;
      } else if (FacetsOnInterfaceNum->data[kEnd] == FacetsOnInterfaceNum->
                 data[kEnd]) {
        i1 = FacetsOnInterfaceNum->data[1 + kEnd] - 1.0;
        i22 = setAll->size[0] * setAll->size[1];
        setAll->size[0] = 1;
        setAll->size[1] = (int)floor(i1 - FacetsOnInterfaceNum->data[kEnd]) + 1;
        emxEnsureCapacity_real_T(setAll, i22);
        loop_ub = (int)floor(i1 - FacetsOnInterfaceNum->data[kEnd]);
        for (i22 = 0; i22 <= loop_ub; i22++) {
          setAll->data[i22] = FacetsOnInterfaceNum->data[kEnd] + (double)i22;
        }
      } else {
        i1 = floor(((FacetsOnInterfaceNum->data[1 + kEnd] - 1.0) -
                    FacetsOnInterfaceNum->data[kEnd]) + 0.5);
        numA = FacetsOnInterfaceNum->data[kEnd] + i1;
        reg = numA - (FacetsOnInterfaceNum->data[1 + kEnd] - 1.0);
        absxk = FacetsOnInterfaceNum->data[kEnd];
        nodeD2 = fabs(FacetsOnInterfaceNum->data[1 + kEnd] - 1.0);
        if ((absxk > nodeD2) || rtIsNaN(nodeD2)) {
          nodeD2 = absxk;
        }

        if (fabs(reg) < 4.4408920985006262E-16 * nodeD2) {
          i1++;
          numA = FacetsOnInterfaceNum->data[1 + kEnd] - 1.0;
        } else if (reg > 0.0) {
          numA = FacetsOnInterfaceNum->data[kEnd] + (i1 - 1.0);
        } else {
          i1++;
        }

        if (i1 >= 0.0) {
          n = (int)i1;
        } else {
          n = 0;
        }

        i22 = setAll->size[0] * setAll->size[1];
        setAll->size[0] = 1;
        setAll->size[1] = n;
        emxEnsureCapacity_real_T(setAll, i22);
        if (n > 0) {
          setAll->data[0] = FacetsOnInterfaceNum->data[kEnd];
          if (n > 1) {
            setAll->data[n - 1] = numA;
            k0 = (n - 1) / 2;
            for (k = 0; k <= k0 - 2; k++) {
              setAll->data[1 + k] = FacetsOnInterfaceNum->data[kEnd] + (1.0 +
                (double)k);
              setAll->data[(n - k) - 2] = numA - (1.0 + (double)k);
            }

            if (k0 << 1 == n - 1) {
              setAll->data[k0] = (FacetsOnInterfaceNum->data[kEnd] + numA) / 2.0;
            } else {
              setAll->data[k0] = FacetsOnInterfaceNum->data[kEnd] + (double)k0;
              setAll->data[k0 + 1] = numA - (double)k0;
            }
          }
        }
      }

      if (FacetsOnPBCNum->data[1 + kEnd] - 1.0 < FacetsOnPBCNum->data[kEnd]) {
        setPBC->size[0] = 1;
        setPBC->size[1] = 0;
      } else if ((rtIsInf(FacetsOnPBCNum->data[kEnd]) || rtIsInf
                  (FacetsOnPBCNum->data[1 + kEnd] - 1.0)) &&
                 (FacetsOnPBCNum->data[kEnd] == FacetsOnPBCNum->data[1 + kEnd] -
                  1.0)) {
        i22 = setPBC->size[0] * setPBC->size[1];
        setPBC->size[0] = 1;
        setPBC->size[1] = 1;
        emxEnsureCapacity_real_T(setPBC, i22);
        setPBC->data[0] = rtNaN;
      } else if (FacetsOnPBCNum->data[kEnd] == FacetsOnPBCNum->data[kEnd]) {
        i1 = FacetsOnPBCNum->data[1 + kEnd] - 1.0;
        i22 = setPBC->size[0] * setPBC->size[1];
        setPBC->size[0] = 1;
        setPBC->size[1] = (int)floor(i1 - FacetsOnPBCNum->data[kEnd]) + 1;
        emxEnsureCapacity_real_T(setPBC, i22);
        loop_ub = (int)floor(i1 - FacetsOnPBCNum->data[kEnd]);
        for (i22 = 0; i22 <= loop_ub; i22++) {
          setPBC->data[i22] = FacetsOnPBCNum->data[kEnd] + (double)i22;
        }
      } else {
        i1 = floor(((FacetsOnPBCNum->data[1 + kEnd] - 1.0) -
                    FacetsOnPBCNum->data[kEnd]) + 0.5);
        numA = FacetsOnPBCNum->data[kEnd] + i1;
        reg = numA - (FacetsOnPBCNum->data[1 + kEnd] - 1.0);
        absxk = FacetsOnPBCNum->data[kEnd];
        nodeD2 = fabs(FacetsOnPBCNum->data[1 + kEnd] - 1.0);
        if ((absxk > nodeD2) || rtIsNaN(nodeD2)) {
          nodeD2 = absxk;
        }

        if (fabs(reg) < 4.4408920985006262E-16 * nodeD2) {
          i1++;
          numA = FacetsOnPBCNum->data[1 + kEnd] - 1.0;
        } else if (reg > 0.0) {
          numA = FacetsOnPBCNum->data[kEnd] + (i1 - 1.0);
        } else {
          i1++;
        }

        if (i1 >= 0.0) {
          n = (int)i1;
        } else {
          n = 0;
        }

        i22 = setPBC->size[0] * setPBC->size[1];
        setPBC->size[0] = 1;
        setPBC->size[1] = n;
        emxEnsureCapacity_real_T(setPBC, i22);
        if (n > 0) {
          setPBC->data[0] = FacetsOnPBCNum->data[kEnd];
          if (n > 1) {
            setPBC->data[n - 1] = numA;
            k0 = (n - 1) / 2;
            for (k = 0; k <= k0 - 2; k++) {
              setPBC->data[1 + k] = FacetsOnPBCNum->data[kEnd] + (1.0 + (double)
                k);
              setPBC->data[(n - k) - 2] = numA - (1.0 + (double)k);
            }

            if (k0 << 1 == n - 1) {
              setPBC->data[k0] = (FacetsOnPBCNum->data[kEnd] + numA) / 2.0;
            } else {
              setPBC->data[k0] = FacetsOnPBCNum->data[kEnd] + (double)k0;
              setPBC->data[k0 + 1] = numA - (double)k0;
            }
          }
        }
      }

      i22 = ElementsOnNodePBCNum->size[0];
      ElementsOnNodePBCNum->size[0] = setAll->size[1];
      emxEnsureCapacity_real_T(ElementsOnNodePBCNum, i22);
      loop_ub = setAll->size[1];
      for (i22 = 0; i22 < loop_ub; i22++) {
        ElementsOnNodePBCNum->data[i22] = FacetsOnInterface->data[(int)
          setAll->data[i22] - 1];
      }

      i22 = ElementsOnNodePBC_d->size[0];
      ElementsOnNodePBC_d->size[0] = setPBC->size[1];
      emxEnsureCapacity_real_T(ElementsOnNodePBC_d, i22);
      loop_ub = setPBC->size[1];
      for (i22 = 0; i22 < loop_ub; i22++) {
        ElementsOnNodePBC_d->data[i22] = ycol->data[(i21 + (int)setPBC->data[i22])
          - 2];
      }

      b_do_vectors(ElementsOnNodePBCNum, ElementsOnNodePBC_d, setnPBC, iwork,
                   ib_size);
      *numSI = setnPBC->size[0];
      FacetsOnIntMinusPBCNum->data[kEnd + 1] = FacetsOnIntMinusPBCNum->data[kEnd]
        + (double)setnPBC->size[0];
      if (setnPBC->size[0] > 0) {
        if (FacetsOnIntMinusPBCNum->data[kEnd] > FacetsOnIntMinusPBCNum->data[1
            + kEnd] - 1.0) {
          i22 = -1;
          i23 = 0;
        } else {
          i22 = (int)FacetsOnIntMinusPBCNum->data[kEnd] - 2;
          i23 = (int)(FacetsOnIntMinusPBCNum->data[1 + kEnd] - 1.0);
        }

        loop_ub = (i23 - i22) - 1;
        for (i23 = 0; i23 < loop_ub; i23++) {
          FacetsOnIntMinusPBC->data[(i22 + i23) + 1] = setnPBC->data[i23];
        }
      }
    }

    emxFree_real_T(&setPBC);
  } else {
    i21 = FacetsOnPBCNum->size[0];
    FacetsOnPBCNum->size[0] = 1;
    emxEnsureCapacity_real_T(FacetsOnPBCNum, i21);
    FacetsOnPBCNum->data[0] = 0.0;
    i21 = FacetsOnIntMinusPBC->size[0];
    FacetsOnIntMinusPBC->size[0] = 1;
    emxEnsureCapacity_real_T(FacetsOnIntMinusPBC, i21);
    FacetsOnIntMinusPBC->data[0] = 0.0;
    i21 = FacetsOnIntMinusPBCNum->size[0];
    FacetsOnIntMinusPBCNum->size[0] = 1;
    emxEnsureCapacity_real_T(FacetsOnIntMinusPBCNum, i21);
    FacetsOnIntMinusPBCNum->data[0] = 0.0;
  }

  /*  Find material interfaces and duplicate nodes */
  NodesOnInterface->size[0] = 0;
  i21 = (int)(nummat + -1.0);
  emxInit_boolean_T(&internodes_d, 1);
  emxInit_boolean_T(&t2_d, 1);
  e_emxInitStruct_coder_internal_(&b_expl_temp);
  for (numPBC = 0; numPBC < i21; numPBC++) {
    for (pEnd = 0; pEnd <= numPBC; pEnd++) {
      /*  ID for material pair (row=mat2, col=mat1) */
      if (InterTypes->data[(numPBC + InterTypes->size[0] * pEnd) + 1] > 0.0) {
        /*  Mark nodes on the interfaces */
        c_sparse_parenReference(NodeReg->d, NodeReg->colidx, NodeReg->rowidx,
          NodeReg->m, 1.0 + (double)pEnd, NodeRegSum.d, NodeRegSum.colidx,
          NodeRegSum.rowidx, &NodeRegSum.m);
        sparse_gt(NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                  NodeRegSum.m, &b_expl_temp);
        i22 = elems->size[0];
        elems->size[0] = b_expl_temp.d->size[0];
        emxEnsureCapacity_boolean_T(elems, i22);
        loop_ub = b_expl_temp.d->size[0];
        for (i22 = 0; i22 < loop_ub; i22++) {
          elems->data[i22] = b_expl_temp.d->data[i22];
        }

        i22 = ElementsOnNodePBC_rowidx->size[0];
        ElementsOnNodePBC_rowidx->size[0] = b_expl_temp.colidx->size[0];
        emxEnsureCapacity_int32_T(ElementsOnNodePBC_rowidx, i22);
        loop_ub = b_expl_temp.colidx->size[0];
        for (i22 = 0; i22 < loop_ub; i22++) {
          ElementsOnNodePBC_rowidx->data[i22] = b_expl_temp.colidx->data[i22];
        }

        i22 = j->size[0];
        j->size[0] = b_expl_temp.rowidx->size[0];
        emxEnsureCapacity_int32_T(j, i22);
        loop_ub = b_expl_temp.rowidx->size[0];
        for (i22 = 0; i22 < loop_ub; i22++) {
          j->data[i22] = b_expl_temp.rowidx->data[i22];
        }

        c_sparse_parenReference(NodeReg->d, NodeReg->colidx, NodeReg->rowidx,
          NodeReg->m, 2.0 + (double)numPBC, NodeRegSum.d, NodeRegSum.colidx,
          NodeRegSum.rowidx, &NodeRegSum.m);
        sparse_gt(NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                  NodeRegSum.m, &b_expl_temp);
        i22 = internodes_d->size[0];
        internodes_d->size[0] = b_expl_temp.d->size[0];
        emxEnsureCapacity_boolean_T(internodes_d, i22);
        loop_ub = b_expl_temp.d->size[0];
        for (i22 = 0; i22 < loop_ub; i22++) {
          internodes_d->data[i22] = b_expl_temp.d->data[i22];
        }

        i22 = t0_rowidx->size[0];
        t0_rowidx->size[0] = b_expl_temp.colidx->size[0];
        emxEnsureCapacity_int32_T(t0_rowidx, i22);
        loop_ub = b_expl_temp.colidx->size[0];
        for (i22 = 0; i22 < loop_ub; i22++) {
          t0_rowidx->data[i22] = b_expl_temp.colidx->data[i22];
        }

        i22 = internodes_rowidx->size[0];
        internodes_rowidx->size[0] = b_expl_temp.rowidx->size[0];
        emxEnsureCapacity_int32_T(internodes_rowidx, i22);
        loop_ub = b_expl_temp.rowidx->size[0];
        for (i22 = 0; i22 < loop_ub; i22++) {
          internodes_rowidx->data[i22] = b_expl_temp.rowidx->data[i22];
        }

        k0 = b_expl_temp.m;
        sparse_and(elems, ElementsOnNodePBC_rowidx, j, internodes_d, t0_rowidx,
                   internodes_rowidx, k0, t2_d, idx, iwork, &i2);
        c_sparse_parenReference(NodeReg->d, NodeReg->colidx, NodeReg->rowidx,
          NodeReg->m, 2.0 + (double)numPBC, NodeRegSum.d, NodeRegSum.colidx,
          NodeRegSum.rowidx, &NodeRegSum.m);
        c_sparse_parenReference(NodeReg->d, NodeReg->colidx, NodeReg->rowidx,
          NodeReg->m, 1.0 + (double)pEnd, b_NodesOnPBCnum, t0_colidx, t0_rowidx,
          &k0);
        sparse_eq(NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                  b_NodesOnPBCnum, t0_colidx, t0_rowidx, k0, elems,
                  ElementsOnNodePBC_rowidx, j, &kEnd);
        sparse_and(t2_d, idx, iwork, elems, ElementsOnNodePBC_rowidx, j, kEnd,
                   internodes_d, t0_rowidx, internodes_rowidx, &k0);
        b_idx = 0;
        i22 = iwork->size[0];
        iwork->size[0] = t0_rowidx->data[t0_rowidx->size[0] - 1] - 1;
        emxEnsureCapacity_int32_T(iwork, i22);
        kEnd = 1;
        while (b_idx < t0_rowidx->data[t0_rowidx->size[0] - 1] - 1) {
          if (b_idx == t0_rowidx->data[kEnd] - 1) {
            kEnd++;
          } else {
            b_idx++;
            iwork->data[b_idx - 1] = (kEnd - 1) * k0 + internodes_rowidx->
              data[b_idx - 1];
          }
        }

        if (t0_rowidx->data[t0_rowidx->size[0] - 1] - 1 == 1) {
          if (b_idx == 0) {
            iwork->size[0] = 0;
          }
        } else if (1 > b_idx) {
          iwork->size[0] = 0;
        } else {
          i22 = iwork->size[0];
          iwork->size[0] = b_idx;
          emxEnsureCapacity_int32_T(iwork, i22);
        }

        i22 = NodesOnInterface->size[0];
        i23 = ElementsOnNodePBC_d->size[0];
        ElementsOnNodePBC_d->size[0] = iwork->size[0];
        emxEnsureCapacity_real_T(ElementsOnNodePBC_d, i23);
        loop_ub = iwork->size[0];
        for (i23 = 0; i23 < loop_ub; i23++) {
          ElementsOnNodePBC_d->data[i23] = iwork->data[i23];
        }

        i23 = ElementsOnNodePBC_d->size[0];
        i24 = NodesOnInterface->size[0];
        NodesOnInterface->size[0] = i22 + i23;
        emxEnsureCapacity_real_T(NodesOnInterface, i24);
        loop_ub = iwork->size[0];
        for (i23 = 0; i23 < loop_ub; i23++) {
          NodesOnInterface->data[i22 + i23] = iwork->data[i23];
        }
      }
    }
  }

  emxFree_real_T(&b_NodesOnPBCnum);
  if (usePBC != 0.0) {
    /*  Add PBC nodes into duplicating list */
    sum(NodeReg->d, NodeReg->colidx, NodeReg->rowidx, NodeReg->m, NodeReg->n,
        NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx, &NodeRegSum.m);

    /*  from zipped nodes, ONLY the one with regions attached is in the zipped connectivity */
    i22 = elems->size[0];
    elems->size[0] = NodesOnPBCnum->size[0];
    emxEnsureCapacity_boolean_T(elems, i22);
    loop_ub = NodesOnPBCnum->size[0];
    for (i22 = 0; i22 < loop_ub; i22++) {
      elems->data[i22] = (NodesOnPBCnum->data[i22] > 0.0);
    }

    k0 = elems->size[0];
    b_idx = 0;
    i22 = iwork->size[0];
    iwork->size[0] = elems->size[0];
    emxEnsureCapacity_int32_T(iwork, i22);
    notdone = 0;
    exitg2 = false;
    while ((!exitg2) && (notdone <= k0 - 1)) {
      if (elems->data[notdone]) {
        b_idx++;
        iwork->data[b_idx - 1] = notdone + 1;
        if (b_idx >= k0) {
          exitg2 = true;
        } else {
          notdone++;
        }
      } else {
        notdone++;
      }
    }

    if (elems->size[0] == 1) {
      if (b_idx == 0) {
        iwork->size[0] = 0;
      }
    } else if (1 > b_idx) {
      iwork->size[0] = 0;
    } else {
      i22 = iwork->size[0];
      iwork->size[0] = b_idx;
      emxEnsureCapacity_int32_T(iwork, i22);
    }

    sparse_gt(NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx, NodeRegSum.m,
              &b_expl_temp);
    i22 = t0_rowidx->size[0];
    t0_rowidx->size[0] = b_expl_temp.colidx->size[0];
    emxEnsureCapacity_int32_T(t0_rowidx, i22);
    loop_ub = b_expl_temp.colidx->size[0];
    for (i22 = 0; i22 < loop_ub; i22++) {
      t0_rowidx->data[i22] = b_expl_temp.colidx->data[i22];
    }

    i22 = internodes_rowidx->size[0];
    internodes_rowidx->size[0] = b_expl_temp.rowidx->size[0];
    emxEnsureCapacity_int32_T(internodes_rowidx, i22);
    loop_ub = b_expl_temp.rowidx->size[0];
    for (i22 = 0; i22 < loop_ub; i22++) {
      internodes_rowidx->data[i22] = b_expl_temp.rowidx->data[i22];
    }

    k0 = b_expl_temp.m;
    b_idx = 0;
    i22 = idx->size[0];
    idx->size[0] = t0_rowidx->data[t0_rowidx->size[0] - 1] - 1;
    emxEnsureCapacity_int32_T(idx, i22);
    kEnd = 1;
    while (b_idx < t0_rowidx->data[t0_rowidx->size[0] - 1] - 1) {
      if (b_idx == t0_rowidx->data[kEnd] - 1) {
        kEnd++;
      } else {
        b_idx++;
        idx->data[b_idx - 1] = (kEnd - 1) * k0 + internodes_rowidx->data[b_idx -
          1];
      }
    }

    if (t0_rowidx->data[t0_rowidx->size[0] - 1] - 1 == 1) {
      if (b_idx == 0) {
        idx->size[0] = 0;
      }
    } else if (1 > b_idx) {
      idx->size[0] = 0;
    } else {
      i22 = idx->size[0];
      idx->size[0] = b_idx;
      emxEnsureCapacity_int32_T(idx, i22);
    }

    i22 = ElementsOnNodePBC_d->size[0];
    ElementsOnNodePBC_d->size[0] = iwork->size[0];
    emxEnsureCapacity_real_T(ElementsOnNodePBC_d, i22);
    loop_ub = iwork->size[0];
    for (i22 = 0; i22 < loop_ub; i22++) {
      ElementsOnNodePBC_d->data[i22] = iwork->data[i22];
    }

    i22 = ElementsOnNodePBCNum->size[0];
    ElementsOnNodePBCNum->size[0] = idx->size[0];
    emxEnsureCapacity_real_T(ElementsOnNodePBCNum, i22);
    loop_ub = idx->size[0];
    for (i22 = 0; i22 < loop_ub; i22++) {
      ElementsOnNodePBCNum->data[i22] = idx->data[i22];
    }

    c_do_vectors(ElementsOnNodePBC_d, ElementsOnNodePBCNum, ycol, iwork, j);
    i22 = NodesOnInterface->size[0];
    i23 = ycol->size[0];
    i24 = NodesOnInterface->size[0];
    NodesOnInterface->size[0] = i22 + i23;
    emxEnsureCapacity_real_T(NodesOnInterface, i24);
    loop_ub = ycol->size[0];
    for (i23 = 0; i23 < loop_ub; i23++) {
      NodesOnInterface->data[i22 + i23] = ycol->data[i23];
    }

    /*  add these zipped PBC nodes into the list for duplicates too */
  }

  i22 = ycol->size[0];
  ycol->size[0] = NodesOnInterface->size[0];
  emxEnsureCapacity_real_T(ycol, i22);
  loop_ub = NodesOnInterface->size[0];
  for (i22 = 0; i22 < loop_ub; i22++) {
    ycol->data[i22] = NodesOnInterface->data[i22];
  }

  notdone = NodesOnInterface->size[0];
  n = NodesOnInterface->size[0] + 1;
  numA = NodesOnInterface->size[0];
  i22 = idx->size[0];
  idx->size[0] = (int)numA;
  emxEnsureCapacity_int32_T(idx, i22);
  loop_ub = (int)numA;
  for (i22 = 0; i22 < loop_ub; i22++) {
    idx->data[i22] = 0;
  }

  if (NodesOnInterface->size[0] != 0) {
    i22 = iwork->size[0];
    iwork->size[0] = (int)numA;
    emxEnsureCapacity_int32_T(iwork, i22);
    i22 = NodesOnInterface->size[0] - 1;
    for (k = 1; k <= i22; k += 2) {
      if ((NodesOnInterface->data[k - 1] <= NodesOnInterface->data[k]) ||
          rtIsNaN(NodesOnInterface->data[k])) {
        idx->data[k - 1] = k;
        idx->data[k] = k + 1;
      } else {
        idx->data[k - 1] = k + 1;
        idx->data[k] = k;
      }
    }

    if ((NodesOnInterface->size[0] & 1) != 0) {
      idx->data[NodesOnInterface->size[0] - 1] = NodesOnInterface->size[0];
    }

    i = 2;
    while (i < n - 1) {
      i2 = i << 1;
      b_j = 1;
      for (pEnd = 1 + i; pEnd < n; pEnd = nb + i) {
        p = b_j;
        q = pEnd - 1;
        nb = b_j + i2;
        if (nb > n) {
          nb = n;
        }

        k = 0;
        kEnd = nb - b_j;
        while (k + 1 <= kEnd) {
          if ((NodesOnInterface->data[idx->data[p - 1] - 1] <=
               NodesOnInterface->data[idx->data[q] - 1]) || rtIsNaN
              (NodesOnInterface->data[idx->data[q] - 1])) {
            iwork->data[k] = idx->data[p - 1];
            p++;
            if (p == pEnd) {
              while (q + 1 < nb) {
                k++;
                iwork->data[k] = idx->data[q];
                q++;
              }
            }
          } else {
            iwork->data[k] = idx->data[q];
            q++;
            if (q + 1 == nb) {
              while (p < pEnd) {
                k++;
                iwork->data[k] = idx->data[p - 1];
                p++;
              }
            }
          }

          k++;
        }

        for (k = 0; k < kEnd; k++) {
          idx->data[(b_j + k) - 1] = iwork->data[k];
        }

        b_j = nb;
      }

      i = i2;
    }
  }

  numA = NodesOnInterface->size[0];
  i22 = NodesOnInterface->size[0];
  NodesOnInterface->size[0] = (int)numA;
  emxEnsureCapacity_real_T(NodesOnInterface, i22);
  for (k = 0; k < notdone; k++) {
    NodesOnInterface->data[k] = ycol->data[idx->data[k] - 1];
  }

  k = 0;
  while ((k + 1 <= notdone) && rtIsInf(NodesOnInterface->data[k]) &&
         (NodesOnInterface->data[k] < 0.0)) {
    k++;
  }

  kEnd = k;
  k = notdone;
  while ((k >= 1) && rtIsNaN(NodesOnInterface->data[k - 1])) {
    k--;
  }

  k0 = notdone - k;
  while ((k >= 1) && rtIsInf(NodesOnInterface->data[k - 1]) &&
         (NodesOnInterface->data[k - 1] > 0.0)) {
    k--;
  }

  i2 = (notdone - k) - k0;
  nb = -1;
  if (kEnd > 0) {
    nb = 0;
  }

  while (kEnd + 1 <= k) {
    i1 = NodesOnInterface->data[kEnd];
    do {
      exitg1 = 0;
      kEnd++;
      if (kEnd + 1 > k) {
        exitg1 = 1;
      } else {
        absxk = fabs(i1 / 2.0);
        if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
          if (absxk <= 2.2250738585072014E-308) {
            absxk = 4.94065645841247E-324;
          } else {
            frexp(absxk, &e_exponent);
            absxk = ldexp(1.0, e_exponent - 53);
          }
        } else {
          absxk = rtNaN;
        }

        if ((fabs(i1 - NodesOnInterface->data[kEnd]) < absxk) || (rtIsInf
             (NodesOnInterface->data[kEnd]) && rtIsInf(i1) &&
             ((NodesOnInterface->data[kEnd] > 0.0) == (i1 > 0.0)))) {
        } else {
          exitg1 = 1;
        }
      }
    } while (exitg1 == 0);

    nb++;
    NodesOnInterface->data[nb] = i1;
  }

  if (i2 > 0) {
    nb++;
    NodesOnInterface->data[nb] = NodesOnInterface->data[k];
  }

  kEnd = k + i2;
  for (b_j = 0; b_j < k0; b_j++) {
    nb++;
    NodesOnInterface->data[nb] = NodesOnInterface->data[kEnd + b_j];
  }

  if (1 > nb + 1) {
    i22 = -1;
  } else {
    i22 = nb;
  }

  i23 = NodesOnInterface->size[0];
  NodesOnInterface->size[0] = i22 + 1;
  emxEnsureCapacity_real_T(NodesOnInterface, i23);

  /*  Now actually duplicate the nodes */
  /*  Criteria: only nodes for which ALL inter-material facs involving a given */
  /*  material are being cut, then they are duplicated. */
  nodeD2 = *numnp;
  emxInit_real_T(&interreg2, 2);
  f_emxInitStruct_coder_internal_(&c_expl_temp);
  for (nume = 0; nume <= i22; nume++) {
    absxk = NodesOnInterface->data[nume];

    /*      matnode = sum(NodeMat(node,:)>0); */
    if (FacetsOnNodeNum->data[(int)NodesOnInterface->data[nume] - 1] == 0.0) {
      /*  midside node */
      if ((usePBC != 0.0) && (NodesOnPBCnum->data[(int)NodesOnInterface->
                              data[nume] - 1] > 0.0)) {
        /*  handle copies of PBC nodes specially */
        /*  Just get the old node numbers back and copy them into place */
        c_sparse_parenReference(ElementsOnNode->d, ElementsOnNode->colidx,
          ElementsOnNode->rowidx, ElementsOnNode->m, NodesOnInterface->data[nume],
          NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx, &NodeRegSum.m);
        b_sparse_full(NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                      NodeRegSum.m, ElementsOnNodeA);

        /*  Loop over all elements attached to node */
        i23 = (int)ElementsOnNodeNum->data[(int)NodesOnInterface->data[nume] - 1];
        if (0 <= i23 - 1) {
          if (1.0 > nen) {
            c_loop_ub = 0;
          } else {
            c_loop_ub = (int)nen;
          }

          nx = c_loop_ub;
        }

        for (q = 0; q < i23; q++) {
          /*  Reset nodal ID for element in higher material ID */
          k0 = (int)ElementsOnNodeA->data[q];
          for (i24 = 0; i24 < nx; i24++) {
            x_data[i24] = (NodesOnElementCG->data[(k0 + NodesOnElementCG->size[0]
              * i24) - 1] == absxk);
          }

          b_idx = 0;
          nb = c_loop_ub;
          notdone = 0;
          exitg2 = false;
          while ((!exitg2) && (notdone <= nx - 1)) {
            if (x_data[notdone]) {
              b_idx++;
              ii_data[b_idx - 1] = (signed char)(notdone + 1);
              if (b_idx >= nx) {
                exitg2 = true;
              } else {
                notdone++;
              }
            } else {
              notdone++;
            }
          }

          if (c_loop_ub == 1) {
            if (b_idx == 0) {
              nb = 0;
            }
          } else if (1 > b_idx) {
            nb = 0;
          } else {
            nb = b_idx;
          }

          if (0 <= nb - 1) {
            memcpy(&node_dup_tmp_data[0], &ii_data[0], (unsigned int)(nb * (int)
                    sizeof(signed char)));
          }

          k0 = (int)ElementsOnNodeA->data[q];
          kEnd = (int)ElementsOnNodeA->data[q];
          for (i24 = 0; i24 < nb; i24++) {
            notdone = node_dup_tmp_data[i24] - 1;
            NodesOnElementDG->data[(kEnd + NodesOnElementDG->size[0] * notdone)
              - 1] = NodesOnElementPBC->data[(k0 + NodesOnElementPBC->size[0] *
              notdone) - 1];
          }
        }
      } else {
        /*  regular midside node */
        for (numPBC = 0; numPBC < i21; numPBC++) {
          for (pEnd = 0; pEnd <= numPBC; pEnd++) {
            /*  ID for material pair (row=mat2, col=mat1) */
            if (InterTypes->data[(numPBC + InterTypes->size[0] * pEnd) + 1] >
                0.0) {
              /*  Duplicate nodes along material interface */
              d_sparse_parenReference(NodeReg->d, NodeReg->colidx,
                NodeReg->rowidx, absxk, 1.0 + (double)pEnd, ElementsOnNodePBC_d,
                t0_colidx, iwork);
              b_sparse_gt(ElementsOnNodePBC_d, t0_colidx, t2_d, j,
                          ElementsOnNodePBC_rowidx);
              d_sparse_parenReference(NodeReg->d, NodeReg->colidx,
                NodeReg->rowidx, absxk, 2.0 + (double)numPBC,
                ElementsOnNodePBC_d, t0_colidx, iwork);
              b_sparse_gt(ElementsOnNodePBC_d, t0_colidx, internodes_d,
                          t0_rowidx, internodes_rowidx);
              b_sparse_and(t2_d, j, internodes_d, t0_rowidx, internodes_rowidx,
                           &c_expl_temp);
              i23 = elems->size[0];
              elems->size[0] = c_expl_temp.d->size[0];
              emxEnsureCapacity_boolean_T(elems, i23);
              loop_ub = c_expl_temp.d->size[0];
              for (i23 = 0; i23 < loop_ub; i23++) {
                elems->data[i23] = c_expl_temp.d->data[i23];
              }

              i23 = idx->size[0];
              idx->size[0] = c_expl_temp.colidx->size[0];
              emxEnsureCapacity_int32_T(idx, i23);
              loop_ub = c_expl_temp.colidx->size[0];
              for (i23 = 0; i23 < loop_ub; i23++) {
                idx->data[i23] = c_expl_temp.colidx->data[i23];
              }

              d_sparse_parenReference(NodeReg->d, NodeReg->colidx,
                NodeReg->rowidx, absxk, 2.0 + (double)numPBC,
                ElementsOnNodePBC_d, t0_colidx, iwork);
              d_sparse_parenReference(NodeReg->d, NodeReg->colidx,
                NodeReg->rowidx, absxk, 1.0 + (double)pEnd, ElementsOnNodePBCNum,
                iwork, ElementsOnNodePBC_rowidx);
              b_sparse_eq(ElementsOnNodePBC_d, t0_colidx, ElementsOnNodePBCNum,
                          iwork, ElementsOnNodePBC_rowidx, &c_expl_temp);
              i23 = t2_d->size[0];
              t2_d->size[0] = c_expl_temp.d->size[0];
              emxEnsureCapacity_boolean_T(t2_d, i23);
              loop_ub = c_expl_temp.d->size[0];
              for (i23 = 0; i23 < loop_ub; i23++) {
                t2_d->data[i23] = c_expl_temp.d->data[i23];
              }

              i23 = j->size[0];
              j->size[0] = c_expl_temp.colidx->size[0];
              emxEnsureCapacity_int32_T(j, i23);
              loop_ub = c_expl_temp.colidx->size[0];
              for (i23 = 0; i23 < loop_ub; i23++) {
                j->data[i23] = c_expl_temp.colidx->data[i23];
              }

              i23 = ElementsOnNodePBC_rowidx->size[0];
              ElementsOnNodePBC_rowidx->size[0] = c_expl_temp.rowidx->size[0];
              emxEnsureCapacity_int32_T(ElementsOnNodePBC_rowidx, i23);
              loop_ub = c_expl_temp.rowidx->size[0];
              for (i23 = 0; i23 < loop_ub; i23++) {
                ElementsOnNodePBC_rowidx->data[i23] = c_expl_temp.rowidx->
                  data[i23];
              }

              b_sparse_and(elems, idx, t2_d, j, ElementsOnNodePBC_rowidx,
                           &c_expl_temp);
              i23 = internodes_d->size[0];
              internodes_d->size[0] = c_expl_temp.d->size[0];
              emxEnsureCapacity_boolean_T(internodes_d, i23);
              loop_ub = c_expl_temp.d->size[0];
              for (i23 = 0; i23 < loop_ub; i23++) {
                internodes_d->data[i23] = c_expl_temp.d->data[i23];
              }

              i23 = t0_rowidx->size[0];
              t0_rowidx->size[0] = c_expl_temp.colidx->size[0];
              emxEnsureCapacity_int32_T(t0_rowidx, i23);
              loop_ub = c_expl_temp.colidx->size[0];
              for (i23 = 0; i23 < loop_ub; i23++) {
                t0_rowidx->data[i23] = c_expl_temp.colidx->data[i23];
              }

              /*  Namely, only find nodes that are part of materials mat1 and */
              /*  mat2, but ONLY if those nodal IDs have not been reset before, */
              /*  in which case the ID for mat1 will equal the ID for mat2. */
              /*  In this way, each new nodal ID is assigned to a single */
              /*  material. */
              if (c_sparse_full(internodes_d, t0_rowidx)) {
                /*  Duplicate the nodal coordinates */
                nodeD2++;
                sparse_parenAssign2D(NodeReg, nodeD2, absxk, 2.0 + (double)
                                     numPBC);

                /*  Loop over all nodes on material interface that need */
                /*  duplicated */
                c_sparse_parenReference(ElementsOnNode->d,
                  ElementsOnNode->colidx, ElementsOnNode->rowidx,
                  ElementsOnNode->m, absxk, NodeRegSum.d, NodeRegSum.colidx,
                  NodeRegSum.rowidx, &NodeRegSum.m);
                b_sparse_full(NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                              NodeRegSum.m, ElementsOnNodeA);

                /*  Loop over all elements attached to node */
                i23 = (int)ElementsOnNodeNum->data[(int)absxk - 1];
                for (q = 0; q < i23; q++) {
                  if (RegionOnElement->data[(int)ElementsOnNodeA->data[q] - 1] ==
                      2.0 + (double)numPBC) {
                    /*  Reset nodal ID for element in higher material ID */
                    if (1.0 > nen) {
                      c_loop_ub = 0;
                    } else {
                      c_loop_ub = (int)nen;
                    }

                    k0 = (int)ElementsOnNodeA->data[q];
                    for (i24 = 0; i24 < c_loop_ub; i24++) {
                      x_data[i24] = (NodesOnElementCG->data[(k0 +
                        NodesOnElementCG->size[0] * i24) - 1] == absxk);
                    }

                    b_idx = 0;
                    nb = c_loop_ub;
                    notdone = 0;
                    exitg2 = false;
                    while ((!exitg2) && (notdone <= c_loop_ub - 1)) {
                      if (x_data[notdone]) {
                        b_idx++;
                        ii_data[b_idx - 1] = (signed char)(notdone + 1);
                        if (b_idx >= c_loop_ub) {
                          exitg2 = true;
                        } else {
                          notdone++;
                        }
                      } else {
                        notdone++;
                      }
                    }

                    if (c_loop_ub == 1) {
                      if (b_idx == 0) {
                        nb = 0;
                      }
                    } else if (1 > b_idx) {
                      nb = 0;
                    } else {
                      nb = b_idx;
                    }

                    if (0 <= nb - 1) {
                      memcpy(&node_dup_tmp_data[0], &ii_data[0], (unsigned int)
                             (nb * (int)sizeof(signed char)));
                    }

                    k0 = (int)ElementsOnNodeA->data[q];
                    for (i24 = 0; i24 < nb; i24++) {
                      NodesOnElementDG->data[(k0 + NodesOnElementDG->size[0] *
                        (node_dup_tmp_data[i24] - 1)) - 1] = nodeD2;
                    }

                    Coordinates3->data[(int)nodeD2 - 1] = Coordinates->data[(int)
                      absxk - 1];
                    Coordinates3->data[((int)nodeD2 + Coordinates3->size[0]) - 1]
                      = Coordinates->data[((int)absxk + Coordinates->size[0]) -
                      1];
                  }
                }
              }
            }
          }
        }
      }
    } else {
      /*  All corner nodes */
      /*  form secs; a sec is a contiguous region of elements that is not */
      /*  cut apart by any CZM facs */
      /*  start by assuming all elements are separated */
      numA = ElementsOnNodeNum->data[(int)NodesOnInterface->data[nume] - 1];
      i23 = interreg2->size[0] * interreg2->size[1];
      interreg2->size[0] = (int)ElementsOnNodeNum->data[(int)
        NodesOnInterface->data[nume] - 1];
      interreg2->size[1] = (int)ElementsOnNodeNum->data[(int)
        NodesOnInterface->data[nume] - 1];
      emxEnsureCapacity_real_T(interreg2, i23);
      loop_ub = (int)ElementsOnNodeNum->data[(int)NodesOnInterface->data[nume] -
        1] * (int)ElementsOnNodeNum->data[(int)NodesOnInterface->data[nume] - 1];
      for (i23 = 0; i23 < loop_ub; i23++) {
        interreg2->data[i23] = 0.0;
      }

      if (ElementsOnNodeNum->data[(int)NodesOnInterface->data[nume] - 1] < 1.0)
      {
        setAll->size[0] = 1;
        setAll->size[1] = 0;
      } else if (rtIsInf(ElementsOnNodeNum->data[(int)NodesOnInterface->
                         data[nume] - 1]) && (1.0 == ElementsOnNodeNum->data
                  [(int)NodesOnInterface->data[nume] - 1])) {
        i23 = setAll->size[0] * setAll->size[1];
        setAll->size[0] = 1;
        setAll->size[1] = 1;
        emxEnsureCapacity_real_T(setAll, i23);
        setAll->data[0] = rtNaN;
      } else {
        i1 = ElementsOnNodeNum->data[(int)NodesOnInterface->data[nume] - 1];
        i23 = setAll->size[0] * setAll->size[1];
        setAll->size[0] = 1;
        loop_ub = (int)floor(i1 - 1.0);
        setAll->size[1] = loop_ub + 1;
        emxEnsureCapacity_real_T(setAll, i23);
        for (i23 = 0; i23 <= loop_ub; i23++) {
          setAll->data[i23] = 1.0 + (double)i23;
        }
      }

      b_sparse_parenReference(ElementsOnNode->d, ElementsOnNode->colidx,
        ElementsOnNode->rowidx, setAll, NodesOnInterface->data[nume],
        NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx, &NodeRegSum.m);
      b_sparse_full(NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                    NodeRegSum.m, ElementsOnNodeA);
      i23 = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      setAll->size[1] = ElementsOnNodeA->size[0];
      emxEnsureCapacity_real_T(setAll, i23);
      loop_ub = ElementsOnNodeA->size[0];
      for (i23 = 0; i23 < loop_ub; i23++) {
        setAll->data[i23] = ElementsOnNodeA->data[i23];
      }

      loop_ub = setAll->size[1];
      for (i23 = 0; i23 < loop_ub; i23++) {
        interreg2->data[interreg2->size[0] * i23] = setAll->data[i23];
      }

      i23 = setnPBC->size[0];
      setnPBC->size[0] = (int)ElementsOnNodeNum->data[(int)
        NodesOnInterface->data[nume] - 1];
      emxEnsureCapacity_real_T(setnPBC, i23);
      loop_ub = (int)ElementsOnNodeNum->data[(int)NodesOnInterface->data[nume] -
        1];
      for (i23 = 0; i23 < loop_ub; i23++) {
        setnPBC->data[i23] = 1.0;
      }

      i23 = (int)FacetsOnNodeNum->data[(int)NodesOnInterface->data[nume] - 1];
      for (b_exponent = 0; b_exponent < i23; b_exponent++) {
        d_sparse_parenReference(FacetsOnNodeInt->d, FacetsOnNodeInt->colidx,
          FacetsOnNodeInt->rowidx, 1.0 + (double)b_exponent, absxk,
          ElementsOnNodePBC_d, t0_colidx, iwork);
        b_sparse_gt(ElementsOnNodePBC_d, t0_colidx, t2_d, j,
                    ElementsOnNodePBC_rowidx);
        if (c_sparse_full(t2_d, j)) {
          /*  exclude internal facs */
          /*                  intramattrue = ~isempty(find(matI==diag(intermat2),1)); I */
          /*                  found out that the code is only putting cut in */
          /*                  nodefaccut for intermaterials, not the diagonal of */
          /*                  intermat2, so no if-test is needed */
          /*                  if ~cuttrue || intramattrue % two elements should be joined into one sector, along with their neighbors currently in the sector */
          d_sparse_parenReference(FacetsOnNodeCut->d, FacetsOnNodeCut->colidx,
            FacetsOnNodeCut->rowidx, 1.0 + (double)b_exponent, absxk,
            ElementsOnNodePBC_d, t0_colidx, iwork);
          sparse_not(t0_colidx, iwork, t2_d, j, ElementsOnNodePBC_rowidx);
          if (c_sparse_full(t2_d, j)) {
            /*  two elements should be joined into one sector, along with their neighbors currently in the sector */
            d_sparse_parenReference(FacetsOnNode->d, FacetsOnNode->colidx,
              FacetsOnNode->rowidx, 1.0 + (double)b_exponent, absxk,
              ElementsOnNodePBC_d, t0_colidx, iwork);
            i1 = sparse_full(ElementsOnNodePBC_d, t0_colidx);
            kEnd = (int)i1;
            i2 = (int)ElementsOnFacet->data[kEnd - 1];
            kEnd = (int)ElementsOnFacet->data[(kEnd + ElementsOnFacet->size[0])
              - 1];

            /*  find the sectors for each element */
            nri = 0U;
            old = false;
            while ((nri < numA) && (!old)) {
              nri++;
              if (setnPBC->data[(int)nri - 1] > 0.0) {
                if (1.0 > setnPBC->data[(int)nri - 1]) {
                  loop_ub = 0;
                } else {
                  loop_ub = (int)setnPBC->data[(int)nri - 1];
                }

                i24 = elems->size[0];
                elems->size[0] = loop_ub;
                emxEnsureCapacity_boolean_T(elems, i24);
                for (i24 = 0; i24 < loop_ub; i24++) {
                  elems->data[i24] = (interreg2->data[i24 + interreg2->size[0] *
                                      ((int)nri - 1)] == i2);
                }

                k0 = elems->size[0];
                b_idx = 0;
                i24 = iwork->size[0];
                iwork->size[0] = elems->size[0];
                emxEnsureCapacity_int32_T(iwork, i24);
                notdone = 0;
                exitg2 = false;
                while ((!exitg2) && (notdone <= k0 - 1)) {
                  if (elems->data[notdone]) {
                    b_idx++;
                    iwork->data[b_idx - 1] = notdone + 1;
                    if (b_idx >= k0) {
                      exitg2 = true;
                    } else {
                      notdone++;
                    }
                  } else {
                    notdone++;
                  }
                }

                if (elems->size[0] == 1) {
                  if (b_idx == 0) {
                    iwork->size[0] = 0;
                  }
                } else if (1 > b_idx) {
                  iwork->size[0] = 0;
                } else {
                  i24 = iwork->size[0];
                  iwork->size[0] = b_idx;
                  emxEnsureCapacity_int32_T(iwork, i24);
                }

                /*  find is MUCH faster than ismember */
                i24 = ElementsOnNodePBC_d->size[0];
                ElementsOnNodePBC_d->size[0] = iwork->size[0];
                emxEnsureCapacity_real_T(ElementsOnNodePBC_d, i24);
                loop_ub = iwork->size[0];
                for (i24 = 0; i24 < loop_ub; i24++) {
                  ElementsOnNodePBC_d->data[i24] = iwork->data[i24];
                }

                old = any(ElementsOnNodePBC_d);

                /*                          if sec1>0 */
                /*                              break */
                /*                          end */
              }
            }

            iSec2 = 0U;
            old = false;
            while ((iSec2 < numA) && (!old)) {
              iSec2++;
              i24 = (int)iSec2 - 1;
              if (setnPBC->data[i24] > 0.0) {
                if (1.0 > setnPBC->data[(int)iSec2 - 1]) {
                  loop_ub = 0;
                } else {
                  loop_ub = (int)setnPBC->data[(int)iSec2 - 1];
                }

                notdone = elems->size[0];
                elems->size[0] = loop_ub;
                emxEnsureCapacity_boolean_T(elems, notdone);
                for (notdone = 0; notdone < loop_ub; notdone++) {
                  elems->data[notdone] = (kEnd == interreg2->data[notdone +
                    interreg2->size[0] * i24]);
                }

                k0 = elems->size[0];
                b_idx = 0;
                i24 = iwork->size[0];
                iwork->size[0] = elems->size[0];
                emxEnsureCapacity_int32_T(iwork, i24);
                notdone = 0;
                exitg2 = false;
                while ((!exitg2) && (notdone <= k0 - 1)) {
                  if (elems->data[notdone]) {
                    b_idx++;
                    iwork->data[b_idx - 1] = notdone + 1;
                    if (b_idx >= k0) {
                      exitg2 = true;
                    } else {
                      notdone++;
                    }
                  } else {
                    notdone++;
                  }
                }

                if (elems->size[0] == 1) {
                  if (b_idx == 0) {
                    iwork->size[0] = 0;
                  }
                } else if (1 > b_idx) {
                  iwork->size[0] = 0;
                } else {
                  i24 = iwork->size[0];
                  iwork->size[0] = b_idx;
                  emxEnsureCapacity_int32_T(iwork, i24);
                }

                i24 = ElementsOnNodePBC_d->size[0];
                ElementsOnNodePBC_d->size[0] = iwork->size[0];
                emxEnsureCapacity_real_T(ElementsOnNodePBC_d, i24);
                loop_ub = iwork->size[0];
                for (i24 = 0; i24 < loop_ub; i24++) {
                  ElementsOnNodePBC_d->data[i24] = iwork->data[i24];
                }

                old = any(ElementsOnNodePBC_d);

                /*                          if sec2>0 */
                /*                              break */
                /*                          end */
              }
            }

            /*  merge secs */
            if ((int)iSec2 != (int)nri) {
              if (1.0 > setnPBC->data[(int)iSec2 - 1]) {
                loop_ub = 0;
              } else {
                loop_ub = (int)setnPBC->data[(int)iSec2 - 1];
              }

              if (1.0 > setnPBC->data[(int)nri - 1]) {
                b_loop_ub = 0;
              } else {
                b_loop_ub = (int)setnPBC->data[(int)nri - 1];
              }

              i24 = ycol->size[0];
              ycol->size[0] = loop_ub + b_loop_ub;
              emxEnsureCapacity_real_T(ycol, i24);
              for (i24 = 0; i24 < loop_ub; i24++) {
                ycol->data[i24] = interreg2->data[i24 + interreg2->size[0] *
                  ((int)iSec2 - 1)];
              }

              for (i24 = 0; i24 < b_loop_ub; i24++) {
                ycol->data[i24 + loop_ub] = interreg2->data[i24 +
                  interreg2->size[0] * ((int)nri - 1)];
              }

              b_sort(ycol);
              loop_ub = ycol->size[0];
              for (i24 = 0; i24 < loop_ub; i24++) {
                interreg2->data[i24 + interreg2->size[0] * ((int)iSec2 - 1)] =
                  ycol->data[i24];
              }

              loop_ub = interreg2->size[0];
              for (i24 = 0; i24 < loop_ub; i24++) {
                interreg2->data[i24 + interreg2->size[0] * ((int)nri - 1)] = 0.0;
              }

              setnPBC->data[(int)iSec2 - 1] += setnPBC->data[(int)nri - 1];
              setnPBC->data[(int)nri - 1] = 0.0;
            }
          }
        }

        /*  if external fac */
      }

      /*  assign node IDs to each sector */
      if ((usePBC != 0.0) && (NodesOnPBCnum->data[(int)NodesOnInterface->
                              data[nume] - 1] > 0.0)) {
        /*  handle copies of PBC nodes specially */
        i1 = NodesOnPBCnum->data[(int)NodesOnInterface->data[nume] - 1] + 1.0;
        i23 = ycol->size[0];
        ycol->size[0] = (int)(NodesOnPBCnum->data[(int)NodesOnInterface->
                              data[nume] - 1] + 1.0);
        emxEnsureCapacity_real_T(ycol, i23);
        loop_ub = (int)(NodesOnPBCnum->data[(int)NodesOnInterface->data[nume] -
                        1] + 1.0);
        for (i23 = 0; i23 < loop_ub; i23++) {
          ycol->data[i23] = 0.0;
        }

        i23 = (int)ElementsOnNodeNum->data[(int)NodesOnInterface->data[nume] - 1];
        for (pEnd = 0; pEnd < i23; pEnd++) {
          if (setnPBC->data[pEnd] > 0.0) {
            if (1.0 > nen) {
              c_loop_ub = 0;
            } else {
              c_loop_ub = (int)nen;
            }

            k0 = (int)interreg2->data[interreg2->size[0] * pEnd];
            for (i24 = 0; i24 < c_loop_ub; i24++) {
              x_data[i24] = (NodesOnElementCG->data[(k0 + NodesOnElementCG->
                size[0] * i24) - 1] == absxk);
            }

            b_idx = 0;
            nb = c_loop_ub;
            notdone = 0;
            exitg2 = false;
            while ((!exitg2) && (notdone <= c_loop_ub - 1)) {
              if (x_data[notdone]) {
                b_idx++;
                ii_data[b_idx - 1] = (signed char)(notdone + 1);
                if (b_idx >= c_loop_ub) {
                  exitg2 = true;
                } else {
                  notdone++;
                }
              } else {
                notdone++;
              }
            }

            if (c_loop_ub == 1) {
              if (b_idx == 0) {
                nb = 0;
              }
            } else if (1 > b_idx) {
              nb = 0;
            } else {
              nb = b_idx;
            }

            for (i24 = 0; i24 < nb; i24++) {
              nodeloc_data[i24] = ii_data[i24];
            }

            for (i24 = 0; i24 < nb; i24++) {
              node_dup_tmp_data[i24] = (signed char)nodeloc_data[i24];
            }

            k0 = (int)interreg2->data[interreg2->size[0] * pEnd];
            numA = NodesOnElementPBC->data[(k0 + NodesOnElementPBC->size[0] *
              (node_dup_tmp_data[0] - 1)) - 1];
            if (0 <= nb - 1) {
              memcpy(&node_dup_tmp_data[0], &ii_data[0], (unsigned int)(nb *
                      (int)sizeof(signed char)));
            }

            k0 = (int)interreg2->data[interreg2->size[0] * pEnd];
            if (NodesOnElementPBC->data[(k0 + NodesOnElementPBC->size[0] *
                 (node_dup_tmp_data[0] - 1)) - 1] > absxk) {
              if (1.0 > i1 - 1.0) {
                loop_ub = 0;
              } else {
                loop_ub = (int)(i1 - 1.0);
              }

              if (0 <= nb - 1) {
                memcpy(&node_dup_tmp_data[0], &ii_data[0], (unsigned int)(nb *
                        (int)sizeof(signed char)));
              }

              k0 = (int)interreg2->data[interreg2->size[0] * pEnd];
              reg = NodesOnElementPBC->data[(k0 + NodesOnElementPBC->size[0] *
                (node_dup_tmp_data[0] - 1)) - 1];
              i24 = elems->size[0];
              elems->size[0] = loop_ub;
              emxEnsureCapacity_boolean_T(elems, i24);
              for (i24 = 0; i24 < loop_ub; i24++) {
                elems->data[i24] = (NodesOnPBC->data[i24 + (((int)absxk - 1) <<
                  2)] == reg);
              }

              k0 = elems->size[0];
              b_idx = 0;
              i24 = iwork->size[0];
              iwork->size[0] = elems->size[0];
              emxEnsureCapacity_int32_T(iwork, i24);
              notdone = 0;
              exitg2 = false;
              while ((!exitg2) && (notdone <= k0 - 1)) {
                if (elems->data[notdone]) {
                  b_idx++;
                  iwork->data[b_idx - 1] = notdone + 1;
                  if (b_idx >= k0) {
                    exitg2 = true;
                  } else {
                    notdone++;
                  }
                } else {
                  notdone++;
                }
              }

              if (elems->size[0] == 1) {
                if (b_idx == 0) {
                  iwork->size[0] = 0;
                }
              } else if (1 > b_idx) {
                iwork->size[0] = 0;
              } else {
                i24 = iwork->size[0];
                iwork->size[0] = b_idx;
                emxEnsureCapacity_int32_T(iwork, i24);
              }

              kEnd = iwork->size[0];
              loop_ub = iwork->size[0];
              for (i24 = 0; i24 < loop_ub; i24++) {
                b_nodeloc_data[i24] = (signed char)((signed char)iwork->data[i24]
                  + 1);
              }
            } else {
              kEnd = 1;
              b_nodeloc_data[0] = 1;
            }

            nodes_size[0] = kEnd;
            for (i24 = 0; i24 < kEnd; i24++) {
              ycol_data[i24] = ycol->data[b_nodeloc_data[i24] - 1];
            }

            if (ifWhileCond(ycol_data, nodes_size)) {
              nodeD2++;
              if (0 <= nb - 1) {
                memcpy(&node_dup_tmp_data[0], &ii_data[0], (unsigned int)(nb *
                        (int)sizeof(signed char)));
              }

              k0 = (int)interreg2->data[interreg2->size[0] * pEnd];
              kEnd = (int)NodesOnElementPBC->data[(k0 + NodesOnElementPBC->size
                [0] * (node_dup_tmp_data[0] - 1)) - 1];
              i24 = (int)nodeD2;
              Coordinates3->data[i24 - 1] = Coordinates->data[kEnd - 1];
              Coordinates3->data[(i24 + Coordinates3->size[0]) - 1] =
                Coordinates->data[(kEnd + Coordinates->size[0]) - 1];

              /*  Loop over all elements attached to node */
              i24 = (int)setnPBC->data[pEnd];
              if (0 <= i24 - 1) {
                if (1.0 > nen) {
                  c_loop_ub = 0;
                } else {
                  c_loop_ub = (int)nen;
                }

                d_nx = c_loop_ub;
              }

              for (q = 0; q < i24; q++) {
                sparse_parenAssign2D(NodeReg, nodeD2, numA,
                                     RegionOnElement->data[(int)interreg2->
                                     data[q + interreg2->size[0] * pEnd] - 1]);
                if (ElementsOnNodeNum->data[(int)absxk - 1] < 1.0) {
                  setAll->size[0] = 1;
                  setAll->size[1] = 0;
                } else if (rtIsInf(ElementsOnNodeNum->data[(int)absxk - 1]) &&
                           (1.0 == ElementsOnNodeNum->data[(int)absxk - 1])) {
                  notdone = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  setAll->size[1] = 1;
                  emxEnsureCapacity_real_T(setAll, notdone);
                  setAll->data[0] = rtNaN;
                } else {
                  notdone = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  setAll->size[1] = (int)floor(ElementsOnNodeNum->data[(int)
                    absxk - 1] - 1.0) + 1;
                  emxEnsureCapacity_real_T(setAll, notdone);
                  loop_ub = (int)floor(ElementsOnNodeNum->data[(int)absxk - 1] -
                                       1.0);
                  for (notdone = 0; notdone <= loop_ub; notdone++) {
                    setAll->data[notdone] = 1.0 + (double)notdone;
                  }
                }

                b_sparse_parenReference(ElementsOnNode->d,
                  ElementsOnNode->colidx, ElementsOnNode->rowidx, setAll, absxk,
                  NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                  &NodeRegSum.m);
                c_sparse_eq(interreg2->data[q + interreg2->size[0] * pEnd],
                            NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                            NodeRegSum.m, &b_expl_temp);
                notdone = t0_rowidx->size[0];
                t0_rowidx->size[0] = b_expl_temp.colidx->size[0];
                emxEnsureCapacity_int32_T(t0_rowidx, notdone);
                loop_ub = b_expl_temp.colidx->size[0];
                for (notdone = 0; notdone < loop_ub; notdone++) {
                  t0_rowidx->data[notdone] = b_expl_temp.colidx->data[notdone];
                }

                notdone = internodes_rowidx->size[0];
                internodes_rowidx->size[0] = b_expl_temp.rowidx->size[0];
                emxEnsureCapacity_int32_T(internodes_rowidx, notdone);
                loop_ub = b_expl_temp.rowidx->size[0];
                for (notdone = 0; notdone < loop_ub; notdone++) {
                  internodes_rowidx->data[notdone] = b_expl_temp.rowidx->
                    data[notdone];
                }

                k0 = b_expl_temp.m;
                b_idx = 0;
                notdone = iwork->size[0];
                iwork->size[0] = t0_rowidx->data[t0_rowidx->size[0] - 1] - 1;
                emxEnsureCapacity_int32_T(iwork, notdone);
                kEnd = 1;
                while (b_idx < t0_rowidx->data[t0_rowidx->size[0] - 1] - 1) {
                  if (b_idx == t0_rowidx->data[kEnd] - 1) {
                    kEnd++;
                  } else {
                    b_idx++;
                    iwork->data[b_idx - 1] = (kEnd - 1) * k0 +
                      internodes_rowidx->data[b_idx - 1];
                  }
                }

                if (t0_rowidx->data[t0_rowidx->size[0] - 1] - 1 == 1) {
                  if (b_idx == 0) {
                    iwork->size[0] = 0;
                  }
                } else if (1 > b_idx) {
                  iwork->size[0] = 0;
                } else {
                  notdone = iwork->size[0];
                  iwork->size[0] = b_idx;
                  emxEnsureCapacity_int32_T(iwork, notdone);
                }

                notdone = ElementsOnNodePBC_d->size[0];
                ElementsOnNodePBC_d->size[0] = iwork->size[0];
                emxEnsureCapacity_real_T(ElementsOnNodePBC_d, notdone);
                loop_ub = iwork->size[0];
                for (notdone = 0; notdone < loop_ub; notdone++) {
                  ElementsOnNodePBC_d->data[notdone] = iwork->data[notdone];
                }

                c_sparse_parenAssign(ElementsOnNodeDup, nodeD2,
                                     ElementsOnNodePBC_d, absxk);

                /*  Reset nodal ID for element in higher material ID */
                k0 = (int)interreg2->data[q + interreg2->size[0] * pEnd];
                for (notdone = 0; notdone < d_nx; notdone++) {
                  x_data[notdone] = (NodesOnElementCG->data[(k0 +
                    NodesOnElementCG->size[0] * notdone) - 1] == absxk);
                }

                b_idx = 0;
                nb = c_loop_ub;
                notdone = 0;
                exitg2 = false;
                while ((!exitg2) && (notdone <= d_nx - 1)) {
                  if (x_data[notdone]) {
                    b_idx++;
                    ii_data[b_idx - 1] = (signed char)(notdone + 1);
                    if (b_idx >= d_nx) {
                      exitg2 = true;
                    } else {
                      notdone++;
                    }
                  } else {
                    notdone++;
                  }
                }

                if (c_loop_ub == 1) {
                  if (b_idx == 0) {
                    nb = 0;
                  }
                } else if (1 > b_idx) {
                  nb = 0;
                } else {
                  nb = b_idx;
                }

                if (0 <= nb - 1) {
                  memcpy(&node_dup_tmp_data[0], &ii_data[0], (unsigned int)(nb *
                          (int)sizeof(signed char)));
                }

                k0 = (int)interreg2->data[q + interreg2->size[0] * pEnd];
                for (notdone = 0; notdone < nb; notdone++) {
                  NodesOnElementDG->data[(k0 + NodesOnElementDG->size[0] *
                    (node_dup_tmp_data[notdone] - 1)) - 1] = nodeD2;
                }
              }
            } else {
              for (i24 = 0; i24 < kEnd; i24++) {
                ycol->data[b_nodeloc_data[i24] - 1] = 1.0;
              }

              i24 = (int)setnPBC->data[pEnd];
              if (0 <= i24 - 1) {
                if (1.0 > nen) {
                  c_loop_ub = 0;
                } else {
                  c_loop_ub = (int)nen;
                }

                c_nx = c_loop_ub;
              }

              for (q = 0; q < i24; q++) {
                sparse_parenAssign2D(NodeReg, numA, numA, RegionOnElement->data
                                     [(int)interreg2->data[q + interreg2->size[0]
                                     * pEnd] - 1]);
                if (ElementsOnNodeNum->data[(int)absxk - 1] < 1.0) {
                  setAll->size[0] = 1;
                  setAll->size[1] = 0;
                } else if (rtIsInf(ElementsOnNodeNum->data[(int)absxk - 1]) &&
                           (1.0 == ElementsOnNodeNum->data[(int)absxk - 1])) {
                  notdone = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  setAll->size[1] = 1;
                  emxEnsureCapacity_real_T(setAll, notdone);
                  setAll->data[0] = rtNaN;
                } else {
                  notdone = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  setAll->size[1] = (int)floor(ElementsOnNodeNum->data[(int)
                    absxk - 1] - 1.0) + 1;
                  emxEnsureCapacity_real_T(setAll, notdone);
                  loop_ub = (int)floor(ElementsOnNodeNum->data[(int)absxk - 1] -
                                       1.0);
                  for (notdone = 0; notdone <= loop_ub; notdone++) {
                    setAll->data[notdone] = 1.0 + (double)notdone;
                  }
                }

                b_sparse_parenReference(ElementsOnNode->d,
                  ElementsOnNode->colidx, ElementsOnNode->rowidx, setAll, absxk,
                  NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                  &NodeRegSum.m);
                c_sparse_eq(interreg2->data[q + interreg2->size[0] * pEnd],
                            NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                            NodeRegSum.m, &b_expl_temp);
                notdone = t0_rowidx->size[0];
                t0_rowidx->size[0] = b_expl_temp.colidx->size[0];
                emxEnsureCapacity_int32_T(t0_rowidx, notdone);
                loop_ub = b_expl_temp.colidx->size[0];
                for (notdone = 0; notdone < loop_ub; notdone++) {
                  t0_rowidx->data[notdone] = b_expl_temp.colidx->data[notdone];
                }

                notdone = internodes_rowidx->size[0];
                internodes_rowidx->size[0] = b_expl_temp.rowidx->size[0];
                emxEnsureCapacity_int32_T(internodes_rowidx, notdone);
                loop_ub = b_expl_temp.rowidx->size[0];
                for (notdone = 0; notdone < loop_ub; notdone++) {
                  internodes_rowidx->data[notdone] = b_expl_temp.rowidx->
                    data[notdone];
                }

                k0 = b_expl_temp.m;
                b_idx = 0;
                notdone = iwork->size[0];
                iwork->size[0] = t0_rowidx->data[t0_rowidx->size[0] - 1] - 1;
                emxEnsureCapacity_int32_T(iwork, notdone);
                kEnd = 1;
                while (b_idx < t0_rowidx->data[t0_rowidx->size[0] - 1] - 1) {
                  if (b_idx == t0_rowidx->data[kEnd] - 1) {
                    kEnd++;
                  } else {
                    b_idx++;
                    iwork->data[b_idx - 1] = (kEnd - 1) * k0 +
                      internodes_rowidx->data[b_idx - 1];
                  }
                }

                if (t0_rowidx->data[t0_rowidx->size[0] - 1] - 1 == 1) {
                  if (b_idx == 0) {
                    iwork->size[0] = 0;
                  }
                } else if (1 > b_idx) {
                  iwork->size[0] = 0;
                } else {
                  notdone = iwork->size[0];
                  iwork->size[0] = b_idx;
                  emxEnsureCapacity_int32_T(iwork, notdone);
                }

                notdone = ElementsOnNodePBC_d->size[0];
                ElementsOnNodePBC_d->size[0] = iwork->size[0];
                emxEnsureCapacity_real_T(ElementsOnNodePBC_d, notdone);
                loop_ub = iwork->size[0];
                for (notdone = 0; notdone < loop_ub; notdone++) {
                  ElementsOnNodePBC_d->data[notdone] = iwork->data[notdone];
                }

                c_sparse_parenAssign(ElementsOnNodeDup, numA,
                                     ElementsOnNodePBC_d, absxk);

                /*  Reset nodal ID for element in higher material ID */
                k0 = (int)interreg2->data[q + interreg2->size[0] * pEnd];
                for (notdone = 0; notdone < c_nx; notdone++) {
                  x_data[notdone] = (NodesOnElementCG->data[(k0 +
                    NodesOnElementCG->size[0] * notdone) - 1] == absxk);
                }

                b_idx = 0;
                nb = c_loop_ub;
                notdone = 0;
                exitg2 = false;
                while ((!exitg2) && (notdone <= c_nx - 1)) {
                  if (x_data[notdone]) {
                    b_idx++;
                    ii_data[b_idx - 1] = (signed char)(notdone + 1);
                    if (b_idx >= c_nx) {
                      exitg2 = true;
                    } else {
                      notdone++;
                    }
                  } else {
                    notdone++;
                  }
                }

                if (c_loop_ub == 1) {
                  if (b_idx == 0) {
                    nb = 0;
                  }
                } else if (1 > b_idx) {
                  nb = 0;
                } else {
                  nb = b_idx;
                }

                if (0 <= nb - 1) {
                  memcpy(&node_dup_tmp_data[0], &ii_data[0], (unsigned int)(nb *
                          (int)sizeof(signed char)));
                }

                k0 = (int)interreg2->data[q + interreg2->size[0] * pEnd];
                for (notdone = 0; notdone < nb; notdone++) {
                  NodesOnElementDG->data[(k0 + NodesOnElementDG->size[0] *
                    (node_dup_tmp_data[notdone] - 1)) - 1] = numA;
                }
              }
            }
          }
        }
      } else {
        /*  regular node */
        i2 = 0;
        i23 = (int)ElementsOnNodeNum->data[(int)NodesOnInterface->data[nume] - 1];
        for (pEnd = 0; pEnd < i23; pEnd++) {
          if (setnPBC->data[pEnd] > 0.0) {
            if (i2 != 0) {
              nodeD2++;
              i24 = (int)nodeD2;
              Coordinates3->data[i24 - 1] = Coordinates->data[(int)absxk - 1];
              Coordinates3->data[(i24 + Coordinates3->size[0]) - 1] =
                Coordinates->data[((int)absxk + Coordinates->size[0]) - 1];

              /*  Loop over all elements attached to node */
              i24 = (int)setnPBC->data[pEnd];
              if (0 <= i24 - 1) {
                if (1.0 > nen) {
                  c_loop_ub = 0;
                } else {
                  c_loop_ub = (int)nen;
                }

                b_nx = c_loop_ub;
              }

              for (q = 0; q < i24; q++) {
                sparse_parenAssign2D(NodeReg, nodeD2, absxk,
                                     RegionOnElement->data[(int)interreg2->
                                     data[q + interreg2->size[0] * pEnd] - 1]);
                if (ElementsOnNodeNum->data[(int)absxk - 1] < 1.0) {
                  setAll->size[0] = 1;
                  setAll->size[1] = 0;
                } else if (rtIsInf(ElementsOnNodeNum->data[(int)absxk - 1]) &&
                           (1.0 == ElementsOnNodeNum->data[(int)absxk - 1])) {
                  notdone = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  setAll->size[1] = 1;
                  emxEnsureCapacity_real_T(setAll, notdone);
                  setAll->data[0] = rtNaN;
                } else {
                  notdone = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  setAll->size[1] = (int)floor(ElementsOnNodeNum->data[(int)
                    absxk - 1] - 1.0) + 1;
                  emxEnsureCapacity_real_T(setAll, notdone);
                  loop_ub = (int)floor(ElementsOnNodeNum->data[(int)absxk - 1] -
                                       1.0);
                  for (notdone = 0; notdone <= loop_ub; notdone++) {
                    setAll->data[notdone] = 1.0 + (double)notdone;
                  }
                }

                b_sparse_parenReference(ElementsOnNode->d,
                  ElementsOnNode->colidx, ElementsOnNode->rowidx, setAll, absxk,
                  NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                  &NodeRegSum.m);
                c_sparse_eq(interreg2->data[q + interreg2->size[0] * pEnd],
                            NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                            NodeRegSum.m, &b_expl_temp);
                notdone = t0_rowidx->size[0];
                t0_rowidx->size[0] = b_expl_temp.colidx->size[0];
                emxEnsureCapacity_int32_T(t0_rowidx, notdone);
                loop_ub = b_expl_temp.colidx->size[0];
                for (notdone = 0; notdone < loop_ub; notdone++) {
                  t0_rowidx->data[notdone] = b_expl_temp.colidx->data[notdone];
                }

                notdone = internodes_rowidx->size[0];
                internodes_rowidx->size[0] = b_expl_temp.rowidx->size[0];
                emxEnsureCapacity_int32_T(internodes_rowidx, notdone);
                loop_ub = b_expl_temp.rowidx->size[0];
                for (notdone = 0; notdone < loop_ub; notdone++) {
                  internodes_rowidx->data[notdone] = b_expl_temp.rowidx->
                    data[notdone];
                }

                k0 = b_expl_temp.m;
                b_idx = 0;
                notdone = iwork->size[0];
                iwork->size[0] = t0_rowidx->data[t0_rowidx->size[0] - 1] - 1;
                emxEnsureCapacity_int32_T(iwork, notdone);
                kEnd = 1;
                while (b_idx < t0_rowidx->data[t0_rowidx->size[0] - 1] - 1) {
                  if (b_idx == t0_rowidx->data[kEnd] - 1) {
                    kEnd++;
                  } else {
                    b_idx++;
                    iwork->data[b_idx - 1] = (kEnd - 1) * k0 +
                      internodes_rowidx->data[b_idx - 1];
                  }
                }

                if (t0_rowidx->data[t0_rowidx->size[0] - 1] - 1 == 1) {
                  if (b_idx == 0) {
                    iwork->size[0] = 0;
                  }
                } else if (1 > b_idx) {
                  iwork->size[0] = 0;
                } else {
                  notdone = iwork->size[0];
                  iwork->size[0] = b_idx;
                  emxEnsureCapacity_int32_T(iwork, notdone);
                }

                notdone = ElementsOnNodePBC_d->size[0];
                ElementsOnNodePBC_d->size[0] = iwork->size[0];
                emxEnsureCapacity_real_T(ElementsOnNodePBC_d, notdone);
                loop_ub = iwork->size[0];
                for (notdone = 0; notdone < loop_ub; notdone++) {
                  ElementsOnNodePBC_d->data[notdone] = iwork->data[notdone];
                }

                c_sparse_parenAssign(ElementsOnNodeDup, nodeD2,
                                     ElementsOnNodePBC_d, absxk);

                /*  Reset nodal ID for element in higher material ID */
                k0 = (int)interreg2->data[q + interreg2->size[0] * pEnd];
                for (notdone = 0; notdone < b_nx; notdone++) {
                  x_data[notdone] = (NodesOnElementCG->data[(k0 +
                    NodesOnElementCG->size[0] * notdone) - 1] == absxk);
                }

                b_idx = 0;
                nb = c_loop_ub;
                notdone = 0;
                exitg2 = false;
                while ((!exitg2) && (notdone <= b_nx - 1)) {
                  if (x_data[notdone]) {
                    b_idx++;
                    ii_data[b_idx - 1] = (signed char)(notdone + 1);
                    if (b_idx >= b_nx) {
                      exitg2 = true;
                    } else {
                      notdone++;
                    }
                  } else {
                    notdone++;
                  }
                }

                if (c_loop_ub == 1) {
                  if (b_idx == 0) {
                    nb = 0;
                  }
                } else if (1 > b_idx) {
                  nb = 0;
                } else {
                  nb = b_idx;
                }

                if (0 <= nb - 1) {
                  memcpy(&node_dup_tmp_data[0], &ii_data[0], (unsigned int)(nb *
                          (int)sizeof(signed char)));
                }

                k0 = (int)interreg2->data[q + interreg2->size[0] * pEnd];
                for (notdone = 0; notdone < nb; notdone++) {
                  NodesOnElementDG->data[(k0 + NodesOnElementDG->size[0] *
                    (node_dup_tmp_data[notdone] - 1)) - 1] = nodeD2;
                }
              }
            } else {
              i2 = 1;
            }
          }
        }
      }
    }

    /* if facnum */
  }

  f_emxFreeStruct_coder_internal_(&c_expl_temp);
  emxFree_boolean_T(&t2_d);
  emxFree_int32_T(&t0_colidx);
  emxFree_int32_T(&j);
  emxFree_int32_T(&idx);
  emxFree_real_T(&interreg2);
  emxFree_boolean_T(&internodes_d);
  emxFree_real_T(&setnPBC);
  emxFree_real_T(&ElementsOnNodeA);
  emxFree_int32_T(&ElementsOnNodePBC_rowidx);
  emxFree_real_T(&ElementsOnNodePBCNum);

  /*  Form interfaces between all facs on interiors of specified material regions */
  diag(InterTypes, ycol);
  if (b_norm(ycol) > 0.0) {
    i21 = NodesOnElementCG->size[0] * NodesOnElementCG->size[1];
    NodesOnElementCG->size[0] = NodesOnElementDG->size[0];
    NodesOnElementCG->size[1] = NodesOnElementDG->size[1];
    emxEnsureCapacity_real_T(NodesOnElementCG, i21);
    loop_ub = NodesOnElementDG->size[0] * NodesOnElementDG->size[1];
    for (i21 = 0; i21 < loop_ub; i21++) {
      NodesOnElementCG->data[i21] = NodesOnElementDG->data[i21];
    }

    /*  Update connectivities to reflect presence of interfaces */
    i21 = ElementsOnNodeNum2->size[0];
    ElementsOnNodeNum2->size[0] = (int)nodeD2;
    emxEnsureCapacity_real_T(ElementsOnNodeNum2, i21);
    loop_ub = (int)nodeD2;
    for (i21 = 0; i21 < loop_ub; i21++) {
      ElementsOnNodeNum2->data[i21] = 0.0;
    }

    i21 = NodeCGDG->size[0] * NodeCGDG->size[1];
    NodeCGDG->size[0] = 12;
    NodeCGDG->size[1] = (int)nodeD2;
    emxEnsureCapacity_real_T(NodeCGDG, i21);
    loop_ub = 12 * (int)nodeD2;
    for (i21 = 0; i21 < loop_ub; i21++) {
      NodeCGDG->data[i21] = 0.0;
    }

    /*  Mapping of DG nodal IDs to CG nodal IDs */
    if (!(nodeD2 < 1.0)) {
      loop_ub = (int)floor(nodeD2 - 1.0);
      for (i21 = 0; i21 <= loop_ub; i21++) {
        NodeCGDG->data[12 * i21] = 1.0 + (double)i21;
      }
    }

    i21 = (int)nummat;
    for (numPBC = 0; numPBC < i21; numPBC++) {
      /*  ID for material pair (row=mat2, col=mat1) */
      if (InterTypes->data[numPBC + InterTypes->size[0] * numPBC] > 0.0) {
        /*  Find elements belonging to material mat2 */
        i23 = elems->size[0];
        elems->size[0] = RegionOnElement->size[0];
        emxEnsureCapacity_boolean_T(elems, i23);
        loop_ub = RegionOnElement->size[0];
        for (i23 = 0; i23 < loop_ub; i23++) {
          elems->data[i23] = (RegionOnElement->data[i23] == 1.0 + (double)numPBC);
        }

        k0 = elems->size[0];
        b_idx = 0;
        i23 = iwork->size[0];
        iwork->size[0] = elems->size[0];
        emxEnsureCapacity_int32_T(iwork, i23);
        notdone = 0;
        exitg2 = false;
        while ((!exitg2) && (notdone <= k0 - 1)) {
          if (elems->data[notdone]) {
            b_idx++;
            iwork->data[b_idx - 1] = notdone + 1;
            if (b_idx >= k0) {
              exitg2 = true;
            } else {
              notdone++;
            }
          } else {
            notdone++;
          }
        }

        if (elems->size[0] == 1) {
          if (b_idx == 0) {
            iwork->size[0] = 0;
          }
        } else if (1 > b_idx) {
          iwork->size[0] = 0;
        } else {
          i23 = iwork->size[0];
          iwork->size[0] = b_idx;
          emxEnsureCapacity_int32_T(iwork, i23);
        }

        i23 = ycol->size[0];
        ycol->size[0] = iwork->size[0];
        emxEnsureCapacity_real_T(ycol, i23);
        loop_ub = iwork->size[0];
        for (i23 = 0; i23 < loop_ub; i23++) {
          ycol->data[i23] = iwork->data[i23];
        }

        /*  Loop over elements in material mat2, explode all the elements */
        i23 = ycol->size[0];
        for (i = 0; i < i23; i++) {
          exponent = (int)ycol->data[i] - 1;
          if (1.0 > nen) {
            loop_ub = 0;
          } else {
            loop_ub = (int)nen;
          }

          k0 = (int)ycol->data[i];
          for (i24 = 0; i24 < loop_ub; i24++) {
            nodeloc_data[i24] = NodesOnElementDG->data[(k0 +
              NodesOnElementDG->size[0] * i24) - 1];
          }

          n = -1;
          i24 = loop_ub - 1;
          for (k = 0; k <= i24; k++) {
            if (nodeloc_data[k] != 0.0) {
              n++;
            }
          }

          for (nume = 0; nume <= n; nume++) {
            /*  Loop over local Nodes */
            absxk = NodesOnElementCG->data[exponent + NodesOnElementCG->size[0] *
              nume];
            i24 = (int)absxk;
            notdone = i24 - 1;
            if (ElementsOnNodeNum2->data[notdone] == 0.0) {
              /*  only duplicate nodes after the first time encountered */
              ElementsOnNodeNum2->data[notdone] = 1.0;
              NodeCGDG->data[12 * notdone] = absxk;

              /* node in DG mesh with same coordinates */
            } else {
              nodeD2++;
              NodesOnElementDG->data[exponent + NodesOnElementDG->size[0] * nume]
                = nodeD2;
              numA = Coordinates3->data[notdone];
              i1 = Coordinates3->data[(i24 + Coordinates3->size[0]) - 1];
              i24 = (int)nodeD2;
              Coordinates3->data[i24 - 1] = numA;
              Coordinates3->data[(i24 + Coordinates3->size[0]) - 1] = i1;
              i1 = NodesOnElement->data[exponent + NodesOnElement->size[0] *
                nume];
              i24 = (int)i1 - 1;
              if (ElementsOnNodeNum->data[i24] < 1.0) {
                setAll->size[0] = 1;
                setAll->size[1] = 0;
              } else if (rtIsInf(ElementsOnNodeNum->data[i24]) && (1.0 ==
                          ElementsOnNodeNum->data[i24])) {
                i24 = setAll->size[0] * setAll->size[1];
                setAll->size[0] = 1;
                setAll->size[1] = 1;
                emxEnsureCapacity_real_T(setAll, i24);
                setAll->data[0] = rtNaN;
              } else {
                k0 = setAll->size[0] * setAll->size[1];
                setAll->size[0] = 1;
                setAll->size[1] = (int)floor(ElementsOnNodeNum->data[i24] - 1.0)
                  + 1;
                emxEnsureCapacity_real_T(setAll, k0);
                loop_ub = (int)floor(ElementsOnNodeNum->data[i24] - 1.0);
                for (i24 = 0; i24 <= loop_ub; i24++) {
                  setAll->data[i24] = 1.0 + (double)i24;
                }
              }

              b_sparse_parenReference(ElementsOnNode->d, ElementsOnNode->colidx,
                ElementsOnNode->rowidx, setAll, i1, NodeRegSum.d,
                NodeRegSum.colidx, NodeRegSum.rowidx, &NodeRegSum.m);
              c_sparse_eq(exponent + 1, NodeRegSum.d, NodeRegSum.colidx,
                          NodeRegSum.rowidx, NodeRegSum.m, &b_expl_temp);
              i24 = t0_rowidx->size[0];
              t0_rowidx->size[0] = b_expl_temp.colidx->size[0];
              emxEnsureCapacity_int32_T(t0_rowidx, i24);
              loop_ub = b_expl_temp.colidx->size[0];
              for (i24 = 0; i24 < loop_ub; i24++) {
                t0_rowidx->data[i24] = b_expl_temp.colidx->data[i24];
              }

              i24 = internodes_rowidx->size[0];
              internodes_rowidx->size[0] = b_expl_temp.rowidx->size[0];
              emxEnsureCapacity_int32_T(internodes_rowidx, i24);
              loop_ub = b_expl_temp.rowidx->size[0];
              for (i24 = 0; i24 < loop_ub; i24++) {
                internodes_rowidx->data[i24] = b_expl_temp.rowidx->data[i24];
              }

              k0 = b_expl_temp.m;
              b_idx = 0;
              i24 = iwork->size[0];
              iwork->size[0] = t0_rowidx->data[t0_rowidx->size[0] - 1] - 1;
              emxEnsureCapacity_int32_T(iwork, i24);
              kEnd = 1;
              while (b_idx < t0_rowidx->data[t0_rowidx->size[0] - 1] - 1) {
                if (b_idx == t0_rowidx->data[kEnd] - 1) {
                  kEnd++;
                } else {
                  b_idx++;
                  iwork->data[b_idx - 1] = (kEnd - 1) * k0 +
                    internodes_rowidx->data[b_idx - 1];
                }
              }

              if (t0_rowidx->data[t0_rowidx->size[0] - 1] - 1 == 1) {
                if (b_idx == 0) {
                  iwork->size[0] = 0;
                }
              } else if (1 > b_idx) {
                iwork->size[0] = 0;
              } else {
                i24 = iwork->size[0];
                iwork->size[0] = b_idx;
                emxEnsureCapacity_int32_T(iwork, i24);
              }

              i24 = ElementsOnNodePBC_d->size[0];
              ElementsOnNodePBC_d->size[0] = iwork->size[0];
              emxEnsureCapacity_real_T(ElementsOnNodePBC_d, i24);
              loop_ub = iwork->size[0];
              for (i24 = 0; i24 < loop_ub; i24++) {
                ElementsOnNodePBC_d->data[i24] = iwork->data[i24];
              }

              c_sparse_parenAssign(ElementsOnNodeDup, nodeD2,
                                   ElementsOnNodePBC_d, i1);

              /*    Add element to star list, increment number of elem in star */
              i1 = ElementsOnNodeNum2->data[notdone] + 1.0;
              ElementsOnNodeNum2->data[notdone]++;
              NodeCGDG->data[((int)i1 + 12 * ((int)absxk - 1)) - 1] = nodeD2;

              /* node in DG mesh with same coordinates */
            }
          }

          /* k */
        }
      }
    }

    i21 = elems->size[0];
    elems->size[0] = ElementsOnNodeNum2->size[0];
    emxEnsureCapacity_boolean_T(elems, i21);
    loop_ub = ElementsOnNodeNum2->size[0];
    for (i21 = 0; i21 < loop_ub; i21++) {
      elems->data[i21] = (ElementsOnNodeNum2->data[i21] == 0.0);
    }

    k0 = elems->size[0];
    b_idx = 0;
    i21 = iwork->size[0];
    iwork->size[0] = elems->size[0];
    emxEnsureCapacity_int32_T(iwork, i21);
    notdone = 0;
    exitg2 = false;
    while ((!exitg2) && (notdone <= k0 - 1)) {
      if (elems->data[notdone]) {
        b_idx++;
        iwork->data[b_idx - 1] = notdone + 1;
        if (b_idx >= k0) {
          exitg2 = true;
        } else {
          notdone++;
        }
      } else {
        notdone++;
      }
    }

    if (elems->size[0] == 1) {
      if (b_idx == 0) {
        iwork->size[0] = 0;
      }
    } else if (1 > b_idx) {
      iwork->size[0] = 0;
    } else {
      i21 = iwork->size[0];
      iwork->size[0] = b_idx;
      emxEnsureCapacity_int32_T(iwork, i21);
    }

    loop_ub = iwork->size[0];
    for (i21 = 0; i21 < loop_ub; i21++) {
      ElementsOnNodeNum2->data[iwork->data[i21] - 1] = 1.0;
    }
  } else {
    /*  no DG in interior of any materials */
    i21 = ElementsOnNodeNum2->size[0];
    loop_ub = (int)nodeD2;
    ElementsOnNodeNum2->size[0] = loop_ub;
    emxEnsureCapacity_real_T(ElementsOnNodeNum2, i21);
    for (i21 = 0; i21 < loop_ub; i21++) {
      ElementsOnNodeNum2->data[i21] = 1.0;
    }

    /*  set one copy of duplicated nodes per material ID so that BC expanding subroutine works properly. */
    i21 = NodeCGDG->size[0] * NodeCGDG->size[1];
    NodeCGDG->size[0] = 12;
    NodeCGDG->size[1] = loop_ub;
    emxEnsureCapacity_real_T(NodeCGDG, i21);
    nb = 12 * loop_ub;
    for (i21 = 0; i21 < nb; i21++) {
      NodeCGDG->data[i21] = 0.0;
    }

    /*  Mapping of DG nodal IDs to CG nodal IDs */
    if (!(nodeD2 < 1.0)) {
      nb = (int)floor(nodeD2 - 1.0);
      for (i21 = 0; i21 <= nb; i21++) {
        NodeCGDG->data[12 * i21] = 1.0 + (double)i21;
      }
    }
  }

  e_emxFreeStruct_coder_internal_(&b_expl_temp);
  emxFree_int32_T(&t0_rowidx);
  emxFree_real_T(&ycol);
  emxFree_int32_T(&iwork);
  emxFree_boolean_T(&elems);
  d_emxFreeStruct_coder_internal_(&NodeRegSum);
  emxFree_int32_T(&internodes_rowidx);
  emxFree_real_T(&setAll);
  emxFree_real_T(&ElementsOnNodePBC_d);

  /*  Form final lists of DG interfaces */
  *numnp = nodeD2;
  if (1.0 > nodeD2) {
    loop_ub = 0;
  } else {
    loop_ub = (int)nodeD2;
  }

  i21 = Coordinates->size[0] * Coordinates->size[1];
  Coordinates->size[0] = loop_ub;
  Coordinates->size[1] = 2;
  emxEnsureCapacity_real_T(Coordinates, i21);
  for (i21 = 0; i21 < loop_ub; i21++) {
    Coordinates->data[i21] = Coordinates3->data[i21];
  }

  for (i21 = 0; i21 < loop_ub; i21++) {
    Coordinates->data[i21 + Coordinates->size[0]] = Coordinates3->data[i21 +
      Coordinates3->size[0]];
  }

  emxFree_real_T(&Coordinates3);
  i21 = NodesOnElement->size[0] * NodesOnElement->size[1];
  NodesOnElement->size[0] = NodesOnElementDG->size[0];
  NodesOnElement->size[1] = NodesOnElementDG->size[1];
  emxEnsureCapacity_real_T(NodesOnElement, i21);
  loop_ub = NodesOnElementDG->size[1];
  for (i21 = 0; i21 < loop_ub; i21++) {
    b_loop_ub = NodesOnElementDG->size[0];
    for (i23 = 0; i23 < b_loop_ub; i23++) {
      NodesOnElement->data[i23 + NodesOnElement->size[0] * i21] =
        NodesOnElementDG->data[i23 + NodesOnElementDG->size[0] * i21];
    }
  }

  if (usePBC != 0.0) {
    i21 = NodesOnElementCG->size[0] * NodesOnElementCG->size[1];
    NodesOnElementCG->size[0] = NodesOnElementPBC->size[0];
    NodesOnElementCG->size[1] = NodesOnElementPBC->size[1];
    emxEnsureCapacity_real_T(NodesOnElementCG, i21);
    loop_ub = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1];
    for (i21 = 0; i21 < loop_ub; i21++) {
      NodesOnElementCG->data[i21] = NodesOnElementPBC->data[i21];
    }
  }

  emxFree_real_T(&NodesOnElementPBC);

  /*  %% Clear out intermediate variables */
  /*  if clearinter */
  /*  KeyList = {'numEonB','numEonF','ElementsOnBoundary','numSI','ElementsOnFacet',... */
  /*  'ElementsOnNode','ElementsOnNodeA','ElementsOnNodeB','ElementsOnNodeDup',... */
  /*  'ElementsOnNodeNum','numfac','ElementsOnNodeNum2','numinttype','FacetsOnElement',... */
  /*  'FacetsOnElementInt','FacetsOnInterface','FacetsOnInterfaceNum','FacetsOnNode',... */
  /*  'FacetsOnNodeCut','FacetsOnNodeInt','FacetsOnNodeNum','NodeCGDG','NodeReg',... */
  /*  'NodesOnElementCG','NodesOnElementDG','NodesOnInterface','NodesOnInterfaceNum',... */
  /*  'numCL','maxel','numelPBC','RegionOnElementDG','numPBC','TieNodesNew','TieNodes',... */
  /*  'NodesOnPBC','NodesOnPBCnum','NodesOnLink','NodesOnLinknum','numEonPBC',... */
  /*  'FacetsOnPBC','FacetsOnPBCNum','FacetsOnIntMinusPBC','FacetsOnIntMinusPBCNum','MPCList'}; */
  /*  if isOctave */
  /*      wholething = [{'-x'},KeyList,currvariables']; */
  /*      clear(wholething{:}) */
  /*  else */
  /*      wholething = [{'-except'},KeyList,currvariables']; */
  /*      clearvars(wholething{:}) */
  /*  end */
  /*  end */
  *NodesOnInterfaceNum = i22 + 1;
  *numCL = 0.0;
}

/* End of code generation (DEIPFunc2.c) */
