/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * DEIPFunc2_emxutil.h
 *
 * Code generation for function 'DEIPFunc2_emxutil'
 *
 */

#ifndef DEIPFUNC2_EMXUTIL_H
#define DEIPFUNC2_EMXUTIL_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "DEIPFunc2_types.h"

/* Function Declarations */
extern void c_emxFreeStruct_coder_internal_(coder_internal_sparse *pStruct);
extern void c_emxInitStruct_coder_internal_(coder_internal_sparse *pStruct);
extern void d_emxFreeStruct_coder_internal_(coder_internal_sparse_2 *pStruct);
extern void d_emxInitStruct_coder_internal_(coder_internal_sparse_2 *pStruct);
extern void e_emxFreeStruct_coder_internal_(coder_internal_sparse_1 *pStruct);
extern void e_emxInitStruct_coder_internal_(coder_internal_sparse_1 *pStruct);
extern void emxEnsureCapacity_boolean_T(emxArray_boolean_T *emxArray, int
  oldNumel);
extern void emxEnsureCapacity_int32_T(emxArray_int32_T *emxArray, int oldNumel);
extern void emxEnsureCapacity_int64_T(emxArray_int64_T *emxArray, int oldNumel);
extern void emxEnsureCapacity_int8_T(emxArray_int8_T *emxArray, int oldNumel);
extern void emxEnsureCapacity_real_T(emxArray_real_T *emxArray, int oldNumel);
extern void emxFreeMatrix_cell_wrap_1(cell_wrap_1 pMatrix[1]);
extern void emxFreeMatrix_cell_wrap_11(cell_wrap_1 pMatrix[2]);
extern void emxFree_boolean_T(emxArray_boolean_T **pEmxArray);
extern void emxFree_int32_T(emxArray_int32_T **pEmxArray);
extern void emxFree_int64_T(emxArray_int64_T **pEmxArray);
extern void emxFree_int8_T(emxArray_int8_T **pEmxArray);
extern void emxFree_real_T(emxArray_real_T **pEmxArray);
extern void emxInitMatrix_cell_wrap_1(cell_wrap_1 pMatrix[1]);
extern void emxInitMatrix_cell_wrap_11(cell_wrap_1 pMatrix[2]);
extern void emxInit_boolean_T(emxArray_boolean_T **pEmxArray, int numDimensions);
extern void emxInit_int32_T(emxArray_int32_T **pEmxArray, int numDimensions);
extern void emxInit_int64_T(emxArray_int64_T **pEmxArray, int numDimensions);
extern void emxInit_int8_T(emxArray_int8_T **pEmxArray, int numDimensions);
extern void emxInit_real_T(emxArray_real_T **pEmxArray, int numDimensions);
extern void f_emxFreeStruct_coder_internal_(coder_internal_sparse_4 *pStruct);
extern void f_emxInitStruct_coder_internal_(coder_internal_sparse_4 *pStruct);

#endif

/* End of code generation (DEIPFunc2_emxutil.h) */
