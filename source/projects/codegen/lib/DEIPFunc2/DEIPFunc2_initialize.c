/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * DEIPFunc2_initialize.c
 *
 * Code generation for function 'DEIPFunc2_initialize'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "DEIPFunc2.h"
#include "DEIPFunc2_initialize.h"

/* Function Definitions */
void DEIPFunc2_initialize(void)
{
  rt_InitInfAndNaN(8U);
}

/* End of code generation (DEIPFunc2_initialize.c) */
