/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * DEIPFunc2_initialize.h
 *
 * Code generation for function 'DEIPFunc2_initialize'
 *
 */

#ifndef DEIPFUNC2_INITIALIZE_H
#define DEIPFUNC2_INITIALIZE_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "DEIPFunc2_types.h"

/* Function Declarations */
extern void DEIPFunc2_initialize(void);

#endif

/* End of code generation (DEIPFunc2_initialize.h) */
