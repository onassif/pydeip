/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * DEIPFunc2_terminate.c
 *
 * Code generation for function 'DEIPFunc2_terminate'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "DEIPFunc2.h"
#include "DEIPFunc2_terminate.h"

/* Function Definitions */
void DEIPFunc2_terminate(void)
{
  /* (no terminate code required) */
}

/* End of code generation (DEIPFunc2_terminate.c) */
