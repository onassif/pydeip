/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * eml_setop.c
 *
 * Code generation for function 'eml_setop'
 *
 */

/* Include files */
#include <math.h>
#include "rt_nonfinite.h"
#include "DEIPFunc2.h"
#include "eml_setop.h"
#include "DEIPFunc2_emxutil.h"

/* Function Declarations */
static double skip_to_last_equal_value(int *k, const emxArray_real_T *x);

/* Function Definitions */
static double skip_to_last_equal_value(int *k, const emxArray_real_T *x)
{
  double xk;
  boolean_T exitg1;
  double absxk;
  int exponent;
  xk = x->data[*k - 1];
  exitg1 = false;
  while ((!exitg1) && (*k < x->size[0])) {
    absxk = fabs(xk / 2.0);
    if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
      if (absxk <= 2.2250738585072014E-308) {
        absxk = 4.94065645841247E-324;
      } else {
        frexp(absxk, &exponent);
        absxk = ldexp(1.0, exponent - 53);
      }
    } else {
      absxk = rtNaN;
    }

    if ((fabs(xk - x->data[*k]) < absxk) || (rtIsInf(x->data[*k]) && rtIsInf(xk)
         && ((x->data[*k] > 0.0) == (xk > 0.0)))) {
      (*k)++;
    } else {
      exitg1 = true;
    }
  }

  return xk;
}

void b_do_vectors(const emxArray_real_T *a, const emxArray_real_T *b,
                  emxArray_real_T *c, emxArray_int32_T *ia, int ib_size[1])
{
  int na;
  unsigned int a_idx_0;
  int iafirst;
  int nc;
  int nia;
  int ialast;
  int iblast;
  int b_ialast;
  double ak;
  double bk;
  double absxk;
  int exponent;
  boolean_T b1;
  na = a->size[0];
  a_idx_0 = (unsigned int)a->size[0];
  iafirst = c->size[0];
  c->size[0] = (int)a_idx_0;
  emxEnsureCapacity_real_T(c, iafirst);
  iafirst = ia->size[0];
  ia->size[0] = a->size[0];
  emxEnsureCapacity_int32_T(ia, iafirst);
  ib_size[0] = 0;
  nc = 0;
  nia = 0;
  iafirst = 0;
  ialast = 1;
  iblast = 1;
  while ((ialast <= na) && (iblast <= b->size[0])) {
    b_ialast = ialast;
    ak = skip_to_last_equal_value(&b_ialast, a);
    ialast = b_ialast;
    bk = skip_to_last_equal_value(&iblast, b);
    absxk = fabs(bk / 2.0);
    if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
      if (absxk <= 2.2250738585072014E-308) {
        absxk = 4.94065645841247E-324;
      } else {
        frexp(absxk, &exponent);
        absxk = ldexp(1.0, exponent - 53);
      }
    } else {
      absxk = rtNaN;
    }

    if ((fabs(bk - ak) < absxk) || (rtIsInf(ak) && rtIsInf(bk) && ((ak > 0.0) ==
          (bk > 0.0)))) {
      ialast = b_ialast + 1;
      iafirst = b_ialast;
      iblast++;
    } else {
      if (rtIsNaN(bk)) {
        b1 = !rtIsNaN(ak);
      } else {
        b1 = ((!rtIsNaN(ak)) && (ak < bk));
      }

      if (b1) {
        nc++;
        nia++;
        c->data[nc - 1] = ak;
        ia->data[nia - 1] = iafirst + 1;
        ialast = b_ialast + 1;
        iafirst = b_ialast;
      } else {
        iblast++;
      }
    }
  }

  while (ialast <= na) {
    iafirst = ialast;
    ak = skip_to_last_equal_value(&iafirst, a);
    nc++;
    nia++;
    c->data[nc - 1] = ak;
    ia->data[nia - 1] = ialast;
    ialast = iafirst + 1;
  }

  if (a->size[0] > 0) {
    if (1 > nia) {
      ia->size[0] = 0;
    } else {
      iafirst = ia->size[0];
      ia->size[0] = nia;
      emxEnsureCapacity_int32_T(ia, iafirst);
    }

    if (1 > nc) {
      c->size[0] = 0;
    } else {
      iafirst = c->size[0];
      c->size[0] = nc;
      emxEnsureCapacity_real_T(c, iafirst);
    }
  }
}

void c_do_vectors(const emxArray_real_T *a, const emxArray_real_T *b,
                  emxArray_real_T *c, emxArray_int32_T *ia, emxArray_int32_T *ib)
{
  int iafirst;
  int ncmax;
  int nc;
  int ialast;
  int ibfirst;
  int iblast;
  int b_ialast;
  double ak;
  int b_iblast;
  double bk;
  double absxk;
  int exponent;
  boolean_T b2;
  iafirst = a->size[0];
  ncmax = b->size[0];
  if (iafirst < ncmax) {
    ncmax = iafirst;
  }

  iafirst = c->size[0];
  c->size[0] = ncmax;
  emxEnsureCapacity_real_T(c, iafirst);
  iafirst = ia->size[0];
  ia->size[0] = ncmax;
  emxEnsureCapacity_int32_T(ia, iafirst);
  iafirst = ib->size[0];
  ib->size[0] = ncmax;
  emxEnsureCapacity_int32_T(ib, iafirst);
  nc = 0;
  iafirst = 0;
  ialast = 1;
  ibfirst = 0;
  iblast = 1;
  while ((ialast <= a->size[0]) && (iblast <= b->size[0])) {
    b_ialast = ialast;
    ak = skip_to_last_equal_value(&b_ialast, a);
    ialast = b_ialast;
    b_iblast = iblast;
    bk = skip_to_last_equal_value(&b_iblast, b);
    iblast = b_iblast;
    absxk = fabs(bk / 2.0);
    if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
      if (absxk <= 2.2250738585072014E-308) {
        absxk = 4.94065645841247E-324;
      } else {
        frexp(absxk, &exponent);
        absxk = ldexp(1.0, exponent - 53);
      }
    } else {
      absxk = rtNaN;
    }

    if ((fabs(bk - ak) < absxk) || (rtIsInf(ak) && rtIsInf(bk) && ((ak > 0.0) ==
          (bk > 0.0)))) {
      nc++;
      c->data[nc - 1] = ak;
      ia->data[nc - 1] = iafirst + 1;
      ib->data[nc - 1] = ibfirst + 1;
      ialast = b_ialast + 1;
      iafirst = b_ialast;
      iblast = b_iblast + 1;
      ibfirst = b_iblast;
    } else {
      if (rtIsNaN(bk)) {
        b2 = !rtIsNaN(ak);
      } else {
        b2 = ((!rtIsNaN(ak)) && (ak < bk));
      }

      if (b2) {
        ialast = b_ialast + 1;
        iafirst = b_ialast;
      } else {
        iblast = b_iblast + 1;
        ibfirst = b_iblast;
      }
    }
  }

  if (ncmax > 0) {
    if (1 > nc) {
      ia->size[0] = 0;
      ib->size[0] = 0;
      c->size[0] = 0;
    } else {
      iafirst = ia->size[0];
      ia->size[0] = nc;
      emxEnsureCapacity_int32_T(ia, iafirst);
      iafirst = ib->size[0];
      ib->size[0] = nc;
      emxEnsureCapacity_int32_T(ib, iafirst);
      iafirst = c->size[0];
      c->size[0] = nc;
      emxEnsureCapacity_real_T(c, iafirst);
    }
  }
}

void do_vectors(const double a_data[], const int a_size[1], double b, double
                c_data[], int c_size[1], int ia_data[], int ia_size[1], int
                ib_size[1])
{
  int na;
  signed char a_idx_0;
  int nc;
  int nia;
  int iafirst;
  int ialast;
  int iblast;
  int b_ialast;
  emxArray_real_T b_a_data;
  emxArray_real_T c_a_data;
  double ak;
  double absxk;
  int exponent;
  boolean_T b0;
  na = a_size[0];
  a_idx_0 = (signed char)a_size[0];
  c_size[0] = a_idx_0;
  ia_size[0] = a_size[0];
  ib_size[0] = 0;
  nc = 0;
  nia = 0;
  iafirst = 0;
  ialast = 1;
  iblast = 1;
  while ((ialast <= na) && (iblast <= 1)) {
    b_ialast = ialast;
    b_a_data.data = (double *)&a_data[0];
    b_a_data.size = (int *)&a_size[0];
    b_a_data.allocatedSize = -1;
    b_a_data.numDimensions = 1;
    b_a_data.canFreeData = false;
    ak = skip_to_last_equal_value(&b_ialast, &b_a_data);
    ialast = b_ialast;
    absxk = fabs(b / 2.0);
    if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
      if (absxk <= 2.2250738585072014E-308) {
        absxk = 4.94065645841247E-324;
      } else {
        frexp(absxk, &exponent);
        absxk = ldexp(1.0, exponent - 53);
      }
    } else {
      absxk = rtNaN;
    }

    if ((fabs(b - ak) < absxk) || (rtIsInf(ak) && rtIsInf(b) && ((ak > 0.0) ==
          (b > 0.0)))) {
      ialast = b_ialast + 1;
      iafirst = b_ialast;
      iblast = 2;
    } else {
      if (rtIsNaN(b)) {
        b0 = !rtIsNaN(ak);
      } else {
        b0 = ((!rtIsNaN(ak)) && (ak < b));
      }

      if (b0) {
        nc++;
        nia++;
        c_data[nc - 1] = ak;
        ia_data[nia - 1] = iafirst + 1;
        ialast = b_ialast + 1;
        iafirst = b_ialast;
      } else {
        iblast = 2;
      }
    }
  }

  while (ialast <= na) {
    iafirst = ialast;
    c_a_data.data = (double *)&a_data[0];
    c_a_data.size = (int *)&a_size[0];
    c_a_data.allocatedSize = -1;
    c_a_data.numDimensions = 1;
    c_a_data.canFreeData = false;
    ak = skip_to_last_equal_value(&iafirst, &c_a_data);
    nc++;
    nia++;
    c_data[nc - 1] = ak;
    ia_data[nia - 1] = ialast;
    ialast = iafirst + 1;
  }

  if (a_size[0] > 0) {
    if (1 > nia) {
      ia_size[0] = 0;
    } else {
      ia_size[0] = nia;
    }

    if (1 > nc) {
      c_size[0] = 0;
    } else {
      c_size[0] = nc;
    }
  }
}

/* End of code generation (eml_setop.c) */
