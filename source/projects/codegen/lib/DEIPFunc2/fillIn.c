/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * fillIn.c
 *
 * Code generation for function 'fillIn'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "DEIPFunc2.h"
#include "fillIn.h"

/* Function Definitions */
void b_sparse_fillIn(coder_internal_sparse_1 *this)
{
  int idx;
  int i35;
  int c;
  int ridx;
  int currRowIdx;
  boolean_T val;
  idx = 1;
  i35 = this->colidx->size[0];
  for (c = 0; c <= i35 - 2; c++) {
    ridx = this->colidx->data[c];
    this->colidx->data[c] = idx;
    while (ridx < this->colidx->data[c + 1]) {
      currRowIdx = this->rowidx->data[ridx - 1];
      val = this->d->data[ridx - 1];
      ridx++;
      if (val) {
        this->d->data[idx - 1] = true;
        this->rowidx->data[idx - 1] = currRowIdx;
        idx++;
      }
    }
  }

  this->colidx->data[this->colidx->size[0] - 1] = idx;
}

void c_sparse_fillIn(coder_internal_sparse_4 *this)
{
  int idx;
  int i38;
  int c;
  int ridx;
  int currRowIdx;
  boolean_T val;
  idx = 1;
  i38 = this->colidx->size[0];
  for (c = 0; c <= i38 - 2; c++) {
    ridx = this->colidx->data[c];
    this->colidx->data[c] = idx;
    while (ridx < this->colidx->data[c + 1]) {
      currRowIdx = this->rowidx->data[ridx - 1];
      val = this->d->data[ridx - 1];
      ridx++;
      if (val) {
        this->d->data[idx - 1] = true;
        this->rowidx->data[idx - 1] = currRowIdx;
        idx++;
      }
    }
  }

  this->colidx->data[this->colidx->size[0] - 1] = idx;
}

void sparse_fillIn(coder_internal_sparse *this)
{
  int idx;
  int i30;
  int c;
  int ridx;
  double val;
  int currRowIdx;
  idx = 1;
  i30 = this->colidx->size[0];
  for (c = 0; c <= i30 - 2; c++) {
    ridx = this->colidx->data[c];
    this->colidx->data[c] = idx;
    while (ridx < this->colidx->data[c + 1]) {
      val = 0.0;
      currRowIdx = this->rowidx->data[ridx - 1];
      while ((ridx < this->colidx->data[c + 1]) && (this->rowidx->data[ridx - 1]
              == currRowIdx)) {
        val += this->d->data[ridx - 1];
        ridx++;
      }

      if (val != 0.0) {
        this->d->data[idx - 1] = val;
        this->rowidx->data[idx - 1] = currRowIdx;
        idx++;
      }
    }
  }

  this->colidx->data[this->colidx->size[0] - 1] = idx;
}

/* End of code generation (fillIn.c) */
