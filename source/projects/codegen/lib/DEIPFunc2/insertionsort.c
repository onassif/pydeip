/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * insertionsort.c
 *
 * Code generation for function 'insertionsort'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "DEIPFunc2.h"
#include "insertionsort.h"

/* Function Definitions */
void b_insertionsort(emxArray_int32_T *x, int xstart, int xend, const
                     cell_wrap_1 cmp_tunableEnvironment[1])
{
  int i36;
  int k;
  int xc;
  int idx;
  i36 = xstart + 1;
  for (k = i36; k <= xend; k++) {
    xc = x->data[k - 1];
    idx = k - 1;
    while ((idx >= xstart) && (cmp_tunableEnvironment[0].f1->data[xc - 1] <
            cmp_tunableEnvironment[0].f1->data[x->data[idx - 1] - 1])) {
      x->data[idx] = x->data[idx - 1];
      idx--;
    }

    x->data[idx] = xc;
  }
}

void insertionsort(emxArray_int32_T *x, int xstart, int xend, const cell_wrap_1
                   cmp_tunableEnvironment[2])
{
  int i29;
  int k;
  int xc;
  int idx;
  boolean_T exitg1;
  boolean_T varargout_1;
  i29 = xstart + 1;
  for (k = i29; k <= xend; k++) {
    xc = x->data[k - 1] - 1;
    idx = k - 2;
    exitg1 = false;
    while ((!exitg1) && (idx + 1 >= xstart)) {
      varargout_1 = ((cmp_tunableEnvironment[0].f1->data[xc] <
                      cmp_tunableEnvironment[0].f1->data[x->data[idx] - 1]) ||
                     ((cmp_tunableEnvironment[0].f1->data[xc] ==
                       cmp_tunableEnvironment[0].f1->data[x->data[idx] - 1]) &&
                      (cmp_tunableEnvironment[1].f1->data[xc] <
                       cmp_tunableEnvironment[1].f1->data[x->data[idx] - 1])));
      if (varargout_1) {
        x->data[idx + 1] = x->data[idx];
        idx--;
      } else {
        exitg1 = true;
      }
    }

    x->data[idx + 1] = xc + 1;
  }
}

/* End of code generation (insertionsort.c) */
