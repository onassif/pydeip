/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_DEIPFunc2_api.h
 *
 * Code generation for function '_coder_DEIPFunc2_api'
 *
 */

#ifndef _CODER_DEIPFUNC2_API_H
#define _CODER_DEIPFUNC2_API_H

/* Include files */
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include <stddef.h>
#include <stdlib.h>
#include "_coder_DEIPFunc2_api.h"

/* Type Definitions */
#ifndef struct_emxArray_int32_T
#define struct_emxArray_int32_T

struct emxArray_int32_T
{
  int32_T *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /*struct_emxArray_int32_T*/

#ifndef typedef_emxArray_int32_T
#define typedef_emxArray_int32_T

typedef struct emxArray_int32_T emxArray_int32_T;

#endif                                 /*typedef_emxArray_int32_T*/

#ifndef struct_emxArray_real_T
#define struct_emxArray_real_T

struct emxArray_real_T
{
  real_T *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /*struct_emxArray_real_T*/

#ifndef typedef_emxArray_real_T
#define typedef_emxArray_real_T

typedef struct emxArray_real_T emxArray_real_T;

#endif                                 /*typedef_emxArray_real_T*/

#ifndef typedef_coder_internal_sparse
#define typedef_coder_internal_sparse

typedef struct {
  emxArray_real_T *d;
  emxArray_int32_T *colidx;
  emxArray_int32_T *rowidx;
  int32_T m;
  int32_T n;
  int32_T maxnz;
} coder_internal_sparse;

#endif                                 /*typedef_coder_internal_sparse*/

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern emlrtContext emlrtContextGlobal;

/* Function Declarations */
extern void DEIPFunc2(emxArray_real_T *InterTypes, emxArray_real_T
                      *NodesOnElement, emxArray_real_T *RegionOnElement,
                      emxArray_real_T *Coordinates, real_T *numnp, real_T numel,
                      real_T nummat, real_T nen, real_T ndm, real_T usePBC,
                      real_T *numMPC, emxArray_real_T *MPCList, real_T *numEonB,
                      emxArray_real_T *numEonF, emxArray_real_T
                      *ElementsOnBoundary, real_T *numSI, emxArray_real_T
                      *ElementsOnFacet, coder_internal_sparse *ElementsOnNode,
                      coder_internal_sparse *ElementsOnNodeDup, emxArray_real_T *
                      ElementsOnNodeNum, real_T *numfac, emxArray_real_T
                      *ElementsOnNodeNum2, real_T *numinttype, emxArray_real_T
                      *FacetsOnElement, emxArray_real_T *FacetsOnElementInt,
                      emxArray_real_T *FacetsOnInterface, emxArray_real_T
                      *FacetsOnInterfaceNum, coder_internal_sparse *FacetsOnNode,
                      coder_internal_sparse *FacetsOnNodeCut,
                      coder_internal_sparse *FacetsOnNodeInt, emxArray_real_T
                      *FacetsOnNodeNum, emxArray_real_T *NodeCGDG,
                      coder_internal_sparse *NodeReg, emxArray_real_T
                      *NodesOnElementCG, emxArray_real_T *NodesOnElementDG,
                      emxArray_real_T *NodesOnInterface, real_T
                      *NodesOnInterfaceNum, real_T *numCL, emxArray_real_T
                      *NodesOnPBC, emxArray_real_T *NodesOnPBCnum,
                      emxArray_real_T *NodesOnLink, emxArray_real_T
                      *NodesOnLinknum, emxArray_real_T *numEonPBC,
                      emxArray_real_T *FacetsOnPBC, emxArray_real_T
                      *FacetsOnPBCNum, emxArray_real_T *FacetsOnIntMinusPBC,
                      emxArray_real_T *FacetsOnIntMinusPBCNum);
extern void DEIPFunc2_api(const mxArray *prhs[12], int32_T nlhs, const mxArray
  *plhs[41]);
extern void DEIPFunc2_atexit(void);
extern void DEIPFunc2_initialize(void);
extern void DEIPFunc2_terminate(void);
extern void DEIPFunc2_xil_shutdown(void);
extern void DEIPFunc2_xil_terminate(void);

#endif

/* End of code generation (_coder_DEIPFunc2_api.h) */
