/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * ismember.h
 *
 * Code generation for function 'ismember'
 *
 */

#ifndef ISMEMBER_H
#define ISMEMBER_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "DEIPFunc2_types.h"

/* Function Declarations */
extern int bsearchni(int k, const emxArray_real_T *x, const emxArray_real_T *s);

#endif

/* End of code generation (ismember.h) */
