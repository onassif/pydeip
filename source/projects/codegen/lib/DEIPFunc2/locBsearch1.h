/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * locBsearch1.h
 *
 * Code generation for function 'locBsearch1'
 *
 */

#ifndef LOCBSEARCH1_H
#define LOCBSEARCH1_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "DEIPFunc2_types.h"

/* Function Declarations */
extern void b_locBsearch(const emxArray_int32_T *x, int xi, int xstart, int xend,
  int *n, boolean_T *found);
extern void locBsearch(const emxArray_int32_T *x, double xi, int xstart, int
  xend, int *n, boolean_T *found);

#endif

/* End of code generation (locBsearch1.h) */
