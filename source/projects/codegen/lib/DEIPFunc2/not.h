/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * not.h
 *
 * Code generation for function 'not'
 *
 */

#ifndef NOT_H
#define NOT_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "DEIPFunc2_types.h"

/* Function Declarations */
extern void sparse_not(const emxArray_int32_T *S_colidx, const emxArray_int32_T *
  S_rowidx, emxArray_boolean_T *out_d, emxArray_int32_T *out_colidx,
  emxArray_int32_T *out_rowidx);

#endif

/* End of code generation (not.h) */
