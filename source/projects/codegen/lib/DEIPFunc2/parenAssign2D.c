/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * parenAssign2D.c
 *
 * Code generation for function 'parenAssign2D'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "DEIPFunc2.h"
#include "parenAssign2D.h"
#include "DEIPFunc2_emxutil.h"
#include "bigProduct.h"
#include "locBsearch1.h"
#include <string.h>

/* Function Definitions */
void b_realloc(coder_internal_sparse *this, int numAllocRequested, int ub1, int
               lb2, int ub2)
{
  emxArray_int32_T *rowidxt;
  int overflow;
  int numAlloc;
  emxArray_real_T *dt;
  emxInit_int32_T(&rowidxt, 1);
  overflow = rowidxt->size[0];
  rowidxt->size[0] = this->rowidx->size[0];
  emxEnsureCapacity_int32_T(rowidxt, overflow);
  numAlloc = this->rowidx->size[0];
  for (overflow = 0; overflow < numAlloc; overflow++) {
    rowidxt->data[overflow] = this->rowidx->data[overflow];
  }

  emxInit_real_T(&dt, 1);
  overflow = dt->size[0];
  dt->size[0] = this->d->size[0];
  emxEnsureCapacity_real_T(dt, overflow);
  numAlloc = this->d->size[0];
  for (overflow = 0; overflow < numAlloc; overflow++) {
    dt->data[overflow] = this->d->data[overflow];
  }

  bigProduct(this->m, this->n, &numAlloc, &overflow);
  if (overflow == 0) {
    numAlloc = this->m * this->n;
    if (numAllocRequested <= numAlloc) {
      numAlloc = numAllocRequested;
    }

    if (1 >= numAlloc) {
      numAlloc = 1;
    }
  } else if (1 >= numAllocRequested) {
    numAlloc = 1;
  } else {
    numAlloc = numAllocRequested;
  }

  overflow = this->rowidx->size[0];
  this->rowidx->size[0] = numAlloc;
  emxEnsureCapacity_int32_T(this->rowidx, overflow);
  for (overflow = 0; overflow < numAlloc; overflow++) {
    this->rowidx->data[overflow] = 0;
  }

  overflow = this->d->size[0];
  this->d->size[0] = numAlloc;
  emxEnsureCapacity_real_T(this->d, overflow);
  for (overflow = 0; overflow < numAlloc; overflow++) {
    this->d->data[overflow] = 0.0;
  }

  this->maxnz = numAlloc;
  for (numAlloc = 0; numAlloc < ub1; numAlloc++) {
    this->rowidx->data[numAlloc] = rowidxt->data[numAlloc];
    this->d->data[numAlloc] = dt->data[numAlloc];
  }

  for (numAlloc = lb2; numAlloc <= ub2; numAlloc++) {
    this->rowidx->data[numAlloc] = rowidxt->data[numAlloc - 1];
    this->d->data[numAlloc] = dt->data[numAlloc - 1];
  }

  emxFree_real_T(&dt);
  emxFree_int32_T(&rowidxt);
}

void shiftRowidxAndData(coder_internal_sparse *this, int outstart, int instart,
  int nelem)
{
  if (nelem > 0) {
    memmove((void *)&this->rowidx->data[outstart - 1], (void *)&this->
            rowidx->data[instart - 1], (size_t)nelem * sizeof(int));
    memmove((void *)&this->d->data[outstart - 1], (void *)&this->d->data[instart
            - 1], (size_t)nelem * sizeof(double));
  }
}

void sparse_parenAssign2D(coder_internal_sparse *this, double rhs, double r,
  double c)
{
  int vidx;
  boolean_T found;
  double thisv;
  int i32;
  int i33;
  b_locBsearch(this->rowidx, (int)r, this->colidx->data[(int)c - 1],
               this->colidx->data[(int)c], &vidx, &found);
  if (found) {
    thisv = this->d->data[vidx - 1];
  } else {
    thisv = 0.0;
  }

  if ((thisv == 0.0) && (rhs == 0.0)) {
  } else if ((thisv != 0.0) && (rhs != 0.0)) {
    this->d->data[vidx - 1] = rhs;
  } else if (thisv == 0.0) {
    if (this->colidx->data[this->colidx->size[0] - 1] - 1 == this->maxnz) {
      i32 = this->colidx->data[this->colidx->size[0] - 1] + 9;
      i33 = this->colidx->data[this->colidx->size[0] - 1] - 1;
      b_realloc(this, i32, vidx, vidx + 1, i33);
      this->rowidx->data[vidx] = (int)r;
      this->d->data[vidx] = rhs;
    } else {
      i32 = (this->colidx->data[this->colidx->size[0] - 1] - vidx) - 1;
      shiftRowidxAndData(this, vidx + 2, vidx + 1, i32);
      this->d->data[vidx] = rhs;
      this->rowidx->data[vidx] = (int)r;
    }

    i32 = (int)c + 1;
    i33 = this->n + 1;
    for (vidx = i32; vidx <= i33; vidx++) {
      this->colidx->data[vidx - 1]++;
    }
  } else {
    i32 = (this->colidx->data[this->colidx->size[0] - 1] - vidx) - 1;
    shiftRowidxAndData(this, vidx, vidx + 1, i32);
    i32 = (int)c + 1;
    i33 = this->n + 1;
    for (vidx = i32; vidx <= i33; vidx++) {
      this->colidx->data[vidx - 1]--;
    }
  }
}

/* End of code generation (parenAssign2D.c) */
