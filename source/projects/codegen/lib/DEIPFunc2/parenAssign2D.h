/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * parenAssign2D.h
 *
 * Code generation for function 'parenAssign2D'
 *
 */

#ifndef PARENASSIGN2D_H
#define PARENASSIGN2D_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "DEIPFunc2_types.h"

/* Function Declarations */
extern void b_realloc(coder_internal_sparse *this, int numAllocRequested, int
                      ub1, int lb2, int ub2);
extern void shiftRowidxAndData(coder_internal_sparse *this, int outstart, int
  instart, int nelem);
extern void sparse_parenAssign2D(coder_internal_sparse *this, double rhs, double
  r, double c);

#endif

/* End of code generation (parenAssign2D.h) */
