/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sign.c
 *
 * Code generation for function 'sign'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "DEIPFunc2.h"
#include "sign.h"

/* Function Definitions */
void b_sign(coder_internal_sparse_2 *x)
{
  int nx;
  int k;
  double val;
  int c;
  int ridx;
  int currRowIdx;
  nx = x->d->size[0];
  for (k = 0; k < nx; k++) {
    val = x->d->data[k];
    if (x->d->data[k] < 0.0) {
      val = -1.0;
    } else if (x->d->data[k] > 0.0) {
      val = 1.0;
    } else {
      if (x->d->data[k] == 0.0) {
        val = 0.0;
      }
    }

    x->d->data[k] = val;
  }

  nx = 1;
  k = x->colidx->size[0];
  for (c = 0; c <= k - 2; c++) {
    ridx = x->colidx->data[c];
    x->colidx->data[c] = nx;
    while (ridx < x->colidx->data[c + 1]) {
      currRowIdx = x->rowidx->data[ridx - 1];
      val = x->d->data[ridx - 1];
      ridx++;
      if (val != 0.0) {
        x->d->data[nx - 1] = val;
        x->rowidx->data[nx - 1] = currRowIdx;
        nx++;
      }
    }
  }

  x->colidx->data[x->colidx->size[0] - 1] = nx;
}

/* End of code generation (sign.c) */
