/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sparse.c
 *
 * Code generation for function 'sparse'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "DEIPFunc2.h"
#include "sparse.h"
#include "DEIPFunc2_emxutil.h"
#include "fillIn.h"
#include "sparse1.h"

/* Function Definitions */
void b_sparse(const emxArray_real_T *varargin_1, const emxArray_real_T
              *varargin_2, const emxArray_real_T *varargin_3,
              coder_internal_sparse *y)
{
  emxArray_int32_T *ridxInt;
  int nc;
  int ns;
  int i1;
  int k;
  emxArray_int32_T *cidxInt;
  emxArray_int32_T *sortedIndices;
  int thism;
  emxInit_int32_T(&ridxInt, 1);
  nc = varargin_2->size[1];
  ns = varargin_1->size[1];
  i1 = ridxInt->size[0];
  ridxInt->size[0] = varargin_1->size[1];
  emxEnsureCapacity_int32_T(ridxInt, i1);
  for (k = 0; k < ns; k++) {
    ridxInt->data[k] = (int)varargin_1->data[k];
  }

  emxInit_int32_T(&cidxInt, 1);
  ns = varargin_2->size[1];
  i1 = cidxInt->size[0];
  cidxInt->size[0] = varargin_2->size[1];
  emxEnsureCapacity_int32_T(cidxInt, i1);
  for (k = 0; k < ns; k++) {
    cidxInt->data[k] = (int)varargin_2->data[k];
  }

  emxInit_int32_T(&sortedIndices, 1);
  i1 = sortedIndices->size[0];
  sortedIndices->size[0] = varargin_2->size[1];
  emxEnsureCapacity_int32_T(sortedIndices, i1);
  for (k = 0; k < nc; k++) {
    sortedIndices->data[k] = k + 1;
  }

  locSortrows(sortedIndices, cidxInt, ridxInt);
  if ((ridxInt->size[0] == 0) || (cidxInt->size[0] == 0)) {
    thism = 0;
    y->n = 0;
  } else {
    ns = ridxInt->size[0];
    thism = ridxInt->data[0];
    for (k = 2; k <= ns; k++) {
      if (thism < ridxInt->data[k - 1]) {
        thism = ridxInt->data[k - 1];
      }
    }

    y->n = cidxInt->data[cidxInt->size[0] - 1];
  }

  y->m = thism;
  if (varargin_2->size[1] >= 1) {
    ns = varargin_2->size[1];
  } else {
    ns = 1;
  }

  i1 = y->d->size[0];
  y->d->size[0] = ns;
  emxEnsureCapacity_real_T(y->d, i1);
  for (i1 = 0; i1 < ns; i1++) {
    y->d->data[i1] = 0.0;
  }

  y->maxnz = ns;
  i1 = y->colidx->size[0];
  y->colidx->size[0] = y->n + 1;
  emxEnsureCapacity_int32_T(y->colidx, i1);
  y->colidx->data[0] = 1;
  i1 = y->rowidx->size[0];
  y->rowidx->size[0] = ns;
  emxEnsureCapacity_int32_T(y->rowidx, i1);
  for (i1 = 0; i1 < ns; i1++) {
    y->rowidx->data[i1] = 0;
  }

  ns = 0;
  i1 = y->n;
  for (thism = 0; thism < i1; thism++) {
    k = 1 + thism;
    while ((ns + 1 <= nc) && (cidxInt->data[ns] == k)) {
      y->rowidx->data[ns] = ridxInt->data[ns];
      ns++;
    }

    y->colidx->data[k] = ns + 1;
  }

  emxFree_int32_T(&cidxInt);
  emxFree_int32_T(&ridxInt);
  for (k = 0; k < nc; k++) {
    y->d->data[k] = varargin_3->data[sortedIndices->data[k] - 1];
  }

  emxFree_int32_T(&sortedIndices);
  sparse_fillIn(y);
}

void c_sparse(const emxArray_real_T *varargin_1, emxArray_real_T *y_d,
              emxArray_int32_T *y_colidx, emxArray_int32_T *y_rowidx, int *y_m,
              int *y_n, int *y_maxnz)
{
  int nInt;
  int i6;
  int loop_ub;
  nInt = varargin_1->size[1];
  *y_m = varargin_1->size[0];
  *y_n = varargin_1->size[1];
  i6 = y_d->size[0];
  y_d->size[0] = 1;
  emxEnsureCapacity_real_T(y_d, i6);
  y_d->data[0] = 0.0;
  i6 = y_colidx->size[0];
  y_colidx->size[0] = varargin_1->size[1] + 1;
  emxEnsureCapacity_int32_T(y_colidx, i6);
  loop_ub = varargin_1->size[1];
  for (i6 = 0; i6 <= loop_ub; i6++) {
    y_colidx->data[i6] = 0;
  }

  y_colidx->data[0] = 1;
  i6 = y_rowidx->size[0];
  y_rowidx->size[0] = 1;
  emxEnsureCapacity_int32_T(y_rowidx, i6);
  y_rowidx->data[0] = 1;
  for (loop_ub = 0; loop_ub < nInt; loop_ub++) {
    y_colidx->data[1 + loop_ub] = 1;
  }

  *y_maxnz = 1;
}

void d_sparse(boolean_T varargin_1, emxArray_boolean_T *y_d, emxArray_int32_T
              *y_colidx, emxArray_int32_T *y_rowidx)
{
  int i14;
  i14 = y_d->size[0];
  y_d->size[0] = 1;
  emxEnsureCapacity_boolean_T(y_d, i14);
  y_d->data[0] = false;
  i14 = y_colidx->size[0];
  y_colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(y_colidx, i14);
  y_colidx->data[0] = 0;
  y_colidx->data[1] = 0;
  y_colidx->data[0] = 1;
  i14 = y_rowidx->size[0];
  y_rowidx->size[0] = 1;
  emxEnsureCapacity_int32_T(y_rowidx, i14);
  y_rowidx->data[0] = 1;
  y_colidx->data[1] = 1;
  if (varargin_1) {
    y_rowidx->data[0] = 1;
    y_d->data[0] = true;
    y_colidx->data[1] = 2;
  }
}

void sparse(const emxArray_real_T *varargin_1, const emxArray_real_T *varargin_2,
            const emxArray_real_T *varargin_3, coder_internal_sparse *y)
{
  emxArray_int32_T *ridxInt;
  int nc;
  int ns;
  int i0;
  int k;
  emxArray_int32_T *cidxInt;
  emxArray_int32_T *sortedIndices;
  int thism;
  emxInit_int32_T(&ridxInt, 1);
  nc = varargin_2->size[0] * varargin_2->size[1];
  ns = varargin_1->size[0] * varargin_1->size[1];
  i0 = ridxInt->size[0];
  ridxInt->size[0] = ns;
  emxEnsureCapacity_int32_T(ridxInt, i0);
  for (k = 0; k < ns; k++) {
    ridxInt->data[k] = (int)varargin_1->data[k];
  }

  emxInit_int32_T(&cidxInt, 1);
  ns = varargin_2->size[0] * varargin_2->size[1];
  i0 = cidxInt->size[0];
  cidxInt->size[0] = ns;
  emxEnsureCapacity_int32_T(cidxInt, i0);
  for (k = 0; k < ns; k++) {
    cidxInt->data[k] = (int)varargin_2->data[k];
  }

  emxInit_int32_T(&sortedIndices, 1);
  i0 = sortedIndices->size[0];
  sortedIndices->size[0] = nc;
  emxEnsureCapacity_int32_T(sortedIndices, i0);
  for (k = 0; k < nc; k++) {
    sortedIndices->data[k] = k + 1;
  }

  locSortrows(sortedIndices, cidxInt, ridxInt);
  if ((ridxInt->size[0] == 0) || (cidxInt->size[0] == 0)) {
    thism = 0;
    y->n = 0;
  } else {
    ns = ridxInt->size[0];
    thism = ridxInt->data[0];
    for (k = 2; k <= ns; k++) {
      if (thism < ridxInt->data[k - 1]) {
        thism = ridxInt->data[k - 1];
      }
    }

    y->n = cidxInt->data[cidxInt->size[0] - 1];
  }

  y->m = thism;
  if (nc >= 1) {
    ns = nc;
  } else {
    ns = 1;
  }

  i0 = y->d->size[0];
  y->d->size[0] = ns;
  emxEnsureCapacity_real_T(y->d, i0);
  for (i0 = 0; i0 < ns; i0++) {
    y->d->data[i0] = 0.0;
  }

  y->maxnz = ns;
  i0 = y->colidx->size[0];
  y->colidx->size[0] = y->n + 1;
  emxEnsureCapacity_int32_T(y->colidx, i0);
  y->colidx->data[0] = 1;
  i0 = y->rowidx->size[0];
  y->rowidx->size[0] = ns;
  emxEnsureCapacity_int32_T(y->rowidx, i0);
  for (i0 = 0; i0 < ns; i0++) {
    y->rowidx->data[i0] = 0;
  }

  ns = 0;
  i0 = y->n;
  for (thism = 0; thism < i0; thism++) {
    k = 1 + thism;
    while ((ns + 1 <= nc) && (cidxInt->data[ns] == k)) {
      y->rowidx->data[ns] = ridxInt->data[ns];
      ns++;
    }

    y->colidx->data[k] = ns + 1;
  }

  emxFree_int32_T(&cidxInt);
  emxFree_int32_T(&ridxInt);
  for (k = 0; k < nc; k++) {
    y->d->data[k] = varargin_3->data[sortedIndices->data[k] - 1];
  }

  emxFree_int32_T(&sortedIndices);
  sparse_fillIn(y);
}

/* End of code generation (sparse.c) */
