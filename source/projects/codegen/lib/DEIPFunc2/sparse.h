/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sparse.h
 *
 * Code generation for function 'sparse'
 *
 */

#ifndef SPARSE_H
#define SPARSE_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "DEIPFunc2_types.h"

/* Function Declarations */
extern void b_sparse(const emxArray_real_T *varargin_1, const emxArray_real_T
                     *varargin_2, const emxArray_real_T *varargin_3,
                     coder_internal_sparse *y);
extern void c_sparse(const emxArray_real_T *varargin_1, emxArray_real_T *y_d,
                     emxArray_int32_T *y_colidx, emxArray_int32_T *y_rowidx, int
                     *y_m, int *y_n, int *y_maxnz);
extern void d_sparse(boolean_T varargin_1, emxArray_boolean_T *y_d,
                     emxArray_int32_T *y_colidx, emxArray_int32_T *y_rowidx);
extern void sparse(const emxArray_real_T *varargin_1, const emxArray_real_T
                   *varargin_2, const emxArray_real_T *varargin_3,
                   coder_internal_sparse *y);

#endif

/* End of code generation (sparse.h) */
