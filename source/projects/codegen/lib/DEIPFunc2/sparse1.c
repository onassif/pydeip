/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sparse1.c
 *
 * Code generation for function 'sparse1'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "DEIPFunc2.h"
#include "sparse1.h"
#include "parenAssign2D.h"
#include "locBsearch1.h"
#include "fillIn.h"
#include "DEIPFunc2_emxutil.h"
#include "sparse.h"
#include "binOp.h"
#include "introsort.h"

/* Function Declarations */
static void b_sparse_sparse(int m, int nzmaxval, emxArray_boolean_T *this_d,
  emxArray_int32_T *this_colidx, emxArray_int32_T *this_rowidx, int *this_m);
static int div_s32(int numerator, int denominator);
static void permuteVector(const emxArray_int32_T *idx, emxArray_int32_T *y);

/* Function Definitions */
static void b_sparse_sparse(int m, int nzmaxval, emxArray_boolean_T *this_d,
  emxArray_int32_T *this_colidx, emxArray_int32_T *this_rowidx, int *this_m)
{
  int numalloc;
  int i11;
  if (nzmaxval >= 1) {
    numalloc = nzmaxval;
  } else {
    numalloc = 1;
  }

  i11 = this_d->size[0];
  this_d->size[0] = numalloc;
  emxEnsureCapacity_boolean_T(this_d, i11);
  for (i11 = 0; i11 < numalloc; i11++) {
    this_d->data[i11] = false;
  }

  i11 = this_colidx->size[0];
  this_colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(this_colidx, i11);
  this_colidx->data[0] = 1;
  i11 = this_rowidx->size[0];
  this_rowidx->size[0] = numalloc;
  emxEnsureCapacity_int32_T(this_rowidx, i11);
  for (i11 = 0; i11 < numalloc; i11++) {
    this_rowidx->data[i11] = 0;
  }

  this_colidx->data[0] = 1;
  this_colidx->data[1] = 1;
  *this_m = m;
}

static int div_s32(int numerator, int denominator)
{
  int quotient;
  unsigned int b_numerator;
  unsigned int b_denominator;
  unsigned int tempAbsQuotient;
  if (denominator == 0) {
    if (numerator >= 0) {
      quotient = MAX_int32_T;
    } else {
      quotient = MIN_int32_T;
    }
  } else {
    if (numerator < 0) {
      b_numerator = ~(unsigned int)numerator + 1U;
    } else {
      b_numerator = (unsigned int)numerator;
    }

    if (denominator < 0) {
      b_denominator = ~(unsigned int)denominator + 1U;
    } else {
      b_denominator = (unsigned int)denominator;
    }

    tempAbsQuotient = b_numerator / b_denominator;
    if ((numerator < 0) != (denominator < 0)) {
      quotient = -(int)tempAbsQuotient;
    } else {
      quotient = (int)tempAbsQuotient;
    }
  }

  return quotient;
}

static void permuteVector(const emxArray_int32_T *idx, emxArray_int32_T *y)
{
  emxArray_int32_T *t;
  int ny;
  int k;
  int loop_ub;
  emxInit_int32_T(&t, 1);
  ny = y->size[0];
  k = t->size[0];
  t->size[0] = y->size[0];
  emxEnsureCapacity_int32_T(t, k);
  loop_ub = y->size[0];
  for (k = 0; k < loop_ub; k++) {
    t->data[k] = y->data[k];
  }

  for (k = 0; k < ny; k++) {
    y->data[k] = t->data[idx->data[k] - 1];
  }

  emxFree_int32_T(&t);
}

void b_sparse_and(const emxArray_boolean_T *a_d, const emxArray_int32_T
                  *a_colidx, const emxArray_boolean_T *b_d, const
                  emxArray_int32_T *b_colidx, const emxArray_int32_T *b_rowidx,
                  coder_internal_sparse_4 *s)
{
  boolean_T tunableEnvironment_idx_0;
  int nzs;
  int loop_ub;
  emxArray_boolean_T *tmpd;
  int i15;
  tunableEnvironment_idx_0 = ((a_colidx->data[a_colidx->size[0] - 1] - 1 > 0) &&
    a_d->data[0]);
  nzs = b_colidx->data[b_colidx->size[0] - 1];
  if (1 > b_colidx->data[b_colidx->size[0] - 1] - 1) {
    loop_ub = 0;
  } else {
    loop_ub = b_colidx->data[b_colidx->size[0] - 1] - 1;
  }

  emxInit_boolean_T(&tmpd, 1);
  i15 = tmpd->size[0];
  tmpd->size[0] = loop_ub;
  emxEnsureCapacity_boolean_T(tmpd, i15);
  for (i15 = 0; i15 < loop_ub; i15++) {
    tmpd->data[i15] = (tunableEnvironment_idx_0 && b_d->data[i15]);
  }

  c_sparse_sparse(b_colidx->data[b_colidx->size[0] - 1] - 1, s->d, s->colidx,
                  s->rowidx);
  if (1 > b_colidx->data[b_colidx->size[0] - 1] - 1) {
    loop_ub = 1;
  } else {
    loop_ub = b_colidx->data[b_colidx->size[0] - 1];
  }

  for (i15 = 0; i15 <= loop_ub - 2; i15++) {
    s->rowidx->data[i15] = b_rowidx->data[i15];
  }

  i15 = s->colidx->size[0];
  s->colidx->size[0] = b_colidx->size[0];
  emxEnsureCapacity_int32_T(s->colidx, i15);
  loop_ub = b_colidx->size[0];
  for (i15 = 0; i15 < loop_ub; i15++) {
    s->colidx->data[i15] = b_colidx->data[i15];
  }

  for (loop_ub = 0; loop_ub <= nzs - 2; loop_ub++) {
    s->d->data[loop_ub] = tmpd->data[loop_ub];
  }

  emxFree_boolean_T(&tmpd);
  c_sparse_fillIn(s);
}

void b_sparse_eq(const emxArray_real_T *a_d, const emxArray_int32_T *a_colidx,
                 const emxArray_real_T *b_d, const emxArray_int32_T *b_colidx,
                 const emxArray_int32_T *b_rowidx, coder_internal_sparse_4 *s)
{
  double tunableEnvironment_idx_0;
  boolean_T S;
  int nzs;
  int i17;
  int loop_ub;
  emxArray_boolean_T *tmpd;
  tunableEnvironment_idx_0 = getScalar(a_d, a_colidx);
  if (!(tunableEnvironment_idx_0 == 0.0)) {
    nzs = b_colidx->data[b_colidx->size[0] - 1];
    if (1 > b_colidx->data[b_colidx->size[0] - 1] - 1) {
      loop_ub = 0;
    } else {
      loop_ub = b_colidx->data[b_colidx->size[0] - 1] - 1;
    }

    emxInit_boolean_T(&tmpd, 1);
    i17 = tmpd->size[0];
    tmpd->size[0] = loop_ub;
    emxEnsureCapacity_boolean_T(tmpd, i17);
    for (i17 = 0; i17 < loop_ub; i17++) {
      tmpd->data[i17] = (tunableEnvironment_idx_0 == b_d->data[i17]);
    }

    c_sparse_sparse(b_colidx->data[b_colidx->size[0] - 1] - 1, s->d, s->colidx,
                    s->rowidx);
    if (1 > b_colidx->data[b_colidx->size[0] - 1] - 1) {
      loop_ub = 1;
    } else {
      loop_ub = b_colidx->data[b_colidx->size[0] - 1];
    }

    for (i17 = 0; i17 <= loop_ub - 2; i17++) {
      s->rowidx->data[i17] = b_rowidx->data[i17];
    }

    i17 = s->colidx->size[0];
    s->colidx->size[0] = b_colidx->size[0];
    emxEnsureCapacity_int32_T(s->colidx, i17);
    loop_ub = b_colidx->size[0];
    for (i17 = 0; i17 < loop_ub; i17++) {
      s->colidx->data[i17] = b_colidx->data[i17];
    }

    for (loop_ub = 0; loop_ub <= nzs - 2; loop_ub++) {
      s->d->data[loop_ub] = tmpd->data[loop_ub];
    }

    emxFree_boolean_T(&tmpd);
    c_sparse_fillIn(s);
  } else {
    S = true;
    i17 = b_colidx->data[0];
    loop_ub = b_colidx->data[1] - 1;
    for (nzs = i17; nzs <= loop_ub; nzs++) {
      S = (tunableEnvironment_idx_0 == b_d->data[0]);
    }

    d_sparse(S, s->d, s->colidx, s->rowidx);
  }
}

void b_sparse_full(const emxArray_real_T *this_d, const emxArray_int32_T
                   *this_colidx, const emxArray_int32_T *this_rowidx, int this_m,
                   emxArray_real_T *y)
{
  int i9;
  int cend;
  int idx;
  i9 = y->size[0] * y->size[1];
  y->size[0] = this_m;
  y->size[1] = 1;
  emxEnsureCapacity_real_T(y, i9);
  for (i9 = 0; i9 < this_m; i9++) {
    y->data[i9] = 0.0;
  }

  cend = this_colidx->data[1] - 1;
  i9 = this_colidx->data[0];
  for (idx = i9; idx <= cend; idx++) {
    y->data[this_rowidx->data[idx - 1] - 1] = this_d->data[idx - 1];
  }
}

void b_sparse_gt(const emxArray_real_T *a_d, const emxArray_int32_T *a_colidx,
                 emxArray_boolean_T *s_d, emxArray_int32_T *s_colidx,
                 emxArray_int32_T *s_rowidx)
{
  d_sparse(getScalar(a_d, a_colidx) > 0.0, s_d, s_colidx, s_rowidx);
}

void b_sparse_parenAssign(coder_internal_sparse *this, const emxArray_real_T
  *rhs, const emxArray_real_T *varargin_1)
{
  int nidx;
  int k;
  int i34;
  int vk;
  nidx = varargin_1->size[0];
  for (k = 0; k < nidx; k++) {
    i34 = this->m;
    vk = div_s32((int)varargin_1->data[k] - 1, i34);
    sparse_parenAssign2D(this, rhs->data[k], (int)varargin_1->data[k] - vk * i34,
                         vk + 1);
  }
}

void b_sparse_parenReference(const emxArray_real_T *this_d, const
  emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx, const
  emxArray_real_T *varargin_1, double varargin_2, emxArray_real_T *s_d,
  emxArray_int32_T *s_colidx, emxArray_int32_T *s_rowidx, int *s_m)
{
  int sm;
  int i7;
  int colNnz;
  int k;
  int ridx;
  int idx;
  boolean_T found;
  int i8;
  sm = varargin_1->size[1];
  s_d->size[0] = 0;
  s_rowidx->size[0] = 0;
  i7 = s_colidx->size[0];
  s_colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(s_colidx, i7);
  s_colidx->data[0] = 0;
  s_colidx->data[1] = 0;
  s_colidx->data[0] = 1;
  colNnz = 1;
  k = 0;
  for (ridx = 0; ridx < sm; ridx++) {
    b_locBsearch(this_rowidx, (int)varargin_1->data[ridx], this_colidx->data
                 [(int)varargin_2 - 1], this_colidx->data[(int)varargin_2], &idx,
                 &found);
    if (found) {
      i7 = s_d->size[0];
      i8 = s_d->size[0];
      s_d->size[0] = i7 + 1;
      emxEnsureCapacity_real_T(s_d, i8);
      s_d->data[i7] = this_d->data[idx - 1];
      i7 = s_rowidx->size[0];
      i8 = s_rowidx->size[0];
      s_rowidx->size[0] = i7 + 1;
      emxEnsureCapacity_int32_T(s_rowidx, i8);
      s_rowidx->data[i7] = ridx + 1;
      s_d->data[k] = this_d->data[idx - 1];
      s_rowidx->data[k] = ridx + 1;
      k++;
      colNnz++;
    }
  }

  s_colidx->data[1] = colNnz;
  if (s_colidx->data[1] - 1 == 0) {
    i7 = s_rowidx->size[0];
    s_rowidx->size[0] = 1;
    emxEnsureCapacity_int32_T(s_rowidx, i7);
    s_rowidx->data[0] = 1;
    i7 = s_d->size[0];
    s_d->size[0] = 1;
    emxEnsureCapacity_real_T(s_d, i7);
    s_d->data[0] = 0.0;
  }

  sm = varargin_1->size[1];
  *s_m = sm;
}

void c_sparse_eq(double a, const emxArray_real_T *b_d, const emxArray_int32_T
                 *b_colidx, const emxArray_int32_T *b_rowidx, int b_m,
                 coder_internal_sparse_1 *s)
{
  int nzs;
  int loop_ub;
  emxArray_boolean_T *tmpd;
  int i20;
  nzs = b_colidx->data[b_colidx->size[0] - 1];
  if (1 > b_colidx->data[b_colidx->size[0] - 1] - 1) {
    loop_ub = 0;
  } else {
    loop_ub = b_colidx->data[b_colidx->size[0] - 1] - 1;
  }

  emxInit_boolean_T(&tmpd, 1);
  i20 = tmpd->size[0];
  tmpd->size[0] = loop_ub;
  emxEnsureCapacity_boolean_T(tmpd, i20);
  for (i20 = 0; i20 < loop_ub; i20++) {
    tmpd->data[i20] = (a == b_d->data[i20]);
  }

  b_sparse_sparse(b_m, b_colidx->data[b_colidx->size[0] - 1] - 1, s->d,
                  s->colidx, s->rowidx, &s->m);
  if (1 > b_colidx->data[b_colidx->size[0] - 1] - 1) {
    loop_ub = 1;
  } else {
    loop_ub = b_colidx->data[b_colidx->size[0] - 1];
  }

  for (i20 = 0; i20 <= loop_ub - 2; i20++) {
    s->rowidx->data[i20] = b_rowidx->data[i20];
  }

  i20 = s->colidx->size[0];
  s->colidx->size[0] = b_colidx->size[0];
  emxEnsureCapacity_int32_T(s->colidx, i20);
  loop_ub = b_colidx->size[0];
  for (i20 = 0; i20 < loop_ub; i20++) {
    s->colidx->data[i20] = b_colidx->data[i20];
  }

  for (loop_ub = 0; loop_ub <= nzs - 2; loop_ub++) {
    s->d->data[loop_ub] = tmpd->data[loop_ub];
  }

  emxFree_boolean_T(&tmpd);
  b_sparse_fillIn(s);
}

boolean_T c_sparse_full(const emxArray_boolean_T *this_d, const emxArray_int32_T
  *this_colidx)
{
  boolean_T y;
  int cend;
  int i18;
  int idx;
  y = false;
  cend = this_colidx->data[1] - 1;
  i18 = this_colidx->data[0];
  for (idx = i18; idx <= cend; idx++) {
    y = this_d->data[idx - 1];
  }

  return y;
}

void c_sparse_parenAssign(coder_internal_sparse *this, double rhs, const
  emxArray_real_T *varargin_1, double varargin_2)
{
  int sm;
  int ridx;
  double nt;
  int vidx;
  boolean_T found;
  double thisv;
  int i39;
  int i40;
  sm = varargin_1->size[0];
  for (ridx = 0; ridx < sm; ridx++) {
    nt = varargin_1->data[ridx];
    b_locBsearch(this->rowidx, (int)nt, this->colidx->data[(int)varargin_2 - 1],
                 this->colidx->data[(int)varargin_2], &vidx, &found);
    if (found) {
      thisv = this->d->data[vidx - 1];
    } else {
      thisv = 0.0;
    }

    if ((thisv == 0.0) && (rhs == 0.0)) {
    } else if ((thisv != 0.0) && (rhs != 0.0)) {
      this->d->data[vidx - 1] = rhs;
    } else if (thisv == 0.0) {
      if (this->colidx->data[this->colidx->size[0] - 1] - 1 == this->maxnz) {
        i39 = this->colidx->data[this->colidx->size[0] - 1] + 9;
        i40 = this->colidx->data[this->colidx->size[0] - 1] - 1;
        b_realloc(this, i39, vidx, vidx + 1, i40);
        this->rowidx->data[vidx] = (int)nt;
        this->d->data[vidx] = rhs;
      } else {
        i39 = (this->colidx->data[this->colidx->size[0] - 1] - vidx) - 1;
        shiftRowidxAndData(this, vidx + 2, vidx + 1, i39);
        this->d->data[vidx] = rhs;
        this->rowidx->data[vidx] = (int)nt;
      }

      i39 = (int)varargin_2 + 1;
      i40 = this->n + 1;
      for (vidx = i39; vidx <= i40; vidx++) {
        this->colidx->data[vidx - 1]++;
      }
    } else {
      i39 = (this->colidx->data[this->colidx->size[0] - 1] - vidx) - 1;
      shiftRowidxAndData(this, vidx, vidx + 1, i39);
      i39 = (int)varargin_2 + 1;
      i40 = this->n + 1;
      for (vidx = i39; vidx <= i40; vidx++) {
        this->colidx->data[vidx - 1]--;
      }
    }
  }
}

void c_sparse_parenReference(const emxArray_real_T *this_d, const
  emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx, int this_m,
  double varargin_2, emxArray_real_T *s_d, emxArray_int32_T *s_colidx,
  emxArray_int32_T *s_rowidx, int *s_m)
{
  int nd_tmp;
  int b_nd_tmp;
  int nd;
  int b_s_m;
  int outIdx;
  int colstart;
  nd_tmp = (int)varargin_2;
  b_nd_tmp = nd_tmp - 1;
  nd = this_colidx->data[nd_tmp] - this_colidx->data[b_nd_tmp];
  sparse_sparse(this_m, nd, s_d, s_colidx, s_rowidx, &b_s_m);
  if (nd != 0) {
    outIdx = 0;
    colstart = this_colidx->data[b_nd_tmp] - 2;
    nd = this_colidx->data[nd_tmp] - this_colidx->data[b_nd_tmp];
    for (nd_tmp = 0; nd_tmp < nd; nd_tmp++) {
      b_nd_tmp = (colstart + nd_tmp) + 1;
      s_d->data[outIdx] = this_d->data[b_nd_tmp];
      s_rowidx->data[outIdx] = this_rowidx->data[b_nd_tmp];
      outIdx++;
    }

    s_colidx->data[1] = s_colidx->data[0] + nd;
  }

  *s_m = b_s_m;
}

void c_sparse_sparse(int nzmaxval, emxArray_boolean_T *this_d, emxArray_int32_T *
                     this_colidx, emxArray_int32_T *this_rowidx)
{
  int numalloc;
  int i16;
  if (nzmaxval >= 1) {
    numalloc = nzmaxval;
  } else {
    numalloc = 1;
  }

  i16 = this_d->size[0];
  this_d->size[0] = numalloc;
  emxEnsureCapacity_boolean_T(this_d, i16);
  for (i16 = 0; i16 < numalloc; i16++) {
    this_d->data[i16] = false;
  }

  i16 = this_colidx->size[0];
  this_colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(this_colidx, i16);
  this_colidx->data[0] = 1;
  i16 = this_rowidx->size[0];
  this_rowidx->size[0] = numalloc;
  emxEnsureCapacity_int32_T(this_rowidx, i16);
  for (i16 = 0; i16 < numalloc; i16++) {
    this_rowidx->data[i16] = 0;
  }

  this_colidx->data[0] = 1;
  this_colidx->data[1] = 1;
}

void d_sparse_parenReference(const emxArray_real_T *this_d, const
  emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx, double
  varargin_1, double varargin_2, emxArray_real_T *s_d, emxArray_int32_T
  *s_colidx, emxArray_int32_T *s_rowidx)
{
  int i13;
  int idx;
  boolean_T found;
  s_d->size[0] = 0;
  s_rowidx->size[0] = 0;
  i13 = s_colidx->size[0];
  s_colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(s_colidx, i13);
  s_colidx->data[0] = 0;
  s_colidx->data[1] = 0;
  s_colidx->data[0] = 1;
  s_colidx->data[1] = 1;
  b_locBsearch(this_rowidx, (int)varargin_1, this_colidx->data[(int)varargin_2 -
               1], this_colidx->data[(int)varargin_2], &idx, &found);
  if (found) {
    i13 = s_d->size[0];
    s_d->size[0] = 1;
    emxEnsureCapacity_real_T(s_d, i13);
    i13 = s_rowidx->size[0];
    s_rowidx->size[0] = 1;
    emxEnsureCapacity_int32_T(s_rowidx, i13);
    s_d->data[0] = this_d->data[idx - 1];
    s_rowidx->data[0] = 1;
    s_colidx->data[1] = 2;
  }

  if (s_colidx->data[1] - 1 == 0) {
    i13 = s_rowidx->size[0];
    s_rowidx->size[0] = 1;
    emxEnsureCapacity_int32_T(s_rowidx, i13);
    s_rowidx->data[0] = 1;
    i13 = s_d->size[0];
    s_d->size[0] = 1;
    emxEnsureCapacity_real_T(s_d, i13);
    s_d->data[0] = 0.0;
  }
}

void locSortrows(emxArray_int32_T *idx, emxArray_int32_T *a, emxArray_int32_T *b)
{
  cell_wrap_1 this_tunableEnvironment[2];
  int i28;
  int loop_ub;
  emxInitMatrix_cell_wrap_11(this_tunableEnvironment);
  i28 = this_tunableEnvironment[0].f1->size[0];
  this_tunableEnvironment[0].f1->size[0] = a->size[0];
  emxEnsureCapacity_int32_T(this_tunableEnvironment[0].f1, i28);
  loop_ub = a->size[0];
  for (i28 = 0; i28 < loop_ub; i28++) {
    this_tunableEnvironment[0].f1->data[i28] = a->data[i28];
  }

  i28 = this_tunableEnvironment[1].f1->size[0];
  this_tunableEnvironment[1].f1->size[0] = b->size[0];
  emxEnsureCapacity_int32_T(this_tunableEnvironment[1].f1, i28);
  loop_ub = b->size[0];
  for (i28 = 0; i28 < loop_ub; i28++) {
    this_tunableEnvironment[1].f1->data[i28] = b->data[i28];
  }

  introsort(idx, a->size[0], this_tunableEnvironment);
  permuteVector(idx, a);
  permuteVector(idx, b);
  emxFreeMatrix_cell_wrap_11(this_tunableEnvironment);
}

void sparse_and(const emxArray_boolean_T *a_d, const emxArray_int32_T *a_colidx,
                const emxArray_int32_T *a_rowidx, const emxArray_boolean_T *b_d,
                const emxArray_int32_T *b_colidx, const emxArray_int32_T
                *b_rowidx, int b_m, emxArray_boolean_T *s_d, emxArray_int32_T
                *s_colidx, emxArray_int32_T *s_rowidx, int *s_m)
{
  int didx;
  int numalloc;
  int bidx;
  boolean_T moreAToDo;
  boolean_T moreBToDo;
  didx = a_colidx->data[a_colidx->size[0] - 1] - 1;
  numalloc = b_colidx->data[b_colidx->size[0] - 1] - 1;
  if (didx < numalloc) {
    numalloc = didx;
  }

  if (numalloc < 1) {
    numalloc = 1;
  }

  b_sparse_sparse(b_m, numalloc, s_d, s_colidx, s_rowidx, s_m);
  didx = 1;
  s_colidx->data[0] = 1;
  numalloc = a_colidx->data[0];
  bidx = b_colidx->data[0] - 1;
  moreAToDo = (a_colidx->data[0] < a_colidx->data[1]);
  moreBToDo = (b_colidx->data[0] < b_colidx->data[1]);
  while (moreAToDo || moreBToDo) {
    while ((numalloc < a_colidx->data[1]) && ((!moreBToDo) || (a_rowidx->
             data[numalloc - 1] < b_rowidx->data[bidx]))) {
      numalloc++;
    }

    moreAToDo = (numalloc < a_colidx->data[1]);
    while ((bidx + 1 < b_colidx->data[1]) && ((!moreAToDo) || (b_rowidx->
             data[bidx] < a_rowidx->data[numalloc - 1]))) {
      bidx++;
    }

    while ((numalloc < a_colidx->data[1]) && (bidx + 1 < b_colidx->data[1]) &&
           (a_rowidx->data[numalloc - 1] == b_rowidx->data[bidx])) {
      if (a_d->data[numalloc - 1] && b_d->data[bidx]) {
        s_d->data[didx - 1] = true;
        s_rowidx->data[didx - 1] = b_rowidx->data[bidx];
        didx++;
      }

      bidx++;
      numalloc++;
    }

    moreAToDo = (numalloc < a_colidx->data[1]);
    moreBToDo = (bidx + 1 < b_colidx->data[1]);
  }

  s_colidx->data[1] = didx;
}

void sparse_eq(const emxArray_real_T *a_d, const emxArray_int32_T *a_colidx,
               const emxArray_int32_T *a_rowidx, const emxArray_real_T *b_d,
               const emxArray_int32_T *b_colidx, const emxArray_int32_T
               *b_rowidx, int b_m, emxArray_boolean_T *s_d, emxArray_int32_T
               *s_colidx, emxArray_int32_T *s_rowidx, int *s_m)
{
  emxArray_boolean_T *S;
  int aidx;
  int i12;
  int bidx;
  boolean_T moreAToDo;
  boolean_T moreBToDo;
  int mInt;
  emxInit_boolean_T(&S, 1);
  aidx = getBinOpSize(b_m);
  i12 = S->size[0];
  S->size[0] = aidx;
  emxEnsureCapacity_boolean_T(S, i12);
  for (i12 = 0; i12 < aidx; i12++) {
    S->data[i12] = true;
  }

  aidx = a_colidx->data[0] - 1;
  bidx = b_colidx->data[0] - 1;
  moreAToDo = (a_colidx->data[0] < a_colidx->data[1]);
  moreBToDo = (b_colidx->data[0] < b_colidx->data[1]);
  while (moreAToDo || moreBToDo) {
    while ((aidx + 1 < a_colidx->data[1]) && ((!moreBToDo) || (a_rowidx->
             data[aidx] < b_rowidx->data[bidx]))) {
      S->data[a_rowidx->data[aidx] - 1] = (a_d->data[aidx] == 0.0);
      aidx++;
    }

    moreAToDo = (aidx + 1 < a_colidx->data[1]);
    while ((bidx + 1 < b_colidx->data[1]) && ((!moreAToDo) || (b_rowidx->
             data[bidx] < a_rowidx->data[aidx]))) {
      S->data[b_rowidx->data[bidx] - 1] = (0.0 == b_d->data[bidx]);
      bidx++;
    }

    while ((aidx + 1 < a_colidx->data[1]) && (bidx + 1 < b_colidx->data[1]) &&
           (a_rowidx->data[aidx] == b_rowidx->data[bidx])) {
      S->data[b_rowidx->data[bidx] - 1] = (a_d->data[aidx] == b_d->data[bidx]);
      bidx++;
      aidx++;
    }

    moreAToDo = (aidx + 1 < a_colidx->data[1]);
    moreBToDo = (bidx + 1 < b_colidx->data[1]);
  }

  mInt = S->size[0];
  aidx = 0;
  i12 = S->size[0];
  for (bidx = 0; bidx < i12; bidx++) {
    if (S->data[bidx]) {
      aidx++;
    }
  }

  *s_m = S->size[0];
  if (aidx >= 1) {
  } else {
    aidx = 1;
  }

  i12 = s_d->size[0];
  s_d->size[0] = aidx;
  emxEnsureCapacity_boolean_T(s_d, i12);
  for (i12 = 0; i12 < aidx; i12++) {
    s_d->data[i12] = false;
  }

  i12 = s_colidx->size[0];
  s_colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(s_colidx, i12);
  s_colidx->data[0] = 0;
  s_colidx->data[1] = 0;
  s_colidx->data[0] = 1;
  i12 = s_rowidx->size[0];
  s_rowidx->size[0] = aidx;
  emxEnsureCapacity_int32_T(s_rowidx, i12);
  for (i12 = 0; i12 < aidx; i12++) {
    s_rowidx->data[i12] = 0;
  }

  s_rowidx->data[0] = 1;
  aidx = 0;
  for (bidx = 0; bidx < mInt; bidx++) {
    if (S->data[bidx]) {
      s_rowidx->data[aidx] = bidx + 1;
      s_d->data[aidx] = true;
      aidx++;
    }
  }

  emxFree_boolean_T(&S);
  s_colidx->data[1] = aidx + 1;
}

double sparse_full(const emxArray_real_T *this_d, const emxArray_int32_T
                   *this_colidx)
{
  double y;
  int cend;
  int i5;
  int idx;
  y = 0.0;
  cend = this_colidx->data[1] - 1;
  i5 = this_colidx->data[0];
  for (idx = i5; idx <= cend; idx++) {
    y = this_d->data[0];
  }

  return y;
}

void sparse_gt(const emxArray_real_T *a_d, const emxArray_int32_T *a_colidx,
               const emxArray_int32_T *a_rowidx, int a_m,
               coder_internal_sparse_1 *s)
{
  int nzs;
  int loop_ub;
  emxArray_boolean_T *tmpd;
  int i10;
  nzs = a_colidx->data[a_colidx->size[0] - 1];
  if (1 > a_colidx->data[a_colidx->size[0] - 1] - 1) {
    loop_ub = 0;
  } else {
    loop_ub = a_colidx->data[a_colidx->size[0] - 1] - 1;
  }

  emxInit_boolean_T(&tmpd, 1);
  i10 = tmpd->size[0];
  tmpd->size[0] = loop_ub;
  emxEnsureCapacity_boolean_T(tmpd, i10);
  for (i10 = 0; i10 < loop_ub; i10++) {
    tmpd->data[i10] = (a_d->data[i10] > 0.0);
  }

  b_sparse_sparse(a_m, a_colidx->data[a_colidx->size[0] - 1] - 1, s->d,
                  s->colidx, s->rowidx, &s->m);
  if (1 > a_colidx->data[a_colidx->size[0] - 1] - 1) {
    loop_ub = 1;
  } else {
    loop_ub = a_colidx->data[a_colidx->size[0] - 1];
  }

  for (i10 = 0; i10 <= loop_ub - 2; i10++) {
    s->rowidx->data[i10] = a_rowidx->data[i10];
  }

  i10 = s->colidx->size[0];
  s->colidx->size[0] = a_colidx->size[0];
  emxEnsureCapacity_int32_T(s->colidx, i10);
  loop_ub = a_colidx->size[0];
  for (i10 = 0; i10 < loop_ub; i10++) {
    s->colidx->data[i10] = a_colidx->data[i10];
  }

  for (loop_ub = 0; loop_ub <= nzs - 2; loop_ub++) {
    s->d->data[loop_ub] = tmpd->data[loop_ub];
  }

  emxFree_boolean_T(&tmpd);
  b_sparse_fillIn(s);
}

void sparse_parenAssign(coder_internal_sparse *this, const emxArray_real_T
  *rhs_d, const emxArray_int32_T *rhs_colidx, const emxArray_int32_T *rhs_rowidx,
  int rhs_m, const emxArray_real_T *varargin_1)
{
  int nidx;
  emxArray_real_T *s_d;
  emxArray_int8_T *s_colidx;
  int k;
  int i31;
  int vk;
  int b_vk;
  int idx;
  boolean_T found;
  double rhsv;
  int cend;
  nidx = varargin_1->size[0];
  emxInit_real_T(&s_d, 1);
  emxInit_int8_T(&s_colidx, 1);
  for (k = 0; k < nidx; k++) {
    i31 = this->m;
    vk = div_s32((int)varargin_1->data[k] - 1, i31);
    s_d->size[0] = 0;
    b_vk = s_colidx->size[0];
    s_colidx->size[0] = 2;
    emxEnsureCapacity_int8_T(s_colidx, b_vk);
    s_colidx->data[0] = 0;
    s_colidx->data[1] = 0;
    s_colidx->data[0] = 1;
    s_colidx->data[1] = 1;
    b_vk = div_s32(k, rhs_m);
    locBsearch(rhs_rowidx, (k - b_vk * rhs_m) + 1, rhs_colidx->data[b_vk],
               rhs_colidx->data[b_vk + 1], &idx, &found);
    if (found) {
      b_vk = s_d->size[0];
      s_d->size[0] = 1;
      emxEnsureCapacity_real_T(s_d, b_vk);
      s_d->data[0] = rhs_d->data[idx - 1];
      s_colidx->data[1] = 2;
    }

    if (s_d->size[0] == 0) {
      b_vk = s_d->size[0];
      s_d->size[0] = 1;
      emxEnsureCapacity_real_T(s_d, b_vk);
      s_d->data[0] = 0.0;
    }

    rhsv = 0.0;
    cend = s_colidx->data[1] - 1;
    b_vk = s_colidx->data[0];
    for (idx = b_vk; idx <= cend; idx++) {
      rhsv = s_d->data[0];
    }

    sparse_parenAssign2D(this, rhsv, (int)varargin_1->data[k] - vk * i31, vk + 1);
  }

  emxFree_int8_T(&s_colidx);
  emxFree_real_T(&s_d);
}

void sparse_parenReference(const emxArray_real_T *this_d, const emxArray_int32_T
  *this_colidx, const emxArray_int32_T *this_rowidx, int this_m, const
  emxArray_real_T *varargin_1, emxArray_real_T *s_d, emxArray_int32_T *s_colidx,
  emxArray_int32_T *s_rowidx, int *s_m)
{
  int i2;
  int b_s_m;
  int vk;
  int k;
  int colNnz;
  int ridx;
  int idx;
  boolean_T found;
  int i3;
  i2 = varargin_1->size[0];
  b_s_m = varargin_1->size[0];
  s_d->size[0] = 0;
  s_rowidx->size[0] = 0;
  vk = s_colidx->size[0];
  s_colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(s_colidx, vk);
  s_colidx->data[0] = 0;
  s_colidx->data[1] = 0;
  k = 0;
  s_colidx->data[0] = 1;
  colNnz = 1;
  for (ridx = 0; ridx < i2; ridx++) {
    vk = div_s32((int)varargin_1->data[k] - 1, this_m);
    locBsearch(this_rowidx, (int)varargin_1->data[k] - vk * this_m,
               this_colidx->data[vk], this_colidx->data[vk + 1], &idx, &found);
    if (found) {
      vk = s_d->size[0];
      i3 = s_d->size[0];
      s_d->size[0] = vk + 1;
      emxEnsureCapacity_real_T(s_d, i3);
      s_d->data[vk] = this_d->data[idx - 1];
      vk = s_rowidx->size[0];
      i3 = s_rowidx->size[0];
      s_rowidx->size[0] = vk + 1;
      emxEnsureCapacity_int32_T(s_rowidx, i3);
      s_rowidx->data[vk] = 1 + ridx;
      colNnz++;
    }

    k++;
  }

  s_colidx->data[1] = colNnz;
  if (s_d->size[0] == 0) {
    i2 = s_d->size[0];
    s_d->size[0] = 1;
    emxEnsureCapacity_real_T(s_d, i2);
    s_d->data[0] = 0.0;
    i2 = s_rowidx->size[0];
    s_rowidx->size[0] = 1;
    emxEnsureCapacity_int32_T(s_rowidx, i2);
    s_rowidx->data[0] = 0;
  }

  *s_m = b_s_m;
}

void sparse_sparse(int m, int nzmaxval, emxArray_real_T *this_d,
                   emxArray_int32_T *this_colidx, emxArray_int32_T *this_rowidx,
                   int *this_m)
{
  int numalloc;
  int i4;
  if (nzmaxval >= 1) {
    numalloc = nzmaxval;
  } else {
    numalloc = 1;
  }

  i4 = this_d->size[0];
  this_d->size[0] = numalloc;
  emxEnsureCapacity_real_T(this_d, i4);
  for (i4 = 0; i4 < numalloc; i4++) {
    this_d->data[i4] = 0.0;
  }

  i4 = this_colidx->size[0];
  this_colidx->size[0] = 2;
  emxEnsureCapacity_int32_T(this_colidx, i4);
  this_colidx->data[0] = 1;
  i4 = this_rowidx->size[0];
  this_rowidx->size[0] = numalloc;
  emxEnsureCapacity_int32_T(this_rowidx, i4);
  for (i4 = 0; i4 < numalloc; i4++) {
    this_rowidx->data[i4] = 0;
  }

  this_colidx->data[0] = 1;
  this_colidx->data[1] = 1;
  *this_m = m;
}

void sparse_times(const emxArray_real_T *a, const emxArray_real_T *b_d, const
                  emxArray_int32_T *b_colidx, const emxArray_int32_T *b_rowidx,
                  int b_m, emxArray_real_T *s_d, emxArray_int32_T *s_colidx,
                  emxArray_int32_T *s_rowidx, int *s_m)
{
  int sm;
  int numalloc;
  int idx;
  int k;
  double val;
  sm = getBinOpSize(b_m);
  numalloc = -1;
  idx = a->size[0];
  for (k = 0; k < idx; k++) {
    if (a->data[k] != 0.0) {
      numalloc++;
    }
  }

  numalloc += b_colidx->data[b_colidx->size[0] - 1];
  if (numalloc >= sm) {
    numalloc = sm;
  }

  if (numalloc < 1) {
    numalloc = 1;
  }

  sparse_sparse(sm, numalloc, s_d, s_colidx, s_rowidx, s_m);
  s_colidx->data[0] = 1;
  numalloc = 1;
  idx = b_colidx->data[0];
  for (k = 0; k < *s_m; k++) {
    if ((idx < b_colidx->data[1]) && (k + 1 == b_rowidx->data[idx - 1])) {
      val = a->data[k] * b_d->data[idx - 1];
      idx++;
    } else {
      val = 0.0;
    }

    if (val != 0.0) {
      s_d->data[numalloc - 1] = val;
      s_rowidx->data[numalloc - 1] = k + 1;
      numalloc++;
    }
  }

  s_colidx->data[1] = numalloc;
}

/* End of code generation (sparse1.c) */
