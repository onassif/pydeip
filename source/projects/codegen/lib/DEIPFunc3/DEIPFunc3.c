/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * DEIPFunc3.c
 *
 * Code generation for function 'DEIPFunc3'
 *
 */

/* Include files */
#include <math.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "DEIPFunc3.h"
#include "DEIPFunc3_emxutil.h"
#include "sparse1.h"
#include "norm.h"
#include "diag1.h"
#include "parenAssign2D.h"
#include "ifWhileCond.h"
#include "any.h"
#include "sort1.h"
#include "not.h"
#include "eml_setop.h"
#include "sum.h"
#include "sparse.h"
#include "ismember.h"
#include "issorted.h"
#include "all.h"
#include "sign.h"
#include "abs.h"
#include "sortrows.h"
#include "nchoosek.h"
#include "sortIdx.h"
#include "DEIPFunc3_rtwutil.h"
#include <stdio.h>

/* Function Definitions */
void DEIPFunc3(emxArray_real_T *InterTypes, emxArray_real_T *NodesOnElement,
               const emxArray_real_T *RegionOnElement, emxArray_real_T
               *Coordinates, double *numnp, double numel, double nummat, double
               nen, double ndm, double usePBC, const double *numMPC,
               emxArray_real_T *MPCList, double *numEonB, emxArray_real_T
               *numEonF, emxArray_real_T *ElementsOnBoundary, double *numSI,
               emxArray_real_T *ElementsOnFacet, coder_internal_sparse
               *ElementsOnNode, coder_internal_sparse *ElementsOnNodeDup,
               emxArray_real_T *ElementsOnNodeNum, double *numfac,
               emxArray_real_T *ElementsOnNodeNum2, double *numinttype,
               emxArray_real_T *FacetsOnElement, emxArray_real_T
               *FacetsOnElementInt, emxArray_real_T *FacetsOnInterface,
               emxArray_real_T *FacetsOnInterfaceNum, coder_internal_sparse
               *FacetsOnNode, coder_internal_sparse *FacetsOnNodeCut,
               coder_internal_sparse *FacetsOnNodeInt, emxArray_real_T
               *FacetsOnNodeNum, emxArray_real_T *NodeCGDG,
               coder_internal_sparse *NodeReg, emxArray_real_T *NodesOnElementCG,
               emxArray_real_T *NodesOnElementDG, emxArray_real_T
               *NodesOnInterface, double *NodesOnInterfaceNum, double *numCL,
               emxArray_real_T *NodesOnPBC, emxArray_real_T *NodesOnPBCnum,
               emxArray_real_T *NodesOnLink, emxArray_real_T *NodesOnLinknum,
               emxArray_real_T *numEonPBC, emxArray_real_T *FacetsOnPBC,
               emxArray_real_T *FacetsOnPBCNum, emxArray_real_T
               *FacetsOnIntMinusPBC, emxArray_real_T *FacetsOnIntMinusPBCNum)
{
  int i25;
  int loop_ub;
  emxArray_real_T *ElementsOnNodePBCNum;
  emxArray_real_T *Coordinates3;
  int i26;
  int b_loop_ub;
  emxArray_real_T *NodesOnElementPBC;
  emxArray_real_T *PBCList;
  emxArray_real_T *MorePBC;
  emxArray_real_T *PBCList2;
  emxArray_boolean_T *elems;
  emxArray_real_T *idx;
  emxArray_int32_T *b_idx;
  emxArray_int32_T *ii;
  emxArray_real_T *s;
  emxArray_real_T *b_MPCList;
  int i;
  int nx;
  int c_idx;
  int nb;
  boolean_T exitg2;
  emxArray_real_T *pairs;
  int numPBC;
  double reg;
  int k0;
  long long q0;
  double numA;
  long long qY;
  long long mat;
  double i1;
  emxArray_int64_T *inter;
  boolean_T old;
  emxArray_real_T *setAll;
  int k;
  emxArray_int32_T *b_ii;
  int pEnd;
  long long b_qY;
  double absxk;
  emxArray_real_T *ElementsOnNodeRow;
  emxArray_real_T *ElementsOnNodePBC_d;
  emxArray_int32_T *ElementsOnNodePBC_colidx;
  emxArray_int32_T *ElementsOnNodePBC_rowidx;
  emxArray_int32_T *internodes_rowidx;
  coder_internal_sparse_2 NodeRegSum;
  int i27;
  emxArray_int32_T *t0_colidx;
  emxArray_int32_T *t0_rowidx;
  emxArray_real_T *b_setAll;
  emxArray_real_T *NodeRegInd;
  emxArray_real_T *b_NodeRegInd;
  emxArray_real_T *c_NodeRegInd;
  emxArray_real_T *d_NodeRegInd;
  emxArray_real_T *e_NodeRegInd;
  double nodes[100];
  int b_i;
  int exitg1;
  emxArray_real_T *b_NodesOnPBCnum;
  int q;
  int j;
  int notdone;
  emxArray_real_T *ElementsOnNodePBCRow;
  unsigned int nri;
  int elem;
  double links[100];
  int n;
  double nodeloc_data[27];
  int p;
  double node;
  int nodes_size[1];
  double nodes_data[100];
  double links2_data[100];
  int links2_size[1];
  int ia_data[100];
  int ia_size[1];
  int ib_size[1];
  emxArray_real_T *b_PBCList2;
  emxArray_int32_T *b_s;
  emxArray_real_T *newpairs;
  int nel2;
  int allnodes_size[2];
  double locPBC_data[500];
  double allnodes_data[101];
  int exponent;
  boolean_T guard1 = false;
  coder_internal_sparse expl_temp;
  emxArray_real_T b_nodes_data;
  int tmp_data[100];
  double otherP_data[200];
  int b_exponent;
  int b_tmp_data[100];
  int c_tmp_data[101];
  coder_internal_sparse b_ElementsOnNodePBCRow;
  int d_tmp_data[100];
  int e_tmp_data[100];
  double link_data[4];
  int f_tmp_data[101];
  double newPBC[4];
  double numFPBC;
  emxArray_int32_T *r0;
  emxArray_real_T *FacetsOnNodeInd;
  emxArray_real_T *ElementsOnNodeABCD;
  emxArray_int32_T *c_s;
  emxArray_int32_T *d_s;
  signed char numfn_data[6];
  static const signed char numfnW[5] = { 4, 4, 4, 3, 3 };

  static const signed char numfnP[5] = { 3, 3, 3, 3, 4 };

  signed char nloop_data[48];
  static const signed char nloopT[24] = { 2, 1, 1, 1, 3, 3, 2, 2, 4, 4, 4, 3, 6,
    7, 5, 5, 9, 8, 8, 6, 10, 10, 9, 7 };

  static const signed char nloopW[40] = { 2, 1, 1, 1, 4, 3, 3, 2, 2, 5, 5, 4, 4,
    3, 6, 6, 6, 5, 7, 10, 8, 9, 7, 8, 11, 11, 12, 10, 9, 12, 14, 13, 13, 0, 0,
    15, 15, 14, 0, 0 };

  static const signed char nloopH[48] = { 1, 2, 1, 3, 1, 5, 4, 3, 2, 4, 2, 6, 5,
    6, 5, 7, 3, 7, 8, 7, 6, 8, 4, 8, 12, 10, 9, 11, 9, 13, 16, 14, 13, 15, 10,
    14, 17, 18, 17, 19, 11, 15, 20, 19, 18, 20, 12, 16 };

  static const signed char nloopP[40] = { 1, 2, 3, 1, 1, 2, 3, 4, 4, 2, 5, 5, 5,
    5, 3, 6, 7, 8, 9, 4, 10, 11, 12, 10, 6, 11, 12, 13, 13, 7, 0, 0, 0, 0, 8, 0,
    0, 0, 0, 9 };

  int nodeABCD_size[2];
  double nodeABCD_data[5];
  double numABCD_data[4];
  boolean_T b_numABCD_data[4];
  boolean_T guard2 = false;
  signed char nodeAABBCCDD[4];
  int c_exponent;
  signed char nloopA_data[48];
  int nodeAABBCCDD2_size[2];
  double nodeAABBCCDD2_data[4];
  double regB;
  double regI;
  emxArray_boolean_T *internodes_d;
  signed char numfnA_data[6];
  emxArray_boolean_T *t3_d;
  coder_internal_sparse_1 b_expl_temp;
  signed char nloopB_data[48];
  emxArray_real_T *setPBC;
  double nodeEEFFGGHH_data[4];
  int link_size[2];
  double nodeEEFFGGHH2_data[4];
  boolean_T guard3 = false;
  int d_exponent;
  emxArray_real_T *intermat2;
  emxArray_real_T *b_elems;
  coder_internal_sparse_4 c_expl_temp;
  boolean_T x_data[27];
  signed char ii_data[27];
  int b_nx;
  int x_size[2];
  boolean_T guard4 = false;
  signed char node_dup_tmp_data[27];
  int e_exponent;
  int c_nx;
  unsigned int iSec2;
  int d_nx;
  int e_nx;
  (void)ndm;
  (void)numMPC;

  /*  Program: DEIProgram3 */
  /*  Tim Truster */
  /*  08/15/2014 */
  /*  */
  /*  Script to convert CG mesh (e.g. from block program output) into DG mesh */
  /*  Addition of modules to insert interfaces selectively between materials */
  /*   */
  /*  Input: matrNodesOnElement InterTypes(nummat,nummat) for listing the interface */
  /*  material types to assign; only lower triangle is accessed; value 0 means */
  /*  interface is not inserted between those materials (including diagonal). */
  /*  */
  /*  Numbering of interface types (matI) is like: */
  /*  1 */
  /*  2 3 */
  /*  4 5 6 ... */
  /*  */
  /*  Output: */
  /*  Actual list of used interfaces is SurfacesI and numCL, new BC arrays and */
  /*  load arrays */
  /*  Total list of interfaces is in numIfac and Interfac, although there is */
  /*  no designation on whether the interfaces are active or not. These are */
  /*  useful for assigning different DG element types to different interfaces. */
  /*  Revisions: */
  /*  02/10/2017: adding periodic boundary conditions (PBC); uses both zipped mesh */
  /*  (collapsing linked PBC nodes into a single node and updating */
  /*  connectivity), and ghost elements (leaving original mesh, added elements */
  /*  along PBC edges that connect across the domain); both result in a 'torus' */
  /*  mesh. */
  /*  Revisions: */
  /*  04/04/2017: adding periodic BC in the form of multi-point constraints, */
  /*  which do not require the domain to be a cube. Still uses the zipped mesh, */
  /*  but does not add ghost elements. Instead, node duplication is performed */
  /*  directly on the zipped mesh, and MPC elements are applied outside of this */
  /*  subroutine. */
  /*  % flags for clearing out intermediate variables in this script; turned on */
  /*  % by default */
  /*  currvariables = who(); */
  /*  clearinter = 1; */
  *numSI = 0.0;

  /*  Test for Octave vs Matlab compatibility */
  /*  isOctave = exist('OCTAVE_VERSION', 'builtin') ~= 0; */
  /*  ElementsOnNode = zeros(maxel,numnp); % Array of elements connected to nodes */
  /*  ElementsOnNodeDup = zeros(maxel,numnp); % actual node ID given to element for an original node; general counterpart of NodeMat */
  i25 = ElementsOnNodeNum->size[0];
  loop_ub = (int)*numnp;
  ElementsOnNodeNum->size[0] = loop_ub;
  emxEnsureCapacity_real_T(ElementsOnNodeNum, i25);
  for (i25 = 0; i25 < loop_ub; i25++) {
    ElementsOnNodeNum->data[i25] = 0.0;
  }

  /*  if ~exist('usePBC','var') */
  /*      usePBC = 0; */
  /*  end */
  emxInit_real_T(&ElementsOnNodePBCNum, 1);
  if (usePBC != 0.0) {
    /*  create extra arrays for zipped mesh */
    /*  ElementsOnNodePBC = zeros(maxel,numnp); % Array of elements connected to nodes */
    /*  ElementsOnNodePBCDup = zeros(maxel,numnp); % actual node ID given to element for an original node; general counterpart of NodeMat */
    i25 = ElementsOnNodePBCNum->size[0];
    ElementsOnNodePBCNum->size[0] = loop_ub;
    emxEnsureCapacity_real_T(ElementsOnNodePBCNum, i25);
    for (i25 = 0; i25 < loop_ub; i25++) {
      ElementsOnNodePBCNum->data[i25] = 0.0;
    }
  } else {
    i25 = ElementsOnNodePBCNum->size[0];
    ElementsOnNodePBCNum->size[0] = 1;
    emxEnsureCapacity_real_T(ElementsOnNodePBCNum, i25);
    ElementsOnNodePBCNum->data[0] = 0.0;
  }

  emxInit_real_T(&Coordinates3, 2);

  /*  NodeReg = zeros(numnp,nummat); */
  i25 = Coordinates3->size[0] * Coordinates3->size[1];
  i26 = (int)(numel * nen);
  Coordinates3->size[0] = i26;
  Coordinates3->size[1] = 3;
  emxEnsureCapacity_real_T(Coordinates3, i25);
  b_loop_ub = i26 * 3;
  for (i25 = 0; i25 < b_loop_ub; i25++) {
    Coordinates3->data[i25] = 0.0;
  }

  i25 = NodesOnElementCG->size[0] * NodesOnElementCG->size[1];
  NodesOnElementCG->size[0] = NodesOnElement->size[0];
  NodesOnElementCG->size[1] = NodesOnElement->size[1];
  emxEnsureCapacity_real_T(NodesOnElementCG, i25);
  b_loop_ub = NodesOnElement->size[0] * NodesOnElement->size[1];
  for (i25 = 0; i25 < b_loop_ub; i25++) {
    NodesOnElementCG->data[i25] = NodesOnElement->data[i25];
  }

  emxInit_real_T(&NodesOnElementPBC, 2);
  if (usePBC != 0.0) {
    /*  Add space in connectivity for ghost elements */
    /*      if nen == 4 */
    /*          NodesOnElementCG = [NodesOnElementCG zeros(numel,8-nen) */
    /*                              zeros(numPBC*2,8)]; */
    /*          nen = 8; */
    /*      elseif nen == 6 */
    /*          NodesOnElementCG = [NodesOnElementCG zeros(numel,8-nen) */
    /*                              zeros(numPBC*2,8)]; */
    /*          nen = 8; */
    /*      elseif nen == 10 */
    /*          NodesOnElementCG = [NodesOnElementCG zeros(numel,27-nen) */
    /*                              zeros(numPBC*2,27)]; */
    /*          nen = 27; */
    /*      elseif nen == 18 */
    /*          NodesOnElementCG = [NodesOnElementCG zeros(numel,27-nen) */
    /*                              zeros(numPBC*2,27)]; */
    /*          nen = 27; */
    /*      end */
    i25 = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1];
    NodesOnElementPBC->size[0] = NodesOnElement->size[0];
    NodesOnElementPBC->size[1] = NodesOnElement->size[1];
    emxEnsureCapacity_real_T(NodesOnElementPBC, i25);
    b_loop_ub = NodesOnElement->size[0] * NodesOnElement->size[1];
    for (i25 = 0; i25 < b_loop_ub; i25++) {
      NodesOnElementPBC->data[i25] = NodesOnElement->data[i25];
    }

    /*  Connectivity that is zipped, having PBC nodes condensed together */
    /*      RegionOnElement = [RegionOnElement; zeros(numPBC*2,1)]; */
  } else {
    i25 = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1];
    NodesOnElementPBC->size[0] = 1;
    NodesOnElementPBC->size[1] = 1;
    emxEnsureCapacity_real_T(NodesOnElementPBC, i25);
    NodesOnElementPBC->data[0] = 0.0;

    /*  RegionOnElement = RegionOnElement; */
  }

  emxInit_real_T(&PBCList, 2);

  /*  Set up PBC data structures */
  i25 = PBCList->size[0] * PBCList->size[1];
  PBCList->size[0] = MPCList->size[0];
  PBCList->size[1] = 5;
  emxEnsureCapacity_real_T(PBCList, i25);
  b_loop_ub = MPCList->size[0] * MPCList->size[1];
  for (i25 = 0; i25 < b_loop_ub; i25++) {
    PBCList->data[i25] = MPCList->data[i25];
  }

  emxInit_real_T(&MorePBC, 1);
  emxInit_real_T(&PBCList2, 2);
  emxInit_boolean_T(&elems, 1);
  emxInit_real_T(&idx, 1);
  emxInit_int32_T(&b_idx, 1);
  emxInit_int32_T(&ii, 1);
  emxInit_real_T(&s, 1);
  emxInit_real_T(&b_MPCList, 2);
  if (usePBC != 0.0) {
    /*  Lists of node pairs in convenient table format */
    i25 = NodesOnPBC->size[0] * NodesOnPBC->size[1];
    NodesOnPBC->size[0] = 100;
    NodesOnPBC->size[1] = loop_ub;
    emxEnsureCapacity_real_T(NodesOnPBC, i25);
    i = 100 * loop_ub;
    for (i25 = 0; i25 < i; i25++) {
      NodesOnPBC->data[i25] = 0.0;
    }

    /*  nodes that are paired up */
    i25 = NodesOnPBCnum->size[0];
    NodesOnPBCnum->size[0] = loop_ub;
    emxEnsureCapacity_real_T(NodesOnPBCnum, i25);
    for (i25 = 0; i25 < loop_ub; i25++) {
      NodesOnPBCnum->data[i25] = 0.0;
    }

    /*  number of nodes with common pairing */
    i25 = NodesOnLink->size[0] * NodesOnLink->size[1];
    NodesOnLink->size[0] = 100;
    NodesOnLink->size[1] = loop_ub;
    emxEnsureCapacity_real_T(NodesOnLink, i25);
    for (i25 = 0; i25 < i; i25++) {
      NodesOnLink->data[i25] = 0.0;
    }

    /*  IDs of PBC referring to node */
    i25 = NodesOnLinknum->size[0];
    NodesOnLinknum->size[0] = loop_ub;
    emxEnsureCapacity_real_T(NodesOnLinknum, i25);
    for (i25 = 0; i25 < loop_ub; i25++) {
      NodesOnLinknum->data[i25] = 0.0;
    }

    /*  number of PBC referring to node */
    /*  Handle the corners first */
    emxInit_real_T(&pairs, 2);
    if (usePBC == 2.0) {
      numPBC = MPCList->size[0];
    } else {
      numPBC = MPCList->size[0];
      i25 = MPCList->size[0];
      if (i25 == 0) {
        b_loop_ub = MPCList->size[0];
        i25 = pairs->size[0] * pairs->size[1];
        pairs->size[0] = b_loop_ub;
        pairs->size[1] = 2;
        emxEnsureCapacity_real_T(pairs, i25);
        for (i25 = 0; i25 < b_loop_ub; i25++) {
          pairs->data[i25] = MPCList->data[i25 + MPCList->size[0] * 2];
        }

        for (i25 = 0; i25 < b_loop_ub; i25++) {
          pairs->data[i25 + pairs->size[0]] = MPCList->data[i25 + MPCList->size
            [0] * 3];
        }

        b_idx->size[0] = 0;
      } else {
        b_loop_ub = MPCList->size[0];
        i25 = b_MPCList->size[0] * b_MPCList->size[1];
        b_MPCList->size[0] = b_loop_ub;
        b_MPCList->size[1] = 2;
        emxEnsureCapacity_real_T(b_MPCList, i25);
        for (i25 = 0; i25 < b_loop_ub; i25++) {
          b_MPCList->data[i25] = MPCList->data[i25 + MPCList->size[0] * 2];
        }

        for (i25 = 0; i25 < b_loop_ub; i25++) {
          b_MPCList->data[i25 + b_MPCList->size[0]] = MPCList->data[i25 +
            MPCList->size[0] * 3];
        }

        sortIdx(b_MPCList, b_idx);
        b_loop_ub = MPCList->size[0];
        i25 = pairs->size[0] * pairs->size[1];
        pairs->size[0] = b_loop_ub;
        pairs->size[1] = 2;
        emxEnsureCapacity_real_T(pairs, i25);
        for (i25 = 0; i25 < b_loop_ub; i25++) {
          pairs->data[i25] = MPCList->data[i25 + MPCList->size[0] * 2];
        }

        for (i25 = 0; i25 < b_loop_ub; i25++) {
          pairs->data[i25 + pairs->size[0]] = MPCList->data[i25 + MPCList->size
            [0] * 3];
        }

        apply_row_permutation(pairs, b_idx);
        i25 = idx->size[0];
        idx->size[0] = b_idx->size[0];
        emxEnsureCapacity_real_T(idx, i25);
        b_loop_ub = b_idx->size[0];
        for (i25 = 0; i25 < b_loop_ub; i25++) {
          idx->data[i25] = b_idx->data[i25];
        }

        nb = -1;
        i25 = MPCList->size[0];
        k = 0;
        while (k + 1 <= i25) {
          k0 = k;
          do {
            exitg1 = 0;
            k++;
            if (k + 1 > i25) {
              exitg1 = 1;
            } else {
              old = false;
              j = 0;
              exitg2 = false;
              while ((!exitg2) && (j < 2)) {
                i1 = pairs->data[k0 + pairs->size[0] * j];
                reg = pairs->data[k + pairs->size[0] * j];
                absxk = fabs(reg / 2.0);
                if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
                  if (absxk <= 2.2250738585072014E-308) {
                    absxk = 4.94065645841247E-324;
                  } else {
                    frexp(absxk, &pEnd);
                    absxk = ldexp(1.0, pEnd - 53);
                  }
                } else {
                  absxk = rtNaN;
                }

                if ((fabs(reg - i1) < absxk) || (rtIsInf(i1) && rtIsInf(reg) &&
                     ((i1 > 0.0) == (reg > 0.0)))) {
                  j++;
                } else {
                  old = true;
                  exitg2 = true;
                }
              }

              if (old) {
                exitg1 = 1;
              }
            }
          } while (exitg1 == 0);

          nb++;
          pairs->data[nb] = pairs->data[k0];
          pairs->data[nb + pairs->size[0]] = pairs->data[k0 + pairs->size[0]];
          idx->data[nb] = idx->data[k0];
        }

        if (1 > nb + 1) {
          b_loop_ub = -1;
        } else {
          b_loop_ub = nb;
        }

        i25 = b_MPCList->size[0] * b_MPCList->size[1];
        b_MPCList->size[0] = b_loop_ub + 1;
        b_MPCList->size[1] = 2;
        emxEnsureCapacity_real_T(b_MPCList, i25);
        for (i25 = 0; i25 <= b_loop_ub; i25++) {
          b_MPCList->data[i25] = pairs->data[i25];
        }

        for (i25 = 0; i25 <= b_loop_ub; i25++) {
          b_MPCList->data[i25 + b_MPCList->size[0]] = pairs->data[i25 +
            pairs->size[0]];
        }

        i25 = pairs->size[0] * pairs->size[1];
        pairs->size[0] = b_MPCList->size[0];
        pairs->size[1] = 2;
        emxEnsureCapacity_real_T(pairs, i25);
        b_loop_ub = b_MPCList->size[0] * b_MPCList->size[1];
        for (i25 = 0; i25 < b_loop_ub; i25++) {
          pairs->data[i25] = b_MPCList->data[i25];
        }

        i25 = b_idx->size[0];
        b_idx->size[0] = nb + 1;
        emxEnsureCapacity_int32_T(b_idx, i25);
        for (k = 0; k <= nb; k++) {
          b_idx->data[k] = (int)idx->data[k];
        }
      }

      i25 = idx->size[0];
      idx->size[0] = b_idx->size[0];
      emxEnsureCapacity_real_T(idx, i25);
      b_loop_ub = b_idx->size[0];
      for (i25 = 0; i25 < b_loop_ub; i25++) {
        idx->data[i25] = b_idx->data[i25];
      }

      if (pairs->size[0] == 3) {
        NodesOnPBC->data[100 * ((int)pairs->data[0] - 1)] = pairs->data
          [pairs->size[0]];
        NodesOnPBCnum->data[(int)pairs->data[0] - 1] = 1.0;
        NodesOnPBC->data[100 * ((int)pairs->data[pairs->size[0]] - 1)] =
          pairs->data[0];
        NodesOnPBCnum->data[(int)pairs->data[pairs->size[0]] - 1] = 1.0;
        NodesOnLink->data[100 * ((int)pairs->data[0] - 1)] = -idx->data[0];
        NodesOnLinknum->data[(int)pairs->data[0] - 1] = 1.0;
        NodesOnLink->data[100 * ((int)pairs->data[pairs->size[0]] - 1)] =
          -idx->data[0];
        NodesOnLinknum->data[(int)pairs->data[pairs->size[0]] - 1] = 1.0;
        NodesOnPBC->data[100 * ((int)pairs->data[1] - 1)] = pairs->data[1 +
          pairs->size[0]];
        NodesOnPBCnum->data[(int)pairs->data[1] - 1] = 1.0;
        NodesOnPBC->data[1 + 100 * ((int)pairs->data[1 + pairs->size[0]] - 1)] =
          pairs->data[1];
        NodesOnPBCnum->data[(int)pairs->data[1 + pairs->size[0]] - 1] = 2.0;
        NodesOnLink->data[100 * ((int)pairs->data[1] - 1)] = -idx->data[1];
        NodesOnLinknum->data[(int)pairs->data[1] - 1] = 1.0;
        NodesOnLink->data[1 + 100 * ((int)pairs->data[1 + pairs->size[0]] - 1)] =
          -idx->data[1];
        NodesOnLinknum->data[(int)pairs->data[1 + pairs->size[0]] - 1] = 2.0;
        NodesOnPBC->data[100 * ((int)pairs->data[2] - 1)] = pairs->data[2 +
          pairs->size[0]];
        NodesOnPBCnum->data[(int)pairs->data[2] - 1] = 1.0;
        NodesOnPBC->data[2 + 100 * ((int)pairs->data[2 + pairs->size[0]] - 1)] =
          pairs->data[2];
        NodesOnPBCnum->data[(int)pairs->data[2 + pairs->size[0]] - 1] = 3.0;
        NodesOnLink->data[100 * ((int)pairs->data[2] - 1)] = -idx->data[2];
        NodesOnLinknum->data[(int)pairs->data[2] - 1] = 1.0;
        NodesOnLink->data[2 + 100 * ((int)pairs->data[2 + pairs->size[0]] - 1)] =
          -idx->data[2];
        NodesOnLinknum->data[(int)pairs->data[2 + pairs->size[0]] - 1] = 3.0;
      }
    }

    /*  Now do the rest of the nodes */
    for (k0 = 0; k0 < numPBC; k0++) {
      numA = NodesOnPBCnum->data[(int)MPCList->data[k0] - 1];
      if (1.0 > NodesOnPBCnum->data[(int)MPCList->data[k0] - 1]) {
        i25 = 0;
      } else {
        i25 = (int)NodesOnPBCnum->data[(int)MPCList->data[k0] - 1];
      }

      /*  make sure pair isn't already in the list for nodeA */
      /*      if isOctave */
      i1 = MPCList->data[k0 + MPCList->size[0]];
      old = false;
      k = 0;
      exitg2 = false;
      while ((!exitg2) && (k <= i25 - 1)) {
        pEnd = (int)MPCList->data[k0];
        reg = NodesOnPBC->data[k + 100 * (pEnd - 1)];
        absxk = fabs(reg / 2.0);
        if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
          if (absxk <= 2.2250738585072014E-308) {
            absxk = 4.94065645841247E-324;
          } else {
            frexp(absxk, &nx);
            absxk = ldexp(1.0, nx - 53);
          }
        } else {
          absxk = rtNaN;
        }

        pEnd = (int)MPCList->data[k0];
        if (fabs(NodesOnPBC->data[k + 100 * (pEnd - 1)] - i1) < absxk) {
          old = true;
          exitg2 = true;
        } else if (rtIsInf(i1) && rtIsInf(reg)) {
          pEnd = (int)MPCList->data[k0];
          if ((i1 > 0.0) == (NodesOnPBC->data[k + 100 * (pEnd - 1)] > 0.0)) {
            old = true;
            exitg2 = true;
          } else {
            k++;
          }
        } else {
          k++;
        }
      }

      /*      else */
      /*      old = ismembc(nodesAB(2),currPBC); */
      /*      end */
      if (!old) {
        /*  add the pair to nodeA, expand by one */
        NodesOnPBC->data[((int)(NodesOnPBCnum->data[(int)MPCList->data[k0] - 1]
          + 1.0) + 100 * ((int)MPCList->data[k0] - 1)) - 1] = MPCList->data[k0 +
          MPCList->size[0]];
        NodesOnPBCnum->data[(int)MPCList->data[k0] - 1]++;

        /*  Record which PBC ID refers to these nodes */
        numA = NodesOnLinknum->data[(int)MPCList->data[k0] - 1] + 1.0;
        NodesOnLink->data[((int)(NodesOnLinknum->data[(int)MPCList->data[k0] - 1]
          + 1.0) + 100 * ((int)MPCList->data[k0] - 1)) - 1] = 1.0 + (double)k0;
        NodesOnLinknum->data[(int)MPCList->data[k0] - 1]++;
      }

      if (1.0 > NodesOnPBCnum->data[(int)MPCList->data[k0 + MPCList->size[0]] -
          1]) {
        i25 = 0;
      } else {
        i25 = (int)NodesOnPBCnum->data[(int)MPCList->data[k0 + MPCList->size[0]]
          - 1];
      }

      /*  make sure pair isn't already in the list for nodeB */
      /*      if isOctave */
      i1 = MPCList->data[k0];
      old = false;
      k = 0;
      exitg2 = false;
      while ((!exitg2) && (k <= i25 - 1)) {
        pEnd = (int)MPCList->data[k0 + MPCList->size[0]];
        reg = NodesOnPBC->data[k + 100 * (pEnd - 1)];
        absxk = fabs(reg / 2.0);
        if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
          if (absxk <= 2.2250738585072014E-308) {
            absxk = 4.94065645841247E-324;
          } else {
            frexp(absxk, &q);
            absxk = ldexp(1.0, q - 53);
          }
        } else {
          absxk = rtNaN;
        }

        pEnd = (int)MPCList->data[k0 + MPCList->size[0]];
        if (fabs(NodesOnPBC->data[k + 100 * (pEnd - 1)] - i1) < absxk) {
          old = true;
          exitg2 = true;
        } else if (rtIsInf(i1) && rtIsInf(reg)) {
          pEnd = (int)MPCList->data[k0 + MPCList->size[0]];
          if ((i1 > 0.0) == (NodesOnPBC->data[k + 100 * (pEnd - 1)] > 0.0)) {
            old = true;
            exitg2 = true;
          } else {
            k++;
          }
        } else {
          k++;
        }
      }

      /*      else */
      /*      old = ismembc(nodesAB(1),currPBC); */
      /*      end */
      if (!old) {
        /*  add the pair to nodeB, expand by one */
        NodesOnPBC->data[((int)(NodesOnPBCnum->data[(int)MPCList->data[k0 +
          MPCList->size[0]] - 1] + 1.0) + 100 * ((int)MPCList->data[k0 +
          MPCList->size[0]] - 1)) - 1] = MPCList->data[k0];
        NodesOnPBCnum->data[(int)MPCList->data[k0 + MPCList->size[0]] - 1]++;

        /*  Record which PBC ID refers to these nodes */
        NodesOnLinknum->data[(int)MPCList->data[k0 + MPCList->size[0]] - 1]++;
        NodesOnLink->data[((int)numA + 100 * ((int)MPCList->data[k0 +
          MPCList->size[0]] - 1)) - 1] = 1.0 + (double)k0;
      }
    }

    /*  Find additional connections for nodes with pairs of pairs; this WILL find */
    /*  the extra pairs that are usually deleted to remove linear dependence in */
    /*  the stiffness matrix, e.g. the repeated edges in 2d or 3d. */
    i25 = elems->size[0];
    elems->size[0] = NodesOnPBCnum->size[0];
    emxEnsureCapacity_boolean_T(elems, i25);
    b_loop_ub = NodesOnPBCnum->size[0];
    for (i25 = 0; i25 < b_loop_ub; i25++) {
      elems->data[i25] = (NodesOnPBCnum->data[i25] > 1.0);
    }

    nx = elems->size[0];
    c_idx = 0;
    i25 = ii->size[0];
    ii->size[0] = elems->size[0];
    emxEnsureCapacity_int32_T(ii, i25);
    nb = 0;
    exitg2 = false;
    while ((!exitg2) && (nb <= nx - 1)) {
      if (elems->data[nb]) {
        c_idx++;
        ii->data[c_idx - 1] = nb + 1;
        if (c_idx >= nx) {
          exitg2 = true;
        } else {
          nb++;
        }
      } else {
        nb++;
      }
    }

    if (elems->size[0] == 1) {
      if (c_idx == 0) {
        ii->size[0] = 0;
      }
    } else if (1 > c_idx) {
      ii->size[0] = 0;
    } else {
      i25 = ii->size[0];
      ii->size[0] = c_idx;
      emxEnsureCapacity_int32_T(ii, i25);
    }

    i25 = MorePBC->size[0];
    MorePBC->size[0] = ii->size[0];
    emxEnsureCapacity_real_T(MorePBC, i25);
    b_loop_ub = ii->size[0];
    for (i25 = 0; i25 < b_loop_ub; i25++) {
      MorePBC->data[i25] = ii->data[i25];
    }

    memset(&nodes[0], 0, 100U * sizeof(double));
    i25 = MorePBC->size[0];
    for (b_i = 0; b_i < i25; b_i++) {
      nodes[0] = MorePBC->data[b_i];
      if (NodesOnPBCnum->data[(int)MorePBC->data[b_i] - 1] > 0.0) {
        /*  pairs not already condensed */
        notdone = 1;
        c_idx = 1;

        /*  use a tree of connectivity to get to all node pairs involving MorePBC(i) */
        while (notdone != 0) {
          /*              nodesS = nodes(1:lenn); */
          i1 = (double)c_idx + 1.0;
          numA = (double)c_idx + 1.0;
          for (j = 0; j < c_idx; j++) {
            if (1.0 > NodesOnPBCnum->data[(int)nodes[j] - 1]) {
              b_loop_ub = 0;
            } else {
              b_loop_ub = (int)NodesOnPBCnum->data[(int)nodes[j] - 1];
            }

            numA = (i1 + (double)b_loop_ub) - 1.0;
            if (i1 > numA) {
              i26 = 1;
            } else {
              i26 = (int)i1;
            }

            reg = nodes[j];
            for (i27 = 0; i27 < b_loop_ub; i27++) {
              nodes[(i26 + i27) - 1] = NodesOnPBC->data[i27 + 100 * ((int)reg -
                1)];
            }

            i1 = numA + 1.0;
          }

          if (1.0 > numA) {
            b_loop_ub = 0;
          } else {
            b_loop_ub = (int)numA;
          }

          i26 = b_idx->size[0];
          b_idx->size[0] = b_loop_ub;
          emxEnsureCapacity_int32_T(b_idx, i26);
          for (i26 = 0; i26 < b_loop_ub; i26++) {
            b_idx->data[i26] = 0;
          }

          if (b_loop_ub != 0) {
            i26 = ii->size[0];
            ii->size[0] = b_loop_ub;
            emxEnsureCapacity_int32_T(ii, i26);
            i26 = b_loop_ub - 1;
            for (k = 1; k <= i26; k += 2) {
              if ((nodes[k - 1] <= nodes[k]) || rtIsNaN(nodes[k])) {
                b_idx->data[k - 1] = k;
                b_idx->data[k] = k + 1;
              } else {
                b_idx->data[k - 1] = k + 1;
                b_idx->data[k] = k;
              }
            }

            if ((b_loop_ub & 1) != 0) {
              b_idx->data[b_loop_ub - 1] = b_loop_ub;
            }

            i = 2;
            while (i < b_loop_ub) {
              k0 = i << 1;
              j = 1;
              for (pEnd = 1 + i; pEnd < b_loop_ub + 1; pEnd = nb + i) {
                p = j;
                q = pEnd - 1;
                nb = j + k0;
                if (nb > b_loop_ub + 1) {
                  nb = b_loop_ub + 1;
                }

                k = 0;
                nx = nb - j;
                while (k + 1 <= nx) {
                  if ((nodes[b_idx->data[p - 1] - 1] <= nodes[b_idx->data[q] - 1])
                      || rtIsNaN(nodes[b_idx->data[q] - 1])) {
                    ii->data[k] = b_idx->data[p - 1];
                    p++;
                    if (p == pEnd) {
                      while (q + 1 < nb) {
                        k++;
                        ii->data[k] = b_idx->data[q];
                        q++;
                      }
                    }
                  } else {
                    ii->data[k] = b_idx->data[q];
                    q++;
                    if (q + 1 == nb) {
                      while (p < pEnd) {
                        k++;
                        ii->data[k] = b_idx->data[p - 1];
                        p++;
                      }
                    }
                  }

                  k++;
                }

                for (k = 0; k < nx; k++) {
                  b_idx->data[(j + k) - 1] = ii->data[k];
                }

                j = nb;
              }

              i = k0;
            }
          }

          i26 = idx->size[0];
          idx->size[0] = (signed char)b_loop_ub;
          emxEnsureCapacity_real_T(idx, i26);
          for (k = 0; k < b_loop_ub; k++) {
            idx->data[k] = nodes[b_idx->data[k] - 1];
          }

          k = 0;
          while ((k + 1 <= b_loop_ub) && rtIsInf(idx->data[k]) && (idx->data[k] <
                  0.0)) {
            k++;
          }

          nx = k;
          k = b_loop_ub - 1;
          while ((k + 1 >= 1) && rtIsNaN(idx->data[k])) {
            k--;
          }

          q = b_loop_ub - k;
          while ((k + 1 >= 1) && rtIsInf(idx->data[k]) && (idx->data[k] > 0.0))
          {
            k--;
          }

          pEnd = (b_loop_ub - k) - q;
          nb = -1;
          if (nx > 0) {
            nb = 0;
          }

          while (nx + 1 <= k + 1) {
            i1 = idx->data[nx];
            do {
              exitg1 = 0;
              nx++;
              if (nx + 1 > k + 1) {
                exitg1 = 1;
              } else {
                absxk = fabs(i1 / 2.0);
                if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
                  if (absxk <= 2.2250738585072014E-308) {
                    absxk = 4.94065645841247E-324;
                  } else {
                    frexp(absxk, &nel2);
                    absxk = ldexp(1.0, nel2 - 53);
                  }
                } else {
                  absxk = rtNaN;
                }

                if ((fabs(i1 - idx->data[nx]) < absxk) || (rtIsInf(idx->data[nx])
                     && rtIsInf(i1) && ((idx->data[nx] > 0.0) == (i1 > 0.0)))) {
                } else {
                  exitg1 = 1;
                }
              }
            } while (exitg1 == 0);

            nb++;
            idx->data[nb] = i1;
          }

          if (pEnd > 0) {
            nb++;
            idx->data[nb] = idx->data[k + 1];
          }

          nx = (k + pEnd) + 1;
          for (j = 0; j <= q - 2; j++) {
            nb++;
            idx->data[nb] = idx->data[nx + j];
          }

          if (1 > nb + 1) {
            idx->size[0] = 0;
          } else {
            i26 = idx->size[0];
            idx->size[0] = nb + 1;
            emxEnsureCapacity_real_T(idx, i26);
          }

          if (idx->size[0] == c_idx) {
            /*  starting list of node pairs matches the unique short list */
            b_loop_ub = idx->size[0];
            for (i26 = 0; i26 < b_loop_ub; i26++) {
              nodes[i26] = idx->data[i26];
            }

            notdone = 0;
          } else {
            /*  found some new pairs, try searching again */
            b_loop_ub = idx->size[0];
            for (i26 = 0; i26 < b_loop_ub; i26++) {
              nodes[i26] = idx->data[i26];
            }
          }

          c_idx = idx->size[0];
        }

        if (c_idx + 1U > 100U) {
          i26 = 0;
          i27 = -1;
        } else {
          i26 = c_idx;
          i27 = 99;
        }

        b_loop_ub = (i27 - i26) + 1;
        if (0 <= b_loop_ub - 1) {
          memset(&nodes[i26], 0, (unsigned int)(b_loop_ub * (int)sizeof(double)));
        }

        /* clean it out */
        /*  record the maximum connected pairs into the NodesOnPBC for all */
        /*  the connected nodes in the set; also combine together the */
        /*  NodesOnLink at the same time */
        /*          lenn = length(nodes); */
        memset(&links[0], 0, 100U * sizeof(double));
        i1 = 1.0;
        numA = 1.0;
        for (j = 0; j < c_idx; j++) {
          i26 = (int)nodes[j] - 1;
          NodesOnPBCnum->data[i26] = -((double)c_idx - 1.0);

          /*  flag this node as already condensed */
          if (1 > c_idx) {
            b_loop_ub = 0;
          } else {
            b_loop_ub = c_idx;
          }

          nodes_size[0] = b_loop_ub;
          if (0 <= b_loop_ub - 1) {
            memcpy(&nodes_data[0], &nodes[0], (unsigned int)(b_loop_ub * (int)
                    sizeof(double)));
          }

          do_vectors(nodes_data, nodes_size, nodes[j], links2_data, links2_size,
                     ia_data, ia_size, ib_size);
          b_loop_ub = links2_size[0];
          for (i27 = 0; i27 < b_loop_ub; i27++) {
            NodesOnPBC->data[i27 + 100 * i26] = links2_data[i27];
          }

          numA = (i1 + NodesOnLinknum->data[i26]) - 1.0;
          if (1.0 > NodesOnLinknum->data[(int)nodes[j] - 1]) {
            b_loop_ub = 0;
          } else {
            b_loop_ub = (int)NodesOnLinknum->data[(int)nodes[j] - 1];
          }

          if (i1 > numA) {
            i26 = 1;
          } else {
            i26 = (int)i1;
          }

          for (i27 = 0; i27 < b_loop_ub; i27++) {
            links[(i26 + i27) - 1] = NodesOnLink->data[i27 + 100 * ((int)nodes[j]
              - 1)];
          }

          i1 = numA + 1.0;
        }

        if (1.0 > numA) {
          b_loop_ub = 0;
        } else {
          b_loop_ub = (int)numA;
        }

        i26 = b_idx->size[0];
        b_idx->size[0] = b_loop_ub;
        emxEnsureCapacity_int32_T(b_idx, i26);
        for (i26 = 0; i26 < b_loop_ub; i26++) {
          b_idx->data[i26] = 0;
        }

        if (b_loop_ub != 0) {
          i26 = ii->size[0];
          ii->size[0] = b_loop_ub;
          emxEnsureCapacity_int32_T(ii, i26);
          i26 = b_loop_ub - 1;
          for (k = 1; k <= i26; k += 2) {
            if ((links[k - 1] <= links[k]) || rtIsNaN(links[k])) {
              b_idx->data[k - 1] = k;
              b_idx->data[k] = k + 1;
            } else {
              b_idx->data[k - 1] = k + 1;
              b_idx->data[k] = k;
            }
          }

          if ((b_loop_ub & 1) != 0) {
            b_idx->data[b_loop_ub - 1] = b_loop_ub;
          }

          i = 2;
          while (i < b_loop_ub) {
            k0 = i << 1;
            j = 1;
            for (pEnd = 1 + i; pEnd < b_loop_ub + 1; pEnd = nb + i) {
              p = j;
              q = pEnd - 1;
              nb = j + k0;
              if (nb > b_loop_ub + 1) {
                nb = b_loop_ub + 1;
              }

              k = 0;
              nx = nb - j;
              while (k + 1 <= nx) {
                if ((links[b_idx->data[p - 1] - 1] <= links[b_idx->data[q] - 1])
                    || rtIsNaN(links[b_idx->data[q] - 1])) {
                  ii->data[k] = b_idx->data[p - 1];
                  p++;
                  if (p == pEnd) {
                    while (q + 1 < nb) {
                      k++;
                      ii->data[k] = b_idx->data[q];
                      q++;
                    }
                  }
                } else {
                  ii->data[k] = b_idx->data[q];
                  q++;
                  if (q + 1 == nb) {
                    while (p < pEnd) {
                      k++;
                      ii->data[k] = b_idx->data[p - 1];
                      p++;
                    }
                  }
                }

                k++;
              }

              for (k = 0; k < nx; k++) {
                b_idx->data[(j + k) - 1] = ii->data[k];
              }

              j = nb;
            }

            i = k0;
          }
        }

        i26 = idx->size[0];
        idx->size[0] = (signed char)b_loop_ub;
        emxEnsureCapacity_real_T(idx, i26);
        for (k = 0; k < b_loop_ub; k++) {
          idx->data[k] = links[b_idx->data[k] - 1];
        }

        k = 0;
        while ((k + 1 <= b_loop_ub) && rtIsInf(idx->data[k]) && (idx->data[k] <
                0.0)) {
          k++;
        }

        nx = k;
        k = b_loop_ub - 1;
        while ((k + 1 >= 1) && rtIsNaN(idx->data[k])) {
          k--;
        }

        q = b_loop_ub - k;
        while ((k + 1 >= 1) && rtIsInf(idx->data[k]) && (idx->data[k] > 0.0)) {
          k--;
        }

        pEnd = (b_loop_ub - k) - q;
        nb = -1;
        if (nx > 0) {
          nb = 0;
        }

        while (nx + 1 <= k + 1) {
          i1 = idx->data[nx];
          do {
            exitg1 = 0;
            nx++;
            if (nx + 1 > k + 1) {
              exitg1 = 1;
            } else {
              absxk = fabs(i1 / 2.0);
              if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
                if (absxk <= 2.2250738585072014E-308) {
                  absxk = 4.94065645841247E-324;
                } else {
                  frexp(absxk, &exponent);
                  absxk = ldexp(1.0, exponent - 53);
                }
              } else {
                absxk = rtNaN;
              }

              if ((fabs(i1 - idx->data[nx]) < absxk) || (rtIsInf(idx->data[nx]) &&
                   rtIsInf(i1) && ((idx->data[nx] > 0.0) == (i1 > 0.0)))) {
              } else {
                exitg1 = 1;
              }
            }
          } while (exitg1 == 0);

          nb++;
          idx->data[nb] = i1;
        }

        if (pEnd > 0) {
          nb++;
          idx->data[nb] = idx->data[k + 1];
        }

        nx = (k + pEnd) + 1;
        for (j = 0; j <= q - 2; j++) {
          nb++;
          idx->data[nb] = idx->data[nx + j];
        }

        if (1 > nb + 1) {
          idx->size[0] = 0;
        } else {
          i26 = idx->size[0];
          idx->size[0] = nb + 1;
          emxEnsureCapacity_real_T(idx, i26);
        }

        b_loop_ub = idx->size[0];
        for (i26 = 0; i26 < b_loop_ub; i26++) {
          links2_data[i26] = idx->data[i26];
        }

        k0 = idx->size[0] - 1;
        nx = 0;
        for (i = 0; i <= k0; i++) {
          if (idx->data[i] != 0.0) {
            nx++;
          }
        }

        nb = 0;
        for (i = 0; i <= k0; i++) {
          if (links2_data[i] != 0.0) {
            links2_data[nb] = links2_data[i];
            nb++;
          }
        }

        for (i26 = 0; i26 < nx; i26++) {
          for (i27 = 0; i27 < c_idx; i27++) {
            NodesOnLink->data[i26 + 100 * ((int)nodes[i27] - 1)] =
              links2_data[i26];
          }
        }

        if (1 > c_idx) {
          b_loop_ub = 0;
        } else {
          b_loop_ub = c_idx;
        }

        for (i26 = 0; i26 < b_loop_ub; i26++) {
          ia_data[i26] = (int)nodes[i26];
        }

        for (i26 = 0; i26 < b_loop_ub; i26++) {
          NodesOnLinknum->data[ia_data[i26] - 1] = (signed char)nx;
        }
      }
    }

    emxInit_real_T(&b_NodesOnPBCnum, 1);
    i25 = b_NodesOnPBCnum->size[0];
    b_NodesOnPBCnum->size[0] = NodesOnPBCnum->size[0];
    emxEnsureCapacity_real_T(b_NodesOnPBCnum, i25);
    b_loop_ub = NodesOnPBCnum->size[0];
    for (i25 = 0; i25 < b_loop_ub; i25++) {
      b_NodesOnPBCnum->data[i25] = NodesOnPBCnum->data[i25];
    }

    b_abs(b_NodesOnPBCnum, NodesOnPBCnum);
    if (usePBC != 2.0) {
      /*  add in extra PBC on nodes with 3 links, so that ghost elements can always */
      /*  find their direction */
      i25 = elems->size[0];
      elems->size[0] = NodesOnPBCnum->size[0];
      emxEnsureCapacity_boolean_T(elems, i25);
      b_loop_ub = NodesOnPBCnum->size[0];
      for (i25 = 0; i25 < b_loop_ub; i25++) {
        elems->data[i25] = (NodesOnPBCnum->data[i25] == 3.0);
      }

      nx = elems->size[0];
      c_idx = 0;
      i25 = ii->size[0];
      ii->size[0] = elems->size[0];
      emxEnsureCapacity_int32_T(ii, i25);
      nb = 0;
      exitg2 = false;
      while ((!exitg2) && (nb <= nx - 1)) {
        if (elems->data[nb]) {
          c_idx++;
          ii->data[c_idx - 1] = nb + 1;
          if (c_idx >= nx) {
            exitg2 = true;
          } else {
            nb++;
          }
        } else {
          nb++;
        }
      }

      if (elems->size[0] == 1) {
        if (c_idx == 0) {
          ii->size[0] = 0;
        }
      } else if (1 > c_idx) {
        ii->size[0] = 0;
      } else {
        i25 = ii->size[0];
        ii->size[0] = c_idx;
        emxEnsureCapacity_int32_T(ii, i25);
      }

      i25 = MorePBC->size[0];
      MorePBC->size[0] = ii->size[0];
      emxEnsureCapacity_real_T(MorePBC, i25);
      b_loop_ub = ii->size[0];
      for (i25 = 0; i25 < b_loop_ub; i25++) {
        MorePBC->data[i25] = ii->data[i25];
      }

      i25 = PBCList2->size[0] * PBCList2->size[1];
      PBCList2->size[0] = (int)((double)MorePBC->size[0] / 4.0);
      PBCList2->size[1] = 4;
      emxEnsureCapacity_real_T(PBCList2, i25);
      b_loop_ub = (int)((double)MorePBC->size[0] / 4.0) << 2;
      for (i25 = 0; i25 < b_loop_ub; i25++) {
        PBCList2->data[i25] = 0.0;
      }

      numA = numPBC;
      i25 = MorePBC->size[0];
      emxInit_int32_T(&b_s, 1);
      for (b_i = 0; b_i < i25; b_i++) {
        if (NodesOnPBCnum->data[(int)MorePBC->data[b_i] - 1] > 0.0) {
          /*  pairs not already condensed */
          if (1.0 > NodesOnLinknum->data[(int)MorePBC->data[b_i] - 1]) {
            b_loop_ub = 0;
          } else {
            b_loop_ub = (int)NodesOnLinknum->data[(int)MorePBC->data[b_i] - 1];
          }

          k0 = (int)MorePBC->data[b_i];
          for (i26 = 0; i26 < 5; i26++) {
            for (i27 = 0; i27 < b_loop_ub; i27++) {
              locPBC_data[i27 + b_loop_ub * i26] = MPCList->data[((int)
                NodesOnLink->data[i27 + 100 * (k0 - 1)] + MPCList->size[0] * i26)
                - 1];
            }
          }

          i26 = idx->size[0];
          idx->size[0] = b_loop_ub;
          emxEnsureCapacity_real_T(idx, i26);
          for (i26 = 0; i26 < b_loop_ub; i26++) {
            idx->data[i26] = locPBC_data[i26];
          }

          i26 = s->size[0];
          s->size[0] = b_loop_ub;
          emxEnsureCapacity_real_T(s, i26);
          for (i26 = 0; i26 < b_loop_ub; i26++) {
            s->data[i26] = locPBC_data[i26 + b_loop_ub];
          }

          i26 = b_loop_ub - 1;
          i27 = elems->size[0];
          elems->size[0] = b_loop_ub;
          emxEnsureCapacity_boolean_T(elems, i27);
          for (i27 = 0; i27 < b_loop_ub; i27++) {
            elems->data[i27] = false;
          }

          guard1 = false;
          if (b_loop_ub <= 4) {
            guard1 = true;
          } else {
            pEnd = 31;
            k0 = 0;
            exitg2 = false;
            while ((!exitg2) && (pEnd - k0 > 1)) {
              p = (k0 + pEnd) >> 1;
              nx = 1 << p;
              if (nx == b_loop_ub) {
                pEnd = p;
                exitg2 = true;
              } else if (nx > b_loop_ub) {
                pEnd = p;
              } else {
                k0 = p;
              }
            }

            if (b_loop_ub <= 4 + pEnd) {
              guard1 = true;
            } else {
              nodes_size[0] = b_loop_ub;
              for (i27 = 0; i27 < b_loop_ub; i27++) {
                nodes_data[i27] = locPBC_data[i27 + b_loop_ub];
              }

              b_nodes_data.data = &nodes_data[0];
              b_nodes_data.size = &nodes_size[0];
              b_nodes_data.allocatedSize = 100;
              b_nodes_data.numDimensions = 1;
              b_nodes_data.canFreeData = false;
              if (!issorted(&b_nodes_data)) {
                sort(s, b_s);
                for (k = 0; k <= i26; k++) {
                  if (bsearchni(k + 1, idx, s) > 0) {
                    elems->data[k] = true;
                  }
                }
              } else {
                for (k = 0; k <= i26; k++) {
                  if (bsearchni(k + 1, idx, s) > 0) {
                    elems->data[k] = true;
                  }
                }
              }
            }
          }

          if (guard1) {
            for (j = 0; j <= i26; j++) {
              k = 0;
              exitg2 = false;
              while ((!exitg2) && (k <= b_loop_ub - 1)) {
                k0 = k + b_loop_ub;
                absxk = fabs(locPBC_data[k0] / 2.0);
                if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
                  if (absxk <= 2.2250738585072014E-308) {
                    absxk = 4.94065645841247E-324;
                  } else {
                    frexp(absxk, &b_exponent);
                    absxk = ldexp(1.0, b_exponent - 53);
                  }
                } else {
                  absxk = rtNaN;
                }

                if ((fabs(locPBC_data[k0] - locPBC_data[j]) < absxk) || (rtIsInf
                     (locPBC_data[j]) && rtIsInf(locPBC_data[k0]) &&
                     ((locPBC_data[j] > 0.0) == (locPBC_data[k0] > 0.0)))) {
                  elems->data[j] = true;
                  exitg2 = true;
                } else {
                  k++;
                }
              }
            }
          }

          k0 = elems->size[0] - 1;
          nx = 0;
          for (i = 0; i <= k0; i++) {
            if (!elems->data[i]) {
              nx++;
            }
          }

          nb = 0;
          for (i = 0; i <= k0; i++) {
            if (!elems->data[i]) {
              tmp_data[nb] = i + 1;
              nb++;
            }
          }

          for (i26 = 0; i26 < nx; i26++) {
            otherP_data[i26] = locPBC_data[tmp_data[i26] - 1];
          }

          for (i26 = 0; i26 < nx; i26++) {
            otherP_data[i26 + nx] = locPBC_data[(tmp_data[i26] + b_loop_ub) - 1];
          }

          k0 = elems->size[0] - 1;
          nx = 0;
          for (i = 0; i <= k0; i++) {
            if (elems->data[i]) {
              nx++;
            }
          }

          nb = 0;
          for (i = 0; i <= k0; i++) {
            if (elems->data[i]) {
              b_tmp_data[nb] = i + 1;
              nb++;
            }
          }

          if (locPBC_data[(tmp_data[0] + b_loop_ub) - 1] == locPBC_data
              [(b_tmp_data[1 % nx] + b_loop_ub * (1 / nx)) - 1]) {
            k0 = elems->size[0] - 1;
            nx = 0;
            for (i = 0; i <= k0; i++) {
              if (elems->data[i]) {
                nx++;
              }
            }

            nb = 0;
            for (i = 0; i <= k0; i++) {
              if (elems->data[i]) {
                e_tmp_data[nb] = i + 1;
                nb++;
              }
            }

            i1 = otherP_data[1];
            absxk = otherP_data[0];
            for (i26 = 0; i26 < nx; i26++) {
              otherP_data[i26] = locPBC_data[(e_tmp_data[i26] + b_loop_ub * 2) -
                1];
            }

            link_data[0] = i1;
            for (i26 = 0; i26 < nx; i26++) {
              otherP_data[i26 + nx] = locPBC_data[(e_tmp_data[i26] + b_loop_ub *
                3) - 1];
            }

            link_data[1] = absxk;
            if (nx != 0) {
              b_loop_ub = 2;
            } else {
              b_loop_ub = 0;
            }

            if (0 <= b_loop_ub - 1) {
              memcpy(&link_data[2], &otherP_data[0], (unsigned int)(b_loop_ub *
                      (int)sizeof(double)));
            }
          } else {
            k0 = elems->size[0] - 1;
            nx = 0;
            for (i = 0; i <= k0; i++) {
              if (elems->data[i]) {
                nx++;
              }
            }

            nb = 0;
            for (i = 0; i <= k0; i++) {
              if (elems->data[i]) {
                d_tmp_data[nb] = i + 1;
                nb++;
              }
            }

            i1 = otherP_data[0];
            absxk = otherP_data[1];
            for (i26 = 0; i26 < nx; i26++) {
              otherP_data[i26] = locPBC_data[(d_tmp_data[i26] + b_loop_ub * 2) -
                1];
            }

            link_data[0] = i1;
            for (i26 = 0; i26 < nx; i26++) {
              otherP_data[i26 + nx] = locPBC_data[(d_tmp_data[i26] + b_loop_ub *
                3) - 1];
            }

            link_data[1] = absxk;
            if (nx != 0) {
              b_loop_ub = 2;
            } else {
              b_loop_ub = 0;
            }

            if (0 <= b_loop_ub - 1) {
              memcpy(&link_data[2], &otherP_data[0], (unsigned int)(b_loop_ub *
                      (int)sizeof(double)));
            }
          }

          if (1.0 > NodesOnPBCnum->data[(int)MorePBC->data[b_i] - 1]) {
            b_loop_ub = 0;
          } else {
            b_loop_ub = (int)NodesOnPBCnum->data[(int)MorePBC->data[b_i] - 1];
          }

          k0 = (int)MorePBC->data[b_i];
          allnodes_data[0] = MorePBC->data[b_i];
          for (i26 = 0; i26 < b_loop_ub; i26++) {
            allnodes_data[i26 + 1] = NodesOnPBC->data[i26 + 100 * (k0 - 1)];
          }

          numA++;
          i26 = (int)(numA - (double)numPBC) - 1;
          PBCList2->data[i26] = link_data[0];
          PBCList2->data[i26 + PBCList2->size[0]] = link_data[1];
          PBCList2->data[i26 + (PBCList2->size[0] << 1)] = link_data[2];
          PBCList2->data[i26 + PBCList2->size[0] * 3] = link_data[3];
          nb = b_loop_ub + 1;
          for (i26 = 0; i26 < nb; i26++) {
            f_tmp_data[i26] = (int)allnodes_data[i26] - 1;
          }

          newPBC[0] = numA;
          newPBC[1] = numA;
          newPBC[2] = numA;
          newPBC[3] = numA;
          nb = b_loop_ub + 1;
          for (i26 = 0; i26 < nb; i26++) {
            NodesOnLink->data[3 + 100 * f_tmp_data[i26]] = newPBC[i26];
          }

          nb = b_loop_ub + 1;
          for (i26 = 0; i26 < nb; i26++) {
            f_tmp_data[i26] = (int)allnodes_data[i26];
          }

          nb = b_loop_ub + 1;
          for (i26 = 0; i26 < nb; i26++) {
            NodesOnPBCnum->data[f_tmp_data[i26] - 1] = -4.0;
          }

          nb = b_loop_ub + 1;
          for (i26 = 0; i26 < nb; i26++) {
            f_tmp_data[i26] = (int)allnodes_data[i26];
          }

          b_loop_ub++;
          for (i26 = 0; i26 < b_loop_ub; i26++) {
            NodesOnLinknum->data[f_tmp_data[i26] - 1] = 4.0;
          }
        }
      }

      emxFree_int32_T(&b_s);
      i25 = b_NodesOnPBCnum->size[0];
      b_NodesOnPBCnum->size[0] = NodesOnPBCnum->size[0];
      emxEnsureCapacity_real_T(b_NodesOnPBCnum, i25);
      b_loop_ub = NodesOnPBCnum->size[0];
      for (i25 = 0; i25 < b_loop_ub; i25++) {
        b_NodesOnPBCnum->data[i25] = NodesOnPBCnum->data[i25];
      }

      b_abs(b_NodesOnPBCnum, NodesOnPBCnum);
      if (MPCList->size[0] != 0) {
        k0 = MPCList->size[0];
      } else {
        k0 = 0;
      }

      if (PBCList2->size[0] != 0) {
        pEnd = PBCList2->size[0];
      } else {
        pEnd = 0;
      }

      i25 = PBCList->size[0] * PBCList->size[1];
      PBCList->size[0] = k0 + pEnd;
      PBCList->size[1] = 5;
      emxEnsureCapacity_real_T(PBCList, i25);
      for (i25 = 0; i25 < 5; i25++) {
        for (i26 = 0; i26 < k0; i26++) {
          PBCList->data[i26 + PBCList->size[0] * i25] = MPCList->data[i26 +
            MPCList->size[0] * i25];
        }
      }

      for (i25 = 0; i25 < 5; i25++) {
        for (i26 = 0; i26 < pEnd; i26++) {
          PBCList->data[(i26 + k0) + PBCList->size[0] * i25] = PBCList2->
            data[i26 + pEnd * i25];
        }
      }
    } else {
      /*  add in extra PBC on nodes with 3 links, so that ghost elements can always */
      /*  find their direction */
      i25 = elems->size[0];
      elems->size[0] = NodesOnPBCnum->size[0];
      emxEnsureCapacity_boolean_T(elems, i25);
      b_loop_ub = NodesOnPBCnum->size[0];
      for (i25 = 0; i25 < b_loop_ub; i25++) {
        elems->data[i25] = (NodesOnPBCnum->data[i25] > 1.0);
      }

      nx = elems->size[0];
      c_idx = 0;
      i25 = ii->size[0];
      ii->size[0] = elems->size[0];
      emxEnsureCapacity_int32_T(ii, i25);
      nb = 0;
      exitg2 = false;
      while ((!exitg2) && (nb <= nx - 1)) {
        if (elems->data[nb]) {
          c_idx++;
          ii->data[c_idx - 1] = nb + 1;
          if (c_idx >= nx) {
            exitg2 = true;
          } else {
            nb++;
          }
        } else {
          nb++;
        }
      }

      if (elems->size[0] == 1) {
        if (c_idx == 0) {
          ii->size[0] = 0;
        }
      } else if (1 > c_idx) {
        ii->size[0] = 0;
      } else {
        i25 = ii->size[0];
        ii->size[0] = c_idx;
        emxEnsureCapacity_int32_T(ii, i25);
      }

      i25 = MorePBC->size[0];
      MorePBC->size[0] = ii->size[0];
      emxEnsureCapacity_real_T(MorePBC, i25);
      b_loop_ub = ii->size[0];
      for (i25 = 0; i25 < b_loop_ub; i25++) {
        MorePBC->data[i25] = ii->data[i25];
      }

      emxInit_real_T(&b_PBCList2, 2);
      i25 = b_PBCList2->size[0] * b_PBCList2->size[1];
      b_PBCList2->size[0] = 3 * MorePBC->size[0];
      b_PBCList2->size[1] = 5;
      emxEnsureCapacity_real_T(b_PBCList2, i25);
      b_loop_ub = 3 * MorePBC->size[0] * 5;
      for (i25 = 0; i25 < b_loop_ub; i25++) {
        b_PBCList2->data[i25] = 0.0;
      }

      numA = numPBC;
      i25 = MorePBC->size[0];
      emxInit_real_T(&newpairs, 2);
      for (b_i = 0; b_i < i25; b_i++) {
        i1 = NodesOnPBCnum->data[(int)MorePBC->data[b_i] - 1];
        if (NodesOnPBCnum->data[(int)MorePBC->data[b_i] - 1] > 0.0) {
          /*  pairs not already condensed */
          if (1.0 > NodesOnLinknum->data[(int)MorePBC->data[b_i] - 1]) {
            b_loop_ub = 0;
          } else {
            b_loop_ub = (int)NodesOnLinknum->data[(int)MorePBC->data[b_i] - 1];
          }

          nb = (int)NodesOnPBCnum->data[(int)MorePBC->data[b_i] - 1];
          k0 = (int)MorePBC->data[b_i];
          allnodes_size[0] = 1;
          allnodes_size[1] = nb + 1;
          allnodes_data[0] = MorePBC->data[b_i];
          for (i26 = 0; i26 < nb; i26++) {
            allnodes_data[i26 + 1] = NodesOnPBC->data[i26 + 100 * (k0 - 1)];
          }

          b_sort(allnodes_data, allnodes_size);
          nchoosek(allnodes_data, allnodes_size, pairs);
          nb = pairs->size[0] - 1;
          i26 = b_MPCList->size[0] * b_MPCList->size[1];
          b_MPCList->size[0] = nb + 1;
          b_MPCList->size[1] = 2;
          emxEnsureCapacity_real_T(b_MPCList, i26);
          for (i26 = 0; i26 <= nb; i26++) {
            b_MPCList->data[i26] = pairs->data[i26 + pairs->size[0]];
          }

          for (i26 = 0; i26 <= nb; i26++) {
            b_MPCList->data[i26 + b_MPCList->size[0]] = pairs->data[i26];
          }

          i26 = pairs->size[0] * pairs->size[1];
          pairs->size[0] = b_MPCList->size[0];
          pairs->size[1] = 2;
          emxEnsureCapacity_real_T(pairs, i26);
          nb = b_MPCList->size[0] * b_MPCList->size[1];
          for (i26 = 0; i26 < nb; i26++) {
            pairs->data[i26] = b_MPCList->data[i26];
          }

          sortrows(pairs);
          k0 = (int)MorePBC->data[b_i];
          for (i26 = 0; i26 < 5; i26++) {
            for (i27 = 0; i27 < b_loop_ub; i27++) {
              locPBC_data[i27 + b_loop_ub * i26] = MPCList->data[((int)
                NodesOnLink->data[i27 + 100 * (k0 - 1)] + MPCList->size[0] * i26)
                - 1];
            }
          }

          i26 = b_MPCList->size[0] * b_MPCList->size[1];
          b_MPCList->size[0] = b_loop_ub;
          b_MPCList->size[1] = 2;
          emxEnsureCapacity_real_T(b_MPCList, i26);
          for (i26 = 0; i26 < b_loop_ub; i26++) {
            b_MPCList->data[i26] = locPBC_data[i26];
          }

          for (i26 = 0; i26 < b_loop_ub; i26++) {
            b_MPCList->data[i26 + b_MPCList->size[0]] = locPBC_data[i26 +
              b_loop_ub];
          }

          sortrows(b_MPCList);
          do_rows(pairs, b_MPCList->data, b_MPCList->size, newpairs, b_idx,
                  ib_size);
          i26 = newpairs->size[0];
          for (q = 0; q < i26; q++) {
            numA++;
            k0 = (int)newpairs->data[q];
            pEnd = (int)newpairs->data[q + newpairs->size[0]];
            nx = (int)(numA - (double)numPBC) - 1;
            b_PBCList2->data[nx] = newpairs->data[q];
            b_PBCList2->data[nx + b_PBCList2->size[0]] = newpairs->data[q +
              newpairs->size[0]];
            b_PBCList2->data[nx + b_PBCList2->size[0] * 2] = Coordinates->
              data[k0 - 1] - Coordinates->data[pEnd - 1];
            b_PBCList2->data[nx + b_PBCList2->size[0] * 3] = Coordinates->data
              [(k0 + Coordinates->size[0]) - 1] - Coordinates->data[(pEnd +
              Coordinates->size[0]) - 1];
            b_PBCList2->data[nx + b_PBCList2->size[0] * 4] = Coordinates->data
              [(k0 + (Coordinates->size[0] << 1)) - 1] - Coordinates->data[(pEnd
              + (Coordinates->size[0] << 1)) - 1];
            i27 = (int)(i1 + (1.0 + (double)q)) - 1;
            b_loop_ub = allnodes_size[1];
            for (b_exponent = 0; b_exponent < b_loop_ub; b_exponent++) {
              f_tmp_data[b_exponent] = (int)allnodes_data[b_exponent] - 1;
            }

            b_loop_ub = allnodes_size[1];
            for (b_exponent = 0; b_exponent < b_loop_ub; b_exponent++) {
              NodesOnLink->data[i27 + 100 * f_tmp_data[b_exponent]] = numA;
            }
          }

          i = allnodes_size[0] * allnodes_size[1];
          for (i26 = 0; i26 < i; i26++) {
            c_tmp_data[i26] = (int)allnodes_data[i26];
          }

          b_loop_ub = allnodes_size[1];
          for (i26 = 0; i26 < b_loop_ub; i26++) {
            NodesOnPBCnum->data[c_tmp_data[i26] - 1] = (signed char)-
              allnodes_size[1];
          }

          for (i26 = 0; i26 < i; i26++) {
            c_tmp_data[i26] = (int)allnodes_data[i26];
          }

          b_loop_ub = allnodes_size[1];
          for (i26 = 0; i26 < b_loop_ub; i26++) {
            NodesOnLinknum->data[c_tmp_data[i26] - 1] = (double)newpairs->size[0]
              + i1;
          }
        }
      }

      emxFree_real_T(&newpairs);
      i25 = b_NodesOnPBCnum->size[0];
      b_NodesOnPBCnum->size[0] = NodesOnPBCnum->size[0];
      emxEnsureCapacity_real_T(b_NodesOnPBCnum, i25);
      b_loop_ub = NodesOnPBCnum->size[0];
      for (i25 = 0; i25 < b_loop_ub; i25++) {
        b_NodesOnPBCnum->data[i25] = NodesOnPBCnum->data[i25];
      }

      b_abs(b_NodesOnPBCnum, NodesOnPBCnum);
      i25 = PBCList->size[0] * PBCList->size[1];
      PBCList->size[0] = MPCList->size[0] + b_PBCList2->size[0];
      PBCList->size[1] = 5;
      emxEnsureCapacity_real_T(PBCList, i25);
      b_loop_ub = MPCList->size[0];
      for (i25 = 0; i25 < 5; i25++) {
        for (i26 = 0; i26 < b_loop_ub; i26++) {
          PBCList->data[i26 + PBCList->size[0] * i25] = MPCList->data[i26 +
            MPCList->size[0] * i25];
        }
      }

      for (i25 = 0; i25 < 5; i25++) {
        b_loop_ub = b_PBCList2->size[0];
        for (i26 = 0; i26 < b_loop_ub; i26++) {
          PBCList->data[(i26 + MPCList->size[0]) + PBCList->size[0] * i25] =
            b_PBCList2->data[i26 + b_PBCList2->size[0] * i25];
        }
      }

      emxFree_real_T(&b_PBCList2);
    }

    emxFree_real_T(&b_NodesOnPBCnum);
    emxFree_real_T(&pairs);

    /*  Get some memory back */
    /*      NodesOnPBC = sparse(NodesOnPBC); % nodes that are paired up */
    /*      NodesOnLink = sparse(NodesOnLink); % IDs of PBC referring to node */
  } else {
    NodesOnPBC->size[0] = 100;
    NodesOnPBC->size[1] = 0;

    /*  nodes that are paired up */
    NodesOnPBCnum->size[0] = 0;

    /*  number of nodes with common pairing */
    NodesOnLink->size[0] = 100;
    NodesOnLink->size[1] = 0;

    /*  IDs of PBC referring to node */
    NodesOnLinknum->size[0] = 0;

    /*  number of PBC referring to node */
  }

  /*  Mesh error checks */
  /*  Error checks for mesh arrays */
  /*  Nodes */
  /*  Elements */
  /*  Check that interfaces are assigned also when intraface couplers are requested */
  nx = InterTypes->size[0] * InterTypes->size[1];
  c_idx = 0;
  i25 = ii->size[0];
  ii->size[0] = nx;
  emxEnsureCapacity_int32_T(ii, i25);
  nb = 0;
  exitg2 = false;
  while ((!exitg2) && (nb <= nx - 1)) {
    if (InterTypes->data[nb] != 0.0) {
      c_idx++;
      ii->data[c_idx - 1] = nb + 1;
      if (c_idx >= nx) {
        exitg2 = true;
      } else {
        nb++;
      }
    } else {
      nb++;
    }
  }

  if (nx == 1) {
    if (c_idx == 0) {
      ii->size[0] = 0;
    }
  } else if (1 > c_idx) {
    ii->size[0] = 0;
  } else {
    i25 = ii->size[0];
    ii->size[0] = c_idx;
    emxEnsureCapacity_int32_T(ii, i25);
  }

  /*  reset to logical */
  b_loop_ub = ii->size[0] - 1;
  for (i25 = 0; i25 <= b_loop_ub; i25++) {
    InterTypes->data[ii->data[i25] - 1] = 1.0;
  }

  reg = rt_roundd_snf(nummat);
  if (reg < 9.2233720368547758E+18) {
    if (reg >= -9.2233720368547758E+18) {
      q0 = (long long)reg;
    } else {
      q0 = MIN_int64_T;
    }
  } else if (reg >= 9.2233720368547758E+18) {
    q0 = MAX_int64_T;
  } else {
    q0 = 0LL;
  }

  if (q0 < -9223372036854775807LL) {
    qY = MIN_int64_T;
  } else {
    qY = q0 - 1LL;
  }

  mat = 1LL;
  emxInit_int64_T(&inter, 1);
  emxInit_real_T(&setAll, 2);
  emxInit_int32_T(&b_ii, 2);
  while (mat <= qY) {
    if (InterTypes->data[((int)mat + InterTypes->size[0] * ((int)mat - 1)) - 1]
        != 0.0) {
      if (mat > 9223372036854775806LL) {
        b_qY = MAX_int64_T;
      } else {
        b_qY = mat + 1LL;
      }

      if (b_qY > q0) {
        i25 = 0;
        i26 = 0;
      } else {
        i25 = (int)b_qY - 1;
        i26 = (int)q0;
      }

      i27 = idx->size[0];
      b_loop_ub = i26 - i25;
      idx->size[0] = b_loop_ub;
      emxEnsureCapacity_real_T(idx, i27);
      for (i26 = 0; i26 < b_loop_ub; i26++) {
        idx->data[i26] = InterTypes->data[(i25 + i26) + InterTypes->size[0] *
          ((int)mat - 1)] - 1.0;
      }

      nx = idx->size[0];
      c_idx = 0;
      i25 = ii->size[0];
      ii->size[0] = idx->size[0];
      emxEnsureCapacity_int32_T(ii, i25);
      nb = 0;
      exitg2 = false;
      while ((!exitg2) && (nb <= nx - 1)) {
        if (idx->data[nb] != 0.0) {
          c_idx++;
          ii->data[c_idx - 1] = nb + 1;
          if (c_idx >= nx) {
            exitg2 = true;
          } else {
            nb++;
          }
        } else {
          nb++;
        }
      }

      if (idx->size[0] == 1) {
        if (c_idx == 0) {
          ii->size[0] = 0;
        }
      } else if (1 > c_idx) {
        ii->size[0] = 0;
      } else {
        i25 = ii->size[0];
        ii->size[0] = c_idx;
        emxEnsureCapacity_int32_T(ii, i25);
      }

      i25 = inter->size[0];
      inter->size[0] = ii->size[0];
      emxEnsureCapacity_int64_T(inter, i25);
      b_loop_ub = ii->size[0];
      for (i25 = 0; i25 < b_loop_ub; i25++) {
        inter->data[i25] = ii->data[i25];
      }

      if (inter->size[0] != 0) {
        printf("intraface: %lli; additional interface flags have been added\n",
               mat);
        fflush(stdout);
        i25 = b_idx->size[0];
        b_idx->size[0] = inter->size[0];
        emxEnsureCapacity_int32_T(b_idx, i25);
        b_loop_ub = inter->size[0];
        for (i25 = 0; i25 < b_loop_ub; i25++) {
          b_qY = inter->data[i25];
          if ((b_qY < 0LL) && (mat < MIN_int64_T - b_qY)) {
            b_qY = MIN_int64_T;
          } else if ((b_qY > 0LL) && (mat > MAX_int64_T - b_qY)) {
            b_qY = MAX_int64_T;
          } else {
            b_qY += mat;
          }

          b_idx->data[i25] = (int)b_qY;
        }

        pEnd = b_idx->size[0];
        for (i25 = 0; i25 < pEnd; i25++) {
          InterTypes->data[(b_idx->data[i25] + InterTypes->size[0] * ((int)mat -
            1)) - 1] = 1.0;
        }
      }

      if (mat < -9223372036854775807LL) {
        b_qY = MIN_int64_T;
      } else {
        b_qY = mat - 1LL;
      }

      if (1LL > b_qY) {
        b_loop_ub = 0;
      } else {
        b_loop_ub = (int)b_qY;
      }

      i25 = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      setAll->size[1] = b_loop_ub;
      emxEnsureCapacity_real_T(setAll, i25);
      for (i25 = 0; i25 < b_loop_ub; i25++) {
        setAll->data[i25] = InterTypes->data[((int)mat + InterTypes->size[0] *
          i25) - 1] - 1.0;
      }

      nx = setAll->size[1];
      c_idx = 0;
      i25 = b_ii->size[0] * b_ii->size[1];
      b_ii->size[0] = 1;
      b_ii->size[1] = setAll->size[1];
      emxEnsureCapacity_int32_T(b_ii, i25);
      nb = 0;
      exitg2 = false;
      while ((!exitg2) && (nb <= nx - 1)) {
        if (setAll->data[nb] != 0.0) {
          c_idx++;
          b_ii->data[c_idx - 1] = nb + 1;
          if (c_idx >= nx) {
            exitg2 = true;
          } else {
            nb++;
          }
        } else {
          nb++;
        }
      }

      if (setAll->size[1] == 1) {
        if (c_idx == 0) {
          b_ii->size[0] = 1;
          b_ii->size[1] = 0;
        }
      } else if (1 > c_idx) {
        b_ii->size[1] = 0;
      } else {
        i25 = b_ii->size[0] * b_ii->size[1];
        b_ii->size[1] = c_idx;
        emxEnsureCapacity_int32_T(b_ii, i25);
      }

      i25 = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      setAll->size[1] = b_ii->size[1];
      emxEnsureCapacity_real_T(setAll, i25);
      b_loop_ub = b_ii->size[0] * b_ii->size[1];
      for (i25 = 0; i25 < b_loop_ub; i25++) {
        setAll->data[i25] = b_ii->data[i25];
      }

      if (setAll->size[1] != 0) {
        printf("intraface: %lli; additional interface flags have been added\n",
               mat);
        fflush(stdout);
        i25 = b_idx->size[0];
        b_idx->size[0] = setAll->size[1];
        emxEnsureCapacity_int32_T(b_idx, i25);
        b_loop_ub = setAll->size[1];
        for (i25 = 0; i25 < b_loop_ub; i25++) {
          b_idx->data[i25] = (int)setAll->data[i25];
        }

        pEnd = b_idx->size[0];
        for (i25 = 0; i25 < pEnd; i25++) {
          InterTypes->data[((int)mat + InterTypes->size[0] * (b_idx->data[i25] -
            1)) - 1] = 1.0;
        }
      }
    }

    if (mat < -9223372036854775807LL) {
      b_qY = MIN_int64_T;
    } else {
      b_qY = mat - 1LL;
    }

    if (1LL > b_qY) {
      i25 = 1;
    } else {
      i25 = (int)b_qY + 1;
    }

    nx = i25 - 2;
    c_idx = 0;
    i26 = ii->size[0];
    ii->size[0] = i25 - 1;
    emxEnsureCapacity_int32_T(ii, i26);
    nb = 0;
    exitg2 = false;
    while ((!exitg2) && (nb <= nx)) {
      if (InterTypes->data[nb + InterTypes->size[0] * ((int)mat - 1)] != 0.0) {
        c_idx++;
        ii->data[c_idx - 1] = nb + 1;
        if (c_idx >= nx + 1) {
          exitg2 = true;
        } else {
          nb++;
        }
      } else {
        nb++;
      }
    }

    if (i25 - 1 == 1) {
      if (c_idx == 0) {
        ii->size[0] = 0;
      }
    } else if (1 > c_idx) {
      ii->size[0] = 0;
    } else {
      i25 = ii->size[0];
      ii->size[0] = c_idx;
      emxEnsureCapacity_int32_T(ii, i25);
    }

    if (ii->size[0] != 0) {
      printf("Warning: some entries found in the upper triangle of Intertypes(:,%lli)\n",
             mat);
      fflush(stdout);
    }

    mat++;
  }

  emxFree_int32_T(&b_ii);
  emxFree_int64_T(&inter);

  /*  Form ElementsOnNode, ElementsOnNodeNum, DG nodes and connectivity */
  emxInit_real_T(&ElementsOnNodeRow, 2);
  emxInit_real_T(&ElementsOnNodePBC_d, 1);
  emxInit_int32_T(&ElementsOnNodePBC_colidx, 1);
  emxInit_int32_T(&ElementsOnNodePBC_rowidx, 1);
  emxInit_int32_T(&internodes_rowidx, 1);
  d_emxInitStruct_coder_internal_(&NodeRegSum);
  emxInit_int32_T(&t0_colidx, 1);
  emxInit_int32_T(&t0_rowidx, 1);
  emxInit_real_T(&b_setAll, 2);
  emxInit_real_T(&NodeRegInd, 2);
  emxInit_real_T(&b_NodeRegInd, 2);
  emxInit_real_T(&c_NodeRegInd, 2);
  if (usePBC != 0.0) {
    emxInit_real_T(&e_NodeRegInd, 2);

    /*  also form the zipped mesh at the same time */
    i25 = e_NodeRegInd->size[0] * e_NodeRegInd->size[1];
    e_NodeRegInd->size[0] = 3;
    i26 = (int)(numel * 24.0);
    e_NodeRegInd->size[1] = i26;
    emxEnsureCapacity_real_T(e_NodeRegInd, i25);
    b_loop_ub = 3 * i26;
    for (i25 = 0; i25 < b_loop_ub; i25++) {
      e_NodeRegInd->data[i25] = 0.0;
    }

    emxInit_real_T(&ElementsOnNodePBCRow, 2);
    i25 = ElementsOnNodePBCRow->size[0] * ElementsOnNodePBCRow->size[1];
    ElementsOnNodePBCRow->size[0] = (int)numel;
    ElementsOnNodePBCRow->size[1] = (int)nen;
    emxEnsureCapacity_real_T(ElementsOnNodePBCRow, i25);
    b_loop_ub = (int)numel * (int)nen;
    for (i25 = 0; i25 < b_loop_ub; i25++) {
      ElementsOnNodePBCRow->data[i25] = 0.0;
    }

    i25 = ElementsOnNodeRow->size[0] * ElementsOnNodeRow->size[1];
    ElementsOnNodeRow->size[0] = (int)numel;
    ElementsOnNodeRow->size[1] = (int)nen;
    emxEnsureCapacity_real_T(ElementsOnNodeRow, i25);
    b_loop_ub = (int)numel * (int)nen;
    for (i25 = 0; i25 < b_loop_ub; i25++) {
      ElementsOnNodeRow->data[i25] = 0.0;
    }

    nri = 0U;
    numA = 0.0;
    i25 = (int)numel;
    for (elem = 0; elem < i25; elem++) {
      if (1.0 > nen) {
        b_loop_ub = 0;
      } else {
        b_loop_ub = (int)nen;
      }

      for (i26 = 0; i26 < b_loop_ub; i26++) {
        nodeloc_data[i26] = NodesOnElementPBC->data[elem +
          NodesOnElementPBC->size[0] * i26];
      }

      n = 0;
      i26 = b_loop_ub - 1;
      for (k = 0; k <= i26; k++) {
        if (nodeloc_data[k] != 0.0) {
          n++;
        }
      }

      reg = RegionOnElement->data[elem];
      for (p = 0; p < n; p++) {
        /*  Loop over local Nodes */
        node = NodesOnElementPBC->data[elem + NodesOnElementPBC->size[0] * p];

        /*    Add element to star list, increment number of elem in star */
        ElementsOnNodePBCRow->data[elem + ElementsOnNodePBCRow->size[0] * p] =
          ElementsOnNodePBCNum->data[(int)node - 1] + 1.0;
        ElementsOnNodePBCNum->data[(int)node - 1]++;

        /*              ElementsOnNodePBC(locE,node) = elem; */
        /*              ElementsOnNodePBCDup(locE,node) = node; */
        if (NodesOnPBCnum->data[(int)node - 1] > 0.0) {
          absxk = NodesOnPBC->data[100 * ((int)node - 1)];
          if (node > absxk) {
            i1 = absxk;
          } else {
            i1 = node;
          }

          /*  use smallest node number as the zipped node */
          nri++;
          e_NodeRegInd->data[3 * ((int)nri - 1)] = i1;
          e_NodeRegInd->data[1 + 3 * ((int)nri - 1)] = reg;
          e_NodeRegInd->data[2 + 3 * ((int)nri - 1)] = 1.0;

          /*  flag that node is in current material set */
          if (node != i1) {
            nri++;

            /*  also add for original edge */
            i26 = 3 * ((int)nri - 1);
            e_NodeRegInd->data[i26] = node;
            e_NodeRegInd->data[1 + i26] = reg;
            e_NodeRegInd->data[2 + i26] = -1.0;

            /*  flag that node is in current material set */
          }

          NodesOnElementCG->data[elem + NodesOnElementCG->size[0] * p] = i1;

          /*    Add element to star list, increment number of elem in star */
          i26 = (int)i1 - 1;
          ElementsOnNodeRow->data[elem + ElementsOnNodeRow->size[0] * p] =
            ElementsOnNodeNum->data[i26] + 1.0;
          ElementsOnNodeNum->data[i26]++;

          /*                  ElementsOnNode(locE,nodePBC) = elem; */
          /*                  ElementsOnNodeDup(locE,nodePBC) = nodePBC; */
        } else {
          nri++;
          i26 = 3 * ((int)nri - 1);
          e_NodeRegInd->data[i26] = node;
          e_NodeRegInd->data[1 + i26] = reg;
          e_NodeRegInd->data[2 + i26] = 1.0;

          /*  flag that node is in current material set */
          /*                  NodeReg(node,reg) = node; % flag that node is in current material set */
          /*    Add element to star list, increment number of elem in star */
          ElementsOnNodeRow->data[elem + ElementsOnNodeRow->size[0] * p] =
            ElementsOnNodeNum->data[(int)node - 1] + 1.0;
          ElementsOnNodeNum->data[(int)node - 1]++;

          /*                  ElementsOnNode(locE,nodePBC) = elem; */
          /*                  ElementsOnNodeDup(locE,nodePBC) = nodePBC; */
        }
      }

      i26 = (int)(nen + (1.0 - ((double)n + 1.0)));
      for (p = 0; p < i26; p++) {
        numA++;
        i27 = (int)(((double)n + 1.0) + (double)p) - 1;
        ElementsOnNodeRow->data[elem + ElementsOnNodeRow->size[0] * i27] = numA;
        ElementsOnNodePBCRow->data[elem + ElementsOnNodePBCRow->size[0] * i27] =
          numA;
      }
    }

    k0 = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1] - 1;
    nx = 0;
    for (b_i = 0; b_i <= k0; b_i++) {
      if (NodesOnElementPBC->data[b_i] == 0.0) {
        nx++;
      }
    }

    i25 = ElementsOnNodePBC_colidx->size[0];
    ElementsOnNodePBC_colidx->size[0] = nx;
    emxEnsureCapacity_int32_T(ElementsOnNodePBC_colidx, i25);
    nb = 0;
    for (b_i = 0; b_i <= k0; b_i++) {
      if (NodesOnElementPBC->data[b_i] == 0.0) {
        ElementsOnNodePBC_colidx->data[nb] = b_i + 1;
        nb++;
      }
    }

    b_loop_ub = ElementsOnNodePBC_colidx->size[0] - 1;
    for (i25 = 0; i25 <= b_loop_ub; i25++) {
      NodesOnElementPBC->data[ElementsOnNodePBC_colidx->data[i25] - 1] = *numnp
        + 1.0;
    }

    /* move empty numbers to end of array */
    k0 = NodesOnElementCG->size[0] * NodesOnElementCG->size[1] - 1;
    nx = 0;
    for (b_i = 0; b_i <= k0; b_i++) {
      if (NodesOnElementCG->data[b_i] == 0.0) {
        nx++;
      }
    }

    i25 = ElementsOnNodePBC_rowidx->size[0];
    ElementsOnNodePBC_rowidx->size[0] = nx;
    emxEnsureCapacity_int32_T(ElementsOnNodePBC_rowidx, i25);
    nb = 0;
    for (b_i = 0; b_i <= k0; b_i++) {
      if (NodesOnElementCG->data[b_i] == 0.0) {
        ElementsOnNodePBC_rowidx->data[nb] = b_i + 1;
        nb++;
      }
    }

    b_loop_ub = ElementsOnNodePBC_rowidx->size[0] - 1;
    for (i25 = 0; i25 <= b_loop_ub; i25++) {
      NodesOnElementCG->data[ElementsOnNodePBC_rowidx->data[i25] - 1] = *numnp +
        1.0;
    }

    /* move empty numbers to end of array */
    if (numel < 1.0) {
      setAll->size[0] = 1;
      setAll->size[1] = 0;
    } else {
      i25 = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      setAll->size[1] = (int)floor(numel - 1.0) + 1;
      emxEnsureCapacity_real_T(setAll, i25);
      b_loop_ub = (int)floor(numel - 1.0);
      for (i25 = 0; i25 <= b_loop_ub; i25++) {
        setAll->data[i25] = 1.0 + (double)i25;
      }
    }

    i25 = b_setAll->size[0] * b_setAll->size[1];
    b_setAll->size[0] = setAll->size[1];
    b_setAll->size[1] = (int)nen;
    emxEnsureCapacity_real_T(b_setAll, i25);
    b_loop_ub = setAll->size[1];
    for (i25 = 0; i25 < b_loop_ub; i25++) {
      nb = (int)nen;
      for (i26 = 0; i26 < nb; i26++) {
        reg = setAll->data[i25];
        b_setAll->data[i25 + b_setAll->size[0] * i26] = reg;
      }
    }

    sparse(ElementsOnNodeRow, NodesOnElementCG, b_setAll, ElementsOnNode);

    /* zeros(maxel,numnp); % Array of elements connected to nodes */
    sparse(ElementsOnNodeRow, NodesOnElementCG, NodesOnElementCG,
           ElementsOnNodeDup);

    /*  actual node ID given to element for an original node; general counterpart of NodeMat */
    if (numel < 1.0) {
      setAll->size[0] = 1;
      setAll->size[1] = 0;
    } else {
      i25 = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      setAll->size[1] = (int)floor(numel - 1.0) + 1;
      emxEnsureCapacity_real_T(setAll, i25);
      b_loop_ub = (int)floor(numel - 1.0);
      for (i25 = 0; i25 <= b_loop_ub; i25++) {
        setAll->data[i25] = 1.0 + (double)i25;
      }
    }

    i25 = b_setAll->size[0] * b_setAll->size[1];
    b_setAll->size[0] = setAll->size[1];
    b_setAll->size[1] = (int)nen;
    emxEnsureCapacity_real_T(b_setAll, i25);
    b_loop_ub = setAll->size[1];
    for (i25 = 0; i25 < b_loop_ub; i25++) {
      nb = (int)nen;
      for (i26 = 0; i26 < nb; i26++) {
        reg = setAll->data[i25];
        b_setAll->data[i25 + b_setAll->size[0] * i26] = reg;
      }
    }

    c_emxInitStruct_coder_internal_(&expl_temp);
    sparse(ElementsOnNodePBCRow, NodesOnElementPBC, b_setAll, &expl_temp);
    i25 = ElementsOnNodePBC_d->size[0];
    ElementsOnNodePBC_d->size[0] = expl_temp.d->size[0];
    emxEnsureCapacity_real_T(ElementsOnNodePBC_d, i25);
    b_loop_ub = expl_temp.d->size[0];
    for (i25 = 0; i25 < b_loop_ub; i25++) {
      ElementsOnNodePBC_d->data[i25] = expl_temp.d->data[i25];
    }

    i25 = ElementsOnNodePBC_colidx->size[0];
    ElementsOnNodePBC_colidx->size[0] = expl_temp.colidx->size[0];
    emxEnsureCapacity_int32_T(ElementsOnNodePBC_colidx, i25);
    b_loop_ub = expl_temp.colidx->size[0];
    for (i25 = 0; i25 < b_loop_ub; i25++) {
      ElementsOnNodePBC_colidx->data[i25] = expl_temp.colidx->data[i25];
    }

    i25 = ElementsOnNodePBC_rowidx->size[0];
    ElementsOnNodePBC_rowidx->size[0] = expl_temp.rowidx->size[0];
    emxEnsureCapacity_int32_T(ElementsOnNodePBC_rowidx, i25);
    b_loop_ub = expl_temp.rowidx->size[0];
    for (i25 = 0; i25 < b_loop_ub; i25++) {
      ElementsOnNodePBC_rowidx->data[i25] = expl_temp.rowidx->data[i25];
    }

    c_emxFreeStruct_coder_internal_(&expl_temp);
    c_emxInitStruct_coder_internal_(&b_ElementsOnNodePBCRow);

    /* zeros(maxel,numnp); % Array of elements connected to nodes */
    sparse(ElementsOnNodePBCRow, NodesOnElementPBC, NodesOnElementPBC,
           &b_ElementsOnNodePBCRow);

    /*  actual node ID given to element for an original node; general counterpart of NodeMat */
    c_emxFreeStruct_coder_internal_(&b_ElementsOnNodePBCRow);
    emxFree_real_T(&ElementsOnNodePBCRow);
    if (1 > (int)nri) {
      b_loop_ub = 0;
      nb = 0;
      k0 = 0;
    } else {
      b_loop_ub = (int)nri;
      nb = (int)nri;
      k0 = (int)nri;
    }

    i25 = NodeRegInd->size[0] * NodeRegInd->size[1];
    NodeRegInd->size[0] = 1;
    NodeRegInd->size[1] = b_loop_ub;
    emxEnsureCapacity_real_T(NodeRegInd, i25);
    for (i25 = 0; i25 < b_loop_ub; i25++) {
      NodeRegInd->data[i25] = e_NodeRegInd->data[3 * i25];
    }

    i25 = b_NodeRegInd->size[0] * b_NodeRegInd->size[1];
    b_NodeRegInd->size[0] = 1;
    b_NodeRegInd->size[1] = nb;
    emxEnsureCapacity_real_T(b_NodeRegInd, i25);
    for (i25 = 0; i25 < nb; i25++) {
      b_NodeRegInd->data[i25] = e_NodeRegInd->data[1 + 3 * i25];
    }

    i25 = c_NodeRegInd->size[0] * c_NodeRegInd->size[1];
    c_NodeRegInd->size[0] = 1;
    c_NodeRegInd->size[1] = k0;
    emxEnsureCapacity_real_T(c_NodeRegInd, i25);
    for (i25 = 0; i25 < k0; i25++) {
      c_NodeRegInd->data[i25] = e_NodeRegInd->data[2 + 3 * i25];
    }

    emxFree_real_T(&e_NodeRegInd);
    b_sparse(NodeRegInd, b_NodeRegInd, c_NodeRegInd, NodeReg);

    /*  allow allocation of PBC nodes too, but with negatives so as not to be in facet counts */
    nx = NodeReg->colidx->data[NodeReg->colidx->size[0] - 1] - 1;
    if (NodeReg->colidx->data[NodeReg->colidx->size[0] - 1] - 1 == 0) {
      b_idx->size[0] = 0;
      ii->size[0] = 0;
    } else {
      i25 = b_idx->size[0];
      b_idx->size[0] = NodeReg->colidx->data[NodeReg->colidx->size[0] - 1] - 1;
      emxEnsureCapacity_int32_T(b_idx, i25);
      i25 = ii->size[0];
      ii->size[0] = NodeReg->colidx->data[NodeReg->colidx->size[0] - 1] - 1;
      emxEnsureCapacity_int32_T(ii, i25);
      for (c_idx = 0; c_idx < nx; c_idx++) {
        b_idx->data[c_idx] = NodeReg->rowidx->data[c_idx];
      }

      c_idx = 0;
      k0 = 1;
      while (c_idx < nx) {
        if (c_idx == NodeReg->colidx->data[k0] - 1) {
          k0++;
        } else {
          c_idx++;
          ii->data[c_idx - 1] = k0;
        }
      }

      if (NodeReg->colidx->data[NodeReg->colidx->size[0] - 1] - 1 == 1) {
        if (c_idx == 0) {
          b_idx->size[0] = 0;
          ii->size[0] = 0;
        }
      } else if (1 > c_idx) {
        b_idx->size[0] = 0;
        ii->size[0] = 0;
      } else {
        i25 = b_idx->size[0];
        b_idx->size[0] = c_idx;
        emxEnsureCapacity_int32_T(b_idx, i25);
        i25 = ii->size[0];
        ii->size[0] = c_idx;
        emxEnsureCapacity_int32_T(ii, i25);
      }
    }

    i25 = MorePBC->size[0];
    MorePBC->size[0] = b_idx->size[0];
    emxEnsureCapacity_real_T(MorePBC, i25);
    b_loop_ub = b_idx->size[0];
    for (i25 = 0; i25 < b_loop_ub; i25++) {
      MorePBC->data[i25] = b_idx->data[i25];
    }

    i25 = idx->size[0];
    idx->size[0] = ii->size[0];
    emxEnsureCapacity_real_T(idx, i25);
    b_loop_ub = ii->size[0];
    for (i25 = 0; i25 < b_loop_ub; i25++) {
      idx->data[i25] = ii->data[i25];
    }

    /*  sparse adds together entries when duplicated, so reduce back to original magnitude */
    n = MorePBC->size[0];
    if (MorePBC->size[0] <= 2) {
      if (MorePBC->size[0] == 1) {
        k0 = (int)MorePBC->data[0];
      } else if (MorePBC->data[0] < MorePBC->data[1]) {
        k0 = (int)MorePBC->data[1];
      } else {
        k0 = (int)MorePBC->data[0];
      }
    } else {
      k0 = (int)MorePBC->data[0];
      for (k = 2; k <= n; k++) {
        if (k0 < (int)MorePBC->data[k - 1]) {
          k0 = (int)MorePBC->data[k - 1];
        }
      }
    }

    i25 = idx->size[0];
    idx->size[0] = MorePBC->size[0];
    emxEnsureCapacity_real_T(idx, i25);
    b_loop_ub = MorePBC->size[0];
    for (i25 = 0; i25 < b_loop_ub; i25++) {
      idx->data[i25] = (int)MorePBC->data[i25] + k0 * ((int)idx->data[i25] - 1);
    }

    sparse_parenReference(NodeReg->d, NodeReg->colidx, NodeReg->rowidx,
                          NodeReg->m, idx, NodeRegSum.d, NodeRegSum.colidx,
                          NodeRegSum.rowidx, &NodeRegSum.m);
    b_sign(&NodeRegSum);
    sparse_times(MorePBC, NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                 NodeRegSum.m, s, t0_colidx, t0_rowidx, &pEnd);
    sparse_parenAssign(NodeReg, s, t0_colidx, t0_rowidx, pEnd, idx);

    /* keep the negative sign for PBC nodes to ignore below */
    k0 = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1] - 1;
    nx = 0;
    for (b_i = 0; b_i <= k0; b_i++) {
      if (NodesOnElementPBC->data[b_i] == *numnp + 1.0) {
        nx++;
      }
    }

    i25 = internodes_rowidx->size[0];
    internodes_rowidx->size[0] = nx;
    emxEnsureCapacity_int32_T(internodes_rowidx, i25);
    nb = 0;
    for (b_i = 0; b_i <= k0; b_i++) {
      if (NodesOnElementPBC->data[b_i] == *numnp + 1.0) {
        internodes_rowidx->data[nb] = b_i + 1;
        nb++;
      }
    }

    b_loop_ub = internodes_rowidx->size[0] - 1;
    for (i25 = 0; i25 <= b_loop_ub; i25++) {
      NodesOnElementPBC->data[internodes_rowidx->data[i25] - 1] = 0.0;
    }

    /* move empty numbers to end of array */
    k0 = NodesOnElementCG->size[0] * NodesOnElementCG->size[1] - 1;
    nx = 0;
    for (b_i = 0; b_i <= k0; b_i++) {
      if (NodesOnElementCG->data[b_i] == *numnp + 1.0) {
        nx++;
      }
    }

    emxInit_int32_T(&r0, 1);
    i25 = r0->size[0];
    r0->size[0] = nx;
    emxEnsureCapacity_int32_T(r0, i25);
    nb = 0;
    for (b_i = 0; b_i <= k0; b_i++) {
      if (NodesOnElementCG->data[b_i] == *numnp + 1.0) {
        r0->data[nb] = b_i + 1;
        nb++;
      }
    }

    b_loop_ub = r0->size[0] - 1;
    for (i25 = 0; i25 <= b_loop_ub; i25++) {
      NodesOnElementCG->data[r0->data[i25] - 1] = 0.0;
    }

    emxFree_int32_T(&r0);

    /* move empty numbers to end of array */
    /*      % get memory back */
    /*      clear NodeRegInd */
    /*      clear ElementsOnNodeRow */
    /*      clear ElementsOnNodePBCRow */
  } else {
    emxInit_real_T(&d_NodeRegInd, 2);
    i25 = d_NodeRegInd->size[0] * d_NodeRegInd->size[1];
    d_NodeRegInd->size[0] = 2;
    i26 = (int)(numel * 8.0);
    d_NodeRegInd->size[1] = i26;
    emxEnsureCapacity_real_T(d_NodeRegInd, i25);
    b_loop_ub = i26 << 1;
    for (i25 = 0; i25 < b_loop_ub; i25++) {
      d_NodeRegInd->data[i25] = 0.0;
    }

    i25 = ElementsOnNodeRow->size[0] * ElementsOnNodeRow->size[1];
    i26 = (int)numel;
    ElementsOnNodeRow->size[0] = i26;
    b_loop_ub = (int)nen;
    ElementsOnNodeRow->size[1] = b_loop_ub;
    emxEnsureCapacity_real_T(ElementsOnNodeRow, i25);
    i = i26 * b_loop_ub;
    for (i25 = 0; i25 < i; i25++) {
      ElementsOnNodeRow->data[i25] = 0.0;
    }

    /*      ElementsOnNodeCol = zeros(nen,numel); */
    nri = 0U;
    numA = 0.0;
    for (elem = 0; elem < i26; elem++) {
      if (1.0 > nen) {
        nb = 0;
      } else {
        nb = (int)nen;
      }

      for (i25 = 0; i25 < nb; i25++) {
        nodeloc_data[i25] = NodesOnElement->data[elem + NodesOnElement->size[0] *
          i25];
      }

      n = 0;
      i25 = nb - 1;
      for (k = 0; k <= i25; k++) {
        if (nodeloc_data[k] != 0.0) {
          n++;
        }
      }

      for (p = 0; p < n; p++) {
        /*  Loop over local Nodes */
        node = NodesOnElement->data[elem + NodesOnElement->size[0] * p];
        nri++;
        absxk = RegionOnElement->data[elem];
        i25 = ((int)nri - 1) << 1;
        d_NodeRegInd->data[i25] = node;
        d_NodeRegInd->data[1 + i25] = absxk;

        /*              NodeReg(node,reg) = node; % flag that node is in current material set */
        /*    Add element to star list, increment number of elem in star */
        ElementsOnNodeRow->data[elem + ElementsOnNodeRow->size[0] * p] =
          ElementsOnNodeNum->data[(int)node - 1] + 1.0;

        /*              ElementsOnNode(locE,node) = elem; */
        i25 = (int)node - 1;
        ElementsOnNodeNum->data[i25]++;

        /*              ElementsOnNodeCol(locN,elem) = node; */
        /*              ElementsOnNodeDup(locE,node) = node; */
      }

      i25 = (int)(nen + (1.0 - ((double)n + 1.0)));
      for (p = 0; p < i25; p++) {
        numA++;
        ElementsOnNodeRow->data[elem + ElementsOnNodeRow->size[0] * ((int)
          (((double)n + 1.0) + (double)p) - 1)] = numA;
      }
    }

    k0 = NodesOnElement->size[0] * NodesOnElement->size[1] - 1;
    nx = 0;
    for (b_i = 0; b_i <= k0; b_i++) {
      if (NodesOnElement->data[b_i] == 0.0) {
        nx++;
      }
    }

    i25 = ElementsOnNodePBC_colidx->size[0];
    ElementsOnNodePBC_colidx->size[0] = nx;
    emxEnsureCapacity_int32_T(ElementsOnNodePBC_colidx, i25);
    nb = 0;
    for (b_i = 0; b_i <= k0; b_i++) {
      if (NodesOnElement->data[b_i] == 0.0) {
        ElementsOnNodePBC_colidx->data[nb] = b_i + 1;
        nb++;
      }
    }

    nb = ElementsOnNodePBC_colidx->size[0];
    for (i25 = 0; i25 < nb; i25++) {
      NodesOnElement->data[ElementsOnNodePBC_colidx->data[i25] - 1] = *numnp +
        1.0;
    }

    /* move empty numbers to end of array */
    if (numel < 1.0) {
      setAll->size[0] = 1;
      setAll->size[1] = 0;
    } else {
      i25 = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      nb = (int)floor(numel - 1.0);
      setAll->size[1] = nb + 1;
      emxEnsureCapacity_real_T(setAll, i25);
      for (i25 = 0; i25 <= nb; i25++) {
        setAll->data[i25] = 1.0 + (double)i25;
      }
    }

    i25 = b_setAll->size[0] * b_setAll->size[1];
    b_setAll->size[0] = setAll->size[1];
    b_setAll->size[1] = b_loop_ub;
    emxEnsureCapacity_real_T(b_setAll, i25);
    nb = setAll->size[1];
    for (i25 = 0; i25 < nb; i25++) {
      for (i27 = 0; i27 < b_loop_ub; i27++) {
        reg = setAll->data[i25];
        b_setAll->data[i25 + b_setAll->size[0] * i27] = reg;
      }
    }

    sparse(ElementsOnNodeRow, NodesOnElement, b_setAll, ElementsOnNode);

    /* zeros(maxel,numnp); % Array of elements connected to nodes */
    sparse(ElementsOnNodeRow, NodesOnElement, NodesOnElement, ElementsOnNodeDup);

    /*  actual node ID given to element for an original node; general counterpart of NodeMat */
    if (1 > (int)nri) {
      nb = 0;
      k0 = 0;
      pEnd = 0;
    } else {
      nb = (int)nri;
      k0 = (int)nri;
      pEnd = (int)nri;
    }

    i25 = NodeRegInd->size[0] * NodeRegInd->size[1];
    NodeRegInd->size[0] = 1;
    NodeRegInd->size[1] = nb;
    emxEnsureCapacity_real_T(NodeRegInd, i25);
    for (i25 = 0; i25 < nb; i25++) {
      NodeRegInd->data[i25] = d_NodeRegInd->data[i25 << 1];
    }

    i25 = b_NodeRegInd->size[0] * b_NodeRegInd->size[1];
    b_NodeRegInd->size[0] = 1;
    b_NodeRegInd->size[1] = k0;
    emxEnsureCapacity_real_T(b_NodeRegInd, i25);
    for (i25 = 0; i25 < k0; i25++) {
      b_NodeRegInd->data[i25] = d_NodeRegInd->data[1 + (i25 << 1)];
    }

    i25 = c_NodeRegInd->size[0] * c_NodeRegInd->size[1];
    c_NodeRegInd->size[0] = 1;
    c_NodeRegInd->size[1] = pEnd;
    emxEnsureCapacity_real_T(c_NodeRegInd, i25);
    for (i25 = 0; i25 < pEnd; i25++) {
      c_NodeRegInd->data[i25] = d_NodeRegInd->data[i25 << 1];
    }

    emxFree_real_T(&d_NodeRegInd);
    b_sparse(NodeRegInd, b_NodeRegInd, c_NodeRegInd, NodeReg);
    nx = NodeReg->colidx->data[NodeReg->colidx->size[0] - 1] - 1;
    if (NodeReg->colidx->data[NodeReg->colidx->size[0] - 1] - 1 == 0) {
      b_idx->size[0] = 0;
      ii->size[0] = 0;
    } else {
      i25 = b_idx->size[0];
      b_idx->size[0] = NodeReg->colidx->data[NodeReg->colidx->size[0] - 1] - 1;
      emxEnsureCapacity_int32_T(b_idx, i25);
      i25 = ii->size[0];
      ii->size[0] = NodeReg->colidx->data[NodeReg->colidx->size[0] - 1] - 1;
      emxEnsureCapacity_int32_T(ii, i25);
      for (c_idx = 0; c_idx < nx; c_idx++) {
        b_idx->data[c_idx] = NodeReg->rowidx->data[c_idx];
      }

      c_idx = 0;
      k0 = 1;
      while (c_idx < nx) {
        if (c_idx == NodeReg->colidx->data[k0] - 1) {
          k0++;
        } else {
          c_idx++;
          ii->data[c_idx - 1] = k0;
        }
      }

      if (NodeReg->colidx->data[NodeReg->colidx->size[0] - 1] - 1 == 1) {
        if (c_idx == 0) {
          b_idx->size[0] = 0;
          ii->size[0] = 0;
        }
      } else if (1 > c_idx) {
        b_idx->size[0] = 0;
        ii->size[0] = 0;
      } else {
        i25 = b_idx->size[0];
        b_idx->size[0] = c_idx;
        emxEnsureCapacity_int32_T(b_idx, i25);
        i25 = ii->size[0];
        ii->size[0] = c_idx;
        emxEnsureCapacity_int32_T(ii, i25);
      }
    }

    i25 = MorePBC->size[0];
    MorePBC->size[0] = b_idx->size[0];
    emxEnsureCapacity_real_T(MorePBC, i25);
    nb = b_idx->size[0];
    for (i25 = 0; i25 < nb; i25++) {
      MorePBC->data[i25] = b_idx->data[i25];
    }

    i25 = idx->size[0];
    idx->size[0] = ii->size[0];
    emxEnsureCapacity_real_T(idx, i25);
    nb = ii->size[0];
    for (i25 = 0; i25 < nb; i25++) {
      idx->data[i25] = ii->data[i25];
    }

    i25 = idx->size[0];
    idx->size[0] = MorePBC->size[0];
    emxEnsureCapacity_real_T(idx, i25);
    nb = MorePBC->size[0];
    for (i25 = 0; i25 < nb; i25++) {
      idx->data[i25] = (int)MorePBC->data[i25] + loop_ub * ((int)idx->data[i25]
        - 1);
    }

    b_sparse_parenAssign(NodeReg, MorePBC, idx);
    k0 = NodesOnElement->size[0] * NodesOnElement->size[1] - 1;
    nx = 0;
    for (b_i = 0; b_i <= k0; b_i++) {
      if (NodesOnElement->data[b_i] == *numnp + 1.0) {
        nx++;
      }
    }

    i25 = ElementsOnNodePBC_rowidx->size[0];
    ElementsOnNodePBC_rowidx->size[0] = nx;
    emxEnsureCapacity_int32_T(ElementsOnNodePBC_rowidx, i25);
    nb = 0;
    for (b_i = 0; b_i <= k0; b_i++) {
      if (NodesOnElement->data[b_i] == *numnp + 1.0) {
        ElementsOnNodePBC_rowidx->data[nb] = b_i + 1;
        nb++;
      }
    }

    nb = ElementsOnNodePBC_rowidx->size[0];
    for (i25 = 0; i25 < nb; i25++) {
      NodesOnElement->data[ElementsOnNodePBC_rowidx->data[i25] - 1] = 0.0;
    }

    /* move empty numbers to end of array */
    i25 = ElementsOnNodeRow->size[0] * ElementsOnNodeRow->size[1];
    ElementsOnNodeRow->size[0] = i26;
    ElementsOnNodeRow->size[1] = b_loop_ub;
    emxEnsureCapacity_real_T(ElementsOnNodeRow, i25);
    for (i25 = 0; i25 < i; i25++) {
      ElementsOnNodeRow->data[i25] = 0.0;
    }

    c_sparse(ElementsOnNodeRow, ElementsOnNodePBC_d, ElementsOnNodePBC_colidx,
             ElementsOnNodePBC_rowidx, &q, &pEnd, &k0);

    /*      % get memory back */
    /*      clear NodeRegInd */
    /*      clear ElementsOnNodeRow */
  }

  emxFree_real_T(&b_setAll);
  emxFree_real_T(&ElementsOnNodeRow);
  i25 = NodesOnElementDG->size[0] * NodesOnElementDG->size[1];
  NodesOnElementDG->size[0] = NodesOnElementCG->size[0];
  NodesOnElementDG->size[1] = NodesOnElementCG->size[1];
  emxEnsureCapacity_real_T(NodesOnElementDG, i25);
  b_loop_ub = NodesOnElementCG->size[0] * NodesOnElementCG->size[1];
  for (i25 = 0; i25 < b_loop_ub; i25++) {
    NodesOnElementDG->data[i25] = NodesOnElementCG->data[i25];
  }

  if (1.0 > *numnp) {
    b_loop_ub = 0;
  } else {
    b_loop_ub = (int)*numnp;
  }

  for (i25 = 0; i25 < 3; i25++) {
    for (i26 = 0; i26 < b_loop_ub; i26++) {
      Coordinates3->data[i26 + Coordinates3->size[0] * i25] = Coordinates->
        data[i26 + Coordinates->size[0] * i25];
    }
  }

  *numinttype = nummat * (nummat + 1.0) / 2.0;
  i25 = numEonF->size[0];
  b_loop_ub = (int)*numinttype;
  numEonF->size[0] = b_loop_ub;
  emxEnsureCapacity_real_T(numEonF, i25);
  for (i25 = 0; i25 < b_loop_ub; i25++) {
    numEonF->data[i25] = 0.0;
  }

  *numEonB = 0.0;
  if (usePBC != 0.0) {
    i25 = FacetsOnPBC->size[0];
    FacetsOnPBC->size[0] = (int)(8.0 * numel);
    emxEnsureCapacity_real_T(FacetsOnPBC, i25);
    nb = (int)(8.0 * numel);
    for (i25 = 0; i25 < nb; i25++) {
      FacetsOnPBC->data[i25] = 0.0;
    }

    /*  separate list for PBC facets that are found; grouped by interface type */
    i25 = numEonPBC->size[0];
    numEonPBC->size[0] = b_loop_ub;
    emxEnsureCapacity_real_T(numEonPBC, i25);
    for (i25 = 0; i25 < b_loop_ub; i25++) {
      numEonPBC->data[i25] = 0.0;
    }

    numFPBC = 0.0;

    /*  record how many PBC facets are found */
  } else {
    i25 = FacetsOnPBC->size[0];
    FacetsOnPBC->size[0] = 4;
    emxEnsureCapacity_real_T(FacetsOnPBC, i25);
    FacetsOnPBC->data[0] = 0.0;
    FacetsOnPBC->data[1] = 0.0;
    FacetsOnPBC->data[2] = 0.0;
    FacetsOnPBC->data[3] = 0.0;

    /*  separate list for PBC facets that are found; grouped by interface type */
    i25 = numEonPBC->size[0];
    numEonPBC->size[0] = 2;
    emxEnsureCapacity_real_T(numEonPBC, i25);
    numEonPBC->data[0] = 0.0;
    numEonPBC->data[1] = 0.0;
    numFPBC = 0.0;

    /*  record how many PBC facets are found */
  }

  i25 = ElementsOnBoundary->size[0] * ElementsOnBoundary->size[1];
  nb = (int)(6.0 * numel);
  ElementsOnBoundary->size[0] = nb;
  ElementsOnBoundary->size[1] = 2;
  emxEnsureCapacity_real_T(ElementsOnBoundary, i25);
  k0 = nb << 1;
  for (i25 = 0; i25 < k0; i25++) {
    ElementsOnBoundary->data[i25] = 0.0;
  }

  /*  All exposed faces */
  i25 = FacetsOnElement->size[0] * FacetsOnElement->size[1];
  FacetsOnElement->size[0] = (int)numel;
  FacetsOnElement->size[1] = 6;
  emxEnsureCapacity_real_T(FacetsOnElement, i25);
  i = (int)numel * 6;
  for (i25 = 0; i25 < i; i25++) {
    FacetsOnElement->data[i25] = 0.0;
  }

  i25 = FacetsOnElementInt->size[0] * FacetsOnElementInt->size[1];
  FacetsOnElementInt->size[0] = (int)numel;
  FacetsOnElementInt->size[1] = 6;
  emxEnsureCapacity_real_T(FacetsOnElementInt, i25);
  for (i25 = 0; i25 < i; i25++) {
    FacetsOnElementInt->data[i25] = 0.0;
  }

  i25 = ElementsOnFacet->size[0] * ElementsOnFacet->size[1];
  ElementsOnFacet->size[0] = nb;
  ElementsOnFacet->size[1] = 4;
  emxEnsureCapacity_real_T(ElementsOnFacet, i25);
  k0 = nb << 2;
  for (i25 = 0; i25 < k0; i25++) {
    ElementsOnFacet->data[i25] = 0.0;
  }

  i25 = FacetsOnInterface->size[0];
  FacetsOnInterface->size[0] = nb;
  emxEnsureCapacity_real_T(FacetsOnInterface, i25);
  for (i25 = 0; i25 < nb; i25++) {
    FacetsOnInterface->data[i25] = 0.0;
  }

  i25 = FacetsOnNodeNum->size[0];
  FacetsOnNodeNum->size[0] = loop_ub;
  emxEnsureCapacity_real_T(FacetsOnNodeNum, i25);
  for (i25 = 0; i25 < loop_ub; i25++) {
    FacetsOnNodeNum->data[i25] = 0.0;
  }

  emxInit_real_T(&FacetsOnNodeInd, 2);

  /*  number of edges attached to a node. */
  i25 = FacetsOnNodeInd->size[0] * FacetsOnNodeInd->size[1];
  FacetsOnNodeInd->size[0] = 5;
  i26 = (int)(20.0 * numel);
  FacetsOnNodeInd->size[1] = i26;
  emxEnsureCapacity_real_T(FacetsOnNodeInd, i25);
  loop_ub = 5 * i26;
  for (i25 = 0; i25 < loop_ub; i25++) {
    FacetsOnNodeInd->data[i25] = 0.0;
  }

  nri = 0U;
  *numfac = 0.0;

  /*  Templates for nodes and facets */
  /*  Find all facets in mesh */
  i25 = (int)numel;
  emxInit_real_T(&ElementsOnNodeABCD, 2);
  emxInit_int32_T(&c_s, 1);
  emxInit_int32_T(&d_s, 1);
  for (elem = 0; elem < i25; elem++) {
    if (1.0 > nen) {
      loop_ub = 0;
    } else {
      loop_ub = (int)nen;
    }

    for (i26 = 0; i26 < loop_ub; i26++) {
      nodeloc_data[i26] = NodesOnElementCG->data[elem + NodesOnElementCG->size[0]
        * i26];
    }

    n = 0;
    i26 = loop_ub - 1;
    for (k = 0; k <= i26; k++) {
      if (nodeloc_data[k] != 0.0) {
        n++;
      }
    }

    if ((n == 4) || (n == 10)) {
      c_idx = 3;
      for (i26 = 0; i26 < 5; i26++) {
        numfn_data[i26] = 3;
      }

      exponent = 4;
      for (i26 = 0; i26 < 24; i26++) {
        nloop_data[i26] = nloopT[i26];
      }

      nel2 = 4;
    } else if ((n == 6) || (n == 18)) {
      c_idx = 4;
      for (i26 = 0; i26 < 5; i26++) {
        numfn_data[i26] = numfnW[i26];
      }

      exponent = 5;
      for (i26 = 0; i26 < 40; i26++) {
        nloop_data[i26] = nloopW[i26];
      }

      nel2 = 6;
    } else if ((n == 5) || (n == 14)) {
      c_idx = 4;
      for (i26 = 0; i26 < 5; i26++) {
        numfn_data[i26] = numfnP[i26];
      }

      exponent = 5;
      for (i26 = 0; i26 < 40; i26++) {
        nloop_data[i26] = nloopP[i26];
      }

      nel2 = 5;
    } else {
      c_idx = 5;
      for (i26 = 0; i26 < 6; i26++) {
        numfn_data[i26] = 4;
      }

      exponent = 6;
      for (i26 = 0; i26 < 48; i26++) {
        nloop_data[i26] = nloopH[i26];
      }

      nel2 = 8;
    }

    /*  Loop over edges of element */
    for (numPBC = 0; numPBC <= c_idx; numPBC++) {
      if (!(FacetsOnElement->data[elem + FacetsOnElement->size[0] * numPBC] !=
            0.0)) {
        /*  reg1 is overwritten below, so it must be re-evaluated */
        /*  number of nodes on face */
        loop_ub = numfn_data[numPBC];
        nodeABCD_size[0] = 1;
        nodeABCD_size[1] = numfn_data[numPBC];
        for (i26 = 0; i26 < loop_ub; i26++) {
          nodeABCD_data[i26] = NodesOnElementCG->data[elem +
            NodesOnElementCG->size[0] * (nloop_data[numPBC + exponent * i26] - 1)];
        }

        loop_ub = numfn_data[numPBC];
        for (i26 = 0; i26 < loop_ub; i26++) {
          numABCD_data[i26] = ElementsOnNodeNum->data[(int)nodeABCD_data[i26] -
            1];
        }

        pEnd = numfn_data[numPBC];
        i1 = numABCD_data[0];
        for (k = 2; k <= pEnd; k++) {
          reg = numABCD_data[k - 1];
          if (i1 < reg) {
            i1 = reg;
          }
        }

        if (i1 < 1.0) {
          setAll->size[0] = 1;
          setAll->size[1] = 0;
        } else if (rtIsInf(i1) && (1.0 == i1)) {
          i26 = setAll->size[0] * setAll->size[1];
          setAll->size[0] = 1;
          setAll->size[1] = 1;
          emxEnsureCapacity_real_T(setAll, i26);
          setAll->data[0] = rtNaN;
        } else {
          i26 = setAll->size[0] * setAll->size[1];
          setAll->size[0] = 1;
          loop_ub = (int)floor(i1 - 1.0);
          setAll->size[1] = loop_ub + 1;
          emxEnsureCapacity_real_T(setAll, i26);
          for (i26 = 0; i26 <= loop_ub; i26++) {
            setAll->data[i26] = 1.0 + (double)i26;
          }
        }

        b_sparse_parenReference(ElementsOnNode->d, ElementsOnNode->colidx,
          ElementsOnNode->rowidx, setAll, nodeABCD_data, nodeABCD_size, s, b_idx,
          ii, &pEnd, &nx, &q);
        b_sparse_full(s, b_idx, ii, pEnd, nx, ElementsOnNodeABCD);

        /*  Determine if edge is on domain interior or boundary */
        links2_size[0] = numfn_data[numPBC];
        loop_ub = numfn_data[numPBC];
        for (i26 = 0; i26 < loop_ub; i26++) {
          b_numABCD_data[i26] = (numABCD_data[i26] > 1.0);
        }

        guard1 = false;
        if (all(b_numABCD_data, links2_size)) {
          /*  Clean and fast way to intersect the 3 sets of elements, using */
          /*  built-in Matlab functions; change ismembc to ismember if the */
          /*  function is not in the standard package */
          /*              if isOctave */
          if (1.0 > ElementsOnNodeNum->data[(int)nodeABCD_data[0] - 1]) {
            loop_ub = 0;
          } else {
            loop_ub = (int)ElementsOnNodeNum->data[(int)nodeABCD_data[0] - 1];
          }

          i26 = idx->size[0];
          idx->size[0] = loop_ub;
          emxEnsureCapacity_real_T(idx, i26);
          for (i26 = 0; i26 < loop_ub; i26++) {
            idx->data[i26] = ElementsOnNodeABCD->data[i26];
          }

          i26 = numfn_data[numPBC];
          for (j = 0; j <= i26 - 2; j++) {
            if (1.0 > ElementsOnNodeNum->data[(int)nodeABCD_data[j + 1] - 1]) {
              loop_ub = 0;
            } else {
              loop_ub = (int)ElementsOnNodeNum->data[(int)nodeABCD_data[j + 1] -
                1];
            }

            i = idx->size[0] - 1;
            nb = idx->size[0];
            i27 = elems->size[0];
            elems->size[0] = nb;
            emxEnsureCapacity_boolean_T(elems, i27);
            for (i27 = 0; i27 < nb; i27++) {
              elems->data[i27] = false;
            }

            guard2 = false;
            if (loop_ub <= 4) {
              guard2 = true;
            } else {
              pEnd = 31;
              k0 = 0;
              exitg2 = false;
              while ((!exitg2) && (pEnd - k0 > 1)) {
                p = (k0 + pEnd) >> 1;
                nx = 1 << p;
                if (nx == loop_ub) {
                  pEnd = p;
                  exitg2 = true;
                } else if (nx > loop_ub) {
                  pEnd = p;
                } else {
                  k0 = p;
                }
              }

              if (idx->size[0] <= 4 + pEnd) {
                guard2 = true;
              } else {
                i27 = s->size[0];
                s->size[0] = loop_ub;
                emxEnsureCapacity_real_T(s, i27);
                for (i27 = 0; i27 < loop_ub; i27++) {
                  s->data[i27] = ElementsOnNodeABCD->data[i27 +
                    ElementsOnNodeABCD->size[0] * (j + 1)];
                }

                if (!issorted(s)) {
                  i27 = s->size[0];
                  s->size[0] = loop_ub;
                  emxEnsureCapacity_real_T(s, i27);
                  for (i27 = 0; i27 < loop_ub; i27++) {
                    s->data[i27] = ElementsOnNodeABCD->data[i27 +
                      ElementsOnNodeABCD->size[0] * (j + 1)];
                  }

                  sort(s, c_s);
                  for (k = 0; k <= i; k++) {
                    if (bsearchni(k + 1, idx, s) > 0) {
                      elems->data[k] = true;
                    }
                  }
                } else {
                  for (k = 0; k <= i; k++) {
                    i27 = s->size[0];
                    s->size[0] = loop_ub;
                    emxEnsureCapacity_real_T(s, i27);
                    for (i27 = 0; i27 < loop_ub; i27++) {
                      s->data[i27] = ElementsOnNodeABCD->data[i27 +
                        ElementsOnNodeABCD->size[0] * (j + 1)];
                    }

                    if (bsearchni(k + 1, idx, s) > 0) {
                      elems->data[k] = true;
                    }
                  }
                }
              }
            }

            if (guard2) {
              for (pEnd = 0; pEnd <= i; pEnd++) {
                k = 0;
                exitg2 = false;
                while ((!exitg2) && (k <= loop_ub - 1)) {
                  reg = ElementsOnNodeABCD->data[k + ElementsOnNodeABCD->size[0]
                    * (j + 1)];
                  absxk = fabs(reg / 2.0);
                  if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
                    if (absxk <= 2.2250738585072014E-308) {
                      absxk = 4.94065645841247E-324;
                    } else {
                      frexp(absxk, &c_exponent);
                      absxk = ldexp(1.0, c_exponent - 53);
                    }
                  } else {
                    absxk = rtNaN;
                  }

                  if ((fabs(ElementsOnNodeABCD->data[k +
                            ElementsOnNodeABCD->size[0] * (j + 1)] - idx->
                            data[pEnd]) < absxk) || (rtIsInf(idx->data[pEnd]) &&
                       rtIsInf(reg) && ((idx->data[pEnd] > 0.0) ==
                                        (ElementsOnNodeABCD->data[k +
                                         ElementsOnNodeABCD->size[0] * (j + 1)] >
                         0.0)))) {
                    elems->data[pEnd] = true;
                    exitg2 = true;
                  } else {
                    k++;
                  }
                }
              }
            }

            k0 = elems->size[0] - 1;
            nx = 0;
            for (b_i = 0; b_i <= k0; b_i++) {
              if (elems->data[b_i]) {
                nx++;
              }
            }

            nb = 0;
            for (b_i = 0; b_i <= k0; b_i++) {
              if (elems->data[b_i]) {
                idx->data[nb] = idx->data[b_i];
                nb++;
              }
            }

            i27 = idx->size[0];
            idx->size[0] = nx;
            emxEnsureCapacity_real_T(idx, i27);
          }

          /*              else */
          /*                  twoelem = ElementsOnNodeABCD(1:numABCD(1),1); */
          /*                  for j = 2:numfacnod */
          /*                      twoelem = twoelem(ismembc(twoelem,ElementsOnNodeABCD(1:numABCD(j),j))); */
          /*                  end */
          /*              end */
          if (idx->size[0] == 2) {
            /*  element interface */
            if (idx->data[0] == 1.0 + (double)elem) {
              numA = idx->data[1];
            } else {
              numA = idx->data[0];
            }

            /*                  if usePBC % Don't reconsider new ghost elements */
            /*                      if elemA > numel */
            /*                          facIL = 0; */
            /*                      end */
            /*                  end */
            /* element interface, add to SurfacesI */
            /*  Find which slots each node occupies on elemA */
            nodeAABBCCDD[0] = 0;
            nodeAABBCCDD[1] = 0;
            nodeAABBCCDD[2] = 0;
            nodeAABBCCDD[3] = 0;

            /*  Other element may be a different type */
            if (1.0 > nen) {
              loop_ub = 0;
            } else {
              loop_ub = (int)nen;
            }

            for (i26 = 0; i26 < loop_ub; i26++) {
              nodeloc_data[i26] = NodesOnElementCG->data[((int)numA +
                NodesOnElementCG->size[0] * i26) - 1];
            }

            pEnd = 0;
            i26 = loop_ub - 1;
            for (k = 0; k <= i26; k++) {
              if (nodeloc_data[k] != 0.0) {
                pEnd++;
              }
            }

            if ((pEnd == 4) || (pEnd == 10)) {
              k0 = 4;
              for (i26 = 0; i26 < 24; i26++) {
                nloopA_data[i26] = nloopT[i26];
              }
            } else if ((pEnd == 6) || (pEnd == 18)) {
              k0 = 5;
              for (i26 = 0; i26 < 40; i26++) {
                nloopA_data[i26] = nloopW[i26];
              }
            } else if ((pEnd == 5) || (pEnd == 14)) {
              k0 = 5;
              for (i26 = 0; i26 < 40; i26++) {
                nloopA_data[i26] = nloopP[i26];
              }
            } else {
              k0 = 6;
              for (i26 = 0; i26 < 48; i26++) {
                nloopA_data[i26] = nloopH[i26];
              }
            }

            /*  But the number of nodes on face won't change */
            i26 = numfn_data[numPBC];
            for (j = 0; j < i26; j++) {
              b_i = 1;
              i1 = NodesOnElementCG->data[(int)numA - 1];
              while ((b_i < nel2 + 1) && (i1 != nodeABCD_data[j])) {
                b_i++;
                i1 = NodesOnElementCG->data[((int)numA + NodesOnElementCG->size
                  [0] * (b_i - 1)) - 1];
              }

              nodeAABBCCDD[j] = (signed char)b_i;
            }

            loop_ub = numfn_data[numPBC];
            i26 = s->size[0];
            s->size[0] = numfn_data[numPBC];
            emxEnsureCapacity_real_T(s, i26);
            for (i26 = 0; i26 < loop_ub; i26++) {
              s->data[i26] = nodeAABBCCDD[i26];
            }

            c_sort(s);
            loop_ub = numfn_data[numPBC];
            i26 = idx->size[0];
            idx->size[0] = numfn_data[numPBC];
            emxEnsureCapacity_real_T(idx, i26);
            for (i26 = 0; i26 < loop_ub; i26++) {
              idx->data[i26] = nodeAABBCCDD[i26];
            }

            c_sort(idx);
            i26 = NodeRegInd->size[0] * NodeRegInd->size[1];
            NodeRegInd->size[0] = 1;
            NodeRegInd->size[1] = s->size[0];
            emxEnsureCapacity_real_T(NodeRegInd, i26);
            loop_ub = s->size[0];
            for (i26 = 0; i26 < loop_ub; i26++) {
              NodeRegInd->data[i26] = s->data[i26];
            }

            pEnd = idx->size[0];
            for (i26 = 0; i26 < pEnd; i26++) {
              link_data[i26] = NodeRegInd->data[i26];
            }

            i = -1;
            nb = 0;
            exitg2 = false;
            while ((!exitg2) && (nb <= k0 - 1)) {
              if (numfn_data[numPBC] == numfn_data[nb]) {
                nodeAABBCCDD2_size[0] = 1;
                nodeAABBCCDD2_size[1] = pEnd;
                for (i26 = 0; i26 < pEnd; i26++) {
                  nodeAABBCCDD2_data[i26] = link_data[i26] - (double)
                    nloopA_data[nb + k0 * i26];
                }

                if (b_norm(nodeAABBCCDD2_data, nodeAABBCCDD2_size) == 0.0) {
                  i = nb;
                  exitg2 = true;
                } else {
                  nb++;
                }
              } else {
                nb++;
              }
            }

            if (FacetsOnElement->data[((int)numA + FacetsOnElement->size[0] * i)
                - 1] == 0.0) {
              /*  New edge, add to list */
              q = elem;
              nb = numPBC;
              if (RegionOnElement->data[(int)numA - 1] > RegionOnElement->
                  data[elem]) {
                /* swap the order of L and R so that L is always larger material ID */
                node = RegionOnElement->data[elem];
                regB = RegionOnElement->data[(int)numA - 1];
                q = (int)numA - 1;
                numA = 1.0 + (double)elem;
                nb = i;
                i = numPBC;
              } else {
                node = RegionOnElement->data[(int)numA - 1];
                regB = RegionOnElement->data[elem];
              }

              regI = regB * (regB - 1.0) / 2.0 + node;

              /*  ID for material pair (row=mat2, col=mat1) */
              notdone = (int)regI - 1;
              *numSI = numEonF->data[notdone] + 1.0;
              (*numfac)++;
              numEonF->data[notdone]++;
              FacetsOnElement->data[q + FacetsOnElement->size[0] * nb] = *numfac;
              i26 = (int)numA;
              FacetsOnElement->data[(i26 + FacetsOnElement->size[0] * i) - 1] = *
                numfac;
              FacetsOnElementInt->data[q + FacetsOnElementInt->size[0] * nb] =
                regI;
              FacetsOnElementInt->data[(i26 + FacetsOnElementInt->size[0] * i) -
                1] = regI;
              i27 = (int)*numfac;
              b_exponent = i27 - 1;
              ElementsOnFacet->data[b_exponent] = q + 1;

              /* elemL */
              ElementsOnFacet->data[(i27 + ElementsOnFacet->size[0]) - 1] = numA;

              /* elemR */
              ElementsOnFacet->data[(i27 + (ElementsOnFacet->size[0] << 1)) - 1]
                = (double)nb + 1.0;

              /* facL */
              ElementsOnFacet->data[(i27 + ElementsOnFacet->size[0] * 3) - 1] =
                (double)i + 1.0;

              /* facR */
              FacetsOnInterface->data[b_exponent] = regI;
              if (usePBC != 0.0) {
                /*  Other element may be a different type */
                if (1.0 > nen) {
                  loop_ub = 0;
                } else {
                  loop_ub = (int)nen;
                }

                for (i27 = 0; i27 < loop_ub; i27++) {
                  nodeloc_data[i27] = NodesOnElementCG->data[(i26 +
                    NodesOnElementCG->size[0] * i27) - 1];
                }

                pEnd = 0;
                i27 = loop_ub - 1;
                for (k = 0; k <= i27; k++) {
                  if (nodeloc_data[k] != 0.0) {
                    pEnd++;
                  }
                }

                if ((pEnd == 4) || (pEnd == 10)) {
                  for (i27 = 0; i27 < 5; i27++) {
                    numfnA_data[i27] = 3;
                  }

                  k0 = 4;
                  for (i27 = 0; i27 < 24; i27++) {
                    nloopA_data[i27] = nloopT[i27];
                  }
                } else if ((pEnd == 6) || (pEnd == 18)) {
                  for (i27 = 0; i27 < 5; i27++) {
                    numfnA_data[i27] = numfnW[i27];
                  }

                  k0 = 5;
                  for (i27 = 0; i27 < 40; i27++) {
                    nloopA_data[i27] = nloopW[i27];
                  }
                } else if ((pEnd == 5) || (pEnd == 14)) {
                  for (i27 = 0; i27 < 5; i27++) {
                    numfnA_data[i27] = numfnP[i27];
                  }

                  k0 = 5;
                  for (i27 = 0; i27 < 40; i27++) {
                    nloopA_data[i27] = nloopP[i27];
                  }
                } else {
                  for (i27 = 0; i27 < 6; i27++) {
                    numfnA_data[i27] = 4;
                  }

                  k0 = 6;
                  for (i27 = 0; i27 < 48; i27++) {
                    nloopA_data[i27] = nloopH[i27];
                  }
                }

                /*  number of nodes on face */
                loop_ub = numfnA_data[i];
                for (i27 = 0; i27 < loop_ub; i27++) {
                  link_data[i27] = NodesOnElementCG->data[(i26 +
                    NodesOnElementCG->size[0] * (nloopA_data[i + k0 * i27] - 1))
                    - 1];
                }

                loop_ub = numfnA_data[i];
                nodeAABBCCDD2_size[0] = 1;
                nodeAABBCCDD2_size[1] = numfnA_data[i];
                for (i27 = 0; i27 < loop_ub; i27++) {
                  nodeAABBCCDD2_data[i27] = NodesOnElementPBC->data[(i26 +
                    NodesOnElementPBC->size[0] * (nloopA_data[i + k0 * i27] - 1))
                    - 1];
                }

                if (1.0 > nen) {
                  loop_ub = 0;
                } else {
                  loop_ub = (int)nen;
                }

                for (i26 = 0; i26 < loop_ub; i26++) {
                  nodeloc_data[i26] = NodesOnElementCG->data[q +
                    NodesOnElementCG->size[0] * i26];
                }

                pEnd = 0;
                i26 = loop_ub - 1;
                for (k = 0; k <= i26; k++) {
                  if (nodeloc_data[k] != 0.0) {
                    pEnd++;
                  }
                }

                if ((pEnd == 4) || (pEnd == 10)) {
                  pEnd = 4;
                  for (i26 = 0; i26 < 24; i26++) {
                    nloopB_data[i26] = nloopT[i26];
                  }
                } else if ((pEnd == 6) || (pEnd == 18)) {
                  pEnd = 5;
                  for (i26 = 0; i26 < 40; i26++) {
                    nloopB_data[i26] = nloopW[i26];
                  }
                } else if ((pEnd == 5) || (pEnd == 14)) {
                  pEnd = 5;
                  for (i26 = 0; i26 < 40; i26++) {
                    nloopB_data[i26] = nloopP[i26];
                  }
                } else {
                  pEnd = 6;
                  for (i26 = 0; i26 < 48; i26++) {
                    nloopB_data[i26] = nloopH[i26];
                  }
                }

                loop_ub = numfnA_data[i];
                for (i26 = 0; i26 < loop_ub; i26++) {
                  nodeEEFFGGHH_data[i26] = NodesOnElementCG->data[q +
                    NodesOnElementCG->size[0] * (nloopB_data[nb + pEnd * i26] -
                    1)];
                }

                loop_ub = numfnA_data[i];
                for (i26 = 0; i26 < loop_ub; i26++) {
                  nodeEEFFGGHH2_data[i26] = NodesOnElementPBC->data[q +
                    NodesOnElementPBC->size[0] * (nloopB_data[nb + pEnd * i26] -
                    1)];
                }

                link_size[0] = 1;
                link_size[1] = numfnA_data[i];
                loop_ub = numfnA_data[i];
                for (i26 = 0; i26 < loop_ub; i26++) {
                  b_numABCD_data[i26] = (link_data[i26] !=
                    nodeAABBCCDD2_data[i26]);
                }

                guard2 = false;
                guard3 = false;
                if (any(b_numABCD_data, link_size)) {
                  guard3 = true;
                } else {
                  link_size[0] = 1;
                  link_size[1] = numfnA_data[i];
                  loop_ub = numfnA_data[i];
                  for (i26 = 0; i26 < loop_ub; i26++) {
                    b_numABCD_data[i26] = (nodeEEFFGGHH_data[i26] !=
                      nodeEEFFGGHH2_data[i26]);
                  }

                  if (any(b_numABCD_data, link_size)) {
                    guard3 = true;
                  } else {
                    k0 = 0;
                  }
                }

                if (guard3) {
                  /*  Make sure facet is NOT an interface in the */
                  /*  unzipped mesh, because for triangles both nodes */
                  /*  might be in PBCList but they are on different */
                  /*  boundary surfaces */
                  loop_ub = numfnA_data[i];
                  for (i26 = 0; i26 < loop_ub; i26++) {
                    numABCD_data[i26] = ElementsOnNodePBCNum->data[(int)
                      nodeAABBCCDD2_data[i26] - 1];
                  }

                  pEnd = numfnA_data[i];
                  i1 = numABCD_data[0];
                  for (k = 2; k <= pEnd; k++) {
                    reg = numABCD_data[k - 1];
                    if (i1 < reg) {
                      i1 = reg;
                    }
                  }

                  if (i1 < 1.0) {
                    setAll->size[0] = 1;
                    setAll->size[1] = 0;
                  } else if (rtIsInf(i1) && (1.0 == i1)) {
                    i26 = setAll->size[0] * setAll->size[1];
                    setAll->size[0] = 1;
                    setAll->size[1] = 1;
                    emxEnsureCapacity_real_T(setAll, i26);
                    setAll->data[0] = rtNaN;
                  } else {
                    i26 = setAll->size[0] * setAll->size[1];
                    setAll->size[0] = 1;
                    loop_ub = (int)floor(i1 - 1.0);
                    setAll->size[1] = loop_ub + 1;
                    emxEnsureCapacity_real_T(setAll, i26);
                    for (i26 = 0; i26 <= loop_ub; i26++) {
                      setAll->data[i26] = 1.0 + (double)i26;
                    }
                  }

                  b_sparse_parenReference(ElementsOnNodePBC_d,
                    ElementsOnNodePBC_colidx, ElementsOnNodePBC_rowidx, setAll,
                    nodeAABBCCDD2_data, nodeAABBCCDD2_size, s, b_idx, ii, &pEnd,
                    &nx, &q);
                  b_sparse_full(s, b_idx, ii, pEnd, nx, ElementsOnNodeABCD);

                  /*  Determine if edge is on domain interior or boundary */
                  links2_size[0] = numfnA_data[i];
                  loop_ub = numfnA_data[i];
                  for (i26 = 0; i26 < loop_ub; i26++) {
                    b_numABCD_data[i26] = (numABCD_data[i26] > 1.0);
                  }

                  if (all(b_numABCD_data, links2_size)) {
                    /*  Clean and fast way to intersect the 3 sets of elements, using */
                    /*  built-in Matlab functions; change ismembc to ismember if the */
                    /*  function is not in the standard package */
                    /*                          if isOctave */
                    if (1.0 > ElementsOnNodePBCNum->data[(int)
                        nodeAABBCCDD2_data[0] - 1]) {
                      loop_ub = 0;
                    } else {
                      loop_ub = (int)ElementsOnNodePBCNum->data[(int)
                        nodeAABBCCDD2_data[0] - 1];
                    }

                    i26 = idx->size[0];
                    idx->size[0] = loop_ub;
                    emxEnsureCapacity_real_T(idx, i26);
                    for (i26 = 0; i26 < loop_ub; i26++) {
                      idx->data[i26] = ElementsOnNodeABCD->data[i26];
                    }

                    i26 = numfn_data[numPBC];
                    for (j = 0; j <= i26 - 2; j++) {
                      if (1.0 > ElementsOnNodePBCNum->data[(int)
                          nodeAABBCCDD2_data[j + 1] - 1]) {
                        loop_ub = 0;
                      } else {
                        loop_ub = (int)ElementsOnNodePBCNum->data[(int)
                          nodeAABBCCDD2_data[j + 1] - 1];
                      }

                      i = idx->size[0] - 1;
                      nb = idx->size[0];
                      i27 = elems->size[0];
                      elems->size[0] = nb;
                      emxEnsureCapacity_boolean_T(elems, i27);
                      for (i27 = 0; i27 < nb; i27++) {
                        elems->data[i27] = false;
                      }

                      guard4 = false;
                      if (loop_ub <= 4) {
                        guard4 = true;
                      } else {
                        pEnd = 31;
                        k0 = 0;
                        exitg2 = false;
                        while ((!exitg2) && (pEnd - k0 > 1)) {
                          p = (k0 + pEnd) >> 1;
                          nx = 1 << p;
                          if (nx == loop_ub) {
                            pEnd = p;
                            exitg2 = true;
                          } else if (nx > loop_ub) {
                            pEnd = p;
                          } else {
                            k0 = p;
                          }
                        }

                        if (idx->size[0] <= 4 + pEnd) {
                          guard4 = true;
                        } else {
                          i27 = s->size[0];
                          s->size[0] = loop_ub;
                          emxEnsureCapacity_real_T(s, i27);
                          for (i27 = 0; i27 < loop_ub; i27++) {
                            s->data[i27] = ElementsOnNodeABCD->data[i27 +
                              ElementsOnNodeABCD->size[0] * (j + 1)];
                          }

                          if (!issorted(s)) {
                            i27 = s->size[0];
                            s->size[0] = loop_ub;
                            emxEnsureCapacity_real_T(s, i27);
                            for (i27 = 0; i27 < loop_ub; i27++) {
                              s->data[i27] = ElementsOnNodeABCD->data[i27 +
                                ElementsOnNodeABCD->size[0] * (j + 1)];
                            }

                            sort(s, d_s);
                            for (k = 0; k <= i; k++) {
                              if (bsearchni(k + 1, idx, s) > 0) {
                                elems->data[k] = true;
                              }
                            }
                          } else {
                            for (k = 0; k <= i; k++) {
                              i27 = s->size[0];
                              s->size[0] = loop_ub;
                              emxEnsureCapacity_real_T(s, i27);
                              for (i27 = 0; i27 < loop_ub; i27++) {
                                s->data[i27] = ElementsOnNodeABCD->data[i27 +
                                  ElementsOnNodeABCD->size[0] * (j + 1)];
                              }

                              if (bsearchni(k + 1, idx, s) > 0) {
                                elems->data[k] = true;
                              }
                            }
                          }
                        }
                      }

                      if (guard4) {
                        for (pEnd = 0; pEnd <= i; pEnd++) {
                          k = 0;
                          exitg2 = false;
                          while ((!exitg2) && (k <= loop_ub - 1)) {
                            reg = ElementsOnNodeABCD->data[k +
                              ElementsOnNodeABCD->size[0] * (j + 1)];
                            absxk = fabs(reg / 2.0);
                            if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
                              if (absxk <= 2.2250738585072014E-308) {
                                absxk = 4.94065645841247E-324;
                              } else {
                                frexp(absxk, &e_exponent);
                                absxk = ldexp(1.0, e_exponent - 53);
                              }
                            } else {
                              absxk = rtNaN;
                            }

                            if ((fabs(ElementsOnNodeABCD->data[k +
                                      ElementsOnNodeABCD->size[0] * (j + 1)] -
                                      idx->data[pEnd]) < absxk) || (rtIsInf
                                 (idx->data[pEnd]) && rtIsInf(reg) &&
                                 ((idx->data[pEnd] > 0.0) ==
                                  (ElementsOnNodeABCD->data[k +
                                   ElementsOnNodeABCD->size[0] * (j + 1)] > 0.0))))
                            {
                              elems->data[pEnd] = true;
                              exitg2 = true;
                            } else {
                              k++;
                            }
                          }
                        }
                      }

                      k0 = elems->size[0] - 1;
                      nx = 0;
                      for (b_i = 0; b_i <= k0; b_i++) {
                        if (elems->data[b_i]) {
                          nx++;
                        }
                      }

                      nb = 0;
                      for (b_i = 0; b_i <= k0; b_i++) {
                        if (elems->data[b_i]) {
                          idx->data[nb] = idx->data[b_i];
                          nb++;
                        }
                      }

                      i27 = idx->size[0];
                      idx->size[0] = nx;
                      emxEnsureCapacity_real_T(idx, i27);
                    }

                    /*                          else */
                    /*                              twoelem = ElementsOnNodeABCD(1:numABCD(1),1); */
                    /*                              for j = 2:numfacnod */
                    /*                                  twoelem = twoelem(ismembc(twoelem,ElementsOnNodeABCD(1:numABCD(j),j))); */
                    /*                              end */
                    /*                          end */
                    if (idx->size[0] == 2) {
                      /*  element interface */
                      /*                  if usePBC % Don't reconsider new ghost elements */
                      /*                      if elemA > numel */
                      /*                          facIL = 0; */
                      /*                      end */
                      /*                  end */
                      k0 = 0;
                    } else {
                      /*  domain boundary */
                      guard2 = true;
                    }
                  } else {
                    /*  domain boundary */
                    guard2 = true;
                  }
                }

                if (guard2) {
                  k0 = 1;
                  *numSI = numEonPBC->data[notdone] + 1.0;
                  numEonPBC->data[notdone]++;
                  numFPBC++;
                  FacetsOnPBC->data[(int)*numfac - 1] = regI;
                }
              } else {
                k0 = 0;
              }

              /*  Assign nodal edge pairs */
              if ((InterTypes->data[((int)regB + InterTypes->size[0] * ((int)
                     node - 1)) - 1] > 0.0) || (k0 != 0)) {
                old = true;
              } else {
                old = false;
              }

              i26 = numfn_data[numPBC];
              for (j = 0; j < i26; j++) {
                k0 = (int)nodeABCD_data[j] - 1;
                i1 = FacetsOnNodeNum->data[k0] + 1.0;
                FacetsOnNodeNum->data[k0]++;
                nri++;
                FacetsOnNodeInd->data[5 * ((int)nri - 1)] = i1;
                FacetsOnNodeInd->data[1 + 5 * ((int)nri - 1)] = nodeABCD_data[j];
                FacetsOnNodeInd->data[2 + 5 * ((int)nri - 1)] = *numfac;
                FacetsOnNodeInd->data[3 + 5 * ((int)nri - 1)] = regI;
                if (old) {
                  FacetsOnNodeInd->data[4 + 5 * ((int)nri - 1)] = 1.0;
                }

                /*                      FacetsOnNode(facnumABCD,nodeABCD(j)) = numfac; */
                /*                      FacetsOnNodeInt(facnumABCD,nodeABCD(j)) = regI; */
                /*                      if nodecut */
                /*                          FacetsOnNodeCut(facnumABCD,nodeABCD(j)) = 1; */
                /*                      end */
              }

              if (n > nel2) {
                /*  Add in the nodes along the edge too, but not the face */
                loop_ub = (numfn_data[numPBC] << 1) - numfn_data[numPBC];
                for (i26 = 0; i26 < loop_ub; i26++) {
                  nodeABCD_data[i26] = NodesOnElementCG->data[elem +
                    NodesOnElementCG->size[0] * (nloop_data[numPBC + exponent *
                    (numfn_data[numPBC] + i26)] - 1)];
                }

                old = (InterTypes->data[((int)regB + InterTypes->size[0] * ((int)
                         node - 1)) - 1] > 0.0);
                i26 = numfn_data[numPBC];
                for (j = 0; j < i26; j++) {
                  k0 = (int)nodeABCD_data[j] - 1;
                  i1 = FacetsOnNodeNum->data[k0] + 1.0;
                  FacetsOnNodeNum->data[k0]++;
                  nri++;
                  FacetsOnNodeInd->data[5 * ((int)nri - 1)] = i1;
                  FacetsOnNodeInd->data[1 + 5 * ((int)nri - 1)] =
                    nodeABCD_data[j];
                  FacetsOnNodeInd->data[2 + 5 * ((int)nri - 1)] = *numfac;
                  FacetsOnNodeInd->data[3 + 5 * ((int)nri - 1)] = regI;
                  if (old) {
                    FacetsOnNodeInd->data[4 + 5 * ((int)nri - 1)] = 1.0;
                  }

                  /*                          FacetsOnNode(facnumABCD,nodeABCD(j)) = numfac; */
                  /*                          FacetsOnNodeInt(facnumABCD,nodeABCD(j)) = regI; */
                  /*                          if nodecut */
                  /*                              FacetsOnNodeCut(facnumABCD,nodeABCD(j)) = 1; */
                  /*                          end */
                }
              }
            }
          } else {
            /*  domain boundary */
            guard1 = true;
          }
        } else {
          /*  domain boundary */
          guard1 = true;
        }

        if (guard1) {
          /* domain boundary, add to SurfaceF */
          *numSI = *numEonB + 1.0;
          (*numEonB)++;
          FacetsOnElement->data[elem + FacetsOnElement->size[0] * numPBC] =
            -*numSI;
          i26 = (int)*numSI;
          ElementsOnBoundary->data[i26 - 1] = 1.0 + (double)elem;
          ElementsOnBoundary->data[(i26 + ElementsOnBoundary->size[0]) - 1] =
            1.0 + (double)numPBC;

          /*  Assign nodal edge pairs */
          i26 = numfn_data[numPBC];
          for (j = 0; j < i26; j++) {
            k0 = (int)nodeABCD_data[j] - 1;
            i1 = FacetsOnNodeNum->data[k0] + 1.0;
            FacetsOnNodeNum->data[k0]++;
            nri++;
            FacetsOnNodeInd->data[5 * ((int)nri - 1)] = i1;
            i27 = 5 * ((int)nri - 1);
            FacetsOnNodeInd->data[1 + i27] = nodeABCD_data[j];
            FacetsOnNodeInd->data[2 + i27] = *numSI;
            FacetsOnNodeInd->data[3 + i27] = -1.0;

            /*                  FacetsOnNode(facnumABCD,nodeABCD(j)) = numSI; */
            /*                  FacetsOnNodeInt(facnumABCD,nodeABCD(j)) = -1; */
          }

          if (n > nel2) {
            /*  Add in the nodes along the edge too, but not the face */
            i = (numfn_data[numPBC] << 1) - numfn_data[numPBC];
            for (i26 = 0; i26 < i; i26++) {
              nodeABCD_data[i26] = NodesOnElementCG->data[elem +
                NodesOnElementCG->size[0] * (nloop_data[numPBC + exponent *
                (numfn_data[numPBC] + i26)] - 1)];
            }

            i26 = numfn_data[numPBC];
            for (j = 0; j < i26; j++) {
              k0 = (int)nodeABCD_data[j] - 1;
              i1 = FacetsOnNodeNum->data[k0] + 1.0;
              FacetsOnNodeNum->data[k0]++;
              nri++;
              FacetsOnNodeInd->data[5 * ((int)nri - 1)] = i1;
              FacetsOnNodeInd->data[1 + 5 * ((int)nri - 1)] = nodeABCD_data[j];
              FacetsOnNodeInd->data[2 + 5 * ((int)nri - 1)] = *numSI;
              FacetsOnNodeInd->data[3 + 5 * ((int)nri - 1)] = -1.0;

              /*                      FacetsOnNode(facnumABCD,nodeABCD(j)) = numSI; */
              /*                      FacetsOnNodeInt(facnumABCD,nodeABCD(j)) = -1; */
            }
          }
        }

        /* facIL */
      }

      /* facet not identified */
    }

    /* locF */
  }

  emxFree_int32_T(&d_s);
  emxFree_int32_T(&c_s);
  emxFree_real_T(&ElementsOnNodeABCD);

  /* elem */
  if (1.0 > *numfac) {
    loop_ub = 0;
  } else {
    loop_ub = (int)*numfac;
  }

  i25 = PBCList2->size[0] * PBCList2->size[1];
  PBCList2->size[0] = loop_ub;
  PBCList2->size[1] = 4;
  emxEnsureCapacity_real_T(PBCList2, i25);
  for (i25 = 0; i25 < loop_ub; i25++) {
    PBCList2->data[i25] = ElementsOnFacet->data[i25];
  }

  for (i25 = 0; i25 < loop_ub; i25++) {
    PBCList2->data[i25 + PBCList2->size[0]] = ElementsOnFacet->data[i25 +
      ElementsOnFacet->size[0]];
  }

  for (i25 = 0; i25 < loop_ub; i25++) {
    PBCList2->data[i25 + (PBCList2->size[0] << 1)] = ElementsOnFacet->data[i25 +
      (ElementsOnFacet->size[0] << 1)];
  }

  for (i25 = 0; i25 < loop_ub; i25++) {
    PBCList2->data[i25 + PBCList2->size[0] * 3] = ElementsOnFacet->data[i25 +
      ElementsOnFacet->size[0] * 3];
  }

  i25 = ElementsOnFacet->size[0] * ElementsOnFacet->size[1];
  ElementsOnFacet->size[0] = PBCList2->size[0];
  ElementsOnFacet->size[1] = 4;
  emxEnsureCapacity_real_T(ElementsOnFacet, i25);
  loop_ub = PBCList2->size[0] * PBCList2->size[1];
  for (i25 = 0; i25 < loop_ub; i25++) {
    ElementsOnFacet->data[i25] = PBCList2->data[i25];
  }

  emxFree_real_T(&PBCList2);
  if (1.0 > *numEonB) {
    loop_ub = 0;
  } else {
    loop_ub = (int)*numEonB;
  }

  i25 = b_MPCList->size[0] * b_MPCList->size[1];
  b_MPCList->size[0] = loop_ub;
  b_MPCList->size[1] = 2;
  emxEnsureCapacity_real_T(b_MPCList, i25);
  for (i25 = 0; i25 < loop_ub; i25++) {
    b_MPCList->data[i25] = ElementsOnBoundary->data[i25];
  }

  for (i25 = 0; i25 < loop_ub; i25++) {
    b_MPCList->data[i25 + b_MPCList->size[0]] = ElementsOnBoundary->data[i25 +
      ElementsOnBoundary->size[0]];
  }

  i25 = ElementsOnBoundary->size[0] * ElementsOnBoundary->size[1];
  ElementsOnBoundary->size[0] = b_MPCList->size[0];
  ElementsOnBoundary->size[1] = 2;
  emxEnsureCapacity_real_T(ElementsOnBoundary, i25);
  loop_ub = b_MPCList->size[0] * b_MPCList->size[1];
  for (i25 = 0; i25 < loop_ub; i25++) {
    ElementsOnBoundary->data[i25] = b_MPCList->data[i25];
  }

  emxFree_real_T(&b_MPCList);
  if (1 > (int)nri) {
    loop_ub = 0;
    nb = 0;
    k0 = 0;
  } else {
    loop_ub = (int)nri;
    nb = (int)nri;
    k0 = (int)nri;
  }

  i25 = NodeRegInd->size[0] * NodeRegInd->size[1];
  NodeRegInd->size[0] = 1;
  NodeRegInd->size[1] = loop_ub;
  emxEnsureCapacity_real_T(NodeRegInd, i25);
  for (i25 = 0; i25 < loop_ub; i25++) {
    NodeRegInd->data[i25] = FacetsOnNodeInd->data[5 * i25];
  }

  i25 = b_NodeRegInd->size[0] * b_NodeRegInd->size[1];
  b_NodeRegInd->size[0] = 1;
  b_NodeRegInd->size[1] = nb;
  emxEnsureCapacity_real_T(b_NodeRegInd, i25);
  for (i25 = 0; i25 < nb; i25++) {
    b_NodeRegInd->data[i25] = FacetsOnNodeInd->data[1 + 5 * i25];
  }

  i25 = c_NodeRegInd->size[0] * c_NodeRegInd->size[1];
  c_NodeRegInd->size[0] = 1;
  c_NodeRegInd->size[1] = k0;
  emxEnsureCapacity_real_T(c_NodeRegInd, i25);
  for (i25 = 0; i25 < k0; i25++) {
    c_NodeRegInd->data[i25] = FacetsOnNodeInd->data[2 + 5 * i25];
  }

  b_sparse(NodeRegInd, b_NodeRegInd, c_NodeRegInd, FacetsOnNode);

  /*  for each node, which other node is connected to it by an edge; value is the nodal ID of the connecting node */
  if (1 > (int)nri) {
    loop_ub = 0;
    nb = 0;
    k0 = 0;
  } else {
    loop_ub = (int)nri;
    nb = (int)nri;
    k0 = (int)nri;
  }

  i25 = NodeRegInd->size[0] * NodeRegInd->size[1];
  NodeRegInd->size[0] = 1;
  NodeRegInd->size[1] = loop_ub;
  emxEnsureCapacity_real_T(NodeRegInd, i25);
  for (i25 = 0; i25 < loop_ub; i25++) {
    NodeRegInd->data[i25] = FacetsOnNodeInd->data[5 * i25];
  }

  i25 = b_NodeRegInd->size[0] * b_NodeRegInd->size[1];
  b_NodeRegInd->size[0] = 1;
  b_NodeRegInd->size[1] = nb;
  emxEnsureCapacity_real_T(b_NodeRegInd, i25);
  for (i25 = 0; i25 < nb; i25++) {
    b_NodeRegInd->data[i25] = FacetsOnNodeInd->data[1 + 5 * i25];
  }

  i25 = c_NodeRegInd->size[0] * c_NodeRegInd->size[1];
  c_NodeRegInd->size[0] = 1;
  c_NodeRegInd->size[1] = k0;
  emxEnsureCapacity_real_T(c_NodeRegInd, i25);
  for (i25 = 0; i25 < k0; i25++) {
    c_NodeRegInd->data[i25] = FacetsOnNodeInd->data[4 + 5 * i25];
  }

  b_sparse(NodeRegInd, b_NodeRegInd, c_NodeRegInd, FacetsOnNodeCut);

  /*  flag for whether that edge is being cut between two nodes; 1 for cut, 0 for retain. */
  if (1 > (int)nri) {
    loop_ub = 0;
    nb = 0;
    k0 = 0;
  } else {
    loop_ub = (int)nri;
    nb = (int)nri;
    k0 = (int)nri;
  }

  i25 = NodeRegInd->size[0] * NodeRegInd->size[1];
  NodeRegInd->size[0] = 1;
  NodeRegInd->size[1] = loop_ub;
  emxEnsureCapacity_real_T(NodeRegInd, i25);
  for (i25 = 0; i25 < loop_ub; i25++) {
    NodeRegInd->data[i25] = FacetsOnNodeInd->data[5 * i25];
  }

  i25 = b_NodeRegInd->size[0] * b_NodeRegInd->size[1];
  b_NodeRegInd->size[0] = 1;
  b_NodeRegInd->size[1] = nb;
  emxEnsureCapacity_real_T(b_NodeRegInd, i25);
  for (i25 = 0; i25 < nb; i25++) {
    b_NodeRegInd->data[i25] = FacetsOnNodeInd->data[1 + 5 * i25];
  }

  i25 = c_NodeRegInd->size[0] * c_NodeRegInd->size[1];
  c_NodeRegInd->size[0] = 1;
  c_NodeRegInd->size[1] = k0;
  emxEnsureCapacity_real_T(c_NodeRegInd, i25);
  for (i25 = 0; i25 < k0; i25++) {
    c_NodeRegInd->data[i25] = FacetsOnNodeInd->data[3 + 5 * i25];
  }

  emxFree_real_T(&FacetsOnNodeInd);
  b_sparse(NodeRegInd, b_NodeRegInd, c_NodeRegInd, FacetsOnNodeInt);

  /*  flag for materialI ID for that edge; -1 means domain edge. */
  /*  clear FacetsOnNodeInd */
  /*  Group facets according to interface type */
  emxFree_real_T(&c_NodeRegInd);
  emxFree_real_T(&b_NodeRegInd);
  emxFree_real_T(&NodeRegInd);
  if (1.0 > *numfac) {
    loop_ub = 0;
  } else {
    loop_ub = (int)*numfac;
  }

  i25 = idx->size[0];
  idx->size[0] = loop_ub;
  emxEnsureCapacity_real_T(idx, i25);
  for (i25 = 0; i25 < loop_ub; i25++) {
    idx->data[i25] = FacetsOnInterface->data[i25];
  }

  sort(idx, b_idx);
  i25 = FacetsOnInterface->size[0];
  FacetsOnInterface->size[0] = b_idx->size[0];
  emxEnsureCapacity_real_T(FacetsOnInterface, i25);
  loop_ub = b_idx->size[0];
  for (i25 = 0; i25 < loop_ub; i25++) {
    FacetsOnInterface->data[i25] = b_idx->data[i25];
  }

  i25 = FacetsOnIntMinusPBCNum->size[0];
  FacetsOnIntMinusPBCNum->size[0] = 1 + numEonF->size[0];
  emxEnsureCapacity_real_T(FacetsOnIntMinusPBCNum, i25);
  FacetsOnIntMinusPBCNum->data[0] = 1.0;
  loop_ub = numEonF->size[0];
  for (i25 = 0; i25 < loop_ub; i25++) {
    FacetsOnIntMinusPBCNum->data[i25 + 1] = numEonF->data[i25];
  }

  i25 = FacetsOnInterfaceNum->size[0];
  FacetsOnInterfaceNum->size[0] = FacetsOnIntMinusPBCNum->size[0];
  emxEnsureCapacity_real_T(FacetsOnInterfaceNum, i25);
  loop_ub = FacetsOnIntMinusPBCNum->size[0];
  for (i25 = 0; i25 < loop_ub; i25++) {
    FacetsOnInterfaceNum->data[i25] = FacetsOnIntMinusPBCNum->data[i25];
  }

  for (k0 = 0; k0 < b_loop_ub; k0++) {
    FacetsOnInterfaceNum->data[k0 + 1] += FacetsOnInterfaceNum->data[k0];
  }

  /*  % The actual facet identifiers for interface type regI are then: */
  /*  locF = FacetsOnInterfaceNum(regI):(FacetsOnInterfaceNum(regI+1)-1); */
  /*  facs = FacetsOnInterface(locF); */
  /*  true = all(ElementsOnFacet(facs,1:4) == ElementsOnFacetInt(1:numEonF(regI),1:4,regI)); */
  if (usePBC != 0.0) {
    /*  sort to find the list of PBC facets grouped by interface type */
    if (1.0 > *numfac) {
      loop_ub = 0;
    } else {
      loop_ub = (int)*numfac;
    }

    i25 = idx->size[0];
    idx->size[0] = loop_ub;
    emxEnsureCapacity_real_T(idx, i25);
    for (i25 = 0; i25 < loop_ub; i25++) {
      idx->data[i25] = FacetsOnPBC->data[i25];
    }

    sort(idx, b_idx);
    i25 = idx->size[0];
    idx->size[0] = b_idx->size[0];
    emxEnsureCapacity_real_T(idx, i25);
    loop_ub = b_idx->size[0];
    for (i25 = 0; i25 < loop_ub; i25++) {
      idx->data[i25] = b_idx->data[i25];
    }

    reg = *numfac - numFPBC;
    if (reg + 1.0 > *numfac) {
      i25 = 1;
      i26 = 0;
    } else {
      i25 = (int)(reg + 1.0);
      i26 = (int)*numfac;
    }

    i27 = FacetsOnPBC->size[0];
    loop_ub = i26 - i25;
    FacetsOnPBC->size[0] = loop_ub + 1;
    emxEnsureCapacity_real_T(FacetsOnPBC, i27);
    for (i26 = 0; i26 <= loop_ub; i26++) {
      FacetsOnPBC->data[i26] = idx->data[(i25 + i26) - 1];
    }

    /*  delete the zeros */
    i26 = FacetsOnPBCNum->size[0];
    FacetsOnPBCNum->size[0] = 1 + numEonPBC->size[0];
    emxEnsureCapacity_real_T(FacetsOnPBCNum, i26);
    FacetsOnPBCNum->data[0] = 1.0;
    loop_ub = numEonPBC->size[0];
    for (i26 = 0; i26 < loop_ub; i26++) {
      FacetsOnPBCNum->data[i26 + 1] = numEonPBC->data[i26];
    }

    for (k0 = 0; k0 < b_loop_ub; k0++) {
      FacetsOnPBCNum->data[k0 + 1] += FacetsOnPBCNum->data[k0];
    }

    i26 = FacetsOnIntMinusPBC->size[0];
    loop_ub = (int)reg;
    FacetsOnIntMinusPBC->size[0] = loop_ub;
    emxEnsureCapacity_real_T(FacetsOnIntMinusPBC, i26);
    for (i26 = 0; i26 < loop_ub; i26++) {
      FacetsOnIntMinusPBC->data[i26] = 0.0;
    }

    emxInit_real_T(&setPBC, 2);
    for (k0 = 0; k0 < b_loop_ub; k0++) {
      if (FacetsOnInterfaceNum->data[1 + k0] - 1.0 < FacetsOnInterfaceNum->
          data[k0]) {
        setAll->size[0] = 1;
        setAll->size[1] = 0;
      } else if ((rtIsInf(FacetsOnInterfaceNum->data[k0]) || rtIsInf
                  (FacetsOnInterfaceNum->data[1 + k0] - 1.0)) &&
                 (FacetsOnInterfaceNum->data[k0] == FacetsOnInterfaceNum->data[1
                  + k0] - 1.0)) {
        i26 = setAll->size[0] * setAll->size[1];
        setAll->size[0] = 1;
        setAll->size[1] = 1;
        emxEnsureCapacity_real_T(setAll, i26);
        setAll->data[0] = rtNaN;
      } else if (FacetsOnInterfaceNum->data[k0] == FacetsOnInterfaceNum->data[k0])
      {
        reg = FacetsOnInterfaceNum->data[1 + k0] - 1.0;
        i26 = setAll->size[0] * setAll->size[1];
        setAll->size[0] = 1;
        setAll->size[1] = (int)floor(reg - FacetsOnInterfaceNum->data[k0]) + 1;
        emxEnsureCapacity_real_T(setAll, i26);
        loop_ub = (int)floor(reg - FacetsOnInterfaceNum->data[k0]);
        for (i26 = 0; i26 <= loop_ub; i26++) {
          setAll->data[i26] = FacetsOnInterfaceNum->data[k0] + (double)i26;
        }
      } else {
        i1 = floor(((FacetsOnInterfaceNum->data[1 + k0] - 1.0) -
                    FacetsOnInterfaceNum->data[k0]) + 0.5);
        numA = FacetsOnInterfaceNum->data[k0] + i1;
        reg = numA - (FacetsOnInterfaceNum->data[1 + k0] - 1.0);
        absxk = FacetsOnInterfaceNum->data[k0];
        node = fabs(FacetsOnInterfaceNum->data[1 + k0] - 1.0);
        if ((absxk > node) || rtIsNaN(node)) {
          node = absxk;
        }

        if (fabs(reg) < 4.4408920985006262E-16 * node) {
          i1++;
          numA = FacetsOnInterfaceNum->data[1 + k0] - 1.0;
        } else if (reg > 0.0) {
          numA = FacetsOnInterfaceNum->data[k0] + (i1 - 1.0);
        } else {
          i1++;
        }

        if (i1 >= 0.0) {
          n = (int)i1;
        } else {
          n = 0;
        }

        i26 = setAll->size[0] * setAll->size[1];
        setAll->size[0] = 1;
        setAll->size[1] = n;
        emxEnsureCapacity_real_T(setAll, i26);
        if (n > 0) {
          setAll->data[0] = FacetsOnInterfaceNum->data[k0];
          if (n > 1) {
            setAll->data[n - 1] = numA;
            pEnd = (n - 1) / 2;
            for (k = 0; k <= pEnd - 2; k++) {
              setAll->data[1 + k] = FacetsOnInterfaceNum->data[k0] + (1.0 +
                (double)k);
              setAll->data[(n - k) - 2] = numA - (1.0 + (double)k);
            }

            if (pEnd << 1 == n - 1) {
              setAll->data[pEnd] = (FacetsOnInterfaceNum->data[k0] + numA) / 2.0;
            } else {
              setAll->data[pEnd] = FacetsOnInterfaceNum->data[k0] + (double)pEnd;
              setAll->data[pEnd + 1] = numA - (double)pEnd;
            }
          }
        }
      }

      if (FacetsOnPBCNum->data[1 + k0] - 1.0 < FacetsOnPBCNum->data[k0]) {
        setPBC->size[0] = 1;
        setPBC->size[1] = 0;
      } else if ((rtIsInf(FacetsOnPBCNum->data[k0]) || rtIsInf
                  (FacetsOnPBCNum->data[1 + k0] - 1.0)) && (FacetsOnPBCNum->
                  data[k0] == FacetsOnPBCNum->data[1 + k0] - 1.0)) {
        i26 = setPBC->size[0] * setPBC->size[1];
        setPBC->size[0] = 1;
        setPBC->size[1] = 1;
        emxEnsureCapacity_real_T(setPBC, i26);
        setPBC->data[0] = rtNaN;
      } else if (FacetsOnPBCNum->data[k0] == FacetsOnPBCNum->data[k0]) {
        reg = FacetsOnPBCNum->data[1 + k0] - 1.0;
        i26 = setPBC->size[0] * setPBC->size[1];
        setPBC->size[0] = 1;
        setPBC->size[1] = (int)floor(reg - FacetsOnPBCNum->data[k0]) + 1;
        emxEnsureCapacity_real_T(setPBC, i26);
        loop_ub = (int)floor(reg - FacetsOnPBCNum->data[k0]);
        for (i26 = 0; i26 <= loop_ub; i26++) {
          setPBC->data[i26] = FacetsOnPBCNum->data[k0] + (double)i26;
        }
      } else {
        i1 = floor(((FacetsOnPBCNum->data[1 + k0] - 1.0) - FacetsOnPBCNum->
                    data[k0]) + 0.5);
        numA = FacetsOnPBCNum->data[k0] + i1;
        reg = numA - (FacetsOnPBCNum->data[1 + k0] - 1.0);
        absxk = FacetsOnPBCNum->data[k0];
        node = fabs(FacetsOnPBCNum->data[1 + k0] - 1.0);
        if ((absxk > node) || rtIsNaN(node)) {
          node = absxk;
        }

        if (fabs(reg) < 4.4408920985006262E-16 * node) {
          i1++;
          numA = FacetsOnPBCNum->data[1 + k0] - 1.0;
        } else if (reg > 0.0) {
          numA = FacetsOnPBCNum->data[k0] + (i1 - 1.0);
        } else {
          i1++;
        }

        if (i1 >= 0.0) {
          n = (int)i1;
        } else {
          n = 0;
        }

        i26 = setPBC->size[0] * setPBC->size[1];
        setPBC->size[0] = 1;
        setPBC->size[1] = n;
        emxEnsureCapacity_real_T(setPBC, i26);
        if (n > 0) {
          setPBC->data[0] = FacetsOnPBCNum->data[k0];
          if (n > 1) {
            setPBC->data[n - 1] = numA;
            pEnd = (n - 1) / 2;
            for (k = 0; k <= pEnd - 2; k++) {
              setPBC->data[1 + k] = FacetsOnPBCNum->data[k0] + (1.0 + (double)k);
              setPBC->data[(n - k) - 2] = numA - (1.0 + (double)k);
            }

            if (pEnd << 1 == n - 1) {
              setPBC->data[pEnd] = (FacetsOnPBCNum->data[k0] + numA) / 2.0;
            } else {
              setPBC->data[pEnd] = FacetsOnPBCNum->data[k0] + (double)pEnd;
              setPBC->data[pEnd + 1] = numA - (double)pEnd;
            }
          }
        }
      }

      i26 = s->size[0];
      s->size[0] = setAll->size[1];
      emxEnsureCapacity_real_T(s, i26);
      loop_ub = setAll->size[1];
      for (i26 = 0; i26 < loop_ub; i26++) {
        s->data[i26] = FacetsOnInterface->data[(int)setAll->data[i26] - 1];
      }

      i26 = ElementsOnNodePBCNum->size[0];
      ElementsOnNodePBCNum->size[0] = setPBC->size[1];
      emxEnsureCapacity_real_T(ElementsOnNodePBCNum, i26);
      loop_ub = setPBC->size[1];
      for (i26 = 0; i26 < loop_ub; i26++) {
        ElementsOnNodePBCNum->data[i26] = idx->data[(i25 + (int)setPBC->data[i26])
          - 2];
      }

      b_do_vectors(s, ElementsOnNodePBCNum, MorePBC, b_idx, ib_size);
      *numSI = MorePBC->size[0];
      FacetsOnIntMinusPBCNum->data[k0 + 1] = FacetsOnIntMinusPBCNum->data[k0] +
        (double)MorePBC->size[0];
      if (MorePBC->size[0] > 0) {
        if (FacetsOnIntMinusPBCNum->data[k0] > FacetsOnIntMinusPBCNum->data[1 +
            k0] - 1.0) {
          i26 = -1;
          i27 = 0;
        } else {
          i26 = (int)FacetsOnIntMinusPBCNum->data[k0] - 2;
          i27 = (int)(FacetsOnIntMinusPBCNum->data[1 + k0] - 1.0);
        }

        loop_ub = (i27 - i26) - 1;
        for (i27 = 0; i27 < loop_ub; i27++) {
          FacetsOnIntMinusPBC->data[(i26 + i27) + 1] = MorePBC->data[i27];
        }
      }
    }

    emxFree_real_T(&setPBC);
  } else {
    i25 = FacetsOnPBCNum->size[0];
    FacetsOnPBCNum->size[0] = 1;
    emxEnsureCapacity_real_T(FacetsOnPBCNum, i25);
    FacetsOnPBCNum->data[0] = 0.0;
    i25 = FacetsOnIntMinusPBC->size[0];
    FacetsOnIntMinusPBC->size[0] = 1;
    emxEnsureCapacity_real_T(FacetsOnIntMinusPBC, i25);
    FacetsOnIntMinusPBC->data[0] = 0.0;
    i25 = FacetsOnIntMinusPBCNum->size[0];
    FacetsOnIntMinusPBCNum->size[0] = 1;
    emxEnsureCapacity_real_T(FacetsOnIntMinusPBCNum, i25);
    FacetsOnIntMinusPBCNum->data[0] = 0.0;
  }

  /*  Find material interfaces and duplicate nodes */
  NodesOnInterface->size[0] = 0;
  i25 = (int)(nummat + -1.0);
  emxInit_boolean_T(&internodes_d, 1);
  emxInit_boolean_T(&t3_d, 1);
  e_emxInitStruct_coder_internal_(&b_expl_temp);
  for (notdone = 0; notdone < i25; notdone++) {
    for (nx = 0; nx <= notdone; nx++) {
      /*  ID for material pair (row=mat2, col=mat1) */
      if (InterTypes->data[(notdone + InterTypes->size[0] * nx) + 1] > 0.0) {
        /*  Mark nodes on the interfaces */
        c_sparse_parenReference(NodeReg->d, NodeReg->colidx, NodeReg->rowidx,
          NodeReg->m, 1.0 + (double)nx, NodeRegSum.d, NodeRegSum.colidx,
          NodeRegSum.rowidx, &NodeRegSum.m);
        sparse_gt(NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                  NodeRegSum.m, &b_expl_temp);
        i26 = elems->size[0];
        elems->size[0] = b_expl_temp.d->size[0];
        emxEnsureCapacity_boolean_T(elems, i26);
        loop_ub = b_expl_temp.d->size[0];
        for (i26 = 0; i26 < loop_ub; i26++) {
          elems->data[i26] = b_expl_temp.d->data[i26];
        }

        i26 = b_idx->size[0];
        b_idx->size[0] = b_expl_temp.colidx->size[0];
        emxEnsureCapacity_int32_T(b_idx, i26);
        loop_ub = b_expl_temp.colidx->size[0];
        for (i26 = 0; i26 < loop_ub; i26++) {
          b_idx->data[i26] = b_expl_temp.colidx->data[i26];
        }

        i26 = ElementsOnNodePBC_colidx->size[0];
        ElementsOnNodePBC_colidx->size[0] = b_expl_temp.rowidx->size[0];
        emxEnsureCapacity_int32_T(ElementsOnNodePBC_colidx, i26);
        loop_ub = b_expl_temp.rowidx->size[0];
        for (i26 = 0; i26 < loop_ub; i26++) {
          ElementsOnNodePBC_colidx->data[i26] = b_expl_temp.rowidx->data[i26];
        }

        c_sparse_parenReference(NodeReg->d, NodeReg->colidx, NodeReg->rowidx,
          NodeReg->m, 2.0 + (double)notdone, NodeRegSum.d, NodeRegSum.colidx,
          NodeRegSum.rowidx, &NodeRegSum.m);
        sparse_gt(NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                  NodeRegSum.m, &b_expl_temp);
        i26 = internodes_d->size[0];
        internodes_d->size[0] = b_expl_temp.d->size[0];
        emxEnsureCapacity_boolean_T(internodes_d, i26);
        loop_ub = b_expl_temp.d->size[0];
        for (i26 = 0; i26 < loop_ub; i26++) {
          internodes_d->data[i26] = b_expl_temp.d->data[i26];
        }

        i26 = t0_rowidx->size[0];
        t0_rowidx->size[0] = b_expl_temp.colidx->size[0];
        emxEnsureCapacity_int32_T(t0_rowidx, i26);
        loop_ub = b_expl_temp.colidx->size[0];
        for (i26 = 0; i26 < loop_ub; i26++) {
          t0_rowidx->data[i26] = b_expl_temp.colidx->data[i26];
        }

        i26 = internodes_rowidx->size[0];
        internodes_rowidx->size[0] = b_expl_temp.rowidx->size[0];
        emxEnsureCapacity_int32_T(internodes_rowidx, i26);
        loop_ub = b_expl_temp.rowidx->size[0];
        for (i26 = 0; i26 < loop_ub; i26++) {
          internodes_rowidx->data[i26] = b_expl_temp.rowidx->data[i26];
        }

        nb = b_expl_temp.m;
        sparse_and(elems, b_idx, ElementsOnNodePBC_colidx, internodes_d,
                   t0_rowidx, internodes_rowidx, nb, t3_d,
                   ElementsOnNodePBC_rowidx, ii, &q);
        c_sparse_parenReference(NodeReg->d, NodeReg->colidx, NodeReg->rowidx,
          NodeReg->m, 2.0 + (double)notdone, NodeRegSum.d, NodeRegSum.colidx,
          NodeRegSum.rowidx, &NodeRegSum.m);
        c_sparse_parenReference(NodeReg->d, NodeReg->colidx, NodeReg->rowidx,
          NodeReg->m, 1.0 + (double)nx, s, t0_colidx, t0_rowidx, &pEnd);
        sparse_eq(NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx, s,
                  t0_colidx, t0_rowidx, pEnd, elems, b_idx,
                  ElementsOnNodePBC_colidx, &k0);
        sparse_and(t3_d, ElementsOnNodePBC_rowidx, ii, elems, b_idx,
                   ElementsOnNodePBC_colidx, k0, internodes_d, t0_rowidx,
                   internodes_rowidx, &nb);
        c_idx = 0;
        i26 = ii->size[0];
        ii->size[0] = t0_rowidx->data[t0_rowidx->size[0] - 1] - 1;
        emxEnsureCapacity_int32_T(ii, i26);
        k0 = 1;
        while (c_idx < t0_rowidx->data[t0_rowidx->size[0] - 1] - 1) {
          if (c_idx == t0_rowidx->data[k0] - 1) {
            k0++;
          } else {
            c_idx++;
            ii->data[c_idx - 1] = (k0 - 1) * nb + internodes_rowidx->data[c_idx
              - 1];
          }
        }

        if (t0_rowidx->data[t0_rowidx->size[0] - 1] - 1 == 1) {
          if (c_idx == 0) {
            ii->size[0] = 0;
          }
        } else if (1 > c_idx) {
          ii->size[0] = 0;
        } else {
          i26 = ii->size[0];
          ii->size[0] = c_idx;
          emxEnsureCapacity_int32_T(ii, i26);
        }

        i26 = NodesOnInterface->size[0];
        i27 = ElementsOnNodePBC_d->size[0];
        ElementsOnNodePBC_d->size[0] = ii->size[0];
        emxEnsureCapacity_real_T(ElementsOnNodePBC_d, i27);
        loop_ub = ii->size[0];
        for (i27 = 0; i27 < loop_ub; i27++) {
          ElementsOnNodePBC_d->data[i27] = ii->data[i27];
        }

        i27 = ElementsOnNodePBC_d->size[0];
        b_exponent = NodesOnInterface->size[0];
        NodesOnInterface->size[0] = i26 + i27;
        emxEnsureCapacity_real_T(NodesOnInterface, b_exponent);
        loop_ub = ii->size[0];
        for (i27 = 0; i27 < loop_ub; i27++) {
          NodesOnInterface->data[i26 + i27] = ii->data[i27];
        }
      }
    }
  }

  if (usePBC != 0.0) {
    /*  Add PBC nodes into duplicating list */
    sum(NodeReg->d, NodeReg->colidx, NodeReg->rowidx, NodeReg->m, NodeReg->n,
        NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx, &NodeRegSum.m);

    /*  from zipped nodes, ONLY the one with regions attached is in the zipped connectivity */
    i26 = elems->size[0];
    elems->size[0] = NodesOnPBCnum->size[0];
    emxEnsureCapacity_boolean_T(elems, i26);
    loop_ub = NodesOnPBCnum->size[0];
    for (i26 = 0; i26 < loop_ub; i26++) {
      elems->data[i26] = (NodesOnPBCnum->data[i26] > 0.0);
    }

    nx = elems->size[0];
    c_idx = 0;
    i26 = ii->size[0];
    ii->size[0] = elems->size[0];
    emxEnsureCapacity_int32_T(ii, i26);
    nb = 0;
    exitg2 = false;
    while ((!exitg2) && (nb <= nx - 1)) {
      if (elems->data[nb]) {
        c_idx++;
        ii->data[c_idx - 1] = nb + 1;
        if (c_idx >= nx) {
          exitg2 = true;
        } else {
          nb++;
        }
      } else {
        nb++;
      }
    }

    if (elems->size[0] == 1) {
      if (c_idx == 0) {
        ii->size[0] = 0;
      }
    } else if (1 > c_idx) {
      ii->size[0] = 0;
    } else {
      i26 = ii->size[0];
      ii->size[0] = c_idx;
      emxEnsureCapacity_int32_T(ii, i26);
    }

    sparse_gt(NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx, NodeRegSum.m,
              &b_expl_temp);
    i26 = t0_rowidx->size[0];
    t0_rowidx->size[0] = b_expl_temp.colidx->size[0];
    emxEnsureCapacity_int32_T(t0_rowidx, i26);
    loop_ub = b_expl_temp.colidx->size[0];
    for (i26 = 0; i26 < loop_ub; i26++) {
      t0_rowidx->data[i26] = b_expl_temp.colidx->data[i26];
    }

    i26 = internodes_rowidx->size[0];
    internodes_rowidx->size[0] = b_expl_temp.rowidx->size[0];
    emxEnsureCapacity_int32_T(internodes_rowidx, i26);
    loop_ub = b_expl_temp.rowidx->size[0];
    for (i26 = 0; i26 < loop_ub; i26++) {
      internodes_rowidx->data[i26] = b_expl_temp.rowidx->data[i26];
    }

    nb = b_expl_temp.m;
    c_idx = 0;
    i26 = b_idx->size[0];
    b_idx->size[0] = t0_rowidx->data[t0_rowidx->size[0] - 1] - 1;
    emxEnsureCapacity_int32_T(b_idx, i26);
    k0 = 1;
    while (c_idx < t0_rowidx->data[t0_rowidx->size[0] - 1] - 1) {
      if (c_idx == t0_rowidx->data[k0] - 1) {
        k0++;
      } else {
        c_idx++;
        b_idx->data[c_idx - 1] = (k0 - 1) * nb + internodes_rowidx->data[c_idx -
          1];
      }
    }

    if (t0_rowidx->data[t0_rowidx->size[0] - 1] - 1 == 1) {
      if (c_idx == 0) {
        b_idx->size[0] = 0;
      }
    } else if (1 > c_idx) {
      b_idx->size[0] = 0;
    } else {
      i26 = b_idx->size[0];
      b_idx->size[0] = c_idx;
      emxEnsureCapacity_int32_T(b_idx, i26);
    }

    i26 = ElementsOnNodePBC_d->size[0];
    ElementsOnNodePBC_d->size[0] = ii->size[0];
    emxEnsureCapacity_real_T(ElementsOnNodePBC_d, i26);
    loop_ub = ii->size[0];
    for (i26 = 0; i26 < loop_ub; i26++) {
      ElementsOnNodePBC_d->data[i26] = ii->data[i26];
    }

    i26 = ElementsOnNodePBCNum->size[0];
    ElementsOnNodePBCNum->size[0] = b_idx->size[0];
    emxEnsureCapacity_real_T(ElementsOnNodePBCNum, i26);
    loop_ub = b_idx->size[0];
    for (i26 = 0; i26 < loop_ub; i26++) {
      ElementsOnNodePBCNum->data[i26] = b_idx->data[i26];
    }

    c_do_vectors(ElementsOnNodePBC_d, ElementsOnNodePBCNum, idx, b_idx, ii);
    i26 = NodesOnInterface->size[0];
    i27 = idx->size[0];
    b_exponent = NodesOnInterface->size[0];
    NodesOnInterface->size[0] = i26 + i27;
    emxEnsureCapacity_real_T(NodesOnInterface, b_exponent);
    loop_ub = idx->size[0];
    for (i27 = 0; i27 < loop_ub; i27++) {
      NodesOnInterface->data[i26 + i27] = idx->data[i27];
    }

    /*  add these zipped PBC nodes into the list for duplicates too */
  }

  i26 = idx->size[0];
  idx->size[0] = NodesOnInterface->size[0];
  emxEnsureCapacity_real_T(idx, i26);
  loop_ub = NodesOnInterface->size[0];
  for (i26 = 0; i26 < loop_ub; i26++) {
    idx->data[i26] = NodesOnInterface->data[i26];
  }

  i = NodesOnInterface->size[0];
  n = NodesOnInterface->size[0] + 1;
  i1 = NodesOnInterface->size[0];
  i26 = b_idx->size[0];
  b_idx->size[0] = (int)i1;
  emxEnsureCapacity_int32_T(b_idx, i26);
  loop_ub = (int)i1;
  for (i26 = 0; i26 < loop_ub; i26++) {
    b_idx->data[i26] = 0;
  }

  if (NodesOnInterface->size[0] != 0) {
    i26 = ii->size[0];
    ii->size[0] = (int)i1;
    emxEnsureCapacity_int32_T(ii, i26);
    i26 = NodesOnInterface->size[0] - 1;
    for (k = 1; k <= i26; k += 2) {
      if ((NodesOnInterface->data[k - 1] <= NodesOnInterface->data[k]) ||
          rtIsNaN(NodesOnInterface->data[k])) {
        b_idx->data[k - 1] = k;
        b_idx->data[k] = k + 1;
      } else {
        b_idx->data[k - 1] = k + 1;
        b_idx->data[k] = k;
      }
    }

    if ((NodesOnInterface->size[0] & 1) != 0) {
      b_idx->data[NodesOnInterface->size[0] - 1] = NodesOnInterface->size[0];
    }

    b_i = 2;
    while (b_i < n - 1) {
      k0 = b_i << 1;
      j = 1;
      for (pEnd = 1 + b_i; pEnd < n; pEnd = nb + b_i) {
        p = j;
        q = pEnd - 1;
        nb = j + k0;
        if (nb > n) {
          nb = n;
        }

        k = 0;
        nx = nb - j;
        while (k + 1 <= nx) {
          if ((NodesOnInterface->data[b_idx->data[p - 1] - 1] <=
               NodesOnInterface->data[b_idx->data[q] - 1]) || rtIsNaN
              (NodesOnInterface->data[b_idx->data[q] - 1])) {
            ii->data[k] = b_idx->data[p - 1];
            p++;
            if (p == pEnd) {
              while (q + 1 < nb) {
                k++;
                ii->data[k] = b_idx->data[q];
                q++;
              }
            }
          } else {
            ii->data[k] = b_idx->data[q];
            q++;
            if (q + 1 == nb) {
              while (p < pEnd) {
                k++;
                ii->data[k] = b_idx->data[p - 1];
                p++;
              }
            }
          }

          k++;
        }

        for (k = 0; k < nx; k++) {
          b_idx->data[(j + k) - 1] = ii->data[k];
        }

        j = nb;
      }

      b_i = k0;
    }
  }

  i1 = NodesOnInterface->size[0];
  i26 = NodesOnInterface->size[0];
  NodesOnInterface->size[0] = (int)i1;
  emxEnsureCapacity_real_T(NodesOnInterface, i26);
  for (k = 0; k < i; k++) {
    NodesOnInterface->data[k] = idx->data[b_idx->data[k] - 1];
  }

  k = 0;
  while ((k + 1 <= i) && rtIsInf(NodesOnInterface->data[k]) &&
         (NodesOnInterface->data[k] < 0.0)) {
    k++;
  }

  nx = k;
  k = i;
  while ((k >= 1) && rtIsNaN(NodesOnInterface->data[k - 1])) {
    k--;
  }

  q = i - k;
  while ((k >= 1) && rtIsInf(NodesOnInterface->data[k - 1]) &&
         (NodesOnInterface->data[k - 1] > 0.0)) {
    k--;
  }

  pEnd = (i - k) - q;
  nb = -1;
  if (nx > 0) {
    nb = 0;
  }

  while (nx + 1 <= k) {
    i1 = NodesOnInterface->data[nx];
    do {
      exitg1 = 0;
      nx++;
      if (nx + 1 > k) {
        exitg1 = 1;
      } else {
        absxk = fabs(i1 / 2.0);
        if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
          if (absxk <= 2.2250738585072014E-308) {
            absxk = 4.94065645841247E-324;
          } else {
            frexp(absxk, &d_exponent);
            absxk = ldexp(1.0, d_exponent - 53);
          }
        } else {
          absxk = rtNaN;
        }

        if ((fabs(i1 - NodesOnInterface->data[nx]) < absxk) || (rtIsInf
             (NodesOnInterface->data[nx]) && rtIsInf(i1) &&
             ((NodesOnInterface->data[nx] > 0.0) == (i1 > 0.0)))) {
        } else {
          exitg1 = 1;
        }
      }
    } while (exitg1 == 0);

    nb++;
    NodesOnInterface->data[nb] = i1;
  }

  if (pEnd > 0) {
    nb++;
    NodesOnInterface->data[nb] = NodesOnInterface->data[k];
  }

  nx = k + pEnd;
  for (j = 0; j < q; j++) {
    nb++;
    NodesOnInterface->data[nb] = NodesOnInterface->data[nx + j];
  }

  if (1 > nb + 1) {
    i26 = -1;
  } else {
    i26 = nb;
  }

  i27 = NodesOnInterface->size[0];
  NodesOnInterface->size[0] = i26 + 1;
  emxEnsureCapacity_real_T(NodesOnInterface, i27);

  /*  Criteria: only nodes for which ALL inter-material edges involving a given */
  /*  material are being cut, then they are duplicated. */
  absxk = *numnp;
  emxInit_real_T(&intermat2, 2);
  emxInit_real_T(&b_elems, 2);
  f_emxInitStruct_coder_internal_(&c_expl_temp);
  for (p = 0; p <= i26; p++) {
    node = NodesOnInterface->data[p];

    /*      matnode = sum(NodeMat(node,:)>0); */
    if (FacetsOnNodeNum->data[(int)NodesOnInterface->data[p] - 1] == 0.0) {
      /*  Determine if midedge or midface node */
      k0 = 0;
      if ((nen == 10.0) || (nen == 20.0) || (!(nen == 27.0))) {
        /*  tetrahedra, no mid-face elements */
      } else {
        d_sparse_parenReference(ElementsOnNode->d, ElementsOnNode->colidx,
          ElementsOnNode->rowidx, NodesOnInterface->data[p],
          ElementsOnNodePBCNum, t0_colidx, b_idx);
        i1 = NodesOnInterface->data[p];
        b_loop_ub = NodesOnElement->size[1];
        i27 = (int)sparse_full(ElementsOnNodePBCNum, t0_colidx);
        for (b_exponent = 0; b_exponent < b_loop_ub; b_exponent++) {
          x_data[b_exponent] = (NodesOnElement->data[(i27 + NodesOnElement->
            size[0] * b_exponent) - 1] == i1);
        }

        c_idx = 0;
        nb = 0;
        exitg2 = false;
        while ((!exitg2) && (nb <= b_loop_ub - 1)) {
          if (x_data[nb]) {
            c_idx++;
            ii_data[c_idx - 1] = (signed char)(nb + 1);
            if (c_idx >= b_loop_ub) {
              exitg2 = true;
            } else {
              nb++;
            }
          } else {
            nb++;
          }
        }

        if (b_loop_ub == 1) {
          if (c_idx == 0) {
            b_loop_ub = 0;
          }
        } else if (1 > c_idx) {
          b_loop_ub = 0;
        } else {
          b_loop_ub = c_idx;
        }

        for (i27 = 0; i27 < b_loop_ub; i27++) {
          nodeloc_data[i27] = ii_data[i27];
        }

        x_size[0] = 1;
        x_size[1] = b_loop_ub;
        for (i27 = 0; i27 < b_loop_ub; i27++) {
          x_data[i27] = (nodeloc_data[i27] > 26.0);
        }

        if (!ifWhileCond(x_data, x_size)) {
          x_size[0] = 1;
          x_size[1] = b_loop_ub;
          for (i27 = 0; i27 < b_loop_ub; i27++) {
            x_data[i27] = (nodeloc_data[i27] > 20.0);
          }

          if (ifWhileCond(x_data, x_size)) {
            k0 = 1;
          }
        }
      }

      if (k0 != 0) {
        /*  midface; midedge will be handled below */
        if ((usePBC != 0.0) && (NodesOnPBCnum->data[(int)NodesOnInterface->
                                data[p] - 1] > 0.0)) {
          /*  handle copies of PBC nodes specially */
          /*  Just get the old node numbers back and copy them into place */
          c_sparse_parenReference(ElementsOnNode->d, ElementsOnNode->colidx,
            ElementsOnNode->rowidx, ElementsOnNode->m, NodesOnInterface->data[p],
            NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx, &NodeRegSum.m);
          c_sparse_full(NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                        NodeRegSum.m, b_elems);

          /*  Loop over all elements attached to node */
          i27 = (int)ElementsOnNodeNum->data[(int)NodesOnInterface->data[p] - 1];
          if (0 <= i27 - 1) {
            if (1.0 > nen) {
              b_nx = 0;
            } else {
              b_nx = (int)nen;
            }

            x_size[1] = b_nx;
          }

          for (nx = 0; nx < i27; nx++) {
            /*  Reset nodal ID for element in higher material ID */
            pEnd = (int)b_elems->data[nx];
            for (b_exponent = 0; b_exponent < b_nx; b_exponent++) {
              x_data[b_exponent] = (NodesOnElementCG->data[(pEnd +
                NodesOnElementCG->size[0] * b_exponent) - 1] == node);
            }

            c_idx = 0;
            b_loop_ub = x_size[1];
            nb = 0;
            exitg2 = false;
            while ((!exitg2) && (nb <= b_nx - 1)) {
              if (x_data[nb]) {
                c_idx++;
                ii_data[c_idx - 1] = (signed char)(nb + 1);
                if (c_idx >= b_nx) {
                  exitg2 = true;
                } else {
                  nb++;
                }
              } else {
                nb++;
              }
            }

            if (x_size[1] == 1) {
              if (c_idx == 0) {
                b_loop_ub = 0;
              }
            } else if (1 > c_idx) {
              b_loop_ub = 0;
            } else {
              b_loop_ub = c_idx;
            }

            if (0 <= b_loop_ub - 1) {
              memcpy(&node_dup_tmp_data[0], &ii_data[0], (unsigned int)
                     (b_loop_ub * (int)sizeof(signed char)));
            }

            pEnd = (int)b_elems->data[nx];
            k0 = (int)b_elems->data[nx];
            for (b_exponent = 0; b_exponent < b_loop_ub; b_exponent++) {
              i = node_dup_tmp_data[b_exponent] - 1;
              NodesOnElementDG->data[(k0 + NodesOnElementDG->size[0] * i) - 1] =
                NodesOnElementPBC->data[(pEnd + NodesOnElementPBC->size[0] * i)
                - 1];
            }
          }
        } else {
          /*  regular midside node */
          for (notdone = 0; notdone < i25; notdone++) {
            for (nx = 0; nx <= notdone; nx++) {
              /*  ID for material pair (row=mat2, col=mat1) */
              if (InterTypes->data[(notdone + InterTypes->size[0] * nx) + 1] >
                  0.0) {
                /*  Duplicate nodes along material interface */
                e_sparse_parenReference(NodeReg->d, NodeReg->colidx,
                  NodeReg->rowidx, node, 1.0 + (double)nx, ElementsOnNodePBCNum,
                  t0_colidx, b_idx);
                b_sparse_gt(ElementsOnNodePBCNum, t0_colidx, t3_d,
                            ElementsOnNodePBC_colidx, ii);
                e_sparse_parenReference(NodeReg->d, NodeReg->colidx,
                  NodeReg->rowidx, node, 2.0 + (double)notdone,
                  ElementsOnNodePBCNum, t0_colidx, b_idx);
                b_sparse_gt(ElementsOnNodePBCNum, t0_colidx, internodes_d,
                            t0_rowidx, internodes_rowidx);
                b_sparse_and(t3_d, ElementsOnNodePBC_colidx, internodes_d,
                             t0_rowidx, internodes_rowidx, &c_expl_temp);
                i27 = elems->size[0];
                elems->size[0] = c_expl_temp.d->size[0];
                emxEnsureCapacity_boolean_T(elems, i27);
                loop_ub = c_expl_temp.d->size[0];
                for (i27 = 0; i27 < loop_ub; i27++) {
                  elems->data[i27] = c_expl_temp.d->data[i27];
                }

                i27 = ElementsOnNodePBC_rowidx->size[0];
                ElementsOnNodePBC_rowidx->size[0] = c_expl_temp.colidx->size[0];
                emxEnsureCapacity_int32_T(ElementsOnNodePBC_rowidx, i27);
                loop_ub = c_expl_temp.colidx->size[0];
                for (i27 = 0; i27 < loop_ub; i27++) {
                  ElementsOnNodePBC_rowidx->data[i27] = c_expl_temp.colidx->
                    data[i27];
                }

                e_sparse_parenReference(NodeReg->d, NodeReg->colidx,
                  NodeReg->rowidx, node, 2.0 + (double)notdone,
                  ElementsOnNodePBCNum, t0_colidx, b_idx);
                e_sparse_parenReference(NodeReg->d, NodeReg->colidx,
                  NodeReg->rowidx, node, 1.0 + (double)nx, s, b_idx, ii);
                b_sparse_eq(ElementsOnNodePBCNum, t0_colidx, s, b_idx, ii,
                            &c_expl_temp);
                i27 = t3_d->size[0];
                t3_d->size[0] = c_expl_temp.d->size[0];
                emxEnsureCapacity_boolean_T(t3_d, i27);
                loop_ub = c_expl_temp.d->size[0];
                for (i27 = 0; i27 < loop_ub; i27++) {
                  t3_d->data[i27] = c_expl_temp.d->data[i27];
                }

                i27 = ElementsOnNodePBC_colidx->size[0];
                ElementsOnNodePBC_colidx->size[0] = c_expl_temp.colidx->size[0];
                emxEnsureCapacity_int32_T(ElementsOnNodePBC_colidx, i27);
                loop_ub = c_expl_temp.colidx->size[0];
                for (i27 = 0; i27 < loop_ub; i27++) {
                  ElementsOnNodePBC_colidx->data[i27] = c_expl_temp.colidx->
                    data[i27];
                }

                i27 = ii->size[0];
                ii->size[0] = c_expl_temp.rowidx->size[0];
                emxEnsureCapacity_int32_T(ii, i27);
                loop_ub = c_expl_temp.rowidx->size[0];
                for (i27 = 0; i27 < loop_ub; i27++) {
                  ii->data[i27] = c_expl_temp.rowidx->data[i27];
                }

                b_sparse_and(elems, ElementsOnNodePBC_rowidx, t3_d,
                             ElementsOnNodePBC_colidx, ii, &c_expl_temp);
                i27 = internodes_d->size[0];
                internodes_d->size[0] = c_expl_temp.d->size[0];
                emxEnsureCapacity_boolean_T(internodes_d, i27);
                loop_ub = c_expl_temp.d->size[0];
                for (i27 = 0; i27 < loop_ub; i27++) {
                  internodes_d->data[i27] = c_expl_temp.d->data[i27];
                }

                i27 = t0_rowidx->size[0];
                t0_rowidx->size[0] = c_expl_temp.colidx->size[0];
                emxEnsureCapacity_int32_T(t0_rowidx, i27);
                loop_ub = c_expl_temp.colidx->size[0];
                for (i27 = 0; i27 < loop_ub; i27++) {
                  t0_rowidx->data[i27] = c_expl_temp.colidx->data[i27];
                }

                /*  Namely, only find nodes that are part of materials mat1 and */
                /*  mat2, but ONLY if those nodal IDs have not been reset before, */
                /*  in which case the ID for mat1 will equal the ID for mat2. */
                /*  In this way, each new nodal ID is assigned to a single */
                /*  material. */
                if (d_sparse_full(internodes_d, t0_rowidx)) {
                  /*  Duplicate the nodal coordinates */
                  absxk++;
                  sparse_parenAssign2D(NodeReg, absxk, node, 2.0 + (double)
                                       notdone);

                  /*  Loop over all nodes on material interface that need */
                  /*  duplicated */
                  c_sparse_parenReference(ElementsOnNode->d,
                    ElementsOnNode->colidx, ElementsOnNode->rowidx,
                    ElementsOnNode->m, node, NodeRegSum.d, NodeRegSum.colidx,
                    NodeRegSum.rowidx, &NodeRegSum.m);
                  c_sparse_full(NodeRegSum.d, NodeRegSum.colidx,
                                NodeRegSum.rowidx, NodeRegSum.m, b_elems);

                  /*  Loop over all elements attached to node */
                  i27 = (int)ElementsOnNodeNum->data[(int)node - 1];
                  for (j = 0; j < i27; j++) {
                    if (RegionOnElement->data[(int)b_elems->data[j] - 1] == 2.0
                        + (double)notdone) {
                      /*  Reset nodal ID for element in higher material ID */
                      if (1.0 > nen) {
                        b_loop_ub = 0;
                      } else {
                        b_loop_ub = (int)nen;
                      }

                      pEnd = (int)b_elems->data[j];
                      x_size[1] = b_loop_ub;
                      for (b_exponent = 0; b_exponent < b_loop_ub; b_exponent++)
                      {
                        x_data[b_exponent] = (NodesOnElementCG->data[(pEnd +
                          NodesOnElementCG->size[0] * b_exponent) - 1] == node);
                      }

                      c_idx = 0;
                      nb = 0;
                      exitg2 = false;
                      while ((!exitg2) && (nb <= b_loop_ub - 1)) {
                        if (x_data[nb]) {
                          c_idx++;
                          ii_data[c_idx - 1] = (signed char)(nb + 1);
                          if (c_idx >= b_loop_ub) {
                            exitg2 = true;
                          } else {
                            nb++;
                          }
                        } else {
                          nb++;
                        }
                      }

                      if (b_loop_ub == 1) {
                        if (c_idx == 0) {
                          b_loop_ub = 0;
                        }
                      } else if (1 > c_idx) {
                        b_loop_ub = 0;
                      } else {
                        b_loop_ub = c_idx;
                      }

                      if (0 <= b_loop_ub - 1) {
                        memcpy(&node_dup_tmp_data[0], &ii_data[0], (unsigned int)
                               (b_loop_ub * (int)sizeof(signed char)));
                      }

                      pEnd = (int)b_elems->data[j];
                      for (b_exponent = 0; b_exponent < b_loop_ub; b_exponent++)
                      {
                        NodesOnElementDG->data[(pEnd + NodesOnElementDG->size[0]
                          * (node_dup_tmp_data[b_exponent] - 1)) - 1] = absxk;
                      }

                      Coordinates3->data[(int)absxk - 1] = Coordinates->data
                        [(int)node - 1];
                      Coordinates3->data[((int)absxk + Coordinates3->size[0]) -
                        1] = Coordinates->data[((int)node + Coordinates->size[0])
                        - 1];
                      Coordinates3->data[((int)absxk + (Coordinates3->size[0] <<
                        1)) - 1] = Coordinates->data[((int)node +
                        (Coordinates->size[0] << 1)) - 1];
                    }
                  }
                }
              }
            }
          }
        }
      }

      /*  midface/midedge */
    } else {
      /*  All corner nodes */
      /*  form secs; a sec is a contiguous region of elements that is not */
      /*  cut apart by any CZM facs */
      /*  start by assuming all elements are separated */
      numA = ElementsOnNodeNum->data[(int)NodesOnInterface->data[p] - 1];
      i27 = intermat2->size[0] * intermat2->size[1];
      intermat2->size[0] = (int)ElementsOnNodeNum->data[(int)
        NodesOnInterface->data[p] - 1];
      intermat2->size[1] = (int)ElementsOnNodeNum->data[(int)
        NodesOnInterface->data[p] - 1];
      emxEnsureCapacity_real_T(intermat2, i27);
      loop_ub = (int)ElementsOnNodeNum->data[(int)NodesOnInterface->data[p] - 1]
        * (int)ElementsOnNodeNum->data[(int)NodesOnInterface->data[p] - 1];
      for (i27 = 0; i27 < loop_ub; i27++) {
        intermat2->data[i27] = 0.0;
      }

      if (ElementsOnNodeNum->data[(int)NodesOnInterface->data[p] - 1] < 1.0) {
        setAll->size[0] = 1;
        setAll->size[1] = 0;
      } else if (rtIsInf(ElementsOnNodeNum->data[(int)NodesOnInterface->data[p]
                         - 1]) && (1.0 == ElementsOnNodeNum->data[(int)
                  NodesOnInterface->data[p] - 1])) {
        i27 = setAll->size[0] * setAll->size[1];
        setAll->size[0] = 1;
        setAll->size[1] = 1;
        emxEnsureCapacity_real_T(setAll, i27);
        setAll->data[0] = rtNaN;
      } else {
        reg = ElementsOnNodeNum->data[(int)NodesOnInterface->data[p] - 1];
        i27 = setAll->size[0] * setAll->size[1];
        setAll->size[0] = 1;
        loop_ub = (int)floor(reg - 1.0);
        setAll->size[1] = loop_ub + 1;
        emxEnsureCapacity_real_T(setAll, i27);
        for (i27 = 0; i27 <= loop_ub; i27++) {
          setAll->data[i27] = 1.0 + (double)i27;
        }
      }

      f_sparse_parenReference(ElementsOnNode->d, ElementsOnNode->colidx,
        ElementsOnNode->rowidx, setAll, NodesOnInterface->data[p], NodeRegSum.d,
        NodeRegSum.colidx, NodeRegSum.rowidx, &NodeRegSum.m);
      c_sparse_full(NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                    NodeRegSum.m, b_elems);
      i27 = setAll->size[0] * setAll->size[1];
      setAll->size[0] = 1;
      setAll->size[1] = b_elems->size[0];
      emxEnsureCapacity_real_T(setAll, i27);
      loop_ub = b_elems->size[0];
      for (i27 = 0; i27 < loop_ub; i27++) {
        setAll->data[i27] = b_elems->data[i27];
      }

      loop_ub = setAll->size[1];
      for (i27 = 0; i27 < loop_ub; i27++) {
        intermat2->data[intermat2->size[0] * i27] = setAll->data[i27];
      }

      i27 = MorePBC->size[0];
      MorePBC->size[0] = (int)ElementsOnNodeNum->data[(int)
        NodesOnInterface->data[p] - 1];
      emxEnsureCapacity_real_T(MorePBC, i27);
      loop_ub = (int)ElementsOnNodeNum->data[(int)NodesOnInterface->data[p] - 1];
      for (i27 = 0; i27 < loop_ub; i27++) {
        MorePBC->data[i27] = 1.0;
      }

      i27 = (int)FacetsOnNodeNum->data[(int)NodesOnInterface->data[p] - 1];
      for (numPBC = 0; numPBC < i27; numPBC++) {
        e_sparse_parenReference(FacetsOnNodeInt->d, FacetsOnNodeInt->colidx,
          FacetsOnNodeInt->rowidx, 1.0 + (double)numPBC, node,
          ElementsOnNodePBCNum, t0_colidx, b_idx);
        b_sparse_gt(ElementsOnNodePBCNum, t0_colidx, t3_d,
                    ElementsOnNodePBC_colidx, ii);
        if (d_sparse_full(t3_d, ElementsOnNodePBC_colidx)) {
          /*  exclude internal facs */
          /*                  intramattrue = ~isempty(find(matI==diag(intermat2),1)); I */
          /*                  found out that the code is only putting cut in */
          /*                  nodeedgecut for intermaterials, not the diagonal of */
          /*                  intermat2, so no if-test is needed */
          /*                  if ~cuttrue || intramattrue % two elements should be joined into one sec, along with their neighbors currently in the sec */
          e_sparse_parenReference(FacetsOnNodeCut->d, FacetsOnNodeCut->colidx,
            FacetsOnNodeCut->rowidx, 1.0 + (double)numPBC, node,
            ElementsOnNodePBCNum, t0_colidx, b_idx);
          sparse_not(t0_colidx, b_idx, t3_d, ElementsOnNodePBC_colidx, ii);
          if (d_sparse_full(t3_d, ElementsOnNodePBC_colidx)) {
            /*  two elements should be joined into one sec, along with their neighbors currently in the sec */
            e_sparse_parenReference(FacetsOnNode->d, FacetsOnNode->colidx,
              FacetsOnNode->rowidx, 1.0 + (double)numPBC, node,
              ElementsOnNodePBCNum, t0_colidx, b_idx);
            i1 = sparse_full(ElementsOnNodePBCNum, t0_colidx);
            k0 = (int)ElementsOnFacet->data[(int)i1 - 1];
            pEnd = (int)ElementsOnFacet->data[((int)i1 + ElementsOnFacet->size[0])
              - 1];

            /*  find the secs for each element */
            nri = 0U;
            old = false;
            while ((nri < numA) && (!old)) {
              nri++;
              if (MorePBC->data[(int)nri - 1] > 0.0) {
                if (1.0 > MorePBC->data[(int)nri - 1]) {
                  loop_ub = 0;
                } else {
                  loop_ub = (int)MorePBC->data[(int)nri - 1];
                }

                b_exponent = elems->size[0];
                elems->size[0] = loop_ub;
                emxEnsureCapacity_boolean_T(elems, b_exponent);
                for (b_exponent = 0; b_exponent < loop_ub; b_exponent++) {
                  elems->data[b_exponent] = (intermat2->data[b_exponent +
                    intermat2->size[0] * ((int)nri - 1)] == k0);
                }

                nx = elems->size[0];
                c_idx = 0;
                b_exponent = ii->size[0];
                ii->size[0] = elems->size[0];
                emxEnsureCapacity_int32_T(ii, b_exponent);
                nb = 0;
                exitg2 = false;
                while ((!exitg2) && (nb <= nx - 1)) {
                  if (elems->data[nb]) {
                    c_idx++;
                    ii->data[c_idx - 1] = nb + 1;
                    if (c_idx >= nx) {
                      exitg2 = true;
                    } else {
                      nb++;
                    }
                  } else {
                    nb++;
                  }
                }

                if (elems->size[0] == 1) {
                  if (c_idx == 0) {
                    ii->size[0] = 0;
                  }
                } else if (1 > c_idx) {
                  ii->size[0] = 0;
                } else {
                  b_exponent = ii->size[0];
                  ii->size[0] = c_idx;
                  emxEnsureCapacity_int32_T(ii, b_exponent);
                }

                /*  find is MUCH faster than ismember */
                b_exponent = ElementsOnNodePBC_d->size[0];
                ElementsOnNodePBC_d->size[0] = ii->size[0];
                emxEnsureCapacity_real_T(ElementsOnNodePBC_d, b_exponent);
                loop_ub = ii->size[0];
                for (b_exponent = 0; b_exponent < loop_ub; b_exponent++) {
                  ElementsOnNodePBC_d->data[b_exponent] = ii->data[b_exponent];
                }

                old = b_any(ElementsOnNodePBC_d);

                /*                          if sec1>0 */
                /*                              break */
                /*                          end */
              }
            }

            iSec2 = 0U;
            old = false;
            while ((iSec2 < numA) && (!old)) {
              iSec2++;
              b_exponent = (int)iSec2 - 1;
              if (MorePBC->data[b_exponent] > 0.0) {
                if (1.0 > MorePBC->data[(int)iSec2 - 1]) {
                  loop_ub = 0;
                } else {
                  loop_ub = (int)MorePBC->data[(int)iSec2 - 1];
                }

                i = elems->size[0];
                elems->size[0] = loop_ub;
                emxEnsureCapacity_boolean_T(elems, i);
                for (i = 0; i < loop_ub; i++) {
                  elems->data[i] = (pEnd == intermat2->data[i + intermat2->size
                                    [0] * b_exponent]);
                }

                nx = elems->size[0];
                c_idx = 0;
                b_exponent = ii->size[0];
                ii->size[0] = elems->size[0];
                emxEnsureCapacity_int32_T(ii, b_exponent);
                nb = 0;
                exitg2 = false;
                while ((!exitg2) && (nb <= nx - 1)) {
                  if (elems->data[nb]) {
                    c_idx++;
                    ii->data[c_idx - 1] = nb + 1;
                    if (c_idx >= nx) {
                      exitg2 = true;
                    } else {
                      nb++;
                    }
                  } else {
                    nb++;
                  }
                }

                if (elems->size[0] == 1) {
                  if (c_idx == 0) {
                    ii->size[0] = 0;
                  }
                } else if (1 > c_idx) {
                  ii->size[0] = 0;
                } else {
                  b_exponent = ii->size[0];
                  ii->size[0] = c_idx;
                  emxEnsureCapacity_int32_T(ii, b_exponent);
                }

                b_exponent = ElementsOnNodePBC_d->size[0];
                ElementsOnNodePBC_d->size[0] = ii->size[0];
                emxEnsureCapacity_real_T(ElementsOnNodePBC_d, b_exponent);
                loop_ub = ii->size[0];
                for (b_exponent = 0; b_exponent < loop_ub; b_exponent++) {
                  ElementsOnNodePBC_d->data[b_exponent] = ii->data[b_exponent];
                }

                old = b_any(ElementsOnNodePBC_d);

                /*                          if sec2>0 */
                /*                              break */
                /*                          end */
              }
            }

            /*  merge secs */
            if ((int)iSec2 != (int)nri) {
              if (1.0 > MorePBC->data[(int)iSec2 - 1]) {
                loop_ub = 0;
              } else {
                loop_ub = (int)MorePBC->data[(int)iSec2 - 1];
              }

              if (1.0 > MorePBC->data[(int)nri - 1]) {
                b_loop_ub = 0;
              } else {
                b_loop_ub = (int)MorePBC->data[(int)nri - 1];
              }

              b_exponent = s->size[0];
              s->size[0] = loop_ub + b_loop_ub;
              emxEnsureCapacity_real_T(s, b_exponent);
              for (b_exponent = 0; b_exponent < loop_ub; b_exponent++) {
                s->data[b_exponent] = intermat2->data[b_exponent +
                  intermat2->size[0] * ((int)iSec2 - 1)];
              }

              for (b_exponent = 0; b_exponent < b_loop_ub; b_exponent++) {
                s->data[b_exponent + loop_ub] = intermat2->data[b_exponent +
                  intermat2->size[0] * ((int)nri - 1)];
              }

              c_sort(s);
              loop_ub = s->size[0];
              for (b_exponent = 0; b_exponent < loop_ub; b_exponent++) {
                intermat2->data[b_exponent + intermat2->size[0] * ((int)iSec2 -
                  1)] = s->data[b_exponent];
              }

              loop_ub = intermat2->size[0];
              for (b_exponent = 0; b_exponent < loop_ub; b_exponent++) {
                intermat2->data[b_exponent + intermat2->size[0] * ((int)nri - 1)]
                  = 0.0;
              }

              MorePBC->data[(int)iSec2 - 1] += MorePBC->data[(int)nri - 1];
              MorePBC->data[(int)nri - 1] = 0.0;
            }
          }
        }

        /*  if external edge */
      }

      /*  assign node IDs to each sec */
      if ((usePBC != 0.0) && (NodesOnPBCnum->data[(int)NodesOnInterface->data[p]
           - 1] > 0.0)) {
        /*  handle copies of PBC nodes specially */
        i1 = NodesOnPBCnum->data[(int)NodesOnInterface->data[p] - 1] + 1.0;
        i27 = idx->size[0];
        idx->size[0] = (int)(NodesOnPBCnum->data[(int)NodesOnInterface->data[p]
                             - 1] + 1.0);
        emxEnsureCapacity_real_T(idx, i27);
        loop_ub = (int)(NodesOnPBCnum->data[(int)NodesOnInterface->data[p] - 1]
                        + 1.0);
        for (i27 = 0; i27 < loop_ub; i27++) {
          idx->data[i27] = 0.0;
        }

        i27 = (int)ElementsOnNodeNum->data[(int)NodesOnInterface->data[p] - 1];
        for (q = 0; q < i27; q++) {
          if (MorePBC->data[q] > 0.0) {
            if (1.0 > nen) {
              b_loop_ub = 0;
            } else {
              b_loop_ub = (int)nen;
            }

            k0 = (int)intermat2->data[intermat2->size[0] * q];
            x_size[1] = b_loop_ub;
            for (b_exponent = 0; b_exponent < b_loop_ub; b_exponent++) {
              x_data[b_exponent] = (NodesOnElementCG->data[(k0 +
                NodesOnElementCG->size[0] * b_exponent) - 1] == node);
            }

            c_idx = 0;
            nb = 0;
            exitg2 = false;
            while ((!exitg2) && (nb <= b_loop_ub - 1)) {
              if (x_data[nb]) {
                c_idx++;
                ii_data[c_idx - 1] = (signed char)(nb + 1);
                if (c_idx >= b_loop_ub) {
                  exitg2 = true;
                } else {
                  nb++;
                }
              } else {
                nb++;
              }
            }

            if (b_loop_ub == 1) {
              if (c_idx == 0) {
                b_loop_ub = 0;
              }
            } else if (1 > c_idx) {
              b_loop_ub = 0;
            } else {
              b_loop_ub = c_idx;
            }

            for (b_exponent = 0; b_exponent < b_loop_ub; b_exponent++) {
              nodeloc_data[b_exponent] = ii_data[b_exponent];
            }

            for (b_exponent = 0; b_exponent < b_loop_ub; b_exponent++) {
              node_dup_tmp_data[b_exponent] = (signed char)
                nodeloc_data[b_exponent];
            }

            k0 = (int)intermat2->data[intermat2->size[0] * q];
            numA = NodesOnElementPBC->data[(k0 + NodesOnElementPBC->size[0] *
              (node_dup_tmp_data[0] - 1)) - 1];
            if (0 <= b_loop_ub - 1) {
              memcpy(&node_dup_tmp_data[0], &ii_data[0], (unsigned int)
                     (b_loop_ub * (int)sizeof(signed char)));
            }

            k0 = (int)intermat2->data[intermat2->size[0] * q];
            if (NodesOnElementPBC->data[(k0 + NodesOnElementPBC->size[0] *
                 (node_dup_tmp_data[0] - 1)) - 1] > node) {
              if (1.0 > i1 - 1.0) {
                loop_ub = 0;
              } else {
                loop_ub = (int)(i1 - 1.0);
              }

              if (0 <= b_loop_ub - 1) {
                memcpy(&node_dup_tmp_data[0], &ii_data[0], (unsigned int)
                       (b_loop_ub * (int)sizeof(signed char)));
              }

              k0 = (int)intermat2->data[intermat2->size[0] * q];
              reg = NodesOnElementPBC->data[(k0 + NodesOnElementPBC->size[0] *
                (node_dup_tmp_data[0] - 1)) - 1];
              b_exponent = elems->size[0];
              elems->size[0] = loop_ub;
              emxEnsureCapacity_boolean_T(elems, b_exponent);
              for (b_exponent = 0; b_exponent < loop_ub; b_exponent++) {
                elems->data[b_exponent] = (NodesOnPBC->data[b_exponent + 100 *
                  ((int)node - 1)] == reg);
              }

              nx = elems->size[0];
              c_idx = 0;
              b_exponent = ii->size[0];
              ii->size[0] = elems->size[0];
              emxEnsureCapacity_int32_T(ii, b_exponent);
              nb = 0;
              exitg2 = false;
              while ((!exitg2) && (nb <= nx - 1)) {
                if (elems->data[nb]) {
                  c_idx++;
                  ii->data[c_idx - 1] = nb + 1;
                  if (c_idx >= nx) {
                    exitg2 = true;
                  } else {
                    nb++;
                  }
                } else {
                  nb++;
                }
              }

              if (elems->size[0] == 1) {
                if (c_idx == 0) {
                  ii->size[0] = 0;
                }
              } else if (1 > c_idx) {
                ii->size[0] = 0;
              } else {
                b_exponent = ii->size[0];
                ii->size[0] = c_idx;
                emxEnsureCapacity_int32_T(ii, b_exponent);
              }

              links2_size[0] = ii->size[0];
              loop_ub = ii->size[0];
              for (b_exponent = 0; b_exponent < loop_ub; b_exponent++) {
                links2_data[b_exponent] = (double)ii->data[b_exponent] + 1.0;
              }
            } else {
              links2_size[0] = 1;
              links2_data[0] = 1.0;
            }

            nodes_size[0] = links2_size[0];
            loop_ub = links2_size[0];
            for (b_exponent = 0; b_exponent < loop_ub; b_exponent++) {
              nodes_data[b_exponent] = idx->data[(int)links2_data[b_exponent] -
                1];
            }

            if (b_ifWhileCond(nodes_data, nodes_size)) {
              absxk++;
              if (0 <= b_loop_ub - 1) {
                memcpy(&node_dup_tmp_data[0], &ii_data[0], (unsigned int)
                       (b_loop_ub * (int)sizeof(signed char)));
              }

              k0 = (int)intermat2->data[intermat2->size[0] * q];
              k0 = (int)NodesOnElementPBC->data[(k0 + NodesOnElementPBC->size[0]
                * (node_dup_tmp_data[0] - 1)) - 1];
              b_exponent = (int)absxk;
              Coordinates3->data[b_exponent - 1] = Coordinates->data[k0 - 1];
              Coordinates3->data[(b_exponent + Coordinates3->size[0]) - 1] =
                Coordinates->data[(k0 + Coordinates->size[0]) - 1];
              Coordinates3->data[(b_exponent + (Coordinates3->size[0] << 1)) - 1]
                = Coordinates->data[(k0 + (Coordinates->size[0] << 1)) - 1];

              /*  Loop over all elements attached to node */
              b_exponent = (int)MorePBC->data[q];
              if (0 <= b_exponent - 1) {
                if (1.0 > nen) {
                  e_nx = 0;
                } else {
                  e_nx = (int)nen;
                }

                x_size[1] = e_nx;
              }

              for (nx = 0; nx < b_exponent; nx++) {
                sparse_parenAssign2D(NodeReg, absxk, numA, RegionOnElement->
                                     data[(int)intermat2->data[nx +
                                     intermat2->size[0] * q] - 1]);
                if (ElementsOnNodeNum->data[(int)node - 1] < 1.0) {
                  setAll->size[0] = 1;
                  setAll->size[1] = 0;
                } else if (rtIsInf(ElementsOnNodeNum->data[(int)node - 1]) &&
                           (1.0 == ElementsOnNodeNum->data[(int)node - 1])) {
                  i = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  setAll->size[1] = 1;
                  emxEnsureCapacity_real_T(setAll, i);
                  setAll->data[0] = rtNaN;
                } else {
                  i = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  setAll->size[1] = (int)floor(ElementsOnNodeNum->data[(int)node
                    - 1] - 1.0) + 1;
                  emxEnsureCapacity_real_T(setAll, i);
                  loop_ub = (int)floor(ElementsOnNodeNum->data[(int)node - 1] -
                                       1.0);
                  for (i = 0; i <= loop_ub; i++) {
                    setAll->data[i] = 1.0 + (double)i;
                  }
                }

                f_sparse_parenReference(ElementsOnNode->d,
                  ElementsOnNode->colidx, ElementsOnNode->rowidx, setAll, node,
                  NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                  &NodeRegSum.m);
                c_sparse_eq(intermat2->data[nx + intermat2->size[0] * q],
                            NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                            NodeRegSum.m, &b_expl_temp);
                i = t0_rowidx->size[0];
                t0_rowidx->size[0] = b_expl_temp.colidx->size[0];
                emxEnsureCapacity_int32_T(t0_rowidx, i);
                loop_ub = b_expl_temp.colidx->size[0];
                for (i = 0; i < loop_ub; i++) {
                  t0_rowidx->data[i] = b_expl_temp.colidx->data[i];
                }

                i = internodes_rowidx->size[0];
                internodes_rowidx->size[0] = b_expl_temp.rowidx->size[0];
                emxEnsureCapacity_int32_T(internodes_rowidx, i);
                loop_ub = b_expl_temp.rowidx->size[0];
                for (i = 0; i < loop_ub; i++) {
                  internodes_rowidx->data[i] = b_expl_temp.rowidx->data[i];
                }

                nb = b_expl_temp.m;
                c_idx = 0;
                i = ii->size[0];
                ii->size[0] = t0_rowidx->data[t0_rowidx->size[0] - 1] - 1;
                emxEnsureCapacity_int32_T(ii, i);
                k0 = 1;
                while (c_idx < t0_rowidx->data[t0_rowidx->size[0] - 1] - 1) {
                  if (c_idx == t0_rowidx->data[k0] - 1) {
                    k0++;
                  } else {
                    c_idx++;
                    ii->data[c_idx - 1] = (k0 - 1) * nb +
                      internodes_rowidx->data[c_idx - 1];
                  }
                }

                if (t0_rowidx->data[t0_rowidx->size[0] - 1] - 1 == 1) {
                  if (c_idx == 0) {
                    ii->size[0] = 0;
                  }
                } else if (1 > c_idx) {
                  ii->size[0] = 0;
                } else {
                  i = ii->size[0];
                  ii->size[0] = c_idx;
                  emxEnsureCapacity_int32_T(ii, i);
                }

                i = ElementsOnNodePBC_d->size[0];
                ElementsOnNodePBC_d->size[0] = ii->size[0];
                emxEnsureCapacity_real_T(ElementsOnNodePBC_d, i);
                loop_ub = ii->size[0];
                for (i = 0; i < loop_ub; i++) {
                  ElementsOnNodePBC_d->data[i] = ii->data[i];
                }

                c_sparse_parenAssign(ElementsOnNodeDup, absxk,
                                     ElementsOnNodePBC_d, node);

                /*  Reset nodal ID for element in higher material ID */
                k0 = (int)intermat2->data[nx + intermat2->size[0] * q];
                for (i = 0; i < e_nx; i++) {
                  x_data[i] = (NodesOnElementCG->data[(k0 +
                    NodesOnElementCG->size[0] * i) - 1] == node);
                }

                c_idx = 0;
                b_loop_ub = x_size[1];
                nb = 0;
                exitg2 = false;
                while ((!exitg2) && (nb <= e_nx - 1)) {
                  if (x_data[nb]) {
                    c_idx++;
                    ii_data[c_idx - 1] = (signed char)(nb + 1);
                    if (c_idx >= e_nx) {
                      exitg2 = true;
                    } else {
                      nb++;
                    }
                  } else {
                    nb++;
                  }
                }

                if (x_size[1] == 1) {
                  if (c_idx == 0) {
                    b_loop_ub = 0;
                  }
                } else if (1 > c_idx) {
                  b_loop_ub = 0;
                } else {
                  b_loop_ub = c_idx;
                }

                if (0 <= b_loop_ub - 1) {
                  memcpy(&node_dup_tmp_data[0], &ii_data[0], (unsigned int)
                         (b_loop_ub * (int)sizeof(signed char)));
                }

                k0 = (int)intermat2->data[nx + intermat2->size[0] * q];
                for (i = 0; i < b_loop_ub; i++) {
                  NodesOnElementDG->data[(k0 + NodesOnElementDG->size[0] *
                    (node_dup_tmp_data[i] - 1)) - 1] = absxk;
                }
              }
            } else {
              loop_ub = links2_size[0];
              for (b_exponent = 0; b_exponent < loop_ub; b_exponent++) {
                ia_data[b_exponent] = (int)links2_data[b_exponent];
              }

              loop_ub = links2_size[0];
              for (b_exponent = 0; b_exponent < loop_ub; b_exponent++) {
                idx->data[ia_data[b_exponent] - 1] = 1.0;
              }

              b_exponent = (int)MorePBC->data[q];
              if (0 <= b_exponent - 1) {
                if (1.0 > nen) {
                  d_nx = 0;
                } else {
                  d_nx = (int)nen;
                }

                x_size[1] = d_nx;
              }

              for (nx = 0; nx < b_exponent; nx++) {
                sparse_parenAssign2D(NodeReg, numA, numA, RegionOnElement->data
                                     [(int)intermat2->data[nx + intermat2->size
                                     [0] * q] - 1]);
                if (ElementsOnNodeNum->data[(int)node - 1] < 1.0) {
                  setAll->size[0] = 1;
                  setAll->size[1] = 0;
                } else if (rtIsInf(ElementsOnNodeNum->data[(int)node - 1]) &&
                           (1.0 == ElementsOnNodeNum->data[(int)node - 1])) {
                  i = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  setAll->size[1] = 1;
                  emxEnsureCapacity_real_T(setAll, i);
                  setAll->data[0] = rtNaN;
                } else {
                  i = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  setAll->size[1] = (int)floor(ElementsOnNodeNum->data[(int)node
                    - 1] - 1.0) + 1;
                  emxEnsureCapacity_real_T(setAll, i);
                  loop_ub = (int)floor(ElementsOnNodeNum->data[(int)node - 1] -
                                       1.0);
                  for (i = 0; i <= loop_ub; i++) {
                    setAll->data[i] = 1.0 + (double)i;
                  }
                }

                f_sparse_parenReference(ElementsOnNode->d,
                  ElementsOnNode->colidx, ElementsOnNode->rowidx, setAll, node,
                  NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                  &NodeRegSum.m);
                c_sparse_eq(intermat2->data[nx + intermat2->size[0] * q],
                            NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                            NodeRegSum.m, &b_expl_temp);
                i = t0_rowidx->size[0];
                t0_rowidx->size[0] = b_expl_temp.colidx->size[0];
                emxEnsureCapacity_int32_T(t0_rowidx, i);
                loop_ub = b_expl_temp.colidx->size[0];
                for (i = 0; i < loop_ub; i++) {
                  t0_rowidx->data[i] = b_expl_temp.colidx->data[i];
                }

                i = internodes_rowidx->size[0];
                internodes_rowidx->size[0] = b_expl_temp.rowidx->size[0];
                emxEnsureCapacity_int32_T(internodes_rowidx, i);
                loop_ub = b_expl_temp.rowidx->size[0];
                for (i = 0; i < loop_ub; i++) {
                  internodes_rowidx->data[i] = b_expl_temp.rowidx->data[i];
                }

                nb = b_expl_temp.m;
                c_idx = 0;
                i = ii->size[0];
                ii->size[0] = t0_rowidx->data[t0_rowidx->size[0] - 1] - 1;
                emxEnsureCapacity_int32_T(ii, i);
                k0 = 1;
                while (c_idx < t0_rowidx->data[t0_rowidx->size[0] - 1] - 1) {
                  if (c_idx == t0_rowidx->data[k0] - 1) {
                    k0++;
                  } else {
                    c_idx++;
                    ii->data[c_idx - 1] = (k0 - 1) * nb +
                      internodes_rowidx->data[c_idx - 1];
                  }
                }

                if (t0_rowidx->data[t0_rowidx->size[0] - 1] - 1 == 1) {
                  if (c_idx == 0) {
                    ii->size[0] = 0;
                  }
                } else if (1 > c_idx) {
                  ii->size[0] = 0;
                } else {
                  i = ii->size[0];
                  ii->size[0] = c_idx;
                  emxEnsureCapacity_int32_T(ii, i);
                }

                i = ElementsOnNodePBC_d->size[0];
                ElementsOnNodePBC_d->size[0] = ii->size[0];
                emxEnsureCapacity_real_T(ElementsOnNodePBC_d, i);
                loop_ub = ii->size[0];
                for (i = 0; i < loop_ub; i++) {
                  ElementsOnNodePBC_d->data[i] = ii->data[i];
                }

                c_sparse_parenAssign(ElementsOnNodeDup, numA,
                                     ElementsOnNodePBC_d, node);

                /*  Reset nodal ID for element in higher material ID */
                k0 = (int)intermat2->data[nx + intermat2->size[0] * q];
                for (i = 0; i < d_nx; i++) {
                  x_data[i] = (NodesOnElementCG->data[(k0 +
                    NodesOnElementCG->size[0] * i) - 1] == node);
                }

                c_idx = 0;
                b_loop_ub = x_size[1];
                nb = 0;
                exitg2 = false;
                while ((!exitg2) && (nb <= d_nx - 1)) {
                  if (x_data[nb]) {
                    c_idx++;
                    ii_data[c_idx - 1] = (signed char)(nb + 1);
                    if (c_idx >= d_nx) {
                      exitg2 = true;
                    } else {
                      nb++;
                    }
                  } else {
                    nb++;
                  }
                }

                if (x_size[1] == 1) {
                  if (c_idx == 0) {
                    b_loop_ub = 0;
                  }
                } else if (1 > c_idx) {
                  b_loop_ub = 0;
                } else {
                  b_loop_ub = c_idx;
                }

                if (0 <= b_loop_ub - 1) {
                  memcpy(&node_dup_tmp_data[0], &ii_data[0], (unsigned int)
                         (b_loop_ub * (int)sizeof(signed char)));
                }

                k0 = (int)intermat2->data[nx + intermat2->size[0] * q];
                for (i = 0; i < b_loop_ub; i++) {
                  NodesOnElementDG->data[(k0 + NodesOnElementDG->size[0] *
                    (node_dup_tmp_data[i] - 1)) - 1] = numA;
                }
              }
            }
          }
        }
      } else {
        /*  regular node */
        pEnd = 0;
        i27 = (int)ElementsOnNodeNum->data[(int)NodesOnInterface->data[p] - 1];
        for (q = 0; q < i27; q++) {
          if (MorePBC->data[q] > 0.0) {
            if (pEnd != 0) {
              absxk++;
              b_exponent = (int)absxk;
              Coordinates3->data[b_exponent - 1] = Coordinates->data[(int)node -
                1];
              Coordinates3->data[(b_exponent + Coordinates3->size[0]) - 1] =
                Coordinates->data[((int)node + Coordinates->size[0]) - 1];
              Coordinates3->data[(b_exponent + (Coordinates3->size[0] << 1)) - 1]
                = Coordinates->data[((int)node + (Coordinates->size[0] << 1)) -
                1];

              /*  Loop over all elements attached to node */
              b_exponent = (int)MorePBC->data[q];
              if (0 <= b_exponent - 1) {
                if (1.0 > nen) {
                  c_nx = 0;
                } else {
                  c_nx = (int)nen;
                }

                x_size[1] = c_nx;
              }

              for (j = 0; j < b_exponent; j++) {
                sparse_parenAssign2D(NodeReg, absxk, node, RegionOnElement->
                                     data[(int)intermat2->data[j +
                                     intermat2->size[0] * q] - 1]);
                if (ElementsOnNodeNum->data[(int)node - 1] < 1.0) {
                  setAll->size[0] = 1;
                  setAll->size[1] = 0;
                } else if (rtIsInf(ElementsOnNodeNum->data[(int)node - 1]) &&
                           (1.0 == ElementsOnNodeNum->data[(int)node - 1])) {
                  i = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  setAll->size[1] = 1;
                  emxEnsureCapacity_real_T(setAll, i);
                  setAll->data[0] = rtNaN;
                } else {
                  i = setAll->size[0] * setAll->size[1];
                  setAll->size[0] = 1;
                  setAll->size[1] = (int)floor(ElementsOnNodeNum->data[(int)node
                    - 1] - 1.0) + 1;
                  emxEnsureCapacity_real_T(setAll, i);
                  loop_ub = (int)floor(ElementsOnNodeNum->data[(int)node - 1] -
                                       1.0);
                  for (i = 0; i <= loop_ub; i++) {
                    setAll->data[i] = 1.0 + (double)i;
                  }
                }

                f_sparse_parenReference(ElementsOnNode->d,
                  ElementsOnNode->colidx, ElementsOnNode->rowidx, setAll, node,
                  NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                  &NodeRegSum.m);
                c_sparse_eq(intermat2->data[j + intermat2->size[0] * q],
                            NodeRegSum.d, NodeRegSum.colidx, NodeRegSum.rowidx,
                            NodeRegSum.m, &b_expl_temp);
                i = t0_rowidx->size[0];
                t0_rowidx->size[0] = b_expl_temp.colidx->size[0];
                emxEnsureCapacity_int32_T(t0_rowidx, i);
                loop_ub = b_expl_temp.colidx->size[0];
                for (i = 0; i < loop_ub; i++) {
                  t0_rowidx->data[i] = b_expl_temp.colidx->data[i];
                }

                i = internodes_rowidx->size[0];
                internodes_rowidx->size[0] = b_expl_temp.rowidx->size[0];
                emxEnsureCapacity_int32_T(internodes_rowidx, i);
                loop_ub = b_expl_temp.rowidx->size[0];
                for (i = 0; i < loop_ub; i++) {
                  internodes_rowidx->data[i] = b_expl_temp.rowidx->data[i];
                }

                nb = b_expl_temp.m;
                c_idx = 0;
                i = ii->size[0];
                ii->size[0] = t0_rowidx->data[t0_rowidx->size[0] - 1] - 1;
                emxEnsureCapacity_int32_T(ii, i);
                k0 = 1;
                while (c_idx < t0_rowidx->data[t0_rowidx->size[0] - 1] - 1) {
                  if (c_idx == t0_rowidx->data[k0] - 1) {
                    k0++;
                  } else {
                    c_idx++;
                    ii->data[c_idx - 1] = (k0 - 1) * nb +
                      internodes_rowidx->data[c_idx - 1];
                  }
                }

                if (t0_rowidx->data[t0_rowidx->size[0] - 1] - 1 == 1) {
                  if (c_idx == 0) {
                    ii->size[0] = 0;
                  }
                } else if (1 > c_idx) {
                  ii->size[0] = 0;
                } else {
                  i = ii->size[0];
                  ii->size[0] = c_idx;
                  emxEnsureCapacity_int32_T(ii, i);
                }

                i = ElementsOnNodePBC_d->size[0];
                ElementsOnNodePBC_d->size[0] = ii->size[0];
                emxEnsureCapacity_real_T(ElementsOnNodePBC_d, i);
                loop_ub = ii->size[0];
                for (i = 0; i < loop_ub; i++) {
                  ElementsOnNodePBC_d->data[i] = ii->data[i];
                }

                c_sparse_parenAssign(ElementsOnNodeDup, absxk,
                                     ElementsOnNodePBC_d, node);

                /*  Reset nodal ID for element in higher material ID */
                k0 = (int)intermat2->data[j + intermat2->size[0] * q];
                for (i = 0; i < c_nx; i++) {
                  x_data[i] = (NodesOnElementCG->data[(k0 +
                    NodesOnElementCG->size[0] * i) - 1] == node);
                }

                c_idx = 0;
                b_loop_ub = x_size[1];
                nb = 0;
                exitg2 = false;
                while ((!exitg2) && (nb <= c_nx - 1)) {
                  if (x_data[nb]) {
                    c_idx++;
                    ii_data[c_idx - 1] = (signed char)(nb + 1);
                    if (c_idx >= c_nx) {
                      exitg2 = true;
                    } else {
                      nb++;
                    }
                  } else {
                    nb++;
                  }
                }

                if (x_size[1] == 1) {
                  if (c_idx == 0) {
                    b_loop_ub = 0;
                  }
                } else if (1 > c_idx) {
                  b_loop_ub = 0;
                } else {
                  b_loop_ub = c_idx;
                }

                if (0 <= b_loop_ub - 1) {
                  memcpy(&node_dup_tmp_data[0], &ii_data[0], (unsigned int)
                         (b_loop_ub * (int)sizeof(signed char)));
                }

                k0 = (int)intermat2->data[j + intermat2->size[0] * q];
                for (i = 0; i < b_loop_ub; i++) {
                  NodesOnElementDG->data[(k0 + NodesOnElementDG->size[0] *
                    (node_dup_tmp_data[i] - 1)) - 1] = absxk;
                }
              }
            } else {
              pEnd = 1;
            }
          }
        }
      }
    }

    /* if facnum */
  }

  f_emxFreeStruct_coder_internal_(&c_expl_temp);
  emxFree_boolean_T(&t3_d);
  emxFree_int32_T(&t0_colidx);
  emxFree_int32_T(&b_idx);
  emxFree_real_T(&b_elems);
  emxFree_real_T(&intermat2);
  emxFree_boolean_T(&internodes_d);
  emxFree_int32_T(&ElementsOnNodePBC_rowidx);
  emxFree_int32_T(&ElementsOnNodePBC_colidx);
  emxFree_real_T(&MorePBC);
  emxFree_real_T(&ElementsOnNodePBCNum);

  /*  Form interfaces between all edges on interiors of specified material regions */
  diag(InterTypes, s);
  if (c_norm(s) > 0.0) {
    i25 = NodesOnElementCG->size[0] * NodesOnElementCG->size[1];
    NodesOnElementCG->size[0] = NodesOnElementDG->size[0];
    NodesOnElementCG->size[1] = NodesOnElementDG->size[1];
    emxEnsureCapacity_real_T(NodesOnElementCG, i25);
    loop_ub = NodesOnElementDG->size[0] * NodesOnElementDG->size[1];
    for (i25 = 0; i25 < loop_ub; i25++) {
      NodesOnElementCG->data[i25] = NodesOnElementDG->data[i25];
    }

    /*  Update connectivities to reflect presence of interfaces */
    i25 = ElementsOnNodeNum2->size[0];
    ElementsOnNodeNum2->size[0] = (int)absxk;
    emxEnsureCapacity_real_T(ElementsOnNodeNum2, i25);
    loop_ub = (int)absxk;
    for (i25 = 0; i25 < loop_ub; i25++) {
      ElementsOnNodeNum2->data[i25] = 0.0;
    }

    i25 = NodeCGDG->size[0] * NodeCGDG->size[1];
    NodeCGDG->size[0] = 100;
    NodeCGDG->size[1] = (int)absxk;
    emxEnsureCapacity_real_T(NodeCGDG, i25);
    loop_ub = 100 * (int)absxk;
    for (i25 = 0; i25 < loop_ub; i25++) {
      NodeCGDG->data[i25] = 0.0;
    }

    /*  Mapping of DG nodal IDs to CG nodal IDs */
    if (!(absxk < 1.0)) {
      loop_ub = (int)floor(absxk - 1.0);
      for (i25 = 0; i25 <= loop_ub; i25++) {
        NodeCGDG->data[100 * i25] = 1.0 + (double)i25;
      }
    }

    i25 = (int)nummat;
    for (notdone = 0; notdone < i25; notdone++) {
      /*  ID for material pair (row=mat2, col=mat1) */
      if (InterTypes->data[notdone + InterTypes->size[0] * notdone] > 0.0) {
        /*  Find elements belonging to material mat2 */
        i27 = elems->size[0];
        elems->size[0] = RegionOnElement->size[0];
        emxEnsureCapacity_boolean_T(elems, i27);
        loop_ub = RegionOnElement->size[0];
        for (i27 = 0; i27 < loop_ub; i27++) {
          elems->data[i27] = (RegionOnElement->data[i27] == 1.0 + (double)
                              notdone);
        }

        nx = elems->size[0];
        c_idx = 0;
        i27 = ii->size[0];
        ii->size[0] = elems->size[0];
        emxEnsureCapacity_int32_T(ii, i27);
        nb = 0;
        exitg2 = false;
        while ((!exitg2) && (nb <= nx - 1)) {
          if (elems->data[nb]) {
            c_idx++;
            ii->data[c_idx - 1] = nb + 1;
            if (c_idx >= nx) {
              exitg2 = true;
            } else {
              nb++;
            }
          } else {
            nb++;
          }
        }

        if (elems->size[0] == 1) {
          if (c_idx == 0) {
            ii->size[0] = 0;
          }
        } else if (1 > c_idx) {
          ii->size[0] = 0;
        } else {
          i27 = ii->size[0];
          ii->size[0] = c_idx;
          emxEnsureCapacity_int32_T(ii, i27);
        }

        i27 = idx->size[0];
        idx->size[0] = ii->size[0];
        emxEnsureCapacity_real_T(idx, i27);
        loop_ub = ii->size[0];
        for (i27 = 0; i27 < loop_ub; i27++) {
          idx->data[i27] = ii->data[i27];
        }

        /*  Loop over elements in material mat2, explode all the elements */
        i27 = idx->size[0];
        for (b_i = 0; b_i < i27; b_i++) {
          elem = (int)idx->data[b_i] - 1;
          if (1.0 > nen) {
            loop_ub = 0;
          } else {
            loop_ub = (int)nen;
          }

          c_idx = (int)idx->data[b_i];
          for (b_exponent = 0; b_exponent < loop_ub; b_exponent++) {
            nodeloc_data[b_exponent] = NodesOnElementDG->data[(c_idx +
              NodesOnElementDG->size[0] * b_exponent) - 1];
          }

          n = -1;
          b_exponent = loop_ub - 1;
          for (k = 0; k <= b_exponent; k++) {
            if (nodeloc_data[k] != 0.0) {
              n++;
            }
          }

          for (p = 0; p <= n; p++) {
            /*  Loop over local Nodes */
            node = NodesOnElementCG->data[elem + NodesOnElementCG->size[0] * p];
            b_exponent = (int)node;
            i = b_exponent - 1;
            if (ElementsOnNodeNum2->data[i] == 0.0) {
              /*  only duplicate nodes after the first time encountered */
              ElementsOnNodeNum2->data[i] = 1.0;
              NodeCGDG->data[100 * i] = node;

              /* node in DG mesh with same coordinates */
            } else {
              absxk++;
              NodesOnElementDG->data[elem + NodesOnElementDG->size[0] * p] =
                absxk;
              i1 = Coordinates3->data[i];
              numA = Coordinates3->data[(b_exponent + Coordinates3->size[0]) - 1];
              reg = Coordinates3->data[(b_exponent + (Coordinates3->size[0] << 1))
                - 1];
              b_exponent = (int)absxk;
              Coordinates3->data[b_exponent - 1] = i1;
              Coordinates3->data[(b_exponent + Coordinates3->size[0]) - 1] =
                numA;
              Coordinates3->data[(b_exponent + (Coordinates3->size[0] << 1)) - 1]
                = reg;
              i1 = NodesOnElement->data[elem + NodesOnElement->size[0] * p];
              b_exponent = (int)i1 - 1;
              if (ElementsOnNodeNum->data[b_exponent] < 1.0) {
                setAll->size[0] = 1;
                setAll->size[1] = 0;
              } else if (rtIsInf(ElementsOnNodeNum->data[b_exponent]) && (1.0 ==
                          ElementsOnNodeNum->data[b_exponent])) {
                b_exponent = setAll->size[0] * setAll->size[1];
                setAll->size[0] = 1;
                setAll->size[1] = 1;
                emxEnsureCapacity_real_T(setAll, b_exponent);
                setAll->data[0] = rtNaN;
              } else {
                k0 = setAll->size[0] * setAll->size[1];
                setAll->size[0] = 1;
                setAll->size[1] = (int)floor(ElementsOnNodeNum->data[b_exponent]
                  - 1.0) + 1;
                emxEnsureCapacity_real_T(setAll, k0);
                loop_ub = (int)floor(ElementsOnNodeNum->data[b_exponent] - 1.0);
                for (b_exponent = 0; b_exponent <= loop_ub; b_exponent++) {
                  setAll->data[b_exponent] = 1.0 + (double)b_exponent;
                }
              }

              f_sparse_parenReference(ElementsOnNode->d, ElementsOnNode->colidx,
                ElementsOnNode->rowidx, setAll, i1, NodeRegSum.d,
                NodeRegSum.colidx, NodeRegSum.rowidx, &NodeRegSum.m);
              c_sparse_eq(elem + 1, NodeRegSum.d, NodeRegSum.colidx,
                          NodeRegSum.rowidx, NodeRegSum.m, &b_expl_temp);
              b_exponent = t0_rowidx->size[0];
              t0_rowidx->size[0] = b_expl_temp.colidx->size[0];
              emxEnsureCapacity_int32_T(t0_rowidx, b_exponent);
              loop_ub = b_expl_temp.colidx->size[0];
              for (b_exponent = 0; b_exponent < loop_ub; b_exponent++) {
                t0_rowidx->data[b_exponent] = b_expl_temp.colidx->
                  data[b_exponent];
              }

              b_exponent = internodes_rowidx->size[0];
              internodes_rowidx->size[0] = b_expl_temp.rowidx->size[0];
              emxEnsureCapacity_int32_T(internodes_rowidx, b_exponent);
              loop_ub = b_expl_temp.rowidx->size[0];
              for (b_exponent = 0; b_exponent < loop_ub; b_exponent++) {
                internodes_rowidx->data[b_exponent] = b_expl_temp.rowidx->
                  data[b_exponent];
              }

              nb = b_expl_temp.m;
              c_idx = 0;
              b_exponent = ii->size[0];
              ii->size[0] = t0_rowidx->data[t0_rowidx->size[0] - 1] - 1;
              emxEnsureCapacity_int32_T(ii, b_exponent);
              k0 = 1;
              while (c_idx < t0_rowidx->data[t0_rowidx->size[0] - 1] - 1) {
                if (c_idx == t0_rowidx->data[k0] - 1) {
                  k0++;
                } else {
                  c_idx++;
                  ii->data[c_idx - 1] = (k0 - 1) * nb + internodes_rowidx->
                    data[c_idx - 1];
                }
              }

              if (t0_rowidx->data[t0_rowidx->size[0] - 1] - 1 == 1) {
                if (c_idx == 0) {
                  ii->size[0] = 0;
                }
              } else if (1 > c_idx) {
                ii->size[0] = 0;
              } else {
                b_exponent = ii->size[0];
                ii->size[0] = c_idx;
                emxEnsureCapacity_int32_T(ii, b_exponent);
              }

              b_exponent = ElementsOnNodePBC_d->size[0];
              ElementsOnNodePBC_d->size[0] = ii->size[0];
              emxEnsureCapacity_real_T(ElementsOnNodePBC_d, b_exponent);
              loop_ub = ii->size[0];
              for (b_exponent = 0; b_exponent < loop_ub; b_exponent++) {
                ElementsOnNodePBC_d->data[b_exponent] = ii->data[b_exponent];
              }

              c_sparse_parenAssign(ElementsOnNodeDup, absxk, ElementsOnNodePBC_d,
                                   i1);

              /*    Add element to star list, increment number of elem in star */
              i1 = ElementsOnNodeNum2->data[i] + 1.0;
              ElementsOnNodeNum2->data[i]++;
              NodeCGDG->data[((int)i1 + 100 * ((int)node - 1)) - 1] = absxk;

              /* node in DG mesh with same coordinates */
            }
          }
        }
      }
    }

    i25 = elems->size[0];
    elems->size[0] = ElementsOnNodeNum2->size[0];
    emxEnsureCapacity_boolean_T(elems, i25);
    loop_ub = ElementsOnNodeNum2->size[0];
    for (i25 = 0; i25 < loop_ub; i25++) {
      elems->data[i25] = (ElementsOnNodeNum2->data[i25] == 0.0);
    }

    nx = elems->size[0];
    c_idx = 0;
    i25 = ii->size[0];
    ii->size[0] = elems->size[0];
    emxEnsureCapacity_int32_T(ii, i25);
    nb = 0;
    exitg2 = false;
    while ((!exitg2) && (nb <= nx - 1)) {
      if (elems->data[nb]) {
        c_idx++;
        ii->data[c_idx - 1] = nb + 1;
        if (c_idx >= nx) {
          exitg2 = true;
        } else {
          nb++;
        }
      } else {
        nb++;
      }
    }

    if (elems->size[0] == 1) {
      if (c_idx == 0) {
        ii->size[0] = 0;
      }
    } else if (1 > c_idx) {
      ii->size[0] = 0;
    } else {
      i25 = ii->size[0];
      ii->size[0] = c_idx;
      emxEnsureCapacity_int32_T(ii, i25);
    }

    loop_ub = ii->size[0];
    for (i25 = 0; i25 < loop_ub; i25++) {
      ElementsOnNodeNum2->data[ii->data[i25] - 1] = 1.0;
    }
  } else {
    /*  no DG in interior of any materials */
    i25 = ElementsOnNodeNum2->size[0];
    loop_ub = (int)absxk;
    ElementsOnNodeNum2->size[0] = loop_ub;
    emxEnsureCapacity_real_T(ElementsOnNodeNum2, i25);
    for (i25 = 0; i25 < loop_ub; i25++) {
      ElementsOnNodeNum2->data[i25] = 1.0;
    }

    /*  set one copy of duplicated nodes per material ID so that BC expanding subroutine works properly. */
    i25 = NodeCGDG->size[0] * NodeCGDG->size[1];
    NodeCGDG->size[0] = 100;
    NodeCGDG->size[1] = loop_ub;
    emxEnsureCapacity_real_T(NodeCGDG, i25);
    i = 100 * loop_ub;
    for (i25 = 0; i25 < i; i25++) {
      NodeCGDG->data[i25] = 0.0;
    }

    /*  Mapping of DG nodal IDs to CG nodal IDs */
    if (!(absxk < 1.0)) {
      i = (int)floor(absxk - 1.0);
      for (i25 = 0; i25 <= i; i25++) {
        NodeCGDG->data[100 * i25] = 1.0 + (double)i25;
      }
    }
  }

  e_emxFreeStruct_coder_internal_(&b_expl_temp);
  emxFree_int32_T(&t0_rowidx);
  emxFree_real_T(&s);
  emxFree_int32_T(&ii);
  emxFree_real_T(&idx);
  emxFree_boolean_T(&elems);
  d_emxFreeStruct_coder_internal_(&NodeRegSum);
  emxFree_int32_T(&internodes_rowidx);
  emxFree_real_T(&setAll);
  emxFree_real_T(&ElementsOnNodePBC_d);

  /*  Form final lists of DG interfaces */
  *numnp = absxk;
  if (1.0 > absxk) {
    loop_ub = 0;
  } else {
    loop_ub = (int)absxk;
  }

  i25 = Coordinates->size[0] * Coordinates->size[1];
  Coordinates->size[0] = loop_ub;
  Coordinates->size[1] = 3;
  emxEnsureCapacity_real_T(Coordinates, i25);
  for (i25 = 0; i25 < 3; i25++) {
    for (i27 = 0; i27 < loop_ub; i27++) {
      Coordinates->data[i27 + Coordinates->size[0] * i25] = Coordinates3->
        data[i27 + Coordinates3->size[0] * i25];
    }
  }

  emxFree_real_T(&Coordinates3);
  i25 = NodesOnElement->size[0] * NodesOnElement->size[1];
  NodesOnElement->size[0] = NodesOnElementDG->size[0];
  NodesOnElement->size[1] = NodesOnElementDG->size[1];
  emxEnsureCapacity_real_T(NodesOnElement, i25);
  loop_ub = NodesOnElementDG->size[1];
  for (i25 = 0; i25 < loop_ub; i25++) {
    b_loop_ub = NodesOnElementDG->size[0];
    for (i27 = 0; i27 < b_loop_ub; i27++) {
      NodesOnElement->data[i27 + NodesOnElement->size[0] * i25] =
        NodesOnElementDG->data[i27 + NodesOnElementDG->size[0] * i25];
    }
  }

  if (usePBC != 0.0) {
    i25 = NodesOnElementCG->size[0] * NodesOnElementCG->size[1];
    NodesOnElementCG->size[0] = NodesOnElementPBC->size[0];
    NodesOnElementCG->size[1] = NodesOnElementPBC->size[1];
    emxEnsureCapacity_real_T(NodesOnElementCG, i25);
    loop_ub = NodesOnElementPBC->size[0] * NodesOnElementPBC->size[1];
    for (i25 = 0; i25 < loop_ub; i25++) {
      NodesOnElementCG->data[i25] = NodesOnElementPBC->data[i25];
    }
  }

  emxFree_real_T(&NodesOnElementPBC);
  if (usePBC == 2.0) {
    i25 = MPCList->size[0] * MPCList->size[1];
    MPCList->size[0] = PBCList->size[0];
    MPCList->size[1] = 5;
    emxEnsureCapacity_real_T(MPCList, i25);
    for (i25 = 0; i25 < 5; i25++) {
      loop_ub = PBCList->size[0];
      for (i27 = 0; i27 < loop_ub; i27++) {
        MPCList->data[i27 + MPCList->size[0] * i25] = PBCList->data[i27 +
          PBCList->size[0] * i25];
      }
    }
  }

  emxFree_real_T(&PBCList);

  /*  %% Clear out intermediate variables */
  /*  if clearinter */
  /*  KeyList = {'numEonB','numEonF','ElementsOnBoundary','numSI','ElementsOnFacet',... */
  /*  'ElementsOnNode','ElementsOnNodeA','ElementsOnNodeB','ElementsOnNodeDup',... */
  /*  'ElementsOnNodeNum','numfac','ElementsOnNodeNum2','numinttype','FacetsOnElement',... */
  /*  'FacetsOnElementInt','FacetsOnInterface','FacetsOnInterfaceNum','FacetsOnNode',... */
  /*  'FacetsOnNodeCut','FacetsOnNodeInt','FacetsOnNodeNum','NodeCGDG','NodeReg',... */
  /*  'NodesOnElementCG','NodesOnElementDG','NodesOnInterface','NodesOnInterfaceNum',... */
  /*  'numCL','maxel','numelPBC','RegionOnElementDG','numPBC','TieNodesNew','TieNodes',... */
  /*  'NodesOnPBC','NodesOnPBCnum','NodesOnLink','NodesOnLinknum','numEonPBC',... */
  /*  'FacetsOnPBC','FacetsOnPBCNum','FacetsOnIntMinusPBC','FacetsOnIntMinusPBCNum','MPCList'}; */
  /*  if isOctave */
  /*      wholething = [{'-x'},KeyList,currvariables']; */
  /*      clear(wholething{:}) */
  /*  else */
  /*      wholething = [{'-except'},KeyList,currvariables']; */
  /*      clearvars(wholething{:}) */
  /*  end */
  /*  end */
  *NodesOnInterfaceNum = i26 + 1;
  *numCL = 0.0;
}

/* End of code generation (DEIPFunc3.c) */
