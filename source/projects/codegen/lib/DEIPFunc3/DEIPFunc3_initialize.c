/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * DEIPFunc3_initialize.c
 *
 * Code generation for function 'DEIPFunc3_initialize'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "DEIPFunc3.h"
#include "DEIPFunc3_initialize.h"

/* Function Definitions */
void DEIPFunc3_initialize(void)
{
  rt_InitInfAndNaN(8U);
}

/* End of code generation (DEIPFunc3_initialize.c) */
