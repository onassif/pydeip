/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * DEIPFunc3_rtwutil.h
 *
 * Code generation for function 'DEIPFunc3_rtwutil'
 *
 */

#ifndef DEIPFUNC3_RTWUTIL_H
#define DEIPFUNC3_RTWUTIL_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "DEIPFunc3_types.h"

/* Function Declarations */
extern double rt_roundd_snf(double u);

#endif

/* End of code generation (DEIPFunc3_rtwutil.h) */
