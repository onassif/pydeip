/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * DEIPFunc3_terminate.c
 *
 * Code generation for function 'DEIPFunc3_terminate'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "DEIPFunc3.h"
#include "DEIPFunc3_terminate.h"

/* Function Definitions */
void DEIPFunc3_terminate(void)
{
  /* (no terminate code required) */
}

/* End of code generation (DEIPFunc3_terminate.c) */
