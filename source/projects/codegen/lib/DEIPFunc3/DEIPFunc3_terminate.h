/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * DEIPFunc3_terminate.h
 *
 * Code generation for function 'DEIPFunc3_terminate'
 *
 */

#ifndef DEIPFUNC3_TERMINATE_H
#define DEIPFUNC3_TERMINATE_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "DEIPFunc3_types.h"

/* Function Declarations */
extern void DEIPFunc3_terminate(void);

#endif

/* End of code generation (DEIPFunc3_terminate.h) */
