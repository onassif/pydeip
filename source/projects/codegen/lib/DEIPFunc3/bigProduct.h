/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * bigProduct.h
 *
 * Code generation for function 'bigProduct'
 *
 */

#ifndef BIGPRODUCT_H
#define BIGPRODUCT_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "DEIPFunc3_types.h"

/* Function Declarations */
extern void bigProduct(int a, int b, int *loworderbits, int *highorderbits);

#endif

/* End of code generation (bigProduct.h) */
