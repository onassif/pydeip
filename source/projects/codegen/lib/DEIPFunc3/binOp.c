/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * binOp.c
 *
 * Code generation for function 'binOp'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "DEIPFunc3.h"
#include "binOp.h"

/* Function Definitions */
int getBinOpSize(int b_m)
{
  return b_m;
}

double getScalar(const emxArray_real_T *x_d, const emxArray_int32_T *x_colidx)
{
  double scalarx;
  if (x_colidx->data[x_colidx->size[0] - 1] - 1 > 0) {
    scalarx = x_d->data[0];
  } else {
    scalarx = 0.0;
  }

  return scalarx;
}

/* End of code generation (binOp.c) */
