/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * binOp.h
 *
 * Code generation for function 'binOp'
 *
 */

#ifndef BINOP_H
#define BINOP_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "DEIPFunc3_types.h"

/* Function Declarations */
extern int getBinOpSize(int b_m);
extern double getScalar(const emxArray_real_T *x_d, const emxArray_int32_T
  *x_colidx);

#endif

/* End of code generation (binOp.h) */
