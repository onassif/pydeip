/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * diag1.h
 *
 * Code generation for function 'diag1'
 *
 */

#ifndef DIAG1_H
#define DIAG1_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "DEIPFunc3_types.h"

/* Function Declarations */
extern void diag(const emxArray_real_T *v, emxArray_real_T *d);

#endif

/* End of code generation (diag1.h) */
