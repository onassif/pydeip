/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * eml_setop.c
 *
 * Code generation for function 'eml_setop'
 *
 */

/* Include files */
#include <math.h>
#include "rt_nonfinite.h"
#include "DEIPFunc3.h"
#include "eml_setop.h"
#include "DEIPFunc3_emxutil.h"

/* Function Declarations */
static unsigned char relop_rows(const emxArray_real_T *a, int arow, const
  emxArray_real_T *b, int brow);
static double skip_to_last_equal_value(int *k, const emxArray_real_T *x);

/* Function Definitions */
static unsigned char relop_rows(const emxArray_real_T *a, int arow, const
  emxArray_real_T *b, int brow)
{
  unsigned char p;
  int k;
  int exitg1;
  double b_a;
  double b_b;
  double absxk;
  int exponent;
  boolean_T b1;
  k = 0;
  do {
    exitg1 = 0;
    if (k < 2) {
      b_a = a->data[(arow + a->size[0] * k) - 1];
      b_b = b->data[(brow + b->size[0] * k) - 1];
      absxk = fabs(b_b / 2.0);
      if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
        if (absxk <= 2.2250738585072014E-308) {
          absxk = 4.94065645841247E-324;
        } else {
          frexp(absxk, &exponent);
          absxk = ldexp(1.0, exponent - 53);
        }
      } else {
        absxk = rtNaN;
      }

      if ((fabs(b_b - b_a) < absxk) || (rtIsInf(b_a) && rtIsInf(b_b) && ((b_a >
             0.0) == (b_b > 0.0)))) {
        k++;
      } else {
        b_a = a->data[(arow + a->size[0] * k) - 1];
        b_b = b->data[(brow + b->size[0] * k) - 1];
        if (rtIsNaN(b_b)) {
          b1 = !rtIsNaN(b_a);
        } else {
          b1 = ((!rtIsNaN(b_a)) && (b_a < b_b));
        }

        if (b1) {
          p = 1U;
        } else {
          p = 2U;
        }

        exitg1 = 1;
      }
    } else {
      p = 0U;
      exitg1 = 1;
    }
  } while (exitg1 == 0);

  return p;
}

static double skip_to_last_equal_value(int *k, const emxArray_real_T *x)
{
  double xk;
  boolean_T exitg1;
  double absxk;
  int exponent;
  xk = x->data[*k - 1];
  exitg1 = false;
  while ((!exitg1) && (*k < x->size[0])) {
    absxk = fabs(xk / 2.0);
    if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
      if (absxk <= 2.2250738585072014E-308) {
        absxk = 4.94065645841247E-324;
      } else {
        frexp(absxk, &exponent);
        absxk = ldexp(1.0, exponent - 53);
      }
    } else {
      absxk = rtNaN;
    }

    if ((fabs(xk - x->data[*k]) < absxk) || (rtIsInf(x->data[*k]) && rtIsInf(xk)
         && ((x->data[*k] > 0.0) == (xk > 0.0)))) {
      (*k)++;
    } else {
      exitg1 = true;
    }
  }

  return xk;
}

void b_do_vectors(const emxArray_real_T *a, const emxArray_real_T *b,
                  emxArray_real_T *c, emxArray_int32_T *ia, int ib_size[1])
{
  int na;
  unsigned int a_idx_0;
  int iafirst;
  int nc;
  int nia;
  int ialast;
  int iblast;
  int b_ialast;
  double ak;
  double bk;
  double absxk;
  int exponent;
  boolean_T b2;
  na = a->size[0];
  a_idx_0 = (unsigned int)a->size[0];
  iafirst = c->size[0];
  c->size[0] = (int)a_idx_0;
  emxEnsureCapacity_real_T(c, iafirst);
  iafirst = ia->size[0];
  ia->size[0] = a->size[0];
  emxEnsureCapacity_int32_T(ia, iafirst);
  ib_size[0] = 0;
  nc = 0;
  nia = 0;
  iafirst = 0;
  ialast = 1;
  iblast = 1;
  while ((ialast <= na) && (iblast <= b->size[0])) {
    b_ialast = ialast;
    ak = skip_to_last_equal_value(&b_ialast, a);
    ialast = b_ialast;
    bk = skip_to_last_equal_value(&iblast, b);
    absxk = fabs(bk / 2.0);
    if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
      if (absxk <= 2.2250738585072014E-308) {
        absxk = 4.94065645841247E-324;
      } else {
        frexp(absxk, &exponent);
        absxk = ldexp(1.0, exponent - 53);
      }
    } else {
      absxk = rtNaN;
    }

    if ((fabs(bk - ak) < absxk) || (rtIsInf(ak) && rtIsInf(bk) && ((ak > 0.0) ==
          (bk > 0.0)))) {
      ialast = b_ialast + 1;
      iafirst = b_ialast;
      iblast++;
    } else {
      if (rtIsNaN(bk)) {
        b2 = !rtIsNaN(ak);
      } else {
        b2 = ((!rtIsNaN(ak)) && (ak < bk));
      }

      if (b2) {
        nc++;
        nia++;
        c->data[nc - 1] = ak;
        ia->data[nia - 1] = iafirst + 1;
        ialast = b_ialast + 1;
        iafirst = b_ialast;
      } else {
        iblast++;
      }
    }
  }

  while (ialast <= na) {
    iafirst = ialast;
    ak = skip_to_last_equal_value(&iafirst, a);
    nc++;
    nia++;
    c->data[nc - 1] = ak;
    ia->data[nia - 1] = ialast;
    ialast = iafirst + 1;
  }

  if (a->size[0] > 0) {
    if (1 > nia) {
      ia->size[0] = 0;
    } else {
      iafirst = ia->size[0];
      ia->size[0] = nia;
      emxEnsureCapacity_int32_T(ia, iafirst);
    }

    if (1 > nc) {
      c->size[0] = 0;
    } else {
      iafirst = c->size[0];
      c->size[0] = nc;
      emxEnsureCapacity_real_T(c, iafirst);
    }
  }
}

void c_do_vectors(const emxArray_real_T *a, const emxArray_real_T *b,
                  emxArray_real_T *c, emxArray_int32_T *ia, emxArray_int32_T *ib)
{
  int iafirst;
  int ncmax;
  int nc;
  int ialast;
  int ibfirst;
  int iblast;
  int b_ialast;
  double ak;
  int b_iblast;
  double bk;
  double absxk;
  int exponent;
  boolean_T b3;
  iafirst = a->size[0];
  ncmax = b->size[0];
  if (iafirst < ncmax) {
    ncmax = iafirst;
  }

  iafirst = c->size[0];
  c->size[0] = ncmax;
  emxEnsureCapacity_real_T(c, iafirst);
  iafirst = ia->size[0];
  ia->size[0] = ncmax;
  emxEnsureCapacity_int32_T(ia, iafirst);
  iafirst = ib->size[0];
  ib->size[0] = ncmax;
  emxEnsureCapacity_int32_T(ib, iafirst);
  nc = 0;
  iafirst = 0;
  ialast = 1;
  ibfirst = 0;
  iblast = 1;
  while ((ialast <= a->size[0]) && (iblast <= b->size[0])) {
    b_ialast = ialast;
    ak = skip_to_last_equal_value(&b_ialast, a);
    ialast = b_ialast;
    b_iblast = iblast;
    bk = skip_to_last_equal_value(&b_iblast, b);
    iblast = b_iblast;
    absxk = fabs(bk / 2.0);
    if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
      if (absxk <= 2.2250738585072014E-308) {
        absxk = 4.94065645841247E-324;
      } else {
        frexp(absxk, &exponent);
        absxk = ldexp(1.0, exponent - 53);
      }
    } else {
      absxk = rtNaN;
    }

    if ((fabs(bk - ak) < absxk) || (rtIsInf(ak) && rtIsInf(bk) && ((ak > 0.0) ==
          (bk > 0.0)))) {
      nc++;
      c->data[nc - 1] = ak;
      ia->data[nc - 1] = iafirst + 1;
      ib->data[nc - 1] = ibfirst + 1;
      ialast = b_ialast + 1;
      iafirst = b_ialast;
      iblast = b_iblast + 1;
      ibfirst = b_iblast;
    } else {
      if (rtIsNaN(bk)) {
        b3 = !rtIsNaN(ak);
      } else {
        b3 = ((!rtIsNaN(ak)) && (ak < bk));
      }

      if (b3) {
        ialast = b_ialast + 1;
        iafirst = b_ialast;
      } else {
        iblast = b_iblast + 1;
        ibfirst = b_iblast;
      }
    }
  }

  if (ncmax > 0) {
    if (1 > nc) {
      ia->size[0] = 0;
      ib->size[0] = 0;
      c->size[0] = 0;
    } else {
      iafirst = ia->size[0];
      ia->size[0] = nc;
      emxEnsureCapacity_int32_T(ia, iafirst);
      iafirst = ib->size[0];
      ib->size[0] = nc;
      emxEnsureCapacity_int32_T(ib, iafirst);
      iafirst = c->size[0];
      c->size[0] = nc;
      emxEnsureCapacity_real_T(c, iafirst);
    }
  }
}

void do_rows(const emxArray_real_T *a, const double b_data[], const int b_size[2],
             emxArray_real_T *c, emxArray_int32_T *ia, int ib_size[1])
{
  int na;
  unsigned int a_idx_0;
  int ibfirst;
  int nc;
  int nia;
  int iafirst;
  int ialast;
  int iblast;
  int j;
  boolean_T exitg1;
  emxArray_real_T b_b_data;
  emxArray_real_T c_b_data;
  emxArray_real_T *b_c;
  unsigned char r;
  emxArray_real_T d_b_data;
  na = a->size[0];
  a_idx_0 = (unsigned int)a->size[0];
  ibfirst = c->size[0] * c->size[1];
  c->size[0] = (int)a_idx_0;
  c->size[1] = 2;
  emxEnsureCapacity_real_T(c, ibfirst);
  nc = -1;
  ibfirst = ia->size[0];
  ia->size[0] = a->size[0];
  emxEnsureCapacity_int32_T(ia, ibfirst);
  nia = 0;
  ib_size[0] = 0;
  iafirst = 1;
  ibfirst = 1;
  ialast = 1;
  iblast = 1;
  while ((iafirst <= na) && (ibfirst <= b_size[0])) {
    j = ialast;
    while ((ialast < a->size[0]) && (relop_rows(a, j, a, ialast + 1) == 0)) {
      ialast++;
    }

    j = iblast;
    exitg1 = false;
    while ((!exitg1) && (iblast < b_size[0])) {
      c_b_data.data = (double *)&b_data[0];
      c_b_data.size = (int *)&b_size[0];
      c_b_data.allocatedSize = -1;
      c_b_data.numDimensions = 2;
      c_b_data.canFreeData = false;
      d_b_data.data = (double *)&b_data[0];
      d_b_data.size = (int *)&b_size[0];
      d_b_data.allocatedSize = -1;
      d_b_data.numDimensions = 2;
      d_b_data.canFreeData = false;
      if (relop_rows(&c_b_data, j, &d_b_data, iblast + 1) == 0) {
        iblast++;
      } else {
        exitg1 = true;
      }
    }

    b_b_data.data = (double *)&b_data[0];
    b_b_data.size = (int *)&b_size[0];
    b_b_data.allocatedSize = -1;
    b_b_data.numDimensions = 2;
    b_b_data.canFreeData = false;
    r = relop_rows(a, iafirst, &b_b_data, ibfirst);
    if (r == 0) {
      ialast++;
      iafirst = ialast;
      iblast++;
      ibfirst = iblast;
    } else if (r == 1) {
      nc++;
      nia++;
      c->data[nc] = a->data[iafirst - 1];
      c->data[nc + c->size[0]] = a->data[(iafirst + a->size[0]) - 1];
      ia->data[nia - 1] = iafirst;
      ialast++;
      iafirst = ialast;
    } else {
      iblast++;
      ibfirst = iblast;
    }
  }

  while (ialast <= na) {
    iafirst = ialast;
    j = ialast;
    while ((ialast < a->size[0]) && (relop_rows(a, j, a, ialast + 1) == 0)) {
      ialast++;
    }

    nc++;
    nia++;
    c->data[nc] = a->data[iafirst - 1];
    c->data[nc + c->size[0]] = a->data[(iafirst + a->size[0]) - 1];
    ia->data[nia - 1] = iafirst;
    ialast++;
  }

  if (1 > nia) {
    ia->size[0] = 0;
  } else {
    ibfirst = ia->size[0];
    ia->size[0] = nia;
    emxEnsureCapacity_int32_T(ia, ibfirst);
  }

  if (1 > nc + 1) {
    iafirst = -1;
  } else {
    iafirst = nc;
  }

  emxInit_real_T(&b_c, 2);
  ibfirst = b_c->size[0] * b_c->size[1];
  b_c->size[0] = iafirst + 1;
  b_c->size[1] = 2;
  emxEnsureCapacity_real_T(b_c, ibfirst);
  for (ibfirst = 0; ibfirst <= iafirst; ibfirst++) {
    b_c->data[ibfirst] = c->data[ibfirst];
  }

  for (ibfirst = 0; ibfirst <= iafirst; ibfirst++) {
    b_c->data[ibfirst + b_c->size[0]] = c->data[ibfirst + c->size[0]];
  }

  ibfirst = c->size[0] * c->size[1];
  c->size[0] = b_c->size[0];
  c->size[1] = 2;
  emxEnsureCapacity_real_T(c, ibfirst);
  iafirst = b_c->size[0] * b_c->size[1];
  for (ibfirst = 0; ibfirst < iafirst; ibfirst++) {
    c->data[ibfirst] = b_c->data[ibfirst];
  }

  emxFree_real_T(&b_c);
}

void do_vectors(const double a_data[], const int a_size[1], double b, double
                c_data[], int c_size[1], int ia_data[], int ia_size[1], int
                ib_size[1])
{
  int na;
  signed char a_idx_0;
  int nc;
  int nia;
  int iafirst;
  int ialast;
  int iblast;
  int b_ialast;
  emxArray_real_T b_a_data;
  emxArray_real_T c_a_data;
  double ak;
  double absxk;
  int exponent;
  boolean_T b0;
  na = a_size[0];
  a_idx_0 = (signed char)a_size[0];
  c_size[0] = a_idx_0;
  ia_size[0] = a_size[0];
  ib_size[0] = 0;
  nc = 0;
  nia = 0;
  iafirst = 0;
  ialast = 1;
  iblast = 1;
  while ((ialast <= na) && (iblast <= 1)) {
    b_ialast = ialast;
    b_a_data.data = (double *)&a_data[0];
    b_a_data.size = (int *)&a_size[0];
    b_a_data.allocatedSize = -1;
    b_a_data.numDimensions = 1;
    b_a_data.canFreeData = false;
    ak = skip_to_last_equal_value(&b_ialast, &b_a_data);
    ialast = b_ialast;
    absxk = fabs(b / 2.0);
    if ((!rtIsInf(absxk)) && (!rtIsNaN(absxk))) {
      if (absxk <= 2.2250738585072014E-308) {
        absxk = 4.94065645841247E-324;
      } else {
        frexp(absxk, &exponent);
        absxk = ldexp(1.0, exponent - 53);
      }
    } else {
      absxk = rtNaN;
    }

    if ((fabs(b - ak) < absxk) || (rtIsInf(ak) && rtIsInf(b) && ((ak > 0.0) ==
          (b > 0.0)))) {
      ialast = b_ialast + 1;
      iafirst = b_ialast;
      iblast = 2;
    } else {
      if (rtIsNaN(b)) {
        b0 = !rtIsNaN(ak);
      } else {
        b0 = ((!rtIsNaN(ak)) && (ak < b));
      }

      if (b0) {
        nc++;
        nia++;
        c_data[nc - 1] = ak;
        ia_data[nia - 1] = iafirst + 1;
        ialast = b_ialast + 1;
        iafirst = b_ialast;
      } else {
        iblast = 2;
      }
    }
  }

  while (ialast <= na) {
    iafirst = ialast;
    c_a_data.data = (double *)&a_data[0];
    c_a_data.size = (int *)&a_size[0];
    c_a_data.allocatedSize = -1;
    c_a_data.numDimensions = 1;
    c_a_data.canFreeData = false;
    ak = skip_to_last_equal_value(&iafirst, &c_a_data);
    nc++;
    nia++;
    c_data[nc - 1] = ak;
    ia_data[nia - 1] = ialast;
    ialast = iafirst + 1;
  }

  if (a_size[0] > 0) {
    if (1 > nia) {
      ia_size[0] = 0;
    } else {
      ia_size[0] = nia;
    }

    if (1 > nc) {
      c_size[0] = 0;
    } else {
      c_size[0] = nc;
    }
  }
}

/* End of code generation (eml_setop.c) */
