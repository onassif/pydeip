/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_DEIPFunc3_api.c
 *
 * Code generation for function '_coder_DEIPFunc3_api'
 *
 */

/* Include files */
#include <string.h>
#include "tmwtypes.h"
#include "_coder_DEIPFunc3_api.h"
#include "_coder_DEIPFunc3_mex.h"

/* Variable Definitions */
emlrtCTX emlrtRootTLSGlobal = NULL;
emlrtContext emlrtContextGlobal = { true,/* bFirstTime */
  false,                               /* bInitialized */
  131482U,                             /* fVersionInfo */
  NULL,                                /* fErrorFunction */
  "DEIPFunc3",                         /* fFunctionName */
  NULL,                                /* fRTCallStack */
  false,                               /* bDebugMode */
  { 2045744189U, 2170104910U, 2743257031U, 4284093946U },/* fSigWrd */
  NULL                                 /* fSigMem */
};

/* Function Declarations */
static void b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y);
static void b_emlrt_marshallOut(const emxArray_real_T *u, const mxArray *y);
static void c_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *NodesOnElement, const char_T *identifier, emxArray_real_T *y);
static const mxArray *c_emlrt_marshallOut(const real_T u);
static void c_emxFreeStruct_coder_internal_(coder_internal_sparse *pStruct);
static void c_emxInitStruct_coder_internal_(const emlrtStack *sp,
  coder_internal_sparse *pStruct, boolean_T doPush);
static void d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y);
static const mxArray *d_emlrt_marshallOut(const emxArray_real_T *u);
static void e_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *RegionOnElement, const char_T *identifier, emxArray_real_T *y);
static const mxArray *e_emlrt_marshallOut(const emxArray_real_T *u);
static void emlrt_marshallIn(const emlrtStack *sp, const mxArray *InterTypes,
  const char_T *identifier, emxArray_real_T *y);
static void emlrt_marshallOut(const emxArray_real_T *u, const mxArray *y);
static void emxEnsureCapacity_real_T(emxArray_real_T *emxArray, int32_T oldNumel);
static void emxFree_int32_T(emxArray_int32_T **pEmxArray);
static void emxFree_real_T(emxArray_real_T **pEmxArray);
static void emxInit_int32_T(const emlrtStack *sp, emxArray_int32_T **pEmxArray,
  int32_T numDimensions, boolean_T doPush);
static void emxInit_real_T(const emlrtStack *sp, emxArray_real_T **pEmxArray,
  int32_T numDimensions, boolean_T doPush);
static void f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y);
static const mxArray *f_emlrt_marshallOut(const coder_internal_sparse u);
static void g_emlrt_marshallIn(const emlrtStack *sp, const mxArray *Coordinates,
  const char_T *identifier, emxArray_real_T *y);
static void h_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y);
static real_T i_emlrt_marshallIn(const emlrtStack *sp, const mxArray *numnp,
  const char_T *identifier);
static real_T j_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId);
static void k_emlrt_marshallIn(const emlrtStack *sp, const mxArray *MPCList,
  const char_T *identifier, emxArray_real_T *y);
static void l_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y);
static void m_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret);
static void n_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret);
static void o_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret);
static void p_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret);
static real_T q_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId);
static void r_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret);

/* Function Definitions */
static void b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y)
{
  m_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void b_emlrt_marshallOut(const emxArray_real_T *u, const mxArray *y)
{
  emlrtMxSetData((mxArray *)y, &u->data[0]);
  emlrtSetDimensions((mxArray *)y, u->size, 1);
}

static void c_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *NodesOnElement, const char_T *identifier, emxArray_real_T *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  d_emlrt_marshallIn(sp, emlrtAlias(NodesOnElement), &thisId, y);
  emlrtDestroyArray(&NodesOnElement);
}

static const mxArray *c_emlrt_marshallOut(const real_T u)
{
  const mxArray *y;
  const mxArray *m0;
  y = NULL;
  m0 = emlrtCreateDoubleScalar(u);
  emlrtAssign(&y, m0);
  return y;
}

static void c_emxFreeStruct_coder_internal_(coder_internal_sparse *pStruct)
{
  emxFree_real_T(&pStruct->d);
  emxFree_int32_T(&pStruct->colidx);
  emxFree_int32_T(&pStruct->rowidx);
}

static void c_emxInitStruct_coder_internal_(const emlrtStack *sp,
  coder_internal_sparse *pStruct, boolean_T doPush)
{
  emxInit_real_T(sp, &pStruct->d, 1, doPush);
  emxInit_int32_T(sp, &pStruct->colidx, 1, doPush);
  emxInit_int32_T(sp, &pStruct->rowidx, 1, doPush);
}

static void d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y)
{
  n_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static const mxArray *d_emlrt_marshallOut(const emxArray_real_T *u)
{
  const mxArray *y;
  const mxArray *m1;
  static const int32_T iv0[1] = { 0 };

  y = NULL;
  m1 = emlrtCreateNumericArray(1, iv0, mxDOUBLE_CLASS, mxREAL);
  emlrtMxSetData((mxArray *)m1, &u->data[0]);
  emlrtSetDimensions((mxArray *)m1, u->size, 1);
  emlrtAssign(&y, m1);
  return y;
}

static void e_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *RegionOnElement, const char_T *identifier, emxArray_real_T *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  f_emlrt_marshallIn(sp, emlrtAlias(RegionOnElement), &thisId, y);
  emlrtDestroyArray(&RegionOnElement);
}

static const mxArray *e_emlrt_marshallOut(const emxArray_real_T *u)
{
  const mxArray *y;
  const mxArray *m2;
  static const int32_T iv1[2] = { 0, 0 };

  y = NULL;
  m2 = emlrtCreateNumericArray(2, iv1, mxDOUBLE_CLASS, mxREAL);
  emlrtMxSetData((mxArray *)m2, &u->data[0]);
  emlrtSetDimensions((mxArray *)m2, u->size, 2);
  emlrtAssign(&y, m2);
  return y;
}

static void emlrt_marshallIn(const emlrtStack *sp, const mxArray *InterTypes,
  const char_T *identifier, emxArray_real_T *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  b_emlrt_marshallIn(sp, emlrtAlias(InterTypes), &thisId, y);
  emlrtDestroyArray(&InterTypes);
}

static void emlrt_marshallOut(const emxArray_real_T *u, const mxArray *y)
{
  emlrtMxSetData((mxArray *)y, &u->data[0]);
  emlrtSetDimensions((mxArray *)y, u->size, 2);
}

static void emxEnsureCapacity_real_T(emxArray_real_T *emxArray, int32_T oldNumel)
{
  int32_T newNumel;
  int32_T i;
  void *newData;
  if (oldNumel < 0) {
    oldNumel = 0;
  }

  newNumel = 1;
  for (i = 0; i < emxArray->numDimensions; i++) {
    newNumel *= emxArray->size[i];
  }

  if (newNumel > emxArray->allocatedSize) {
    i = emxArray->allocatedSize;
    if (i < 16) {
      i = 16;
    }

    while (i < newNumel) {
      if (i > 1073741823) {
        i = MAX_int32_T;
      } else {
        i <<= 1;
      }
    }

    newData = emlrtCallocMex((uint32_T)i, sizeof(real_T));
    if (emxArray->data != NULL) {
      memcpy(newData, emxArray->data, sizeof(real_T) * oldNumel);
      if (emxArray->canFreeData) {
        emlrtFreeMex(emxArray->data);
      }
    }

    emxArray->data = (real_T *)newData;
    emxArray->allocatedSize = i;
    emxArray->canFreeData = true;
  }
}

static void emxFree_int32_T(emxArray_int32_T **pEmxArray)
{
  if (*pEmxArray != (emxArray_int32_T *)NULL) {
    if (((*pEmxArray)->data != (int32_T *)NULL) && (*pEmxArray)->canFreeData) {
      emlrtFreeMex((*pEmxArray)->data);
    }

    emlrtFreeMex((*pEmxArray)->size);
    emlrtFreeMex(*pEmxArray);
    *pEmxArray = (emxArray_int32_T *)NULL;
  }
}

static void emxFree_real_T(emxArray_real_T **pEmxArray)
{
  if (*pEmxArray != (emxArray_real_T *)NULL) {
    if (((*pEmxArray)->data != (real_T *)NULL) && (*pEmxArray)->canFreeData) {
      emlrtFreeMex((*pEmxArray)->data);
    }

    emlrtFreeMex((*pEmxArray)->size);
    emlrtFreeMex(*pEmxArray);
    *pEmxArray = (emxArray_real_T *)NULL;
  }
}

static void emxInit_int32_T(const emlrtStack *sp, emxArray_int32_T **pEmxArray,
  int32_T numDimensions, boolean_T doPush)
{
  emxArray_int32_T *emxArray;
  int32_T i;
  *pEmxArray = (emxArray_int32_T *)emlrtMallocMex(sizeof(emxArray_int32_T));
  if (doPush) {
    emlrtPushHeapReferenceStackR2012b(sp, (void *)pEmxArray, (void (*)(void *))
      emxFree_int32_T);
  }

  emxArray = *pEmxArray;
  emxArray->data = (int32_T *)NULL;
  emxArray->numDimensions = numDimensions;
  emxArray->size = (int32_T *)emlrtMallocMex(sizeof(int32_T) * numDimensions);
  emxArray->allocatedSize = 0;
  emxArray->canFreeData = true;
  for (i = 0; i < numDimensions; i++) {
    emxArray->size[i] = 0;
  }
}

static void emxInit_real_T(const emlrtStack *sp, emxArray_real_T **pEmxArray,
  int32_T numDimensions, boolean_T doPush)
{
  emxArray_real_T *emxArray;
  int32_T i;
  *pEmxArray = (emxArray_real_T *)emlrtMallocMex(sizeof(emxArray_real_T));
  if (doPush) {
    emlrtPushHeapReferenceStackR2012b(sp, (void *)pEmxArray, (void (*)(void *))
      emxFree_real_T);
  }

  emxArray = *pEmxArray;
  emxArray->data = (real_T *)NULL;
  emxArray->numDimensions = numDimensions;
  emxArray->size = (int32_T *)emlrtMallocMex(sizeof(int32_T) * numDimensions);
  emxArray->allocatedSize = 0;
  emxArray->canFreeData = true;
  for (i = 0; i < numDimensions; i++) {
    emxArray->size[i] = 0;
  }
}

static void f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y)
{
  o_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static const mxArray *f_emlrt_marshallOut(const coder_internal_sparse u)
{
  const mxArray *y;
  y = NULL;
  emlrtAssign(&y, emlrtCreateSparse(&u.d->data[0], &u.colidx->data[0],
    &u.rowidx->data[0], u.m, u.n, u.maxnz, mxDOUBLE_CLASS, mxREAL));
  return y;
}

static void g_emlrt_marshallIn(const emlrtStack *sp, const mxArray *Coordinates,
  const char_T *identifier, emxArray_real_T *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  h_emlrt_marshallIn(sp, emlrtAlias(Coordinates), &thisId, y);
  emlrtDestroyArray(&Coordinates);
}

static void h_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y)
{
  p_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static real_T i_emlrt_marshallIn(const emlrtStack *sp, const mxArray *numnp,
  const char_T *identifier)
{
  real_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = j_emlrt_marshallIn(sp, emlrtAlias(numnp), &thisId);
  emlrtDestroyArray(&numnp);
  return y;
}

static real_T j_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  real_T y;
  y = q_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

static void k_emlrt_marshallIn(const emlrtStack *sp, const mxArray *MPCList,
  const char_T *identifier, emxArray_real_T *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  l_emlrt_marshallIn(sp, emlrtAlias(MPCList), &thisId, y);
  emlrtDestroyArray(&MPCList);
}

static void l_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y)
{
  r_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void m_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret)
{
  static const int32_T dims[2] = { -1, -1 };

  const boolean_T bv0[2] = { true, true };

  int32_T iv6[2];
  int32_T i0;
  emlrtCheckVsBuiltInR2012b(sp, msgId, src, "double", false, 2U, dims, &bv0[0],
    iv6);
  ret->allocatedSize = iv6[0] * iv6[1];
  i0 = ret->size[0] * ret->size[1];
  ret->size[0] = iv6[0];
  ret->size[1] = iv6[1];
  emxEnsureCapacity_real_T(ret, i0);
  ret->data = (real_T *)emlrtMxGetData(src);
  ret->canFreeData = false;
  emlrtDestroyArray(&src);
}

static void n_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret)
{
  static const int32_T dims[2] = { -1, 27 };

  const boolean_T bv1[2] = { true, true };

  int32_T iv7[2];
  int32_T i1;
  emlrtCheckVsBuiltInR2012b(sp, msgId, src, "double", false, 2U, dims, &bv1[0],
    iv7);
  ret->allocatedSize = iv7[0] * iv7[1];
  i1 = ret->size[0] * ret->size[1];
  ret->size[0] = iv7[0];
  ret->size[1] = iv7[1];
  emxEnsureCapacity_real_T(ret, i1);
  ret->data = (real_T *)emlrtMxGetData(src);
  ret->canFreeData = false;
  emlrtDestroyArray(&src);
}

static void o_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret)
{
  static const int32_T dims[1] = { -1 };

  const boolean_T bv2[1] = { true };

  int32_T iv8[1];
  int32_T i2;
  emlrtCheckVsBuiltInR2012b(sp, msgId, src, "double", false, 1U, dims, &bv2[0],
    iv8);
  ret->allocatedSize = iv8[0];
  i2 = ret->size[0];
  ret->size[0] = iv8[0];
  emxEnsureCapacity_real_T(ret, i2);
  ret->data = (real_T *)emlrtMxGetData(src);
  ret->canFreeData = false;
  emlrtDestroyArray(&src);
}

static void p_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret)
{
  static const int32_T dims[2] = { -1, 3 };

  const boolean_T bv3[2] = { true, false };

  int32_T iv9[2];
  int32_T i3;
  emlrtCheckVsBuiltInR2012b(sp, msgId, src, "double", false, 2U, dims, &bv3[0],
    iv9);
  ret->allocatedSize = iv9[0] * iv9[1];
  i3 = ret->size[0] * ret->size[1];
  ret->size[0] = iv9[0];
  ret->size[1] = iv9[1];
  emxEnsureCapacity_real_T(ret, i3);
  ret->data = (real_T *)emlrtMxGetData(src);
  ret->canFreeData = false;
  emlrtDestroyArray(&src);
}

static real_T q_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId)
{
  real_T ret;
  static const int32_T dims = 0;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 0U, &dims);
  ret = *(real_T *)emlrtMxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

static void r_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret)
{
  static const int32_T dims[2] = { -1, 5 };

  const boolean_T bv4[2] = { true, false };

  int32_T iv10[2];
  int32_T i4;
  emlrtCheckVsBuiltInR2012b(sp, msgId, src, "double", false, 2U, dims, &bv4[0],
    iv10);
  ret->allocatedSize = iv10[0] * iv10[1];
  i4 = ret->size[0] * ret->size[1];
  ret->size[0] = iv10[0];
  ret->size[1] = iv10[1];
  emxEnsureCapacity_real_T(ret, i4);
  ret->data = (real_T *)emlrtMxGetData(src);
  ret->canFreeData = false;
  emlrtDestroyArray(&src);
}

void DEIPFunc3_api(const mxArray *prhs[12], int32_T nlhs, const mxArray *plhs[41])
{
  emxArray_real_T *InterTypes;
  emxArray_real_T *NodesOnElement;
  emxArray_real_T *RegionOnElement;
  emxArray_real_T *Coordinates;
  emxArray_real_T *MPCList;
  emxArray_real_T *numEonF;
  emxArray_real_T *ElementsOnBoundary;
  emxArray_real_T *ElementsOnFacet;
  coder_internal_sparse ElementsOnNode;
  coder_internal_sparse ElementsOnNodeDup;
  emxArray_real_T *ElementsOnNodeNum;
  emxArray_real_T *ElementsOnNodeNum2;
  emxArray_real_T *FacetsOnElement;
  emxArray_real_T *FacetsOnElementInt;
  emxArray_real_T *FacetsOnInterface;
  emxArray_real_T *FacetsOnInterfaceNum;
  coder_internal_sparse FacetsOnNode;
  coder_internal_sparse FacetsOnNodeCut;
  coder_internal_sparse FacetsOnNodeInt;
  emxArray_real_T *FacetsOnNodeNum;
  emxArray_real_T *NodeCGDG;
  coder_internal_sparse NodeReg;
  emxArray_real_T *NodesOnElementCG;
  emxArray_real_T *NodesOnElementDG;
  emxArray_real_T *NodesOnInterface;
  emxArray_real_T *NodesOnPBC;
  emxArray_real_T *NodesOnPBCnum;
  emxArray_real_T *NodesOnLink;
  emxArray_real_T *NodesOnLinknum;
  emxArray_real_T *numEonPBC;
  emxArray_real_T *FacetsOnPBC;
  emxArray_real_T *FacetsOnPBCNum;
  emxArray_real_T *FacetsOnIntMinusPBC;
  emxArray_real_T *FacetsOnIntMinusPBCNum;
  const mxArray *prhs_copy_idx_0;
  const mxArray *prhs_copy_idx_1;
  const mxArray *prhs_copy_idx_2;
  const mxArray *prhs_copy_idx_3;
  const mxArray *prhs_copy_idx_4;
  const mxArray *prhs_copy_idx_5;
  const mxArray *prhs_copy_idx_6;
  const mxArray *prhs_copy_idx_7;
  const mxArray *prhs_copy_idx_8;
  const mxArray *prhs_copy_idx_9;
  const mxArray *prhs_copy_idx_10;
  const mxArray *prhs_copy_idx_11;
  real_T numnp;
  real_T numel;
  real_T nummat;
  real_T nen;
  real_T ndm;
  real_T usePBC;
  real_T numMPC;
  real_T numEonB;
  real_T numSI;
  real_T numfac;
  real_T numinttype;
  real_T NodesOnInterfaceNum;
  real_T numCL;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  emlrtHeapReferenceStackEnterFcnR2012b(&st);
  emxInit_real_T(&st, &InterTypes, 2, true);
  emxInit_real_T(&st, &NodesOnElement, 2, true);
  emxInit_real_T(&st, &RegionOnElement, 1, true);
  emxInit_real_T(&st, &Coordinates, 2, true);
  emxInit_real_T(&st, &MPCList, 2, true);
  emxInit_real_T(&st, &numEonF, 1, true);
  emxInit_real_T(&st, &ElementsOnBoundary, 2, true);
  emxInit_real_T(&st, &ElementsOnFacet, 2, true);
  c_emxInitStruct_coder_internal_(&st, &ElementsOnNode, true);
  c_emxInitStruct_coder_internal_(&st, &ElementsOnNodeDup, true);
  emxInit_real_T(&st, &ElementsOnNodeNum, 1, true);
  emxInit_real_T(&st, &ElementsOnNodeNum2, 1, true);
  emxInit_real_T(&st, &FacetsOnElement, 2, true);
  emxInit_real_T(&st, &FacetsOnElementInt, 2, true);
  emxInit_real_T(&st, &FacetsOnInterface, 1, true);
  emxInit_real_T(&st, &FacetsOnInterfaceNum, 1, true);
  c_emxInitStruct_coder_internal_(&st, &FacetsOnNode, true);
  c_emxInitStruct_coder_internal_(&st, &FacetsOnNodeCut, true);
  c_emxInitStruct_coder_internal_(&st, &FacetsOnNodeInt, true);
  emxInit_real_T(&st, &FacetsOnNodeNum, 1, true);
  emxInit_real_T(&st, &NodeCGDG, 2, true);
  c_emxInitStruct_coder_internal_(&st, &NodeReg, true);
  emxInit_real_T(&st, &NodesOnElementCG, 2, true);
  emxInit_real_T(&st, &NodesOnElementDG, 2, true);
  emxInit_real_T(&st, &NodesOnInterface, 1, true);
  emxInit_real_T(&st, &NodesOnPBC, 2, true);
  emxInit_real_T(&st, &NodesOnPBCnum, 1, true);
  emxInit_real_T(&st, &NodesOnLink, 2, true);
  emxInit_real_T(&st, &NodesOnLinknum, 1, true);
  emxInit_real_T(&st, &numEonPBC, 1, true);
  emxInit_real_T(&st, &FacetsOnPBC, 1, true);
  emxInit_real_T(&st, &FacetsOnPBCNum, 1, true);
  emxInit_real_T(&st, &FacetsOnIntMinusPBC, 1, true);
  emxInit_real_T(&st, &FacetsOnIntMinusPBCNum, 1, true);
  prhs_copy_idx_0 = emlrtProtectR2012b(prhs[0], 0, false, -1);
  prhs_copy_idx_1 = emlrtProtectR2012b(prhs[1], 1, true, -1);
  prhs_copy_idx_2 = emlrtProtectR2012b(prhs[2], 2, true, -1);
  prhs_copy_idx_3 = emlrtProtectR2012b(prhs[3], 3, true, -1);
  prhs_copy_idx_4 = prhs[4];
  prhs_copy_idx_5 = prhs[5];
  prhs_copy_idx_6 = prhs[6];
  prhs_copy_idx_7 = prhs[7];
  prhs_copy_idx_8 = prhs[8];
  prhs_copy_idx_9 = prhs[9];
  prhs_copy_idx_10 = prhs[10];
  prhs_copy_idx_11 = emlrtProtectR2012b(prhs[11], 11, true, -1);

  /* Marshall function inputs */
  InterTypes->canFreeData = false;
  emlrt_marshallIn(&st, emlrtAlias(prhs_copy_idx_0), "InterTypes", InterTypes);
  NodesOnElement->canFreeData = false;
  c_emlrt_marshallIn(&st, emlrtAlias(prhs_copy_idx_1), "NodesOnElement",
                     NodesOnElement);
  RegionOnElement->canFreeData = false;
  e_emlrt_marshallIn(&st, emlrtAlias(prhs_copy_idx_2), "RegionOnElement",
                     RegionOnElement);
  Coordinates->canFreeData = false;
  g_emlrt_marshallIn(&st, emlrtAlias(prhs_copy_idx_3), "Coordinates",
                     Coordinates);
  numnp = i_emlrt_marshallIn(&st, emlrtAliasP(prhs_copy_idx_4), "numnp");
  numel = i_emlrt_marshallIn(&st, emlrtAliasP(prhs_copy_idx_5), "numel");
  nummat = i_emlrt_marshallIn(&st, emlrtAliasP(prhs_copy_idx_6), "nummat");
  nen = i_emlrt_marshallIn(&st, emlrtAliasP(prhs_copy_idx_7), "nen");
  ndm = i_emlrt_marshallIn(&st, emlrtAliasP(prhs_copy_idx_8), "ndm");
  usePBC = i_emlrt_marshallIn(&st, emlrtAliasP(prhs_copy_idx_9), "usePBC");
  numMPC = i_emlrt_marshallIn(&st, emlrtAliasP(prhs_copy_idx_10), "numMPC");
  MPCList->canFreeData = false;
  k_emlrt_marshallIn(&st, emlrtAlias(prhs_copy_idx_11), "MPCList", MPCList);

  /* Invoke the target function */
  DEIPFunc3(InterTypes, NodesOnElement, RegionOnElement, Coordinates, &numnp,
            numel, nummat, nen, ndm, usePBC, &numMPC, MPCList, &numEonB, numEonF,
            ElementsOnBoundary, &numSI, ElementsOnFacet, &ElementsOnNode,
            &ElementsOnNodeDup, ElementsOnNodeNum, &numfac, ElementsOnNodeNum2,
            &numinttype, FacetsOnElement, FacetsOnElementInt, FacetsOnInterface,
            FacetsOnInterfaceNum, &FacetsOnNode, &FacetsOnNodeCut,
            &FacetsOnNodeInt, FacetsOnNodeNum, NodeCGDG, &NodeReg,
            NodesOnElementCG, NodesOnElementDG, NodesOnInterface,
            &NodesOnInterfaceNum, &numCL, NodesOnPBC, NodesOnPBCnum, NodesOnLink,
            NodesOnLinknum, numEonPBC, FacetsOnPBC, FacetsOnPBCNum,
            FacetsOnIntMinusPBC, FacetsOnIntMinusPBCNum);

  /* Marshall function outputs */
  NodesOnElement->canFreeData = false;
  emlrt_marshallOut(NodesOnElement, prhs_copy_idx_1);
  plhs[0] = prhs_copy_idx_1;
  emxFree_real_T(&NodesOnElement);
  emxFree_real_T(&InterTypes);
  if (nlhs > 1) {
    RegionOnElement->canFreeData = false;
    b_emlrt_marshallOut(RegionOnElement, prhs_copy_idx_2);
    plhs[1] = prhs_copy_idx_2;
  }

  emxFree_real_T(&RegionOnElement);
  if (nlhs > 2) {
    Coordinates->canFreeData = false;
    emlrt_marshallOut(Coordinates, prhs_copy_idx_3);
    plhs[2] = prhs_copy_idx_3;
  }

  emxFree_real_T(&Coordinates);
  if (nlhs > 3) {
    plhs[3] = c_emlrt_marshallOut(numnp);
  }

  if (nlhs > 4) {
    MPCList->canFreeData = false;
    emlrt_marshallOut(MPCList, prhs_copy_idx_11);
    plhs[4] = prhs_copy_idx_11;
  }

  emxFree_real_T(&MPCList);
  if (nlhs > 5) {
    plhs[5] = c_emlrt_marshallOut(numMPC);
  }

  if (nlhs > 6) {
    plhs[6] = c_emlrt_marshallOut(numEonB);
  }

  if (nlhs > 7) {
    numEonF->canFreeData = false;
    plhs[7] = d_emlrt_marshallOut(numEonF);
  }

  emxFree_real_T(&numEonF);
  if (nlhs > 8) {
    ElementsOnBoundary->canFreeData = false;
    plhs[8] = e_emlrt_marshallOut(ElementsOnBoundary);
  }

  emxFree_real_T(&ElementsOnBoundary);
  if (nlhs > 9) {
    plhs[9] = c_emlrt_marshallOut(numSI);
  }

  if (nlhs > 10) {
    ElementsOnFacet->canFreeData = false;
    plhs[10] = e_emlrt_marshallOut(ElementsOnFacet);
  }

  emxFree_real_T(&ElementsOnFacet);
  if (nlhs > 11) {
    plhs[11] = f_emlrt_marshallOut(ElementsOnNode);
  }

  c_emxFreeStruct_coder_internal_(&ElementsOnNode);
  if (nlhs > 12) {
    plhs[12] = f_emlrt_marshallOut(ElementsOnNodeDup);
  }

  c_emxFreeStruct_coder_internal_(&ElementsOnNodeDup);
  if (nlhs > 13) {
    ElementsOnNodeNum->canFreeData = false;
    plhs[13] = d_emlrt_marshallOut(ElementsOnNodeNum);
  }

  emxFree_real_T(&ElementsOnNodeNum);
  if (nlhs > 14) {
    plhs[14] = c_emlrt_marshallOut(numfac);
  }

  if (nlhs > 15) {
    ElementsOnNodeNum2->canFreeData = false;
    plhs[15] = d_emlrt_marshallOut(ElementsOnNodeNum2);
  }

  emxFree_real_T(&ElementsOnNodeNum2);
  if (nlhs > 16) {
    plhs[16] = c_emlrt_marshallOut(numinttype);
  }

  if (nlhs > 17) {
    FacetsOnElement->canFreeData = false;
    plhs[17] = e_emlrt_marshallOut(FacetsOnElement);
  }

  emxFree_real_T(&FacetsOnElement);
  if (nlhs > 18) {
    FacetsOnElementInt->canFreeData = false;
    plhs[18] = e_emlrt_marshallOut(FacetsOnElementInt);
  }

  emxFree_real_T(&FacetsOnElementInt);
  if (nlhs > 19) {
    FacetsOnInterface->canFreeData = false;
    plhs[19] = d_emlrt_marshallOut(FacetsOnInterface);
  }

  emxFree_real_T(&FacetsOnInterface);
  if (nlhs > 20) {
    FacetsOnInterfaceNum->canFreeData = false;
    plhs[20] = d_emlrt_marshallOut(FacetsOnInterfaceNum);
  }

  emxFree_real_T(&FacetsOnInterfaceNum);
  if (nlhs > 21) {
    plhs[21] = f_emlrt_marshallOut(FacetsOnNode);
  }

  c_emxFreeStruct_coder_internal_(&FacetsOnNode);
  if (nlhs > 22) {
    plhs[22] = f_emlrt_marshallOut(FacetsOnNodeCut);
  }

  c_emxFreeStruct_coder_internal_(&FacetsOnNodeCut);
  if (nlhs > 23) {
    plhs[23] = f_emlrt_marshallOut(FacetsOnNodeInt);
  }

  c_emxFreeStruct_coder_internal_(&FacetsOnNodeInt);
  if (nlhs > 24) {
    FacetsOnNodeNum->canFreeData = false;
    plhs[24] = d_emlrt_marshallOut(FacetsOnNodeNum);
  }

  emxFree_real_T(&FacetsOnNodeNum);
  if (nlhs > 25) {
    NodeCGDG->canFreeData = false;
    plhs[25] = e_emlrt_marshallOut(NodeCGDG);
  }

  emxFree_real_T(&NodeCGDG);
  if (nlhs > 26) {
    plhs[26] = f_emlrt_marshallOut(NodeReg);
  }

  c_emxFreeStruct_coder_internal_(&NodeReg);
  if (nlhs > 27) {
    NodesOnElementCG->canFreeData = false;
    plhs[27] = e_emlrt_marshallOut(NodesOnElementCG);
  }

  emxFree_real_T(&NodesOnElementCG);
  if (nlhs > 28) {
    NodesOnElementDG->canFreeData = false;
    plhs[28] = e_emlrt_marshallOut(NodesOnElementDG);
  }

  emxFree_real_T(&NodesOnElementDG);
  if (nlhs > 29) {
    NodesOnInterface->canFreeData = false;
    plhs[29] = d_emlrt_marshallOut(NodesOnInterface);
  }

  emxFree_real_T(&NodesOnInterface);
  if (nlhs > 30) {
    plhs[30] = c_emlrt_marshallOut(NodesOnInterfaceNum);
  }

  if (nlhs > 31) {
    plhs[31] = c_emlrt_marshallOut(numCL);
  }

  if (nlhs > 32) {
    NodesOnPBC->canFreeData = false;
    plhs[32] = e_emlrt_marshallOut(NodesOnPBC);
  }

  emxFree_real_T(&NodesOnPBC);
  if (nlhs > 33) {
    NodesOnPBCnum->canFreeData = false;
    plhs[33] = d_emlrt_marshallOut(NodesOnPBCnum);
  }

  emxFree_real_T(&NodesOnPBCnum);
  if (nlhs > 34) {
    NodesOnLink->canFreeData = false;
    plhs[34] = e_emlrt_marshallOut(NodesOnLink);
  }

  emxFree_real_T(&NodesOnLink);
  if (nlhs > 35) {
    NodesOnLinknum->canFreeData = false;
    plhs[35] = d_emlrt_marshallOut(NodesOnLinknum);
  }

  emxFree_real_T(&NodesOnLinknum);
  if (nlhs > 36) {
    numEonPBC->canFreeData = false;
    plhs[36] = d_emlrt_marshallOut(numEonPBC);
  }

  emxFree_real_T(&numEonPBC);
  if (nlhs > 37) {
    FacetsOnPBC->canFreeData = false;
    plhs[37] = d_emlrt_marshallOut(FacetsOnPBC);
  }

  emxFree_real_T(&FacetsOnPBC);
  if (nlhs > 38) {
    FacetsOnPBCNum->canFreeData = false;
    plhs[38] = d_emlrt_marshallOut(FacetsOnPBCNum);
  }

  emxFree_real_T(&FacetsOnPBCNum);
  if (nlhs > 39) {
    FacetsOnIntMinusPBC->canFreeData = false;
    plhs[39] = d_emlrt_marshallOut(FacetsOnIntMinusPBC);
  }

  emxFree_real_T(&FacetsOnIntMinusPBC);
  if (nlhs > 40) {
    FacetsOnIntMinusPBCNum->canFreeData = false;
    plhs[40] = d_emlrt_marshallOut(FacetsOnIntMinusPBCNum);
  }

  emxFree_real_T(&FacetsOnIntMinusPBCNum);
  emlrtHeapReferenceStackLeaveFcnR2012b(&st);
}

void DEIPFunc3_atexit(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  st.tls = emlrtRootTLSGlobal;
  emlrtEnterRtStackR2012b(&st);
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
  DEIPFunc3_xil_terminate();
  DEIPFunc3_xil_shutdown();
  emlrtExitTimeCleanup(&emlrtContextGlobal);
}

void DEIPFunc3_initialize(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  st.tls = emlrtRootTLSGlobal;
  emlrtClearAllocCountR2012b(&st, false, 0U, 0);
  emlrtEnterRtStackR2012b(&st);
  emlrtFirstTimeR2012b(emlrtRootTLSGlobal);
}

void DEIPFunc3_terminate(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
}

/* End of code generation (_coder_DEIPFunc3_api.c) */
