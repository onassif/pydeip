/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_DEIPFunc3_mex.c
 *
 * Code generation for function '_coder_DEIPFunc3_mex'
 *
 */

/* Include files */
#include "_coder_DEIPFunc3_api.h"
#include "_coder_DEIPFunc3_mex.h"

/* Function Declarations */
static void DEIPFunc3_mexFunction(int32_T nlhs, mxArray *plhs[41], int32_T nrhs,
  const mxArray *prhs[12]);

/* Function Definitions */
static void DEIPFunc3_mexFunction(int32_T nlhs, mxArray *plhs[41], int32_T nrhs,
  const mxArray *prhs[12])
{
  const mxArray *outputs[41];
  int32_T b_nlhs;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;

  /* Check for proper number of arguments. */
  if (nrhs != 12) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:WrongNumberOfInputs", 5, 12, 12, 4,
                        9, "DEIPFunc3");
  }

  if (nlhs > 41) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:TooManyOutputArguments", 3, 4, 9,
                        "DEIPFunc3");
  }

  /* Call the function. */
  DEIPFunc3_api(prhs, nlhs, outputs);

  /* Copy over outputs to the caller. */
  if (nlhs < 1) {
    b_nlhs = 1;
  } else {
    b_nlhs = nlhs;
  }

  emlrtReturnArrays(b_nlhs, plhs, outputs);
}

void mexFunction(int32_T nlhs, mxArray *plhs[], int32_T nrhs, const mxArray
                 *prhs[])
{
  mexAtExit(DEIPFunc3_atexit);

  /* Module initialization. */
  DEIPFunc3_initialize();

  /* Dispatch the entry-point. */
  DEIPFunc3_mexFunction(nlhs, plhs, nrhs, prhs);

  /* Module termination. */
  DEIPFunc3_terminate();
}

emlrtCTX mexFunctionCreateRootTLS(void)
{
  emlrtCreateRootTLS(&emlrtRootTLSGlobal, &emlrtContextGlobal, NULL, 1);
  return emlrtRootTLSGlobal;
}

/* End of code generation (_coder_DEIPFunc3_mex.c) */
