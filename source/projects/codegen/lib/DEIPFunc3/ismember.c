/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * ismember.c
 *
 * Code generation for function 'ismember'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "DEIPFunc3.h"
#include "ismember.h"

/* Function Definitions */
int bsearchni(int k, const emxArray_real_T *x, const emxArray_real_T *s)
{
  int idx;
  double b_x;
  int ihi;
  int ilo;
  boolean_T exitg1;
  int imid;
  boolean_T p;
  b_x = x->data[k - 1];
  ihi = s->size[0];
  idx = 0;
  ilo = 1;
  exitg1 = false;
  while ((!exitg1) && (ihi >= ilo)) {
    imid = ((ilo >> 1) + (ihi >> 1)) - 1;
    if (((ilo & 1) == 1) && ((ihi & 1) == 1)) {
      imid++;
    }

    if (b_x == s->data[imid]) {
      idx = imid + 1;
      exitg1 = true;
    } else {
      if (rtIsNaN(s->data[imid])) {
        p = !rtIsNaN(b_x);
      } else {
        p = ((!rtIsNaN(b_x)) && (b_x < s->data[imid]));
      }

      if (p) {
        ihi = imid;
      } else {
        ilo = imid + 2;
      }
    }
  }

  if (idx > 0) {
    idx--;
    while ((idx > 0) && (b_x == s->data[idx - 1])) {
      idx--;
    }

    idx++;
  }

  return idx;
}

/* End of code generation (ismember.c) */
