/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * issorted.c
 *
 * Code generation for function 'issorted'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "DEIPFunc3.h"
#include "issorted.h"

/* Function Definitions */
boolean_T issorted(const emxArray_real_T *x)
{
  boolean_T y;
  int dim;
  int n;
  int k;
  boolean_T exitg1;
  int b_n;
  boolean_T exitg2;
  int subs[2];
  double v_idx_0;
  double v_idx_1;
  y = true;
  dim = 2;
  if (x->size[0] != 1) {
    dim = 1;
  }

  if (dim <= 1) {
    n = x->size[0];
  } else {
    n = 1;
  }

  if ((x->size[0] != 0) && (n != 1)) {
    if (dim == 2) {
      n = -1;
    } else {
      n = 0;
    }

    k = 0;
    exitg1 = false;
    while ((!exitg1) && (k <= n)) {
      if (dim == 1) {
        b_n = x->size[0] - 1;
      } else {
        b_n = x->size[0];
      }

      k = 0;
      exitg2 = false;
      while ((!exitg2) && (k <= b_n - 1)) {
        subs[0] = k + 1;
        subs[1] = 1;
        subs[dim - 1]++;
        v_idx_0 = x->data[k];
        v_idx_1 = x->data[subs[0] - 1];
        if ((v_idx_0 <= v_idx_1) || rtIsNaN(v_idx_1)) {
        } else {
          y = false;
        }

        if (!y) {
          exitg2 = true;
        } else {
          k++;
        }
      }

      if (!y) {
        exitg1 = true;
      } else {
        k = 1;
      }
    }
  }

  return y;
}

/* End of code generation (issorted.c) */
