/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * issorted.h
 *
 * Code generation for function 'issorted'
 *
 */

#ifndef ISSORTED_H
#define ISSORTED_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "DEIPFunc3_types.h"

/* Function Declarations */
extern boolean_T issorted(const emxArray_real_T *x);

#endif

/* End of code generation (issorted.h) */
