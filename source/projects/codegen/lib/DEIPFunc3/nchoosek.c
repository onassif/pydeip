/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * nchoosek.c
 *
 * Code generation for function 'nchoosek'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "DEIPFunc3.h"
#include "nchoosek.h"
#include "DEIPFunc3_emxutil.h"
#include "DEIPFunc3_rtwutil.h"

/* Function Definitions */
void nchoosek(const double x_data[], const int x_size[2], emxArray_real_T *y)
{
  int icomb;
  double b_y;
  int nmk;
  int j;
  int nrows;
  int comb[2];
  int row;
  int combj;
  icomb = 1;
  if (2.0 > (double)x_size[1] / 2.0) {
    icomb = x_size[1] - 3;
  }

  b_y = 1.0;
  nmk = x_size[1] - icomb;
  for (j = 0; j <= icomb; j++) {
    b_y *= (double)(j + nmk) / (1.0 + (double)j);
  }

  b_y = rt_roundd_snf(b_y);
  nrows = (int)b_y;
  icomb = y->size[0] * y->size[1];
  y->size[0] = nrows;
  y->size[1] = 2;
  emxEnsureCapacity_real_T(y, icomb);
  comb[0] = 1;
  comb[1] = 2;
  icomb = 1;
  nmk = x_size[1];
  for (row = 0; row < nrows; row++) {
    y->data[row] = x_data[comb[0] - 1];
    y->data[row + y->size[0]] = x_data[comb[1] - 1];
    if (icomb + 1 > 0) {
      j = comb[icomb];
      combj = comb[icomb] + 1;
      comb[icomb]++;
      if (j + 1 < nmk) {
        icomb += 2;
        for (j = icomb; j < 3; j++) {
          combj++;
          comb[1] = combj;
        }

        icomb = 1;
        nmk = x_size[1];
      } else {
        icomb--;
        nmk--;
      }
    }
  }
}

/* End of code generation (nchoosek.c) */
