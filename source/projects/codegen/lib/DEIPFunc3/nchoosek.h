/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * nchoosek.h
 *
 * Code generation for function 'nchoosek'
 *
 */

#ifndef NCHOOSEK_H
#define NCHOOSEK_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "DEIPFunc3_types.h"

/* Function Declarations */
extern void nchoosek(const double x_data[], const int x_size[2], emxArray_real_T
                     *y);

#endif

/* End of code generation (nchoosek.h) */
