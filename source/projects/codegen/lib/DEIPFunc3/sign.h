/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sign.h
 *
 * Code generation for function 'sign'
 *
 */

#ifndef SIGN_H
#define SIGN_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "DEIPFunc3_types.h"

/* Function Declarations */
extern void b_sign(coder_internal_sparse_2 *x);

#endif

/* End of code generation (sign.h) */
