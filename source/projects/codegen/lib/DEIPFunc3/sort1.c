/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sort1.c
 *
 * Code generation for function 'sort1'
 *
 */

/* Include files */
#include <string.h>
#include "rt_nonfinite.h"
#include "DEIPFunc3.h"
#include "sort1.h"
#include "DEIPFunc3_emxutil.h"
#include "sortIdx.h"

/* Function Definitions */
void b_sort(double x_data[], int x_size[2])
{
  int i1;
  int idx_data[101];
  int n;
  double x4[4];
  signed char idx4[4];
  double xwork_data[101];
  int nNaNs;
  int ib;
  int k;
  int i4;
  signed char perm[4];
  int quartetOffset;
  int i3;
  int idx_data_tmp;
  int iwork_data[101];
  double d3;
  double d4;
  i1 = x_size[1];
  if (0 <= i1 - 1) {
    memset(&idx_data[0], 0, (unsigned int)(i1 * (int)sizeof(int)));
  }

  n = x_size[1];
  x4[0] = 0.0;
  idx4[0] = 0;
  x4[1] = 0.0;
  idx4[1] = 0;
  x4[2] = 0.0;
  idx4[2] = 0;
  x4[3] = 0.0;
  idx4[3] = 0;
  i1 = (signed char)x_size[1];
  if (0 <= i1 - 1) {
    memset(&xwork_data[0], 0, (unsigned int)(i1 * (int)sizeof(double)));
  }

  nNaNs = 0;
  ib = -1;
  for (k = 0; k < n; k++) {
    if (rtIsNaN(x_data[k])) {
      idx_data[(n - nNaNs) - 1] = k + 1;
      xwork_data[(n - nNaNs) - 1] = x_data[k];
      nNaNs++;
    } else {
      ib++;
      idx4[ib] = (signed char)(k + 1);
      x4[ib] = x_data[k];
      if (ib + 1 == 4) {
        quartetOffset = k - nNaNs;
        if (x4[0] <= x4[1]) {
          i1 = 1;
          ib = 2;
        } else {
          i1 = 2;
          ib = 1;
        }

        if (x4[2] <= x4[3]) {
          i3 = 3;
          i4 = 4;
        } else {
          i3 = 4;
          i4 = 3;
        }

        d3 = x4[i1 - 1];
        d4 = x4[i3 - 1];
        if (d3 <= d4) {
          d3 = x4[ib - 1];
          if (d3 <= d4) {
            perm[0] = (signed char)i1;
            perm[1] = (signed char)ib;
            perm[2] = (signed char)i3;
            perm[3] = (signed char)i4;
          } else if (d3 <= x4[i4 - 1]) {
            perm[0] = (signed char)i1;
            perm[1] = (signed char)i3;
            perm[2] = (signed char)ib;
            perm[3] = (signed char)i4;
          } else {
            perm[0] = (signed char)i1;
            perm[1] = (signed char)i3;
            perm[2] = (signed char)i4;
            perm[3] = (signed char)ib;
          }
        } else {
          d4 = x4[i4 - 1];
          if (d3 <= d4) {
            if (x4[ib - 1] <= d4) {
              perm[0] = (signed char)i3;
              perm[1] = (signed char)i1;
              perm[2] = (signed char)ib;
              perm[3] = (signed char)i4;
            } else {
              perm[0] = (signed char)i3;
              perm[1] = (signed char)i1;
              perm[2] = (signed char)i4;
              perm[3] = (signed char)ib;
            }
          } else {
            perm[0] = (signed char)i3;
            perm[1] = (signed char)i4;
            perm[2] = (signed char)i1;
            perm[3] = (signed char)ib;
          }
        }

        idx_data_tmp = perm[0] - 1;
        idx_data[quartetOffset - 3] = idx4[idx_data_tmp];
        ib = perm[1] - 1;
        idx_data[quartetOffset - 2] = idx4[ib];
        i1 = perm[2] - 1;
        idx_data[quartetOffset - 1] = idx4[i1];
        i3 = perm[3] - 1;
        idx_data[quartetOffset] = idx4[i3];
        x_data[quartetOffset - 3] = x4[idx_data_tmp];
        x_data[quartetOffset - 2] = x4[ib];
        x_data[quartetOffset - 1] = x4[i1];
        x_data[quartetOffset] = x4[i3];
        ib = -1;
      }
    }
  }

  i4 = (n - nNaNs) - 1;
  if (ib + 1 > 0) {
    perm[1] = 0;
    perm[2] = 0;
    perm[3] = 0;
    if (ib + 1 == 1) {
      perm[0] = 1;
    } else if (ib + 1 == 2) {
      if (x4[0] <= x4[1]) {
        perm[0] = 1;
        perm[1] = 2;
      } else {
        perm[0] = 2;
        perm[1] = 1;
      }
    } else if (x4[0] <= x4[1]) {
      if (x4[1] <= x4[2]) {
        perm[0] = 1;
        perm[1] = 2;
        perm[2] = 3;
      } else if (x4[0] <= x4[2]) {
        perm[0] = 1;
        perm[1] = 3;
        perm[2] = 2;
      } else {
        perm[0] = 3;
        perm[1] = 1;
        perm[2] = 2;
      }
    } else if (x4[0] <= x4[2]) {
      perm[0] = 2;
      perm[1] = 1;
      perm[2] = 3;
    } else if (x4[1] <= x4[2]) {
      perm[0] = 2;
      perm[1] = 3;
      perm[2] = 1;
    } else {
      perm[0] = 3;
      perm[1] = 2;
      perm[2] = 1;
    }

    for (k = 0; k <= ib; k++) {
      idx_data_tmp = (i4 - ib) + k;
      idx_data[idx_data_tmp] = idx4[perm[k] - 1];
      x_data[idx_data_tmp] = x4[perm[k] - 1];
    }
  }

  ib = (nNaNs >> 1) + 1;
  for (k = 0; k <= ib - 2; k++) {
    i1 = (i4 + k) + 1;
    i3 = (signed char)idx_data[i1];
    idx_data_tmp = (n - k) - 1;
    idx_data[i1] = (signed char)idx_data[idx_data_tmp];
    idx_data[idx_data_tmp] = i3;
    x_data[i1] = xwork_data[idx_data_tmp];
    x_data[idx_data_tmp] = xwork_data[i1];
  }

  if ((nNaNs & 1) != 0) {
    ib += i4;
    x_data[ib] = xwork_data[ib];
  }

  ib = x_size[1] - nNaNs;
  if (ib > 1) {
    i1 = (signed char)x_size[1];
    if (0 <= i1 - 1) {
      memset(&iwork_data[0], 0, (unsigned int)(i1 * (int)sizeof(int)));
    }

    b_merge_block(idx_data, x_data, ib, iwork_data, xwork_data);
  }
}

void c_sort(emxArray_real_T *x)
{
  int dim;
  int j;
  emxArray_real_T *vwork;
  int vlen;
  int vstride;
  int k;
  emxArray_int32_T *b_vwork;
  dim = 0;
  if (x->size[0] != 1) {
    dim = -1;
  }

  if (dim + 2 <= 1) {
    j = x->size[0];
  } else {
    j = 1;
  }

  emxInit_real_T(&vwork, 1);
  vlen = j - 1;
  vstride = vwork->size[0];
  vwork->size[0] = j;
  emxEnsureCapacity_real_T(vwork, vstride);
  vstride = 1;
  for (k = 0; k <= dim; k++) {
    vstride *= x->size[0];
  }

  emxInit_int32_T(&b_vwork, 1);
  for (j = 0; j < vstride; j++) {
    for (k = 0; k <= vlen; k++) {
      vwork->data[k] = x->data[j + k * vstride];
    }

    b_sortIdx(vwork, b_vwork);
    for (k = 0; k <= vlen; k++) {
      x->data[j + k * vstride] = vwork->data[k];
    }
  }

  emxFree_int32_T(&b_vwork);
  emxFree_real_T(&vwork);
}

void sort(emxArray_real_T *x, emxArray_int32_T *idx)
{
  int dim;
  int i29;
  emxArray_real_T *vwork;
  int vlen;
  int x_idx_0;
  int vstride;
  int k;
  emxArray_int32_T *iidx;
  dim = 0;
  if (x->size[0] != 1) {
    dim = -1;
  }

  if (dim + 2 <= 1) {
    i29 = x->size[0];
  } else {
    i29 = 1;
  }

  emxInit_real_T(&vwork, 1);
  vlen = i29 - 1;
  x_idx_0 = vwork->size[0];
  vwork->size[0] = i29;
  emxEnsureCapacity_real_T(vwork, x_idx_0);
  x_idx_0 = x->size[0];
  i29 = idx->size[0];
  idx->size[0] = x_idx_0;
  emxEnsureCapacity_int32_T(idx, i29);
  vstride = 1;
  for (k = 0; k <= dim; k++) {
    vstride *= x->size[0];
  }

  emxInit_int32_T(&iidx, 1);
  for (x_idx_0 = 0; x_idx_0 < vstride; x_idx_0++) {
    for (k = 0; k <= vlen; k++) {
      vwork->data[k] = x->data[x_idx_0 + k * vstride];
    }

    b_sortIdx(vwork, iidx);
    for (k = 0; k <= vlen; k++) {
      i29 = x_idx_0 + k * vstride;
      x->data[i29] = vwork->data[k];
      idx->data[i29] = iidx->data[k];
    }
  }

  emxFree_int32_T(&iidx);
  emxFree_real_T(&vwork);
}

/* End of code generation (sort1.c) */
