/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sortIdx.h
 *
 * Code generation for function 'sortIdx'
 *
 */

#ifndef SORTIDX_H
#define SORTIDX_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "DEIPFunc3_types.h"

/* Function Declarations */
extern void b_merge_block(int idx_data[], double x_data[], int n, int
  iwork_data[], double xwork_data[]);
extern void b_sortIdx(emxArray_real_T *x, emxArray_int32_T *idx);
extern void sortIdx(const emxArray_real_T *x, emxArray_int32_T *idx);

#endif

/* End of code generation (sortIdx.h) */
