/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sortLE.h
 *
 * Code generation for function 'sortLE'
 *
 */

#ifndef SORTLE_H
#define SORTLE_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "DEIPFunc3_types.h"

/* Function Declarations */
extern boolean_T sortLE(const emxArray_real_T *v, int idx1, int idx2);

#endif

/* End of code generation (sortLE.h) */
