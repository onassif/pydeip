/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sortrows.c
 *
 * Code generation for function 'sortrows'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "DEIPFunc3.h"
#include "sortrows.h"
#include "DEIPFunc3_emxutil.h"
#include "sortIdx.h"

/* Function Definitions */
void apply_row_permutation(emxArray_real_T *y, const emxArray_int32_T *idx)
{
  emxArray_real_T *ycol;
  int m;
  int y_idx_0;
  int i28;
  emxInit_real_T(&ycol, 1);
  m = y->size[0] - 1;
  y_idx_0 = y->size[0];
  i28 = ycol->size[0];
  ycol->size[0] = y_idx_0;
  emxEnsureCapacity_real_T(ycol, i28);
  for (y_idx_0 = 0; y_idx_0 <= m; y_idx_0++) {
    ycol->data[y_idx_0] = y->data[idx->data[y_idx_0] - 1];
  }

  for (y_idx_0 = 0; y_idx_0 <= m; y_idx_0++) {
    y->data[y_idx_0] = ycol->data[y_idx_0];
  }

  for (y_idx_0 = 0; y_idx_0 <= m; y_idx_0++) {
    ycol->data[y_idx_0] = y->data[(idx->data[y_idx_0] + y->size[0]) - 1];
  }

  for (y_idx_0 = 0; y_idx_0 <= m; y_idx_0++) {
    y->data[y_idx_0 + y->size[0]] = ycol->data[y_idx_0];
  }

  emxFree_real_T(&ycol);
}

void sortrows(emxArray_real_T *y)
{
  emxArray_int32_T *r1;
  emxInit_int32_T(&r1, 1);
  sortIdx(y, r1);
  apply_row_permutation(y, r1);
  emxFree_int32_T(&r1);
}

/* End of code generation (sortrows.c) */
