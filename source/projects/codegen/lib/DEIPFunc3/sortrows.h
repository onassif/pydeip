/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sortrows.h
 *
 * Code generation for function 'sortrows'
 *
 */

#ifndef SORTROWS_H
#define SORTROWS_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "DEIPFunc3_types.h"

/* Function Declarations */
extern void apply_row_permutation(emxArray_real_T *y, const emxArray_int32_T
  *idx);
extern void sortrows(emxArray_real_T *y);

#endif

/* End of code generation (sortrows.h) */
