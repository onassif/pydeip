/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sparse1.h
 *
 * Code generation for function 'sparse1'
 *
 */

#ifndef SPARSE1_H
#define SPARSE1_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "DEIPFunc3_types.h"

/* Function Declarations */
extern void b_sparse_and(const emxArray_boolean_T *a_d, const emxArray_int32_T
  *a_colidx, const emxArray_boolean_T *b_d, const emxArray_int32_T *b_colidx,
  const emxArray_int32_T *b_rowidx, coder_internal_sparse_4 *s);
extern void b_sparse_eq(const emxArray_real_T *a_d, const emxArray_int32_T
  *a_colidx, const emxArray_real_T *b_d, const emxArray_int32_T *b_colidx, const
  emxArray_int32_T *b_rowidx, coder_internal_sparse_4 *s);
extern void b_sparse_full(const emxArray_real_T *this_d, const emxArray_int32_T *
  this_colidx, const emxArray_int32_T *this_rowidx, int this_m, int this_n,
  emxArray_real_T *y);
extern void b_sparse_gt(const emxArray_real_T *a_d, const emxArray_int32_T
  *a_colidx, emxArray_boolean_T *s_d, emxArray_int32_T *s_colidx,
  emxArray_int32_T *s_rowidx);
extern void b_sparse_parenAssign(coder_internal_sparse *this, const
  emxArray_real_T *rhs, const emxArray_real_T *varargin_1);
extern void b_sparse_parenReference(const emxArray_real_T *this_d, const
  emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx, const
  emxArray_real_T *varargin_1, const double varargin_2_data[], const int
  varargin_2_size[2], emxArray_real_T *s_d, emxArray_int32_T *s_colidx,
  emxArray_int32_T *s_rowidx, int *s_m, int *s_n, int *s_maxnz);
extern void c_sparse_eq(double a, const emxArray_real_T *b_d, const
  emxArray_int32_T *b_colidx, const emxArray_int32_T *b_rowidx, int b_m,
  coder_internal_sparse_1 *s);
extern void c_sparse_full(const emxArray_real_T *this_d, const emxArray_int32_T *
  this_colidx, const emxArray_int32_T *this_rowidx, int this_m, emxArray_real_T *
  y);
extern void c_sparse_parenAssign(coder_internal_sparse *this, double rhs, const
  emxArray_real_T *varargin_1, double varargin_2);
extern void c_sparse_parenReference(const emxArray_real_T *this_d, const
  emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx, int this_m,
  double varargin_2, emxArray_real_T *s_d, emxArray_int32_T *s_colidx,
  emxArray_int32_T *s_rowidx, int *s_m);
extern void c_sparse_sparse(int nzmaxval, emxArray_boolean_T *this_d,
  emxArray_int32_T *this_colidx, emxArray_int32_T *this_rowidx);
extern boolean_T d_sparse_full(const emxArray_boolean_T *this_d, const
  emxArray_int32_T *this_colidx);
extern void d_sparse_parenReference(const emxArray_real_T *this_d, const
  emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx, double
  varargin_2, emxArray_real_T *s_d, emxArray_int32_T *s_colidx, emxArray_int32_T
  *s_rowidx);
extern void e_sparse_parenReference(const emxArray_real_T *this_d, const
  emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx, double
  varargin_1, double varargin_2, emxArray_real_T *s_d, emxArray_int32_T
  *s_colidx, emxArray_int32_T *s_rowidx);
extern void f_sparse_parenReference(const emxArray_real_T *this_d, const
  emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx, const
  emxArray_real_T *varargin_1, double varargin_2, emxArray_real_T *s_d,
  emxArray_int32_T *s_colidx, emxArray_int32_T *s_rowidx, int *s_m);
extern void locSortrows(emxArray_int32_T *idx, emxArray_int32_T *a,
  emxArray_int32_T *b);
extern void sparse_and(const emxArray_boolean_T *a_d, const emxArray_int32_T
  *a_colidx, const emxArray_int32_T *a_rowidx, const emxArray_boolean_T *b_d,
  const emxArray_int32_T *b_colidx, const emxArray_int32_T *b_rowidx, int b_m,
  emxArray_boolean_T *s_d, emxArray_int32_T *s_colidx, emxArray_int32_T
  *s_rowidx, int *s_m);
extern void sparse_eq(const emxArray_real_T *a_d, const emxArray_int32_T
                      *a_colidx, const emxArray_int32_T *a_rowidx, const
                      emxArray_real_T *b_d, const emxArray_int32_T *b_colidx,
                      const emxArray_int32_T *b_rowidx, int b_m,
                      emxArray_boolean_T *s_d, emxArray_int32_T *s_colidx,
                      emxArray_int32_T *s_rowidx, int *s_m);
extern double sparse_full(const emxArray_real_T *this_d, const emxArray_int32_T *
  this_colidx);
extern void sparse_gt(const emxArray_real_T *a_d, const emxArray_int32_T
                      *a_colidx, const emxArray_int32_T *a_rowidx, int a_m,
                      coder_internal_sparse_1 *s);
extern void sparse_parenAssign(coder_internal_sparse *this, const
  emxArray_real_T *rhs_d, const emxArray_int32_T *rhs_colidx, const
  emxArray_int32_T *rhs_rowidx, int rhs_m, const emxArray_real_T *varargin_1);
extern void sparse_parenReference(const emxArray_real_T *this_d, const
  emxArray_int32_T *this_colidx, const emxArray_int32_T *this_rowidx, int this_m,
  const emxArray_real_T *varargin_1, emxArray_real_T *s_d, emxArray_int32_T
  *s_colidx, emxArray_int32_T *s_rowidx, int *s_m);
extern void sparse_sparse(int m, int nzmaxval, emxArray_real_T *this_d,
  emxArray_int32_T *this_colidx, emxArray_int32_T *this_rowidx, int *this_m);
extern void sparse_times(const emxArray_real_T *a, const emxArray_real_T *b_d,
  const emxArray_int32_T *b_colidx, const emxArray_int32_T *b_rowidx, int b_m,
  emxArray_real_T *s_d, emxArray_int32_T *s_colidx, emxArray_int32_T *s_rowidx,
  int *s_m);

#endif

/* End of code generation (sparse1.h) */
