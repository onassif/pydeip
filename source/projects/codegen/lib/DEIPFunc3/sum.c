/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sum.c
 *
 * Code generation for function 'sum'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "DEIPFunc3.h"
#include "sum.h"
#include "DEIPFunc3_emxutil.h"
#include "sparse1.h"
#include "introsort.h"

/* Function Definitions */
void sum(const emxArray_real_T *x_d, const emxArray_int32_T *x_colidx, const
         emxArray_int32_T *x_rowidx, int x_m, int x_n, emxArray_real_T *y_d,
         emxArray_int32_T *y_colidx, emxArray_int32_T *y_rowidx, int *y_m)
{
  boolean_T p;
  emxArray_int32_T *xrowidxPerm;
  emxArray_int32_T *y;
  cell_wrap_3 this_tunableEnvironment[1];
  emxArray_real_T *yt;
  int loop_ub;
  int nzx;
  int currentRow;
  int n;
  int yk;
  if ((x_m == 0) || (x_n == 0)) {
    p = true;
  } else {
    p = false;
  }

  emxInit_int32_T(&xrowidxPerm, 1);
  emxInit_int32_T(&y, 2);
  emxInitMatrix_cell_wrap_3(this_tunableEnvironment);
  emxInit_real_T(&yt, 1);
  if (p || (x_n == 0)) {
    loop_ub = x_m;
    currentRow = y_colidx->size[0];
    y_colidx->size[0] = 2;
    emxEnsureCapacity_int32_T(y_colidx, currentRow);
    y_colidx->data[0] = 1;
    y_colidx->data[1] = 1;
    currentRow = y_d->size[0];
    y_d->size[0] = 1;
    emxEnsureCapacity_real_T(y_d, currentRow);
    y_d->data[0] = 0.0;
    currentRow = y_rowidx->size[0];
    y_rowidx->size[0] = 1;
    emxEnsureCapacity_int32_T(y_rowidx, currentRow);
    y_rowidx->data[0] = 1;
  } else if ((x_n != 0) && ((x_m <= x_colidx->data[x_colidx->size[0] - 1] - 1) ||
              (x_m <= x_n + 1))) {
    currentRow = yt->size[0];
    yt->size[0] = x_m;
    emxEnsureCapacity_real_T(yt, currentRow);
    for (currentRow = 0; currentRow < x_m; currentRow++) {
      yt->data[currentRow] = 0.0;
    }

    nzx = x_colidx->data[x_colidx->size[0] - 1];
    for (yk = 0; yk <= nzx - 2; yk++) {
      yt->data[x_rowidx->data[yk] - 1] += x_d->data[yk];
    }

    yk = 0;
    for (currentRow = 0; currentRow < x_m; currentRow++) {
      if (yt->data[currentRow] != 0.0) {
        yk++;
      }
    }

    sparse_sparse(x_m, yk, y_d, y_colidx, y_rowidx, &loop_ub);
    y_colidx->data[0] = 1;
    y_colidx->data[y_colidx->size[0] - 1] = yk + 1;
    yk = 0;
    for (currentRow = 0; currentRow < x_m; currentRow++) {
      if (yt->data[currentRow] != 0.0) {
        y_rowidx->data[yk] = currentRow + 1;
        y_d->data[yk] = yt->data[currentRow];
        yk++;
      }
    }
  } else {
    nzx = x_colidx->data[x_colidx->size[0] - 1] - 1;
    if (1 > x_colidx->data[x_colidx->size[0] - 1] - 1) {
      loop_ub = 0;
    } else {
      loop_ub = x_colidx->data[x_colidx->size[0] - 1] - 1;
    }

    if (loop_ub < 1) {
      n = 0;
    } else {
      n = loop_ub;
    }

    currentRow = y->size[0] * y->size[1];
    y->size[0] = 1;
    y->size[1] = n;
    emxEnsureCapacity_int32_T(y, currentRow);
    if (n > 0) {
      y->data[0] = 1;
      yk = 1;
      for (currentRow = 2; currentRow <= n; currentRow++) {
        yk++;
        y->data[currentRow - 1] = yk;
      }
    }

    currentRow = this_tunableEnvironment[0].f1->size[0];
    this_tunableEnvironment[0].f1->size[0] = loop_ub;
    emxEnsureCapacity_int32_T(this_tunableEnvironment[0].f1, currentRow);
    for (currentRow = 0; currentRow < loop_ub; currentRow++) {
      this_tunableEnvironment[0].f1->data[currentRow] = x_rowidx->
        data[currentRow];
    }

    currentRow = xrowidxPerm->size[0];
    xrowidxPerm->size[0] = y->size[1];
    emxEnsureCapacity_int32_T(xrowidxPerm, currentRow);
    yk = y->size[1];
    for (currentRow = 0; currentRow < yk; currentRow++) {
      xrowidxPerm->data[currentRow] = y->data[currentRow];
    }

    b_introsort(xrowidxPerm, loop_ub, this_tunableEnvironment);
    sparse_sparse(x_m, x_colidx->data[x_colidx->size[0] - 1] - 1, y_d, y_colidx,
                  y_rowidx, &loop_ub);
    yk = 0;
    n = 0;
    while (yk + 1 <= nzx) {
      currentRow = x_rowidx->data[xrowidxPerm->data[yk] - 1];
      y_d->data[n] = x_d->data[xrowidxPerm->data[yk] - 1];
      yk++;
      while ((yk + 1 <= nzx) && (x_rowidx->data[xrowidxPerm->data[yk] - 1] ==
              currentRow)) {
        y_d->data[n] += x_d->data[xrowidxPerm->data[yk] - 1];
        yk++;
      }

      if (y_d->data[n] != 0.0) {
        y_rowidx->data[n] = currentRow;
        n++;
      }
    }

    y_colidx->data[y_colidx->size[0] - 1] = n + 1;
  }

  emxFree_real_T(&yt);
  emxFreeMatrix_cell_wrap_3(this_tunableEnvironment);
  emxFree_int32_T(&y);
  emxFree_int32_T(&xrowidxPerm);
  *y_m = loop_ub;
}

/* End of code generation (sum.c) */
