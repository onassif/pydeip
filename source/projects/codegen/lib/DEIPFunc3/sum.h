/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * sum.h
 *
 * Code generation for function 'sum'
 *
 */

#ifndef SUM_H
#define SUM_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "DEIPFunc3_types.h"

/* Function Declarations */
extern void sum(const emxArray_real_T *x_d, const emxArray_int32_T *x_colidx,
                const emxArray_int32_T *x_rowidx, int x_m, int x_n,
                emxArray_real_T *y_d, emxArray_int32_T *y_colidx,
                emxArray_int32_T *y_rowidx, int *y_m);

#endif

/* End of code generation (sum.h) */
