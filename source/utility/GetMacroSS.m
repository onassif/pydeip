function [RVE_E,RVE_F] = GetMacroSS(DispList,ForcList,NodeTypeNum,ndm)
%
% Tim Truster
% 12/19/2019
%
% Extract the macro strain and stress (stress times volume) from an RVE problem

if NodeTypeNum(3)-NodeTypeNum(2) == ndm
    RVE_E = DispList(1:ndm,NodeTypeNum(2):NodeTypeNum(2)+ndm-1);
    RVE_F = -ForcList(1:ndm,NodeTypeNum(2):NodeTypeNum(2)+ndm-1);
else
    RVE_E = DispList(1:ndm,NodeTypeNum(2)+ndm:NodeTypeNum(2)+ndm-1+ndm);
    RVE_F = -ForcList(1:ndm,NodeTypeNum(2)+ndm:NodeTypeNum(2)+ndm-1+ndm);
end   
